import React, { Component } from 'react';
import {
  AppRegistry, SectionList, StyleSheet, Text, View,TouchableOpacity,Modal,ActivityIndicator,Dimensions,Image
} from 'react-native';
import _ from 'lodash';
import {settings} from '../../config/settings';
import ApiHelper from '../../lib/ApiHelper/ApiHelper';
const api= new ApiHelper;
const { height, width } = Dimensions.get('window');
export default class ModalCat extends Component {
constructor(props) {
    super(props);
    this.state={
        visible:false,
        text:'',
        SectionList:[],
        load:false,
        selectedItem:{},
    }
}
// componentDidMount(){
//     this.getData();
// }
// getData(){
//      let request={
//         url:settings.endpoints.getAllCategory,
//         method:'POST',
//         params:{}
//         } 
//         let catArray = [];  
//         this.setState({load:true});
//         api.FetchData(request).then((result)=>{ 
//         // console.log(result);  
//         let i= 0;
//             for(key in  result.category){
//                 catArray.push({'title':key,'key':i});
//                 catArray[i]['data']=[];
//                 for(key1 in result.category[key]){
//                     catArray[i]['data'].push({id:result.category[key][key1]['id'],name:result.category[key][key1]['name'],key:result.category[key][key1]['id'],price:result.category[key][key1]['price']});
//                 }  
//                 i++;          
//             }
//             this.setState({SectionList:catArray,load:false});
//             //console.log(catArray);   
//         }).catch((error)=>{
//             console.log(error);
//         });
// }

reset(){
    this.setState({
        visible:false,
        text:'',
        SectionList:[],
        load:false,
        selectedItem:{},
    },()=>{
        this.props.onPress(0, null);
    });
}

resetWithoutProps(){
    this.setState({
        visible:false,
        text:'',
        SectionList:[],
        load:false,
        selectedItem:{},
    });
}

modalShowHide(){
    this.setState({visible:!this.state.visible});
}

setDataFromId(id){
    let name = this.getCatNameById(id);
    this.setState({
        text : name
    });
}

onItemPress(item){
    const { withParent, onPress } = this.props;
    let parentId = null;
    if(withParent) {
        parentId = this.getParentID(item.id);
    }
    this.setState({visible:false,text:item.name,selectedItem:item},()=>this.props.onPress(item.id, parentId));
}

onSectionPress(section){
    this.setState({visible:false,text:section.title,selectedItem:section},()=>this.props.onPress(section.id));
}

getParentID(id){
    const { SectionList } = this.props;
    // console.log(SectionList);
    let pId = null;
    if(_.isArray(SectionList) && SectionList.length > 0) {
        SectionList.map((ele) => {
            if(_.isArray(ele.data) && ele.data.length > 0) {
                ele.data.map((subEle) => {
                    if(subEle.id  === id) {
                        pId = ele.id;
                    }
                });
            }
        });
    }
    return pId;
}

getPrice(){
    if(Object.keys(this.state.selectedItem).length !== 0){
        return parseInt(this.state.selectedItem.price);
    }else{
        //console.log('NaN');
        return 0;
    }
}

inputRef(){
    return this.modalCategory;
}

setSelectedCat(name){
    this.setState({text:name});
}

getCatNameById(id){
    const sList=this.props.SectionList;
    //console.log(id);
    let name = ''
    if(id !== undefined && id !== null && id !== ''){
        if(sList !== undefined && sList !== null && sList.length > 0){
            sList.map((d,i)=>{
                if(d.id === id ){
                    name = d.title;
                }else if(d.data !== undefined && d.data !== null && d.data.length > 0){
                    d.data.map((v,j)=>{
                        if(v.id === id){
                            name = v.name;
                        }
                    });
                }
            })
        }
    }
    if(name !== ''){
        return name;
    }else{
        return 'Toutes catégories';
    }
    //console.log(this.props.SectionList);
}

render() {
    const {onPress,containerStyle,selectedCatId,postAd} = this.props;
    // console.log('selectedCatId ++'+selectedCatId);
     return(
        <View>
            <TouchableOpacity onPress={()=>this.modalShowHide()} style={containerStyle ? [{flexDirection:'row',alignItems:'center',justifyContent:'center',borderWidth:1,borderColor:'#ddd',padding:7},containerStyle] :{width:width/2,flexDirection:'row',alignItems:'center',justifyContent:'center',borderWidth:1,borderColor:'#ddd',padding:7}} ref={o=>this.modalCategory=o}>
                <View style={{flex:1,flexDirection:'row',alignItems:'center',justifyContent:'center'}}>
                    <Text numberOfLines={1} style={{padding:3}}>{this.state.text != '' ? this.state.text : selectedCatId ? this.getCatNameById(selectedCatId) : 'Toutes catégories'}</Text>
                </View>
                <View style={{width:(width/2)/4,alignItems:'center',justifyContent:'center'}}>
                    {(this.state.text != '')? <TouchableOpacity  activeOpacity={0.8} onPress={()=>{this.reset()}}><Text style={[{fontFamily:'icomoon',fontSize:14 }]}>{'}'}</Text></TouchableOpacity> :<Text style={{ fontFamily:'icomoon',fontSize:12,justifyContent:'center'}}>g</Text> }
                    
                </View>
            </TouchableOpacity>
            <Modal style={styles.modal} animationType={"slide"} transparent={true} visible={this.state.visible} onRequestClose={() =>this.modalShowHide()}>
                {this.state.load ?  <View style={{height:height}}>
                <ActivityIndicator animating={true} color={'#000'} size="large"/></View> : (
                <View style={styles.container}>
                    <View style={{height:50,flexDirection:'row',alignItems:'center',justifyContent:'center'}}>
                        <TouchableOpacity onPress={() =>this.modalShowHide()}>
                            <Text style={{ fontFamily:'icomoon',width:30,textAlign:'center',color:'#FD5009',fontSize:16}}>5</Text>
                        </TouchableOpacity>
                        <Text style={[styles.title]}>Toutes catégories</Text>
                        <Text style={{width:30}}></Text>
                    </View>
                    <SectionList
                    sections={this.props.SectionList}
                    renderItem={({item}) => <TouchableOpacity style={styles.item} onPress={()=>this.onItemPress(item)}><Text>{item.name}</Text></TouchableOpacity>}
                    renderSectionHeader={({section}) => section.title !== 'toutes' ? (
                        <TouchableOpacity style={styles.sectionHeader} activeOpacity={0.9} 
                        onPress={()=> postAd ? null : this.onSectionPress(section)}>
                            <View style={{flexDirection:'row'}}>
                                {/* <Image style={{height:14,width:14,alignSelf:'center',marginRight:5}} source={{uri:section.image}} /> */}
                                <Text style={styles.sectionName}>{section.title}</Text>
                            </View>
                        </TouchableOpacity>
                        ): (null)}
                />
                </View>)}
            </Modal>
        </View>
    );
  }
}

const styles = StyleSheet.create({
container: {
    flex: 1,
    paddingTop: 22,
    backgroundColor:"#fffF"
},
sectionHeader: {
    paddingTop:5,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 5,
    backgroundColor: '#FD5009',
},
sectionName:{
    fontSize: 14,
    fontWeight: 'bold',
    color:'#fff'
}, 
title:{
    color:'#000',
    textAlign:'center',
    fontSize: 20,
    marginBottom:5,
    flex:1,
    color:'#FD5009'
},
item: {
    padding: 10,
    height: 40,
},
})