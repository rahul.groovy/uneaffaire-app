import React, {Component} from 'react';
import {StyleSheet, Text, TouchableOpacity, Image, View,ActivityIndicator,Dimensions} from 'react-native';
import {colors} from '../../config/styles';
import images from '../../config/images';
//import styles from '../../config/genStyle';
import {SharedElementTransition} from 'react-native-navigation';
import {FORTAB,TABLANDSCAPE,TABPORTRAIT} from '../../config/MQ'; 
import {Column as Col, Row} from 'react-native-flexbox-grid';
import { Icon } from '../../config/icons';
import FaIcon from 'react-native-vector-icons/FontAwesome';
const {height,width}=Dimensions.get('window');

export default class GridItem extends Component {

    constructor(props) {
        super(props);
    }

    getDispPrice(number){
        if(!isNaN(parseFloat(number))){
            if(number >= 1000){
                let ret='';
                let factor= parseInt(number/1000);
                ret+=factor
                ret+=' ';
                ret+=('000' + (number - (factor*1000))).slice(-3);
                ret+=' €';
                return ret;
            }else{
                let oret='';
                oret+=number;
                oret+=' €';
                return oret;
            }
        }
      }

    _renderItem(data,key){
        const {onPress, onFav, onChat, currentUser , searchType} = this.props;
        key++;
        return(
            <Col sm={6} md={4} lg={3} style={styles.proCol} key={`list_item_${key}`}>
                <View style={[styles.proList,data.is_in_showcase==1 ? {borderColor:"#f15a23",borderWidth:1}:null]}>
                    <TouchableOpacity style={styles.canvasContainer}
                        onPress={()=>onPress(data)}
                        activeOpacity={0.8}>
                        {typeof data.video != 'undefined' && data.video != "" ?
                        (<Image style={styles.hasVideo} source={images.video} />):(null)}
                                <SharedElementTransition sharedElementId={'SharedTextId' + data.id}>
                                    <Image
                                        style={[styles.proImg, styles.progridWidth]}
                                        source={{uri: data.single_image}}/>
                                </SharedElementTransition>
                                {data.user_type=='professional' ?
                                (<Image style={[styles.proSign, styles.proSignAbsolute]} source={images.pro}/>):(null)}
                                <View style={styles.absoluteBottomRight}>
                                    {/* <Text style={[styles.globalIcon, styles.red,{paddingHorizontal:3}]}>y</Text>
                                    <Text style={[styles.globalIcon, styles.red,{paddingHorizontal:3}]}>w</Text> */}
                                    {data.is_urgent == 1 && 
                                        <Text style={[styles.globalIcon,styles.inlineWithico,styles.red]}>y</Text>
                                    }
                                    {data.is_direct_sale == 1 && 
                                            <Text style={[styles.globalIcon,styles.inlineWithico,styles.red,{fontSize:14}]}>w</Text> 
                                    } 
                                </View>
                    </TouchableOpacity>
                    <View style={styles.proCaption}>
                        <TouchableOpacity
                            onPress={()=>onPress(data)}
                            activeOpacity={0.8}>
                            {searchType === 'trocs' ? (
                                <View style={{justifyContent:'center',flex:1}}>
                                    <View style={{flexDirection:'row'}}>
                                        <FaIcon name="mail-forward" color={'#34c24c'} size={15} style={{marginRight:5,marginTop:3}}/>
                                        <Text style={[styles.proName]} numberOfLines={1}>{data.title}</Text>
                                    </View>
                                    <View style={{flexDirection:'row'}}>
                                        <FaIcon name="reply" color={'#2e5f9b'} size={15} style={{marginRight:5,marginTop:3}}/>
                                        <Text style={[styles.proName]} numberOfLines={1}>{data.title_contre}</Text>
                                    </View>
                                </View>) 
                            : 
                                ( <Text style={[styles.proName]} numberOfLines={1}>{data.title}</Text>)}
                           
                        </TouchableOpacity>
                        <View style={styles.bottomDetail}>
                            {/* <View style={styles.priceView}> */}
                                <Text style={[styles.priceText,width < 360 ? {fontSize:12}:null]}>{(data.price)}</Text>
                            {/* </View> */}
                            <View style={styles.camView}>
                                <Text style={[styles.globalIcon, styles.camIco]}>s</Text>
                                <Text style={[styles.abscamtxt]}>{data.totalImageCount}</Text>
                            </View>
                            <View style={{flex:1}}></View>
                            <View style={styles.userChat}>
                                <Text style={data.havefave == 1 ? [styles.globalIcon, styles.smallIco, styles.orangeColor] : [styles.globalIcon, styles.smallIco]} onPress={()=>onFav(key,data.havefave)}>{data.havefave == 1 ? 'n' : 'm'}</Text>
                            </View>
                            {data.user_id  != currentUser.id ? 
                            (<TouchableOpacity
                                style={styles.userChat}
                                activeOpacity={0.8}
                                onPress={()=>onChat(data)}>
                                <Text style={[styles.globalIcon, styles.smallIco]}>f</Text>
                            </TouchableOpacity>):(null)}
                            {data.user_id  != currentUser.id ? 
                            (<View style={[styles.chatCircle, data.userOnline == 1 ? styles.userOnline : null]}></View>):(null)}
                        </View>
                        {/* <View style={styles.bottomDetail}>
                            <View style={styles.priceView}>
                                <View>
                                    <Text style={[styles.priceText]}>{data.price}{' €'}</Text>
                                </View>
                                <View style={styles.camView}>
                                    <Text style={[styles.globalIcon, styles.camIco]}>s</Text> 
                                    <Text style={[styles.abscamtxt]}>{data.totalImageCount}</Text>
                                </View>
                            </View>
                            <View style={styles.rightView}>
                                 <View style={{flex:2}}></View> 
                                    <View style={styles.flex}>
                                        <Text
                                            style={data.havefave == 1
                                            ? [styles.globalIcon, styles.smallIco, styles.orangeColor]
                                            : [styles.globalIcon, styles.smallIco]}
                                            onPress={()=>onFav(key,data.havefave)}>
                                            {data.havefave == 1 ? 'n' : 'm'}</Text>
                                    </View>
                                    <TouchableOpacity
                                        style={[styles.userChat,styles.flex]}
                                        activeOpacity={0.8}
                                        onPress={()=>onChat(data.id)}>
                                        <Text style={[styles.globalIcon, styles.smallIco]}>f</Text>
                                    </TouchableOpacity>
                                    <View style={[styles.chatCircle]}></View>
                            </View>
                        </View> */}
                    </View>
                </View>
            </Col>
        );
    }

    render() {
        const {dataList} = this.props;
        return (
            <Row size={12} style={[styles.proRow]}>                          
            {dataList.map((data,i)=>{
                if(data.loader){
                    return <Col sm={12} md={12} lg={12} style={styles.proCol} key={`list_item_loader`}><ActivityIndicator size="large" color={colors.navBarButtonColor}/><View style={{height:10}}></View></Col>
                }else{
                    return this._renderItem(data,i);
                }
            })}
        </Row>
        );
    }
}

const styles = StyleSheet.create({
    proRow:{
        flexDirection: 'row',
        //flexWrap: 'wrap',
        //position:'relative',
        marginLeft:0,
        marginRight:0,
        //backgroundColor:'#fff',
        //flex:1,
    },
    proCol: {
        paddingLeft: 4,
        paddingRight: 4,
        paddingBottom: 8,
        position: 'relative',
        backgroundColor: 'rgba(0,0,0,0)'
    },
    proList: {
        borderWidth: 1,
        borderColor: '#ddd',
        backgroundColor: '#fff',
        elevation: 3
    },
    canvasContainer: {
        flex: 1,
        alignItems: 'stretch',
        padding: 0,
        position: 'relative'
    },
    imgInner: {
        position: 'relative',
        flex: 1
    },
    proImg: {
        width: null,
        resizeMode: 'cover',
        height: TABLANDSCAPE ? 135 : TABPORTRAIT ? 135 : 110,
        backgroundColor:"#eee",
    },
    progridWidth: {
        height: 151,
        width: null,
        resizeMode: 'cover'
    },
    proSign: {
        width: 35,
        height: 20
    },
    proSignAbsolute: {
        position: 'absolute',
        right: 0,
        paddingTop: 5,
        width: 20,
        height: 10
    },
    absoluteBottomRight: {
        backgroundColor: 'rgba(255,255,255,0.7)',
        position: 'absolute',
        right: 0,
        bottom: 0,
        flexDirection: 'row',
        paddingVertical:5,
    },
    globalIcon: {
        fontFamily: 'icomoon',
        fontSize: 12,
        justifyContent: 'center'
    },
    red: {
        color: '#ff0000'
    },
    proCaption: {
        padding:5,
        backgroundColor: "#FFF",
        //width: null,
        //alignSelf: 'stretch'
    },
    proName: {
        fontSize: FORTAB ? 14 : 12,
        paddingBottom: 0,
        color: '#333',
        fontFamily: 'Montserrat',
        //flex: 1,
    },
    bottomDetail: {
        //flex: 1,
        paddingTop:5,
        flexDirection: 'row',
        //justifyContent: 'space-between',
    },
    inlineDatarow: {
        flexDirection: 'row',
        alignItems: 'center',
        //alignSelf: 'stretch',
        overflow: 'visible'
    },
    priceText: {
        fontWeight:'600',
        fontSize:14,
        color: '#f15a23'
    },
    rightAlign: {
        top: 2,
        right: 0
    },
    camIco: {
        fontSize: 20,
        zIndex: -1,
        width: 20,
        position: 'absolute',
    },
    abscamtxt: {
        backgroundColor: "#0000",
        fontWeight:'600',
        fontSize:FORTAB ? 12:10
    },
    small: {
        fontSize: FORTAB ? 12 : 10
    },
    smallIco: {
        fontSize: FORTAB ? 22: 20
    },
    xlIco: {
        fontSize: FORTAB? 22: 20
    },
    orangeColor: {
        color: '#f15a23'
    },
    userChat: {
        width:26,
        //marginLeft:8,
        //flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
    },
    chatCircle: {
        position:'absolute',
        width:10,
        height:10,
        borderRadius:5,
        backgroundColor:'#ccc',
        right:0,
        top:3,
        zIndex:1,
    },
    userOnline:{
        backgroundColor:'#72bb53',
    },
    priceView:{
        //flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between',
    },
    camView:{
        width:20,
        marginLeft:8,
        marginBottom:3,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
    },
    rightView:{
        flex:2,
        flexDirection: 'row',
        alignItems: 'center',
        overflow: 'visible',
        justifyContent:'flex-end'
    },
    flex:{
        flex:1
    },
    hasVideo:{
        width:20,
        height:20,
        position:'absolute',    
        left:3,
        //opacity:0.8,
        // top:-4,
        zIndex:1,
    },
});