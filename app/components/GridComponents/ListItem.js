import React, {Component} from 'react';
import {StyleSheet, Text, TouchableOpacity, Image, View} from 'react-native';
import {colors} from '../../config/styles';
import images from '../../config/images';
//import styles from '../../config/genStyle';
import {SharedElementTransition} from 'react-native-navigation';
import {FORTAB,TABLANDSCAPE,TABPORTRAIT} from '../../config/MQ'; 
import {Column as Col, Row} from 'react-native-flexbox-grid';
import moment from 'moment';
import FaIcon from 'react-native-vector-icons/FontAwesome';

export default class ListItem extends Component {

  constructor(props) {
    super(props);
  }

  getDispPrice(number){
    if(!isNaN(parseFloat(number))){
        if(number >= 1000){
            let ret='';
            let factor= parseInt(number/1000);
            ret+=factor
            ret+=' ';
            ret+=('000' + (number - (factor*1000))).slice(-3);
            ret+=' €';
            return ret;
        }else{
            let oret='';
            oret+=number;
            oret+=' €';
            return oret;
        }
    }
  }
  
 render() {
     const {data, vkey, onPress, onFav, onChat, currentUser,searchType} = this.props;
    //  let dataDate = moment(data.post_on).format("DD MMM [|] HH:MM");
     return(
        data != 'undefined' ? 
        <Row size={12} style={styles.proRow} key={`list_item_${vkey}`} onPress={()=>onPress(data)}>                
            <Col sm={5} md={3} lg={2} style={styles.proCol}>
                <TouchableOpacity style={styles.imgInner} onPress={()=>onPress(data)} activeOpacity={0.8}>
                        {typeof data.video != 'undefined' && data.video != "" ?
                        (<Image style={styles.hasVideo} source={images.video} />):(null)}
                        {data.single_image != ''&& data.id != '' && 
                        <SharedElementTransition sharedElementId={'SharedTextId'+data.id}>
                            <Image style={styles.proImg} source={{uri:data.single_image}} resizeMode="cover"/>
                        </SharedElementTransition>
                        }
                </TouchableOpacity>                       
                <View style={styles.camView}>
                    <Text style={[styles.globalIcon, styles.camIco]}>C</Text> 
                    <Text style={[styles.abscamtxt]}>{typeof data.totalImageCount != 'undefined'? data.totalImageCount : data.imageCount}</Text>
                </View>                  
            </Col>
            <Col sm={7} md={9} lg={10} style={styles.proCol}>
                <TouchableOpacity style={styles.imgInner} onPress={()=>onPress(data)} activeOpacity={0.8}>
                <View style={{paddingHorizontal:8,paddingVertical:5}}>
                    <TouchableOpacity  onPress={()=>onPress(data)}>
                    {searchType === 'trocs' ? (
                            <View style={{justifyContent:'center',flex:1}}>
                                <View style={{flexDirection:'row'}}>
                                    <FaIcon name="mail-forward" color={'#34c24c'} size={15} style={{marginRight:5,marginTop:3}}/>
                                    <Text style={[styles.proName,styles.big]} numberOfLines={1}>{data.title}</Text>
                                </View>
                                <View style={{flexDirection:'row'}}>
                                    <FaIcon name="reply" color={'#2e5f9b'} size={15} style={{marginRight:5,marginTop:3}}/>
                                    <Text style={[styles.proName,styles.big]} numberOfLines={1}>{data.title_contre}</Text>
                                </View>
                            </View>) 
                        : 
                        <Text style={[styles.proName,styles.big]} numberOfLines={1}>{data.title}</Text>
                    }
                    </TouchableOpacity>
                    <View style={styles.priceview}>
                        {/* <View style={styles.priceView}> */}
                            <Text style={[styles.orangeColor,styles.bold]}>{(data.price)}</Text>
                        {/* </View> */}
                        <View style={{flex:1}}></View>
                        <View style={styles.userChat}>
                            <Text style={data.havefave == 1 ? [styles.globalIcon, styles.smallIco, styles.orangeColor] : [styles.globalIcon, styles.smallIco]} onPress={()=>onFav(vkey,data.havefave)}>{data.havefave == 1 ? 'n' : 'm'}</Text>
                        </View>
                        {data.user_id  != currentUser.id ? 
                            (<TouchableOpacity
                                style={styles.userChat}
                                activeOpacity={0.8}
                                onPress={()=>onChat(data)}>
                                <Text style={[styles.globalIcon, styles.smallIco]}>f</Text>
                            </TouchableOpacity>):(null)}
                            {data.user_id  != currentUser.id ? 
                            (<View style={[styles.chatCircle, data.userOnline == 1 ? styles.userOnline : null]}></View>):(null)}
                    </View>
                    <View style={[styles.inlineDatarow,styles._MT10_]}>
                    {data.is_urgent == 1 && 
                        <Text style={[styles.globalIcon,styles.inlineWithico,styles.red]}>y</Text>
                    }
                    {data.is_direct_sale == 1 && 
                            <Text style={[styles.globalIcon,styles.inlineWithico,styles.red]}>w</Text> 
                    }                        
                    </View>                        
                    <View style={{flexDirection:'row',alignContent:'center'}}>
                        {data.user_type=='professional' ? (
                            <Text style={styles.small}>{data.categoryname} <Image style={styles.proSign} source={images.pro} /></Text>
                        ):(
                            <Text style={styles.small}>{data.categoryname} </Text>
                        )}
                            
                            {/* <Image style={styles.proSign} source={images.pro} /> */}
                    </View>
    
                    <View><Text style={styles.small}>{data.town} {data.town != '' && data.department != ''? '/' :null} {data.department}</Text></View>
                    <View>
                        <Text style={styles.small}>{data.post_date}</Text>
                    </View>
                </View>
                </TouchableOpacity>
                    {/* <View style={styles.proCaption}>
                    <Text style={[styles.proName,styles.big]} numberOfLines={1}>{data.title}</Text>
                    <View style={styles.row}>
                        <View style={[styles.inlineDatarow,styles.spaceBitw,styles._MT5_]}>
                            <Text style={[styles.orangeColor,styles.bold]}>{data.price}{' €'}</Text>
                            <View style={[styles.inlineDatarow,styles.justRight]}>
                                <View style={[styles.inlineDatarow]}>
                                    <View style={styles._MR5_}>
                                        <Text style={data.havefave==1 ? [styles.globalIcon,styles.xlIco,styles.orangeColor]:[styles.globalIcon,styles.xlIco]} 
                                        onPress={()=>{console.log('change favorite of item '+data.id)}}>{data.havefave==1?'n':'m'}</Text>
                                    </View>
                                    <TouchableOpacity style={styles.userChat} activeOpacity={0.8} onPress={()=>{this.navigateTo('Singlechat',data.id)}}>
                                        <View style={[styles.relative]}>
                                            <Text style={[styles.globalIcon,styles.xlIco]}>f</Text>
                                            <Text style={[styles.circle,styles.online,styles.absCircle]}></Text>
                                        </View>
                                    </TouchableOpacity>
                                </View> 
                            </View>  
                        </View>
                        <View style={[styles.inlineDatarow,styles._MT10_,styles._PB15_]}>
                            <Text style={[styles.inlineWithico,styles.borderRight,styles.red,styles.small]}>Urgent</Text>
                            <Text style={styles.globalIcon}>x</Text>
                            <Text style={[styles.inlineWithico,styles.red,styles.small]}>Achat Ditect</Text>
                            <Text style={styles.globalIcon}>w</Text>
                        </View>
                        <View style={[styles.inlineDatarow]}>
                            <View>
                            <Text style={styles.small}>Sport {'&'} Hobbies</Text> 
                            <Image style={styles.proSign} source={images.pro} />
                            </View>
                            <Text>St Martin du Mont / Saone et loire</Text>
                        </View>  
                    </View>
                </View>  */}                      
            </Col>
        </Row>:null
    );
  }
}

const styles =StyleSheet.create({  
    proCol:{    
        backgroundColor: 'rgba(0,0,0,0)',
        paddingLeft:0,
        paddingRight:0,
        paddingTop:0,
        paddingBottom:0,
        position:'relative',
    },
    proRow:{
        flex:1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginLeft:0,
        marginRight:0,
        marginTop:0,
        marginBottom:10,
        borderWidth:1,
        borderColor:'#eee',
        backgroundColor:'#fff',
        paddingTop:0,
        elevation: 3,  
    },
   prolistRow:{
        
    },
   Prolistgray:{
        
    },
   listCol:{
        paddingLeft:0,
        paddingRight:0,
        paddingTop:0,
        paddingBottom:0,
    },
   imgInner:{
        flex:1,
    },
    hasVideo:{
        width:20,
        height:20,
        position:'absolute',    
        left:3,
        //opacity:0.8,
        // top:-4,
        zIndex:1,
    },
    bottomDetail:{
        flex:1,
        flexDirection: 'row',
        alignItems:'center',
        justifyContent: 'space-between',
        bottom:0,
        left:0,
        right:0,
        //backgroundColor:'#333',
        paddingLeft:10,
        paddingRight:10,
        paddingTop:2,
        paddingBottom:4,
    },
    proImg:{    
        width:null,
        resizeMode:'cover',
        height:TABLANDSCAPE ? 135 : TABPORTRAIT ? 135:120,
    },
    bottomCaption:{
        
    },
    rightAlign:{
        top:2,
        right:0,
    },
    dateText:{
        fontSize:12,
        fontFamily:'Montserrat',
        justifyContent: 'space-between',
        fontSize:FORTAB ? 12:10,
        color:'#fff'    
    },
    bold:{
        fontWeight:'600',
    },
    orangeColor:{
        color:'#f15a23',
    },
    small:{ 
        fontSize:FORTAB ? 12:10
    },
    globalIcon:{
        fontFamily:'icomoon',
        fontSize:12,
        justifyContent:'center'
    },
    camIco:{
        fontSize: 20,
        //zIndex: -1,
        left:2,
        top:-3, 
        width: 26,
        position: 'absolute',
        color:'rgba(255, 255, 255, 0.5)',   
    },
    white:{
        color:'#fff'    
    },
    abscamtxt:{
        backgroundColor: "#0000",
        fontWeight:'400',
        fontSize:FORTAB ? 13:11,
        color:'#fff'
    },
    camIcoAbs:{
        marginLeft:15,
        zIndex:-1,
    },
    camView:{
        position:'absolute',
        width:22,
        bottom:5,
        right:9,
        //marginLeft:8,
        //marginBottom:3,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center'
    },
    userChat: {
        width:24,
        marginLeft:5,
        //flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
    },
    priceview:{
        paddingTop:5,
        flexDirection: 'row',
    },
    xlIco:{
        fontSize:FORTAB ? 22:20,
    },
    smallIco:{
        fontSize:FORTAB ? 22:20,
    },
    chatCircle: {
        position:'absolute',
        width:10,
        height:10,
        borderRadius:5,
        backgroundColor:'#ccc',
        right:0,
        top:3,
        zIndex:1,
    },
    userOnline:{
        backgroundColor:'#72bb53',
    },
    inlineDatarow:{
      flexDirection:'row',
      alignItems:'center',
      alignSelf:'stretch',
      overflow:'visible',
      marginTop:5,
      paddingBottom:5,
  },
      inlineWithico:{
    flexDirection:'row',
    paddingRight:10,
    marginRight:10,
    justifyContent:'center',
    alignItems:'center',
  },
   red:{
        color:'#ff0000',
    },
     proSign:{
        width:35,
        height:20,
    },
});