import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    TouchableOpacity,
    Image,
    View,
    ActivityIndicator,
    Switch
} from 'react-native';
import {colors} from '../../config/styles';
import images from '../../config/images';
import {Icon} from '../../config/icons';
import genStyles from '../../config/genStyle';
import CheckBox from '../../lib/react-native-check-box';
import {SharedElementTransition} from 'react-native-navigation';
import {FORTAB, TABLANDSCAPE, TABPORTRAIT} from '../../config/MQ';
import {Column as Col, Row} from 'react-native-flexbox-grid';
import FaIcon from 'react-native-vector-icons/FontAwesome';
import MaIcon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class DashBoardItem extends Component {

    constructor(props) {
        super(props);
    }

    getDispPrice(number) {
        if (!isNaN(parseFloat(number))) {
            if (number >= 1000) {
                let ret = '';
                let factor = parseInt(number / 1000);
                ret += factor
                ret += ' ';
                ret += ('000' + (number - (factor * 1000))).slice(-3);
                ret += ' €';
                return ret;
            } else {
                let oret = '';
                oret += number;
                oret += ' €';
                return oret;
            }
        }
    }

    render() {
        const {
            item,
            onPress,
            onChecked,
            onChangeSwitch,
            onEdit,
            onDelete,
            onPromote,
            vkey,
            isChecked,
            searchType
        } = this.props;

        /*
        props required
        _this.navigateTo('ProductDetail', item)
        _this.checked(checked, item.id)}
        isChecked .. means ck
        change switch
        on edit
        on delete
        on promote

        */
        return (
            <Row size={12} style={styles.proRow} key={vkey}>
                <Col sm={5} md={4} lg={2} style={styles.proCol}>
                    <View style={styles.colInner}>
                        <TouchableOpacity
                            style={styles.imageCanvas}
                            onPress={() => onPress(item)}
                            activeOpacity={0.8}>
                            <View style={styles.imgInner}>
                                {typeof item.video != 'undefined' && item.video != ""
                                    ? (<Image style={styles.hasVideo1} source={images.video}/>)
                                    : (null)}
                                {item.image != ''
                                    ? (<SharedElementTransition sharedElementId={'SharedTextId' + item.id}>
                                            <Image
                                                style={styles.proImg}
                                                source={{uri: item.image}}/>
                                        </SharedElementTransition>)
                                    : (null)}
                                <CheckBox
                                    style={styles.genChk}
                                    onClick={(checked) => onChecked(checked, item.id)}
                                    labelStyle={{fontSize:18}}
                                    isChecked={isChecked}/>
                            </View>
                        </TouchableOpacity>
                        <View style={styles.bottomDetail}>
                            <Text style={[styles.leftAlign, styles.white]}>
                                <Text style={styles.small}>{item.post_date}</Text>
                            </Text>
                            <View style={[styles.rightAlign]}>
                                <View style={styles.relative}>
                                    <Text style={[styles.globalIcon, styles.camIco, styles.white, styles.camIcoAbs]}>s</Text>
                                    <Text style={[styles.bold, styles.abscamtxt, styles.white, styles.small]}>{item.imageCount}</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                </Col>
                <Col sm={7} md={8} lg={10} style={styles.proCol}>
                    <View style={styles.proCaption}>

                        <View style={styles.captionRow}>
                        {searchType === 'trocs' ? (
                            <View style={{justifyContent:'center',flex:1}}>
                                <View style={{flexDirection:'row'}}>
                                    <FaIcon name="mail-forward" color={'#34c24c'} size={15} style={{marginRight:5,marginTop:3}}/>
                                    <Text style={styles.proName} numberOfLines={1}>{item.title}</Text>
                                </View>
                                <View style={{flexDirection:'row'}}>
                                    <FaIcon name="reply" color={'#2e5f9b'} size={15} style={{marginRight:5,marginTop:3}}/>
                                    <Text style={styles.proName} numberOfLines={1}>{item.title_contre}</Text>
                                    </View>
                                </View>) 
                            : 
                            <Text style={styles.proName} numberOfLines={1}>{item.title}</Text>
                        }
                            
                        </View>
                        <View style={[styles.captionRow,styles.captionRowInline]}>
                            <View style={styles.captionRow}>
                            <Text style={[styles.orangeColor, styles.bold, styles.midum]}>{(item.price)}</Text>
                            </View>
                            <View style={[styles.captionRow,styles.captionRowInline,genStyles.justCenter]}>
                            {/* <View style={[styles.captionRowInline,{paddingRight:5,marginRight:5}]}>
                                <Icon name="star-fill" color={"#F00"} size={12} />
                                <Text style={[styles.red,styles.small,{paddingLeft:3}]}>Urgent</Text>
                                </View> */}
                                {item.is_urgent === 1 ? <Icon name="star-fill" color={"#F00"} size={12} /> : null }
                                

                                    {/* <View style={[styles.captionRowInline,{paddingRight:5,marginRight:5,paddingTop: item.is_urgent  === 1 ? 3:0}]}>
                                <Icon name="shopping-cart" color={"#F00"} size={12} />
                                <Text style={[styles.red,styles.small,{paddingLeft:3}]}>Achat Ditect</Text>
                                </View>  is_direct_sale*/}
                                {item.is_direct_sale === 1 ? <Icon style={{paddingLeft: item.is_urgent  === 1 ? 3:0}} name="shopping-cart" color={"#F00"} size={12} /> : null }
                            </View>
                        </View>
                        {item.remain_days !== null ? <View style={[styles.captionRow,styles.captionRowInline]}>
                        <View style={[styles.captionRowInline,genStyles.justCenter]}>
                        <Icon name="calendar" color={"#333"} size={FORTAB ? 18 : 14} />
                        <Text style={styles.icoText}>{item.remain_days}{' Jours Restants'}</Text></View>
                        <View style={styles.captionRow}></View>
                            </View> : <View style={[styles.captionRow,styles.captionRowInline]}></View>}
                        <View style={[styles.captionRow,styles.captionRowInline]}>
                            <View style={[styles.captionRow,styles.captionRowInline,{alignItems:'center'}]}>
                                <View style={[styles.circle,
                                item.is_active == 1 ? {backgroundColor: '#72bb53'} : {backgroundColor: '#f15a23'}]}>
                                </View>
                                <Text style={[styles.orangeColor,{marginLeft:5}]}>{item.is_active == 1 ? 'Approuvée' : 'En attente de validation'}</Text>
                                <View style={styles.captionRow}></View>
                            </View>
                            {item.is_active == 1 ? (
                                <View style={[styles.captionRow,styles.captionRowInline,genStyles.justCenter]}>
                                <View style={styles.captionRow}></View>
                                    <View style={[styles.captionRowInline,styles.viewBtn]}>
                                        <Icon name="eye" size={12} color={'#888'}/>
                                        <Text style={{color: "#888",fontSize: 12,marginLeft: 3}}>{item.total_views}</Text>
                                    </View>
                                    <View style={[styles.captionRowInline,styles.viewBtn]}>
                                        <Icon name="telephone" size={12} color={'#888'}/>
                                        <Text style={{color: "#888",fontSize: 12,marginLeft: 3}}>{item.total_clicks}</Text>
                                    </View>
                                </View>
                            ):(<View style={[styles.captionRow,styles.captionRowInline]}></View>)}
                        </View>
                        <View style={[styles.captionRowBig,styles.captionRowInline]}>
                            <View style={styles.iconView}>
                            {item.is_active == 1 ? 
                            (<Switch style={genStyles.genSwitch} value={item.is_online == 1 ? true : false}
                                onValueChange={(value) => onChangeSwitch(value, vkey)} />):(null)}
                            </View>
                            
                            <View style={[styles.captionRowBig,styles.captionRowInline]}>
                            <View style={styles.captionRow}></View> 

                                {item.is_active == 1
                                            ? item.have_update != 1
                                                ? (
                                                    item.remain_days <= 7 ? 
                                                    <TouchableOpacity  style={styles.iconView} onPress={() => onEdit(item)} activeOpacity={0.8}>
                                                        {/* <Icon name="sync" color={colors.navBarButtonColor} size={FORTAB? 22: 20} /> */}
                                                        <FaIcon name="refresh" color={colors.navBarButtonColor} size={FORTAB ? 18 : 16} style={{marginRight:5,marginTop:3}}/>
                                                    </TouchableOpacity> 
                                                    : 
                                                    <TouchableOpacity  style={styles.iconView} onPress={() => onEdit(item)} activeOpacity={0.8}>
                                                        <Icon name="edit" color={colors.navBarButtonColor} size={FORTAB? 22: 20} />
                                                    </TouchableOpacity>
                                                )
                                                : (
                                                    <TouchableOpacity style={styles.iconView} onPress={() => {}} activeOpacity={0.8}>
                                                        <MaIcon name="timer-sand" color={colors.navBarButtonColor} size={FORTAB? 30: 25} />
                                                    </TouchableOpacity>
                                                )
                                            : (null)}

                                        <TouchableOpacity style={styles.iconView} onPress={() => onDelete(item)} activeOpacity={0.8}>
                                            <Icon name="delete-button" color={colors.navBarButtonColor} size={FORTAB? 22: 20} />
                                        </TouchableOpacity>

                                        {item.is_active == 1
                                            ? (
                                                <TouchableOpacity style={styles.iconView} onPress={() => onPromote(item)} activeOpacity={0.8}>
                                                    <Icon name="signal" color={colors.navBarButtonColor} size={FORTAB? 22: 20} />
                                                </TouchableOpacity>
                                            )
                                            : (null)}
                            </View>
                        </View>
                    </View>
                </Col>
            </Row>
        );
    }
}

const styles = StyleSheet.create({
    proRow: {
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        position: 'relative',
        borderBottomWidth: 1,
        borderBottomColor: '#eee',
        backgroundColor: '#fff',
        elevation: 3
    },
    proCol: {
        position: 'relative',
        backgroundColor: 'rgba(0,0,0,0)'
    },
    colInner: {
        position: 'relative'
    },
    imageCanvas: {
        position: 'relative',
        overflow: 'hidden',
        flex: 1,
        borderWidth: 1,
        borderBottomWidth: 0,
        borderColor: '#ddd'
    },
    imgInner: {
        position: 'relative',
        flex: 1
    },
    hasVideo1: {
        width: 20,
        height: 20,
        position: 'absolute',
        left: 3,
        zIndex: 1
    },
    proImg: {
        width: null,
        resizeMode: 'cover',
        height: TABLANDSCAPE? 135: TABPORTRAIT? 135: 110
    },
    genChk: {
        position: 'absolute',
        zIndex: 1,
        right: -1,
        top: 0,
        backgroundColor: 'rgba(255,255,255,0.8)',
        paddingTop: 0,
        paddingBottom: 0
    },
    midum: {
        fontSize: FORTAB? 16: 12
    },
    bottomDetail: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        position: 'relative',
        backgroundColor: '#333',
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 2,
        paddingBottom: 4,
        left: 0,
        right: 0,
        bottom: 0
    },
    leftAlign: {
        fontSize: 12,
        fontFamily: 'Montserrat',
        justifyContent: 'space-between'
    },
    white: {
        color: "#FFF"
    },
    small: {
        fontSize: FORTAB
            ? 12
            : 10
    },
    rightAlign: {
        top: 2,
        right: 0
    },
    relative: {
        position: 'relative'
    },
    globalIcon: {
        fontFamily: 'icomoon',
        fontSize: 12,
        justifyContent: 'center'
    },
    camIco: {
        fontSize: 18,
        zIndex: -1
    },
    camIcoAbs: {
        marginLeft: 15,
        zIndex: -1
    },
    bold: {
        fontWeight: '600'
    },
    abscamtxt: {
        position: 'absolute',
        right: 0,
        top: 2,
        width: 18,
        textAlign: 'center',
        backgroundColor: "#0000"
    },
    proCaption: {
        paddingVertical:5,
        paddingHorizontal:8,
        backgroundColor: "#FFF",
        flex:1
    },
    proName: {
        fontSize: FORTAB
            ? 14
            : 12,
        paddingBottom: 0,
        color: '#333',
        fontFamily: 'Montserrat',
    },
    row: {
        alignItems: 'flex-start',
        flex: 1,
        position: 'relative',
        marginTop: 10
    },
    inlineDatarow: {
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'stretch',
        overflow: 'visible'
    },
    spaceBitw: {
        justifyContent: 'space-between',
        alignSelf: 'stretch'
    },
    orangeColor: {
        color: '#f15a23'
    },
    inlineWithico: {
        flexDirection: 'row',
        paddingRight: 10,
        marginRight: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    red: {
        color: '#ff0000'
    },
    borderRight: {
        borderBottomWidth: 0,
        borderLeftWidth: 0,
        borderTopWidth: 0,
        borderRightWidth: 1,
        borderColor: '#ff0000'
    },
    _MPR0_: {
        paddingRight: 0,
        marginRight: 0
    },
    big: {
        fontSize: FORTAB? 18: 14
    },
    orangeColor: {
        color: '#f15a23'
    },
    xlIco: {
        fontSize: FORTAB? 22: 20
    },
    circle:{
        width:10,
        height:10,
        backgroundColor:'#eee',
        borderRadius:5,
        top:0,
        zIndex:1,
    },
    icoText:{
        fontSize:12,
        color:'#333',
        paddingLeft:3
      },
      captionRow:{
        flex:1,
        justifyContent:'center',
      },
      captionRowBig:{
        flex:1.5,
      },
      captionRowInline:{
          flexDirection:'row'
      },
      iconView:{
        width:30,
        alignItems:'center',
        justifyContent:'center'
      },
      viewBtn:{
          width:30
      }
});


/* old code */

 /* <Text style={styles.proName} numberOfLines={1}>{item.title}</Text>
                        <View style={styles.row}>
                            <View style={[styles.inlineDatarow, styles.spaceBitw]}>
                                <Text style={[styles.orangeColor, styles.bold, styles.midum]}>{this.getDispPrice(item.price)}</Text>
                                <View style={styles.inlineWithico}>{item.is_urgent == 1 && <Text
                                        style={[
                                        styles.inlineWithico,
                                        styles.red,
                                        styles.small, {
                                            paddingRight: 5,
                                            marginRight: 5
                                        }
                                    ]}>
                                        <Text style={[styles.globalIcon, styles.inlineWithico, styles.red]}>y</Text>
                                        Urgent</Text>}{item.is_direct_sale == 1 && item.is_urgent == 1
                                        ? <Text style={styles.borderRight}></Text>
                                        : null}{item.is_direct_sale == 1 && <Text style={[styles.inlineWithico, styles.red, styles.small, styles._MPR0_]}>
                                        <Text style={[styles.globalIcon, styles.inlineWithico, styles.red]}>w</Text>Achat Ditect</Text>}
                                </View>

                            </View>

                            {item.remain_days != null && <View
                                style={[
                                styles.inlineWithico, {
                                    marginTop: 5
                                }
                            ]}>
                                <Text style={[styles.globalIcon, styles.big]}>e
                                </Text>

                                <Text style={styles.icoText}>{item.remain_days}
                                    Jours Restants</Text>
                            </View>}
                            <View
                                style={[
                                {
                                    height: 24,
                                    width: 60,
                                    flexDirection: 'row'
                                },
                                styles.justCenter
                            ]}>
                                <View
                                    style={[
                                    {
                                        flex: 1,
                                        flexDirection: 'row'
                                    },
                                    styles.justCenter
                                ]}>
                                    <Icon name="eye" size={12} color={'#888'}/>
                                    <Text
                                        style={{
                                        color: "#888",
                                        fontSize: 12,
                                        marginLeft: 3
                                    }}>{item.total_views}</Text>
                                </View>
                                <View
                                    style={[
                                    {
                                        flex: 1,
                                        flexDirection: 'row'
                                    },
                                    styles.justCenter
                                ]}>
                                    <Icon name="telephone" size={12} color={'#888'}/>
                                    <Text
                                        style={{
                                        color: "#888",
                                        fontSize: 12,
                                        marginLeft: 3
                                    }}>{item.total_clicks}</Text>
                                </View>
                            </View>
                            <View
                                style={[
                                styles.inlineDatarow, {
                                    marginTop: 5
                                }
                            ]}>
                                <View
                                    style={[
                                    styles.circle, styles.inlineWithico, item.is_active == 1
                                        ? {
                                            backgroundColor: '#72bb53'
                                        }
                                        : {
                                            backgroundColor: '#f15a23'
                                        }
                                ]}></View>
                                <Text style={[styles.inlineWithico, styles.orangeColor]}>{item.is_active == 1
                                        ? 'Approved'
                                        : 'Pending'}</Text>
                            </View>
                            <View
                                style={[
                                styles.inlineDatarow,
                                styles.spaceBitw, {
                                    marginTop: 5
                                }
                            ]}>
                                <View style={[styles.inlineDatarow, styles.spaceBitw]}>
                                    <View
                                        style={[
                                        styles.inlineDatarow,
                                        styles.justCenter, {
                                            marginTop: 5,
                                            alignSelf: 'stretch'
                                        }
                                    ]}>
                                        <View
                                            style={{
                                            width: 15
                                        }}>
                                            <Switch
                                                style={genStyles.genSwitch}
                                                value={item.is_online == 1
                                                ? true
                                                : false}
                                                onValueChange={(value) => {
                                                onChangeSwitch(value, vkey)
                                            }}/>
                                        </View>
                                        {item.is_active == 1
                                            ? item.have_update != 1
                                                ? (
                                                    <TouchableOpacity onPress={() => onEdit(item)} activeOpacity={0.8}>
                                                        <Text style={[styles.globalIcon, styles.orangeColor, styles.xlIco]}>h
                                                        </Text>
                                                    </TouchableOpacity>
                                                )
                                                : (
                                                    <TouchableOpacity onPress={() => {}} activeOpacity={0.8}>
                                                        <Text
                                                            style={[
                                                            styles.globalIcon,
                                                            styles.orangeColor,
                                                            styles.xlIco, {
                                                                fontSize: 25
                                                            }
                                                        ]}>{'&'}</Text>
                                                    </TouchableOpacity>
                                                )
                                            : (null)}

                                        <TouchableOpacity onPress={() => onDelete(item)} activeOpacity={0.8}>
                                            <Text style={[styles.globalIcon, styles.orangeColor, styles.xlIco]}>8
                                            </Text>
                                        </TouchableOpacity>

                                        {item.is_active == 1
                                            ? (
                                                <TouchableOpacity onPress={() => onPromote(item)} activeOpacity={0.8}>
                                                    <Text style={[styles.globalIcon, styles.orangeColor, styles.xlIco]}>|</Text>
                                                </TouchableOpacity>
                                            )
                                            : (null)}
                                    </View>
                                </View>
                            </View>
                        </View> */


