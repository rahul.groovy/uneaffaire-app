import React, { Component } from 'react';
import {
  AppRegistry, FlatList, StyleSheet, Text, View,TouchableOpacity,Modal,ActivityIndicator,Dimensions
} from 'react-native';
import {settings} from '../../config/settings';
import ApiHelper from '../../lib/ApiHelper/ApiHelper';
const api=new ApiHelper;
export default class Select extends Component {
constructor(props) {
    super(props);
    this.state={
        visible:false,
        text:typeof this.props.selected != 'undefined' ? this.props.selected : '',
        id:typeof this.props.selectedid != 'undefined' ? this.props.selectedid : '',
        //options:[],
        //load:false,
    }
}

_keyExtractor = (item, index) => item.id;
modalShowHide(){
    this.setState({visible:!this.state.visible});
}
onItemPress(item){
    this.setState({visible:false,text:item.value,id:item.id},()=>this.props.onPress(item.id));
}

reset(){
    this.setState({visible:false,text:'',id:null},()=>this.props.onPress(null));
}

getText(id,ary){
let ret="";
ary.map((data,i)=>{
  if(data.id == id){
    ret=data.value;
  }
});
return ret;
}

render() {
    const {onPress,containerStyle,optionsList,header,vkey,selectedid,selected,disabled,showClose} = this.props;
    const {text,visible,id}=this.state;
    let newtext ="";
    if(selectedid){
        newtext=this.getText(selectedid,optionsList);
    }
    
    let dispText = selectedid && newtext !="" ? newtext : (selected ? selected : text != '' ? text : header);
     return(
        <View key={vkey}>
            <TouchableOpacity onPress={()=>disabled ? null : this.modalShowHide()} style={containerStyle ? [styles.dropDownCont,containerStyle] :[styles.dropDownCont,{width:150}]}>
                <View style={styles.dropDownTextCont}>
                    <Text numberOfLines={1} style={{padding:3}}>{dispText}</Text>
                </View>
                <View style={styles.arrow}>
                {selectedid && newtext !="" && showClose? <TouchableOpacity  activeOpacity={0.8} onPress={()=>{this.reset()}}><Text style={[{fontFamily:'icomoon',fontSize:15 }]}>{'}'}</Text></TouchableOpacity> : <Text style={{ fontFamily:'icomoon',fontSize:12,justifyContent:'center'}}>g</Text>}
                    
                </View>
            </TouchableOpacity>
            <Modal style={styles.modal} animationType={"slide"} transparent={false} visible={visible} onRequestClose={() =>this.modalShowHide()}>
                
                <View style={styles.container}>
                    <View style={{height:50,flexDirection:'row',alignItems:'center',justifyContent:'center',elevation:3}}>
                        <TouchableOpacity style={{width:50,alignItems:'center',justifyContent:'center'}} onPress={() =>this.modalShowHide()}>
                            <Text style={{ fontFamily:'icomoon',color:'#FD5009',fontSize:20}}>_</Text>
                        </TouchableOpacity>
                        <Text style={[styles.title]}>{header}</Text>
                        <Text style={{width:30}}></Text>
                    </View>
                    <FlatList
                    data={optionsList}
                    keyExtractor={this._keyExtractor}
                    renderItem={({item}) => selectedid ? <TouchableOpacity key={item.id} style={item.id == id ? [styles.item,styles.itemSelected]:[styles.item]} onPress={()=>this.onItemPress(item)}>
                        <Text style={item.id == id ? [styles.itemText,styles.itemTextSelected]:[styles.itemText]}>{item.value}</Text>
                        </TouchableOpacity> : <TouchableOpacity key={item.id} style={item.value == text ? [styles.item,styles.itemSelected]:[styles.item]} onPress={()=>this.onItemPress(item)}>
                        <Text style={item.value == text ? [styles.itemText,styles.itemTextSelected]:[styles.itemText]}>{item.value}</Text>
                        </TouchableOpacity>}
                    />
                </View>
            </Modal>
        </View>
    );
  }
}

const styles = StyleSheet.create({
container: {
    flex: 1,
    paddingTop: 10
},
dropDownCont:{
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center',
    borderWidth:1,
    borderColor:'#ddd',
    padding:7
},
dropDownTextCont:{
    flex:1,
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center'
},
arrow:{
    width:30,
    alignItems:'center',
    justifyContent:'center'
},
sectionHeader: {
    paddingTop:5,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 5,
    fontSize: 14,
    fontWeight: 'bold',
    backgroundColor: '#FD5009',
    color:'#fff'
}, 
title:{
    color:'#000',
    textAlign:'center',
    fontSize: 16,
    fontWeight:"600",
    //marginBottom:5,
    flex:1,
    color:'#FD5009'
},
item: {
    padding: 10,
    height: 40,
    justifyContent:'center'
},
itemSelected:{
    backgroundColor:"#FD5009"
},
itemText:{
    fontSize:14
    //color:"#888"
},
itemTextSelected:{
    color:"#FFF"
}
})