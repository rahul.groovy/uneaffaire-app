import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    TouchableOpacity,
    ActivityIndicator,
    ListView,
    Image,
    ScrollView,
    Dimensions,
    View,
    Alert
} from 'react-native';
import images from '../../config/images';
//import styles from '../../config/genStyle';
import {settings} from '../../config/settings';
import ImagePicker from 'react-native-image-crop-picker';
import ApiHelper from '../../lib/ApiHelper/ApiHelper';
import {colors} from '../../config/styles';

const { height, width } = Dimensions.get('window');
const FORTAB = width < 1025 && width > 721;
const TABLANDSCAPE = width < 1025 && width > 768;
const TABPORTRAIT = width > 721 && width < 769;
const api = new ApiHelper;

export default class ImageBox extends Component {

    constructor(props) {
        super(props);
        this.state = {
            mainImage: '',
            oImages:[],
            serverImages:[],
            upload:false,
            progress:0
        }
    }
    renderRow(data,key) {
        return (
            <TouchableOpacity key={key} style={styles.placeHolderview} onPress={()=>this.makeMain(key)}>
                <Image 
                    style={[styles.placeHolderImg]}
                    source={{uri:data.path}}/>
                <TouchableOpacity activeOpacity={0.8} style={styles.cancelImg} onPress={()=>this.removeImage(key,data)}>
                    <Text style={styles.globalIcon}>{'}'}</Text>
                </TouchableOpacity>
            </TouchableOpacity>
        );
}

addImage(){
   

}

reset(){
    this.setState({mainImage:'',oImages:[],serverImages:[],upload:false,progress:0});
}

makeMain(i){
    let m=this.state.oImages[i];
    this.setState({mainImage:m});
}

getServerImage(){
    let ret=[];
    
    this.state.oImages.map((data,i)=>{
        console.log(data);
        //if (data.image) {
            ret.push(data.image);
        //}
    });
    console.log(ret);
    return ret;
}

getImages(){
    // console.log(this.state.mainImage);
    return {
        //images:this.state.oImages,
        images:this.getServerImage(),
        //serverImages:this.getServerImage(),
        main:this.state.mainImage,
    }
}

decImage(){
    let img=this.state.oImages;
    const minAllow = this.props.isUserPro ? 6 : 4;
    if(img.length > minAllow){
        let mainImage='';
        img=img.slice(0,minAllow);
        img.map((data,i)=>{
            if(data == this.state.mainImage){
                mainImage=this.state.mainImage;
            }
        });
        this.setState({oImages:img,mainImage:mainImage});
    }
}

rImage(i){
    let img=this.state.oImages;
    if(img[i] == this.state.mainImage){
        img.splice(i,1);
        this.setState({oImages:img,mainImage:''},()=>this.rem());
    }else{
        img.splice(i,1);
        this.setState({oImages:img},()=>this.rem());
    }
    
}

rem(){
    const minAllow = this.props.isUserPro ? 6 : 4;
    if(this.props.decImage && this.state.oImages.length <= minAllow){
        this.props.decImage();
    }
}

checkIfUpload(li){
    if(this.state.upload === false){
    let ret=false;
    this.state.oImages.map((data,i)=>{
        if(!data.isServer){
            ret=true;
        }
    });
    if(ret && this.props.uploadImage){
        this.props.uploadImage(li);
    }
    }
}

removeImage(i,data){
    if(data.isServer){
        let request={
        url:settings.endpoints.removePostAdImage,
            method:"POST",
            params:{token:this.props.token,image:[data.image]},
        }
        //console.log('removing image'+data.image);
        api.FetchData(request).then((result)=>{
            if(result.status){
                this.rImage(i);
            }    
        }).catch((err)=>{
            //console.log(err);
        });
    }else{
        this.rImage(i);
    }
}

onActionSelect(i){
    if(i==0){
        ImagePicker.openCamera({
            width: 1200,
            height: 1200,
            cropping: false,
            mediaType: "photo",
            compressImageQuality : 0.8
            // width: 300,
            // height: 400,
            //cropping: true,
        }).then(image => {
            //this.setState({upload:true});
            //this.uploadImages([image]).then((result)=>{
                //if(result.status){
                    if(typeof image != 'undefined'){

                        let cb=null;
                        if( (1 + this.state.oImages.length ) > this.props.allowed && this.props.allowed < 10){
                            if(this.props.incImage){                                
                                cb=this.props.incImage;
                            }
                        }
                        let imgs=this.state.oImages;
                        //let serverImgs=this.state.serverImages;
                        //serverImgs=serverImgs.concat(result.uploaded_images);
                        if((this.state.oImages.length+1) <= 10 ){
                            image.isServer=false;
                            imgs.push(image);
                            //imgs=imgs.concat(result.uploaded_images);
                            this.setState({oImages:imgs,upload:false},()=>{
                                if(cb!=null){
                                    cb();
                                }
                            });
                        }else{
                            alert('Vous pouvez ajouter '+10+' images maximum autorisées');
                        }
                    }
                //}
            //}).catch((err)=>{
                //this.setState({upload:false});
                //console.log(err);
            //});
            
        }).catch(error =>     
             Alert.alert(
            'Alerte!',
            "Impossible d'ouvrir Gallary",
            [
                {text: 'Ok', onPress: () => {
                    //this.props.navigator.pop({animated:true});
                }},
            ],
            { cancelable: false }
        ) );
    }else if(i == 1){
        ImagePicker.openPicker({
            multiple: true,
            width: 1200,
            height: 1200,
            cropping: false,
            compressImageQuality : 0.8,
            mediaType: "photo",
        }).then(images => {
            //console.log(images);
            if(typeof images != 'undefined'){
                //this.setState({upload:true});
                //this.uploadImages(images).then((result)=>{
                    //if(result.status){
                        let cb=null;
                        if( (images.length + this.state.oImages.length ) > this.props.allowed && this.props.allowed < 10){
                            if(this.props.incImage){
                                cb=this.props.incImage;
                            }
                        }
                        //console.log((images.length + this.state.oImages.length ) <= this.props.allowed );
                    let imgs=this.state.oImages;
                    let flg=false;
                    let dispno = 0;
                    const minAllow = this.props.isUserPro ? 6 : 4;
                    if((imgs.length+images.length) > this.props.allowed && this.props.allowed > minAllow){
                        dispno=imgs.length+images.length-this.props.allowed;
                        images=images.slice(0,this.props.allowed - imgs.length);
                        flg=true;
                    }
                    //let serverImgs=this.state.serverImages;
                    //serverImgs=serverImgs.concat(result.uploaded_images);
                    images.map((data,i)=>{
                        data.isServer=false;
                    });
                    imgs=imgs.concat(images);
                    //imgs=imgs.concat(result.uploaded_images);
                    this.setState({oImages:imgs,upload:false},()=>{
                        if(cb!=null){
                            cb();
                        }
                    }); 
                    if(flg){
                        alert('Les '+dispno+' dernières images ne sont pas incluses car vous pouvez ajouter '+this.props.allowed+' images maximum');
                    }
                    //}
               // }).catch((err)=>{
                   // this.setState({upload:false});
                    //console.log(err);
               // });
            }
            // else{
            //     alert('Maximum '+this.props.allowed+' autorisé, supprimez certaines images pour ajouter de nouvelles');
            // }
            
        }).catch(error => 
            Alert.alert(
                'Alerte!',
                "Impossible d'ouvrir Gallary",
                [
                    {text: 'Ok', onPress: () => {
                        //this.props.navigator.pop({animated:true});
                    }},
                ],
                { cancelable: false }
            ) 
        );
    }
}

            onCancel(){}

            uploadImages(index){
                return new Promise((resolve,reject)=>{
                    const query = new FormData();
                    query.append('token', this.props.token);
                    this.state.oImages.map((data,i)=>{
                        let file = {
                        uri: data.path,            
                        name:data.path.split('/').pop(),           
                        type:data.mime       
                        }
                    if(typeof file['name'] !== 'undefined' && file['name'] !== null && file['name'] !='' && file['uri'] != '' & !data.isServer){
                            query.append('images', file);
                    }
                    });
                    let request={
                        url:settings.endpoints.ajaxAdUpload,
                        method:"POST",
                        query:query,
                    }
                    console.log('uploading Images of tab '+(index+1));
                    console.log(request);
                    this.setState({upload:true},()=>{
                        api.uploadData(request,this.onProgress).then((result)=>{
                                //console.log('result of tab '+(index+1));
                                console.log(result);
                                // return false;
                                //setTimeout(function(){
                                if(result.status){
                                    let oImages=this.state.oImages;
                                    let i2=0;
                                    oImages.map((data,i)=>{
                                        if(!data.isServer){
                                            data.image=result.uploaded_images[i2].image;
                                            data.full_name=result.uploaded_images[i2].full_name;
                                            data.isServer=true;
                                            i2+=1;
                                        }
                                    });
                                    this.setState({upload:false,oImages:oImages},()=>{
                                        result.index=index;
                                        resolve(result); 
                                    });
                                }else{
                                    this.setState({upload:false},()=>{
                                        reject(result);
                                    });
                                }         
                                //},100);
                            }).catch((err)=>{
                                //console.log('error of tab '+(index+1));
                                //console.log(err);
                                this.setState({upload:false},()=>{
                                    reject(err);
                                });
                            }); 
                        });
                });
            }

            onProgress = (progress) => {
                if(progress !== undefined && progress !== null){
                    this.setState({progress:progress});
                }
            }

        setImages(data){
            if(data.length > 0){
                data.map((d,i)=>{
                    d.isServer=true;
                    d.path=d.full_name;
                });
                this.setState({oImages:data,mainImage:data[0]});
            }
        }

            render() {
                // {elevation:upload ? 0: 3}
            // console.log(this.state.oImages);
            const {onclickAdd}=this.props;
            const {upload,progress}=this.state;
            const minAllow = this.props.isUserPro ? 6 : 4;
            return (
                <View style={{overflow:'visible',paddingBottom:25}} removeClippedSubviews={false} >
                    <View style={[styles.imgUpload]}>
                        {this.state.mainImage == "" && this.state.oImages.length < 1 ?
                        <View><View style={{justifyContent:'center',alignItems:'center'}}><Text style={{textAlign:'center'}}>Cliquez sur l'appareil photo pour commencer à ajouter des images</Text></View>
                        <View style={{justifyContent:'center',alignItems:'center'}}>
                        <Text style={{padding:7,textAlign:'center'}}>{`${minAllow} photos gratuites`}</Text>
                    </View></View>:
                        <View>
                            <View style={styles.fullupImgView}>
                                {this.state.mainImage!="" ? (
                                    <Image source={{uri:this.state.mainImage.path}} style={styles.fullupImg}/>
                                ):(
                                    <View>
                                        <View style={styles.fullBlankImg}>
                                            <Text style={{padding:7,textAlign:'center'}}>Cliquez sur n'importe quelle image pour définir l'image principale</Text>
                                        </View>
                                    </View>
                                )}
                                
                                {/* <TouchableOpacity activeOpacity={0.8} style={styles.cancelImg}>
                                    <Text style={styles.globalIcon}>{'}'}</Text>
                                </TouchableOpacity> */}
                            </View>                         
                            <ScrollView
                                automaticallyAdjustContentInsets={false}
                                showsHorizontalScrollIndicator={false}
                                contentContainerStyle={styles.Carousel}
                                horizontal>
                                  {/* <View style={[styles.inlineDatarow, styles._MT5_]}>   */}
                                    {this.state.oImages.map((data,i)=>{
                                        return this.renderRow(data,i);
                                    })}
                                  {/* </View>   */}
                            </ScrollView>
                        </View>}
                    </View>
                    <TouchableOpacity elevation={5} activeOpacity={0.8} style={styles.postadcamera} onPress={upload ? null : onclickAdd}>
                        <Text style={[styles.globalIcon,styles.cameraAd]}>A</Text>
                    </TouchableOpacity>  

                    {upload ? (<View style={styles.progressView}>
                    <View style={{alignItems:'center',justifyContent:'center'}}>
                        <View style={styles.iconContainer}>
                            <Image style={styles.imagestyle} source={images.loader} />
                        </View>
                        <Text style={styles.progressText}>Téléchargement d'images...</Text>
                        <Text style={styles.progressText}>{progress}{'%'}</Text>
                        <View style={styles.progressBarCont}>
                            <View style={[styles.progressBar,{width:progress+'%'}]}></View>
                        </View>
                    </View>
                    </View>):(null)}                    
                </View>                  
    );
  }
}

const styles =StyleSheet.create({
    imgUploadView:{
         flex:1,
         height:180,
    },
    progressView:{
        flex:1,
        height:FORTAB ? 380:270,
        alignItems:'center',
        justifyContent:'center',
        backgroundColor:'#08080855',
        padding:7,
        width:width-14,
        //elevation:5,
        //overflow:'visible',
        position:'absolute',
        top:0, 
        zIndex:22,
    },
    progressBarCont:{
        height:5,
        backgroundColor:"#0009",
        width:200,
        marginTop:15,
        borderRadius:2,
    },
    progressBar:{
        backgroundColor:colors.navBarButtonColor,
        height:5,
        borderRadius:2,
    },    
    imgUpload:{
        flex:1,
        height:FORTAB ? 380:270,
        alignItems:'center',
        justifyContent:'center',
        backgroundColor:'#ddd',
        padding:7,
        //elevation:3,
        overflow:'visible',
        position:'relative',
    },
    progressText:{
        color:"#fff"
    },
    fullBlankImg:{
        width:FORTAB ? width-50:null,
        height:FORTAB ? 200:130,
        backgroundColor:'#eee',
        borderColor:"#ccc",
        borderWidth:1,
        alignItems:'center',
        justifyContent:'center',
    },
    fullupImg:{
        width:FORTAB ? width-50:null,
        resizeMode:'cover',
        height:FORTAB ? 200:130,
    },
    fullupImgView:{
        height:FORTAB ? 220:140,
        width:width-28
    },
    postadcamera:{
        position:'absolute',
        right:15,
        bottom:0,
        padding:13,
        borderRadius:50,
        backgroundColor:'#FFFFFF',
        borderWidth:1,
        borderColor:'#bbbbbb',
        elevation:3,
        zIndex:3
    },
    cameraAd:{
        color:'#f15a23',
        fontSize:22,
    },
    cancelImg:{
        position:'absolute',
        right:0,
        top:0,
        padding:5,
        borderRadius:20,
        borderWidth:1.5,
        borderColor:'#999',
        backgroundColor:'rgba(255,255,255,0.9)',
    },
    globalIcon:{
        fontFamily:'icomoon',
        fontSize:12,
        justifyContent:'center'
    },
    Carousel:{
        flexDirection:'row',
        alignItems:'center',
        overflow:'visible'
    },
    inlineDatarow:{
      flex:1,
    },
    placeHolderview:{
        paddingTop:7,
        paddingRight:7,
        marginHorizontal:7,
        flex:1, 
        alignItems:'center',
    },
    placeHolderImg:{
        width:100,
        height:100,  
    },
    imagestyle:{
        height:40,
        width:35
    },
    iconContainer:{
        backgroundColor:"#fff",
        height:50,
        width:50,
        alignItems:'center',
        justifyContent:'center',
        borderRadius:10,
    },


});

/* <ScrollView
                        ref={(scrollView) => {
                        _scrollView = scrollView;
                    }}
                        automaticallyAdjustContentInsets={false}
                        showsHorizontalScrollIndicator={false}
                        contentContainerStyle={styles.Carousel}
                        horizontal>
                        <View style={[styles.inlineDatarow, styles._MT5_]}>
                            <View style={styles.placeHolderview}><Image style={[styles.placeHolderImg]} source={images.minic}/>
                                <TouchableOpacity activeOpacity={0.8} style={styles.cancelImg}>
                                    <Text style={styles.globalIcon}>{'}'}</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.placeHolderview}><Image style={[styles.placeHolderImg]} source={images.IMG16}/>
                                <TouchableOpacity activeOpacity={0.8} style={styles.cancelImg}>
                                    <Text style={styles.globalIcon}>{'}}'}</Text>
                </TouchableOpacity>
            </View>
            <View style={styles.placeHolderview}><Image style={[styles.placeHolderImg]} source={images.IMG16}/>
                <TouchableOpacity activeOpacity={0.8} style={styles.cancelImg}>
                    <Text style={styles.globalIcon}>{'}}'}</Text>
    </TouchableOpacity>
</View>
<View style={styles.placeHolderview}><Image style={[styles.placeHolderImg]} source={images.IMG16}/>
    <TouchableOpacity activeOpacity={0.8} style={styles.cancelImg}>
        <Text style={styles.globalIcon}>{'}'}</Text>
    </TouchableOpacity>
</View>
<View style={styles.placeHolderview}><Image style={[styles.placeHolderImg]} source={images.IMG16}/>
    <TouchableOpacity activeOpacity={0.8} style={styles.cancelImg}>
        <Text style={styles.globalIcon}>{'}'}</Text>
    </TouchableOpacity>
</View>
</View>
</ScrollView>*/