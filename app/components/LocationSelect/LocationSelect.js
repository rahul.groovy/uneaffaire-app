import React, {Component} from 'react';
import {
    Text,
    View,
    TouchableOpacity,
    ScrollView,
    StyleSheet,
    Modal,
    Platform,
    Alert,
    ActivityIndicator
} from 'react-native';
import {settings} from '../../config/settings';
import { colors } from '../../config/styles';
import styles from '../../config/genStyle';
import ModalDropdown from '../../lib/react-native-modal-dropdown';
import Select from '../../components/Select/Select';
import ApiHelper from '../../lib/ApiHelper/ApiHelper';
import FloatLabelTextInput from '../../lib/react-native-floating-label-text-input';
import genStyles from '../../config/genStyle';
import { iconsMap, iconsLoaded } from '../../config/icons';
import tryGetLocation from '../../redux/utils/location';
import Icon from 'react-native-vector-icons/MaterialIcons';
import oauthconfig from '../../config/oauthconfig';
import FranceMap from '../../components/FranceMap/FranceMap';
var _ = require('lodash');
const api = new ApiHelper;
export default class LocationSelect extends Component {
    constructor(props) {
        super(props);
        this.kmOptions=['10 KM','20 KM','30 KM','50 KM','100 KM', '200 KM'];
        this.state = {
            load: false,
            addressData: {},
            address: '',
            placedata: [],
            isCurrent:false,
            isPermission:false,
            modalVisible:false,
            isCloseS : false,
            //nearByKm:10,
            gettingDirection:false,
            isMapClick : false,
            currentLocationData:{
                lat:'',
                long:'',
                zipcode:'',
                town:'',
                nearByKm:30
            }
        }
    }

    reset(){
        let clData = {
            lat:'',
            long:'',
            zipcode:'',
            town:'',
            nearByKm:30
        };
        this.setState({
            load: false,
            addressData: {},
            address: '',
            placedata: [],
            isCurrent:false,
            isPermission:false,
            modalVisible:false,
            gettingDirection:false,
            currentLocationData:clData
        });
    }

    getData(value) {
        if (value != "" && value.length > 2) {
            let request = {
                url: settings.endpoints.getLocation,
                method: 'POST',
                params: {
                    query: value
                }
            }
            this.setState({load: true},()=>{
                api
                .FetchData(request)
                .then((result) => {
                    if (result.status) {
                        this.setState({placedata: result.locationlist, load: false});
                    } else {
                        this.setState({load: false});
                    }
                })
                .catch((error) => {
                    this.setState({load: false});
                });
            });
            
        }else{
            this.setState({placedata: [], load: false});
        }
        if(this.props.onChange){
            this.props.onChange();
        }
    }

    getAddress() {
        if(this.props.isEnable){
            let obj= this.state.isCurrent ? this.state.currentLocationData : this.state.addressData
            obj.isCurrent=this.state.isCurrent;
            return obj;
        }else{
            return {};
        }
    }

    setAddress(data) {
        if(data.zipcode !== undefined && data.town !== undefined) {
            data.cityzip = data.zipcode+" "+data.town;
            //let addr=data.zipcode+" "+data.town;
            // this.FranceMap.check(true);
            this.setState({addressData:data,address:data.cityzip,placedata:[],modalVisible:false,isCloseS:true});
        } else {
            this.setState({addressData:data,address:'',placedata:[],modalVisible:false});
        }        
        //console.log(data);
    }
    cb(bool){
        const { isCurrent } = this.state;
        if(bool){
            this.setState({gettingDirection:true},()=>{
                // if(this.FranceMap !== undefined && this.FranceMap !== undefined && this.FranceMap.setLocationData) {
                //     this.FranceMap.setLocationData({france:true});
                // }
                this.getLocation();
            });
        }
        if(this.props.onChange){
            this.props.onChange(isCurrent);
        }
    }
    showCurrent(bool){
        if(bool){
            this.setState({isCurrent:bool,placedata:[]},()=>this.cb(bool))
        }else{
            this.setState({isCurrent:bool},()=>this.cb(bool))
        }
    }

    locationOpen = () => {
        this.setState({
            modalVisible : true
        })
    }

    setLocationData(data){
        if(data.isCurrent===false){
            // if(this.FranceMap !== null && this.FranceMap !== undefined) {
            //     this.FranceMap.check(true);
            // }
            this.setAddress(data);
        }else if(data.isCurrent === true){
            let clData=this.state.currentLocationData;
            clData.lat=data.lat;
            clData.long=data.long;
            clData.nearByKm=data.nearByKm;
            this.setState({currentLocationData:clData},()=>{
                this.showCurrent(true);
            })
        }
        //this.setState({})
    }

    getDatafromGoogle(qs){
        if(this.state.currentLocationData.zipcode === "" && this.state.currentLocationData.zipcode == ""){
            //console.log('https://maps.googleapis.com/maps/api/geocode/json?latlng='+qs+'&key='+oauthconfig.googleMapKey);
            fetch('https://maps.googleapis.com/maps/api/geocode/json?latlng='+qs+'&key='+oauthconfig.googleMapKey)
            .then((response) => response.json())
            .then((result)=>{
            //console.log(result);    
            if(result.status === "OK" && result.results !== undefined && result.results.length > 0){
                    let addr_c = result.results[0].address_components;
                    let zipcode = null;
                    let town =  null;
                    if(_.isArray(addr_c)){
                        addr_c.map((d,i)=>{
                            if(_.isArray(d.types) && d.types.indexOf("locality") > -1 && d.types.indexOf("political") > -1){
                                town = d.long_name || d.short_name;
                            }
                            if(_.isArray(d.types) && d.types.indexOf("postal_code") > -1){
                                zipcode = d.long_name || d.short_name;
                            }
                        });
                    }
                    if(zipcode !== null && town !== null){
                        let clData=this.state.currentLocationData;
                        clData.zipcode=zipcode;
                        clData.town=town;
                        this.getDatas(zipcode,town);
                       /// this.setState({currentLocationData:clData,gettingDirection:false});
                        //console.log(clData);
                    }else{
                        this.handleLocationError();
                    }
                }          
            }).catch((err)=>{
                 //console.log(err);
                this.handleLocationError();
            });
        }
    }

    getDatas = (zipcode,town) => {
        let request={
            url:settings.endpoints.checkUserCurrentLocation,
            method:'POST',
            params:{zipcode:zipcode,town:town}
       }
       api.FetchData(request).then((result)=>{
           console.log(result);
            if(result.status){
                // setAddress
                let  currentLocation = result.latlongdata;
                currentLocation.cityzip = currentLocation.zipcode+" "+currentLocation.town;
                this.setState({currentLocationData:currentLocation,gettingDirection:false});
            }else{
                this.setState({gettingDirection:false},()=>{
                    // alert('Something Went Wrong.');
                    setTimeout(()=>{
                        Alert.alert(
                            'Infructueux',
                            result.message,
                            [
                                {text: 'Ok',onPress:()=>{
                                    this.reset()
                                }},
                            ],
                            { cancelable: false }
                        )
                    },500);
                });
            }               
        }).catch((error)=>{ 
                //console.log(error);
        });
    }

    handleLocationError(){
        this.setState({gettingDirection:false},()=>{
            setTimeout(()=>{
                Alert.alert(
                    'Infructueux',
                    'N\'a pas pu obtenir d\'emplacement',
                    [
                        {text: 'Approuvé', onPress: () => {
                           this.showCurrent(false);
                        }},
                    ],
                    { cancelable: false }
                )
            },500);
        });
    }

    getLocation(){
        tryGetLocation(this.props.authActions).then((result)=>{
            console.log(result);
            let clData=this.state.currentLocationData;
            let load = clData.zipcode === "" && clData.zipcode === "" ? true : false;
            clData.lat=result.latitude;
            clData.long=result.longitude;
            console.log(clData);
            this.setState({currentLocationData:clData,gettingDirection:load},()=>{
                //if(load){
                    if(this.FranceMap !== null && this.FranceMap !== undefined && this.FranceMap.clearR !== undefined) {
                        this.FranceMap.clearR();
                    }
                    this.getDatafromGoogle(clData.lat+','+clData.long);
               // }
            });
        }).catch((err)=>{
            //console.log(err);
            this.handleLocationError();
        });  
    }

    inputRef(){
       return this.FloatLabelTextInput.inputRef();
    }

    _getRef(inputRef,o){
        if(inputRef == 0){
            this.FloatLabelTextInput = o;
        }else if(inputRef == 1){
            this.FloatLabelTextInput2 = o;
        }
    }

    _getInput(inputRef){
        if(inputRef == 0){
            return this.FloatLabelTextInput;
        }else if(inputRef == 1){
            return this.FloatLabelTextInput2;
        }
    }

    _onFocus(bool,inputRef){
        const { franceData , showMap } = this.props;
        
        if(!bool){
            // console.log('in if');
            // console.log(franceData);
            this.setState({modalVisible:true,isCloseS:false},()=>{
                if(franceData !== undefined && this.FranceMap!== undefined && this.FranceMap !== null && franceData.france !== undefined){
                        this.FranceMap.setLocationData(franceData);
                }
                if(inputRef == 0){
                    setTimeout(()=>{
                        if(Platform.OS === 'android') this.setFocus(this._getInput(1),true);
                    },50);    
                }
            });
        } else {
            // console.log('in else');
            this.setState({modalVisible:true},()=>{
                if((franceData !== undefined && franceData.france &&  this.FranceMap !== undefined && this.FranceMap !== null) || (this.state.address === '' && _.isEmpty(franceData) && showMap)) {
                    this.FranceMap.check(true);
                } else {
                    this.setState({
                        isCloseS:this.state.address === '' ? false : true
                    },()=>{
                        this.setLocationData(true);
                    })
                }
            });
        }
    }

    setFocus(ref,bool){
        if(ref !== undefined && ref !== null){
            if(bool){
                ref.focus();
            }else{
                ref.blur();
            }
        }
    }

    onModalClose(){
        this.setState({modalVisible:false},()=>{
            setTimeout(()=>{
                this.setFocus(this.FloatLabelTextInput,false);
            },100);
        });
    }

    clearAdd() {
        let clData = {
            lat:'',
            long:'',
            zipcode:'',
            town:'',
            nearByKm:30,
        };
        this.setState({
            load: false,
            addressData: {},
            address: '',
            currentLocationData:clData,
            isCloseS : false,
            france:true
        },);
    }

    _renderGetDirections(){
        const {gettingDirection} = this.state;
        return(
            <Modal visible={gettingDirection} onRequestClose={() => {}}
            transparent={true} style={StyleSheet.Modal}>
        <View style={{flex:1,alignItems:'center',justifyContent:'center',backgroundColor:"#0008"}}>
            <View style={{backgroundColor:"#fff", alignItems:'center',justifyContent:'center',flexDirection:'row',padding:15,borderRadius:10}}>
                <View style={{padding:5}}>
                    <ActivityIndicator size="large" animating={true} color={colors.navBarButtonColor}/>
                </View>
                <View style={{padding:5,maxWidth:150}}>
                    <Text>{'Obtenir Votre Emplacement ...'}</Text>
                </View>
            </View>
        </View></Modal>);
    }

    renderInput(inputRef){
        const {currentLocation,onChange,userdata,isAccount,fieldContainerStyle,removeError,postAd,isEnable,nearMe,advance,fromShop} = this.props;
        const {placedata,isCurrent,nearByKm,modalVisible,currentLocationData,isCloseS} = this.state;
        return (<View>
            {currentLocation ? (
                isCurrent ? (   
                    <View style={{flexDirection:'row',alignItems:'center',justifyContent:'center',marginTop:5}}>
                     <FloatLabelTextInput style={{fontSize:12}}
                     ref={o=>this._getRef(inputRef,o)}
                    forIco={"Ico"} icon={'t'}
                    IconProvider={'FA'}
                    editable={false}
                    value={''}
                    fieldContainerStyle={fieldContainerStyle && inputRef == 0 ? fieldContainerStyle : null}
                    placeholder={(!advance) && currentLocationData.zipcode != "" && currentLocationData.town != "" ? 
                    // 'Autour de moi - rayon de' :
                    currentLocationData.zipcode+" "+currentLocationData.town:
                    "Autour de moi - Rayon de "}
                    underlineColorAndroid={'#eee'}/>

                    {postAd || fromShop ? (null) : (
                        <View style={{position:'relative',marginLeft:5}}>
                        <ModalDropdown ref={o=>this.ModalDropdown =o} 
                        defaultValue={this.state.currentLocationData.nearByKm+' KM'} 
                            options={this.kmOptions}
                                        textStyle={[genStyles.dropdown_2_text,genStyles._F16_,genStyles.grayColor]} 
                                        dropdownStyle={{
                                            //bottom:0,
                                            width:70,
                                            top:-50,
                                            height:120,
                                        }}
                                        onSelect={(item) => {
                                            let currentLocationData=this.state.currentLocationData;
                                            let str=this.kmOptions[item].replace(" KM","");
                                            let nkm= parseInt(str);
                                            currentLocationData.nearByKm=nkm;
                                            this.setState({currentLocationData:currentLocationData},()=>{
                                                if(onChange){ onChange(true); }
                                            });
                                            
                                        }}/>
                        </View>)}

                   
                    <Text onPress={()=>isEnable ? this.showCurrent(!isCurrent) :null } style={[{fontFamily:'icomoon',fontSize:16,color:isEnable ? '#2E89ED':"#ccc" }]}>{'}'}</Text>
                    </View>
                   
                ):(
                    <View style={{flexDirection:'row',alignItems:'center',justifyContent:'center',marginTop:5}}>
                    <FloatLabelTextInput
                    //editIco={'t'}
                    ref={o=>this._getRef(inputRef,o)}
                    editable={isAccount !== undefined ? !isAccount : true}
                    value={isAccount !== undefined ?  isAccount ? userdata.postal_code:this.state.address:this.state.address}
                    placeholder={this.props.placeholder ? this.props.placeholder : "Ville ou code postal...."}
                    fieldContainerStyle={fieldContainerStyle && inputRef == 0 ? fieldContainerStyle : null}
                    onFocus={()=>this._onFocus(modalVisible,inputRef)}
                    onChangeTextValue={(value) => {
                        if(modalVisible){
                            this.setState({address: value},()=>{
                                this.getData(value);
                                if(removeError){
                                    removeError();
                                }
                            });
                        } 
                    }}
                    underlineColorAndroid={'#eee'}/>
                    {
                        postAd ? !isAccount ?  <Icon name="gps-fixed" onPress={()=>isEnable ? this.showCurrent(!isCurrent) :null} size={30} color={isEnable ? colors.navBarButtonColor :"#ccc" } /> : null : isCloseS && this.state.text !== '' ? ( <Text onPress={()=>{this.clearAdd()}} 
                        style={[{fontFamily:'icomoon',fontSize:20,color:'#2E89ED'}]}>{'}'}</Text>) :  <Icon onPress={()=>isEnable ? this.showCurrent(!isCurrent) :null} 
                        style={[{fontSize:28,color:'#2E89ED'}]} name="gps-fixed"></Icon>
                        
                    }
                   
                    {/* <Text onPress={()=>this.showCurrent(!isCurrent)} 
                        style={[{fontFamily:'icomoon',fontSize:20}]}>t</Text> */}
                    </View>
                )
                ):(
                    <View style={{flexDirection:'row',alignItems:'center',justifyContent:'center',marginTop:5}}>
                <FloatLabelTextInput
                    //editIco={this.props.noIco ? '':'t'}
                    ref={o=>this._getRef(inputRef,o)}
                    editable={isAccount !== undefined ? !isAccount : true}
                    value={isAccount !== undefined ?  isAccount ? userdata.postal_code:this.state.address:this.state.address}
                    placeholder={this.props.placeholder ? this.props.placeholder : "Ville ou code postal...."}
                    fieldContainerStyle={fieldContainerStyle && inputRef == 0 ? fieldContainerStyle : null}
                    onFocus={()=>this._onFocus(modalVisible,inputRef)}
                    onChangeTextValue={(value) => {
                        if(modalVisible){
                            this.setState({address: value},()=>{
                                this.getData(value);
                                if(removeError){
                                    removeError();
                                }
                            });
                        }
                    }}
                    underlineColorAndroid={'#eee'}/>
                    {
                        nearMe ? <Icon name="gps-fixed" onPress={()=>isEnable ? this.showCurrent(!isCurrent) :null} size={35} color={isEnable ? colors.navBarButtonColor :"#ccc" } />: <Text onPress={()=>isEnable ? this.showCurrent(!isCurrent) :null} 
                        style={[{fontFamily:'icomoon',fontSize:25,color:'#2E89ED'}]}>t</Text>
                        
                    }
                    </View> )}
                
                {modalVisible ?
                (<ScrollView style={{backgroundColor:"#f8f8f8"}}>
                    {placedata.map((data, i) => {
                    return <TouchableOpacity key={i} style={{
                        flex:1,
                        padding:7,
                        borderBottomColor:"#eee",
                        borderBottomWidth:0.5,
                    }} onPress={()=>{
                        this.setAddress(data);
                    }}><Text style={{fontSize:16,color:"#666"}}>{data.zipcode}{' '}{data.town}</Text></TouchableOpacity>
                    })}
                </ScrollView>):(null)}
              
        </View>);
    }

    setData = () => {
        const { isCurrent  } = this.state;
        if(this.state.address !== '') {
            let data = this.getAddress();
            // this.props.onFranceChange(data);
            console.log(data);
        }else if(isCurrent) {
            this.showCurrent(true);
        }else {
            let data = this.FranceMap.getAddress();
            this.props.onFranceChange(data);
        }       
        this.onModalClose();
    }

    

    render() {
        const {currentLocation,onChange,userdata,isAccount,showMap}=this.props;
        const {placedata,isCurrent,nearByKm,modalVisible,address} = this.state;
        //console.log(showMap);
        return (<View>
                    {this._renderGetDirections()}
                    {/* outer view */}
                    {this.renderInput(0)}


                <Modal onRequestClose={() => this.onModalClose()} 
                    transparent={true} visible={modalVisible} 
                    style={StyleSheet.Modal}>
                    {Platform.OS === 'ios' ? (<View style={{height:20,backgroundColor:"#f8f8f8f8"}}></View>):(null)}
                    <View style={{flexDirection:'row',height:50,backgroundColor:"#f8f8f8f8",alignItems:'center',justifyContent:'center'}}>
                    <TouchableOpacity style={{width:50,alignItems:'center',justifyContent:'center'}} onPress={()=>this.onModalClose()}>
                        {/* <Icon name="back" color={colors.navBarButtonColor} size={20} /> */}
                        <Text style={[{fontFamily:'icomoon',fontSize:20,color:colors.navBarButtonColor}]}>_</Text>
                    </TouchableOpacity>
                    <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
                        <Text style={{color:colors.navBarButtonColor,fontSize:22,fontWeight:'600'}}>{'Localisation'}</Text>
                    </View>
                    <View style={{width:50,alignItems:'center',justifyContent:'center'}}>

                    </View>
                </View>
                <View style={{ flex: 1,backgroundColor:"#f8f8f8" ,padding:7,position:'relative'}}>
                        {this.renderInput(1)}
                        {
                            showMap ? <View style={{marginTop:10,flex:1}}>
                                <FranceMap  ref={o => { 
                                    if(o && o.getWrappedInstance) {
                                        this.FranceMap = o.getWrappedInstance();
                                    } else {
                                        this.FranceMap = o;
                                    }
                                }} 
                                onChange={()=>{
                                        this.showCurrent(false)
                                }}
                                isEnable={true}
                                clearData = {()=>{
                                    this.clearAdd()
                                }}
                                address = {address}
                            />
                            </View> :(null)
                        }
                        
                </View>
                <View style={styles.bottomArea}>
                    <TouchableOpacity style={[styles.brandColor]} activeOpacity={0.8} onPress={()=>this.setData()}>
                        <Text style={[styles.genButton]}>Lancer la recherchez</Text>
                    </TouchableOpacity>
                </View>
            </Modal>
            </View>
        );
    }
}