import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet, Platform,ScrollView } from 'react-native';
import { CheckBox } from 'react-native-elements';
import _ from 'lodash';
import {colors} from '../../config/styles';
import common from '../../config/genStyle';
import {FORTAB} from '../../config/MQ';

const styles = StyleSheet.create({
  checkBoxCont: Platform.select({
    ios: {
      borderWidth: 0,
      backgroundColor: '#0000',
      padding: 0,
      marginLeft: 0,
      marginRight: 0,
    },
    android: {
      backgroundColor: '#fff',
      borderWidth: 0,
      marginLeft: 0,
      marginRight: 0,
      paddingLeft: 0,
      paddingRight: 0,
      borderRadius: 0,
      height: FORTAB ? 50 : 20,
      margin:0,
    },
  }),
  error: {
    color: '#F00',
  },
  warning: {
    color: '#A80',
  },
  validText: {
    color: '#ff6624',
  },
});

class CRadioGroup extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  renderRadioGroup = () => {
    const {
      rData,
      value,
      isCheckBox,
      isDisable,
      onChange,
      containerStyle,
      textStyle,
    } = this.props;
    // console.log(rData);
    // console.log(_.isArray(rData) && rData.length > 0);
    if (isCheckBox) {
      return (
        <View>
          {_.isArray(rData) && rData.length > 0 ? (
            rData.map((ele) => {
              console.log(ele);
              const cc = _.findIndex((_.isArray(value) ? value : []), o => o.id === ele.id);
              return (
                <CheckBox
                  key={ele.id}
                  title={ele.value}
                  textStyle={[common.textNormal, { fontWeight: 'normal' }, textStyle]}
                  containerStyle={[{
                    backgroundColor: '#fff', borderWidth: 0, marginLeft: 0, marginRight: 0, padding: 0, margin: 0, paddingLeft: 10, marginTop: 10,
                  }, containerStyle]}
                  checkedIcon="check-box"
                  uncheckedIcon="check-box-outline-blank"
                  iconType="MaterialIcons"
                  size={FORTAB ? 24 : 12}
                  activeOpacity={1}
                  onPress={!isDisable ? () => {
                    let vv = [...value];
                    if (!_.isArray(vv)) {
                      vv = [];
                    }
                    // const ii = _.findIndex(vv, o => o.id === ele.id);
                    if (cc > -1) {
                      vv.splice(cc, 1);
                    } else {
                      vv.push(ele);
                    }
                    onChange(vv);
                  } : null}
                  checked={cc > -1}
                  checkedColor={!isDisable ? '#ff6624' : colors.disableGrey}
                />
              );
          })
          ) : (null)}
        </View>
      );
    }
    return (
      <View style={{ flex: 1 }}>
        {_.isArray(rData) && rData.length > 0 ? (
          rData.map(ele => (
            <CheckBox
              key={ele.id}
              title={ele.value}
              textStyle={[{    color: '#FFF',
              fontSize: FORTAB ? 8 : 6,
              backgroundColor: '#0000',}, { fontWeight: 'normal' }, textStyle]}
              checkBoxStyle={{ borderWidth: 1 }}
              iconStyle={{ color: '#000' }}
              containerStyle={[styles.checkBoxCont, containerStyle]}
              checkedIcon="check-circle"
              uncheckedIcon="radio-button-unchecked"
              iconType="MaterialIcons"
              size={FORTAB ? 24 : 16}
              onPress={!isDisable ? () => onChange(ele) : null}
              checked={_.isEqual(value, ele.id)}
              checkedColor={!isDisable ? '#ff6624' : colors.disableGrey}
              activeOpacity={1}
            />
          ))
        ) : (null)}
      </View>
    );
  };

  /**
   * to be wrapped with redux-form Field component
   */
  render() {
    const {
      value,
      label,

      inputStyle,
      datePicker,
      onSubmitEditing,
      onChangeText,
      leftIcon,
      textArea,
      placeholder,

      ...inputProps
    } = this.props;
    // console.log('checkbox called');
    // do not display warning if the field has not been touched or if it's currently being edited
      // const validationStyles = meta.touched && !meta.active
      //   ? meta.valid ? styles.valid : styles.invalid
      //   : null;

    // alert(111);
    if (Platform.OS === 'ios') {
      return (
        <ScrollView  
        style={{height:Platform.OS === 'ios' ? 360 :  300}}
        contentContainerStyle={{flexGrow: 1}}
        // automaticallyAdjustContentInsets={false}
        // showsHorizontalScrollIndicator={false}
        >
            <View style={{flex:1}}>
            {this.renderRadioGroup()}
            </View>
      </ScrollView>
      )
    }
    return (
      <ScrollView  
      style={{height:300}}
      contentContainerStyle={{flexGrow: 1}}
      // automaticallyAdjustContentInsets={false}
      // showsHorizontalScrollIndicator={false}
      >
          <View style={{flex:1}}>
            {this.renderRadioGroup()}
          </View>
      </ScrollView>
    );
  }
}


CRadioGroup.propTypes = {
  // input: PropTypes.shape({
  //   onBlur: PropTypes.func.isRequired,
  //   onChange: PropTypes.func.isRequired,
  //   onFocus: PropTypes.func.isRequired,
  //   value: PropTypes.any.isRequired,
  // }).isRequired,
  // meta: PropTypes.shape({
  //   active: PropTypes.bool.isRequired,
  //   error: PropTypes.string,
  //   invalid: PropTypes.bool.isRequired,
  //   pristine: PropTypes.bool.isRequired,
  //   visited: PropTypes.bool.isRequired,
  // }).isRequired,
  label: PropTypes.string,
  // value: PropTypes.any.isRequired,

  inputStyle: PropTypes.number,
  onFocus: PropTypes.func,
  onBlur: PropTypes.func,
  datePicker: PropTypes.bool,
  onSubmitEditing: PropTypes.func,
  onChangeText: PropTypes.func,
  placeholder: PropTypes.string,
  leftIcon: PropTypes.string,
  textArea: PropTypes.bool,
  containerStyle: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.any,
  ]),
  textStyle: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.any,
  ]),
};

CRadioGroup.defaultProps = {
  inputStyle: null,
  onFocus: null,
  onBlur: null,
  datePicker: false,
  onSubmitEditing: null,
  onChangeText: null,
  placeholder: 'Default Placeholder',
  leftIcon: null,
  textArea: false,
  value: '',
  containerStyle: null,
  textStyle: null,
};

export default CRadioGroup;
