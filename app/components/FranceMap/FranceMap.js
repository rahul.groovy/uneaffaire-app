import React, {Component} from 'react';
import {StyleSheet, Image, Text, View, Dimensions, WebView,Animated,Platform} from 'react-native';
//import styles from '../../config/genStyle';
import RadioButton from '../../lib/react-radio/index';
import ApiHelper from '../../lib/ApiHelper/ApiHelper';
// import CheckBox from 'react-native-check-box';
import CheckBox from '../../lib/react-native-check-box';
//import ModalCat from '../../components/ModalCat/ModalCat';
import {FORTAB,TABLANDSCAPE,TABPORTRAIT} from '../../config/MQ';
import {settings} from '../../config/settings';
import genStyles from '../../config/genStyle';
import CRadioGroup from '../CRadioGroup/CRadioGroup';
const {height,width}= Dimensions.get('window');
const api= new ApiHelper;
export default class FranceMap extends Component {

    constructor(props) {
        super(props);
         this.state={
            regionName : '',
            france : false,
            regionNameDisp : '',
            departmentList: [],
            selectedDept:'',
            selectedDeptDisp:'',
        
        };
    }

check(ab){
    if(ab){
        this.setState({france:true,regionName:''});
    }
}
onMapPress(navState){
    //console.log(navState.url);
    if(navState.url.search(settings.mapUrl2) > -1){
       
        let state=navState.url.replace(settings.mapUrl2,"")
        if(state!=""){
            state=state.replace("#","");
            if(state != this.state.regionName){
                if(this.props.clearData !== undefined && this.props.clearData !== null) {
                    this.props.clearData();
                }
                this.setState({regionName:state,france:false,selectedDept:'',selectedDeptDisp:''},()=>{
                    this.getData(state);  
                });
            }
            if(this.props.onChange){
                this.props.onChange();
            }
        }
       
    }
    return true;
}

getData(reg){
     let request={
        url:settings.endpoints.departmentList,
        method:'POST',
        params:{region:reg}
    }
    if(reg !=''){
            api.FetchData(request).then((result)=>{
                if(result.status){
                    let departmentList=[];
                    let regionNameDisp= result.list.length > 0 ? result.list[0].department.nreg16 :'';
                    if( result.list.length > 0){
                        result.list.map((data,i)=>{
                            let dep=data;
                            dep.id=data.department.department_slug;
                            dep.value=data.department.ndpt;
                            departmentList.push(dep);
                        })
                    }
                    this.setState({departmentList:departmentList,regionNameDisp:regionNameDisp});
                }else{
                    this.setState({departmentList:[],regionNameDisp:''});
                }       
            }).catch((error)=>{
                console.log(error);
            });
    }else{
        this.setState({france:true});
    }
    return true;
}

getAddress(){
    console.log(this.props);
    if(this.props.isEnable){
        let obj={
        region:this.state.regionName,
        department:this.state.selectedDept,
        france:this.state.france,
        regionDesp:this.state.regionNameDisp,
        departementDesp:this.state.selectedDeptDisp,
    };
    return obj;
    }else{
        return {};
    }
    
}

setLocationData(data){

    this.setState({regionName:data.region,selectedDept:data.department,france:data.france},()=>{
        setTimeout(() => {
            if(this.WebView !== undefined && this.WebView !== null){
                if(!data.france) {
                    this.WebView.reload();
                    this.getData(data.region);
                }
            } 
        }, 500);
    });
}

getRegions(data){
    let ret = [];
    data.map((d,i)=>{
        ret.push({'id':d.id,'value':d.value});
    });
    return ret;
}
selectRegion(item){
    console.log(item);
    //  let di='';
    //  let di1='';
    // this.state.departmentList.map((d,i)=>{
    //     if(d.value === item){
    //         di=d.value;
    //         di1=d.value;
    //     }
    // });
    // console.log(this.state.departmentList);
    // let di=typeof this.state.departmentList[item].id != 'undefined' ? this.state.departmentList[item].id :'';
    // let di1=typeof this.state.departmentList[item].value != 'undefined' ? this.state.departmentList[item].value :'';
    // console.log(di);
    this.setState({selectedDept:item.id,selectedDeptDisp:item.value},()=>{
        if(this.props.onChange){this.props.onChange();}
    });
}

clearR = () => {
    this.setState({regionName:'',selectedDept:'',departmentList:[],regionNameDisp:''});
}

clear(){
    if(this.WebView !== undefined && this.WebView !== null){
        this.WebView.reload();
    }
    if(this.ModalDropdown !== undefined && this.ModalDropdown !== null){
        this.ModalDropdown.clear();
    }
}

render() {
    const { onChange , address}=this.props;
    const { selectedDept,regionName,france, regionNameDisp, departmentList } = this.state;
    // let SRC=regionName != '' ? 'https://boumelita.eu/uneaffairecom/map_iframe_app_small.html#'+regionName:'https://boumelita.eu/uneaffairecom/map_iframe_app_small.html';
    
    let dpt = selectedDept != "" ? selectedDept : 'departement';
    const SRC = regionName !='' && regionName !== undefined ? settings.mapUrl2+'#'+regionName : settings.mapUrl2;
     console.log(SRC);
    return (
       <View style={styles.container}>
           <View style={styles.mapView}>
                <WebView ref={o=>this.WebView=o} onNavigationStateChange={this.onMapPress.bind(this)} 
                source={{uri:SRC}}
                //style={styles.advmapView}
                style={styles.webMap} />
           </View>
           <View style={[styles.depView,{backgroundColor:'#0000'}]}>
           
                <View style={{backgroundColor:"#0000"}}>
                     <CheckBox
                        style={[styles.genChk]}
                        onClick={(checked) => {
                            if(checked){
                                this.setState({france:checked,regionName:'',selectedDept:'',departmentList:[],regionNameDisp:''},()=>{
                                    if(onChange){ onChange();}
                                });
                            } else {
                                this.setState({france:checked,selectedDept:'',departmentList:[],regionNameDisp:''},()=>{
                                    if(onChange){ onChange();}
                                });
                            }                            
                        }}
                        //labelStyle={[styles.blockTitle, styles.orangeColor]}
                        isChecked={france}
                        rightText={'TOUTE LA FRANCE'} />
                </View>
                {!france ? (
                <View style={{backgroundColor:"#0000"}}>
                <View style={styles.inlineDatarow}>
                    <Text style={[styles.globalIcon]}>6</Text>
                    <Text style={{fontSize:14}}>Region</Text>
                </View>
                <View style={styles.inlineDatarow}>
                    {regionNameDisp != '' ? <RadioButton currentValue={regionNameDisp} 
                    value={selectedDept != '' ? '' : regionNameDisp}
                    //Text={this.state.regionName} 
                    onPress={(val)=>{
                        this.setState({regionName:val,selectedDept:''},()=>this.clear())}}>
                         <Text numberOfLines={1} style={styles.readiText}>{regionNameDisp}</Text> 
                    </RadioButton> : null }
                   
                </View>

                {/* <View style={styles.inlineDatarow}>
                    <Text style={styles.globalIcon}>6</Text>
                    <Text style={{fontSize:14}}>Departments</Text>
                </View> */}
                
                <View style={{position:'relative'}}>
                            {/* <Select containerStyle={{ backgroundColor: "#fff"}}
                                        optionsList={this.state.departmentList}
                                        header={'Sélectionnez le département'}
                                        selectedid={this.state.selectedDept}
                                        onPress={(item) => {
                                            this.setState({selectedDept:item});
                                            if(onChange){onChange();}
                                        //console.log(item);
                                    }}/> */}
                                    <CRadioGroup
                                        rData={this.getRegions(departmentList)}
                                        isChecked={false}
                                        onChange={val =>  this.selectRegion(val)}
                                        value={dpt}
                                        containerStyle={{ backgroundColor: '#0000', paddingLeft: 0 }}
                                        textStyle={[genStyles._F14_,genStyles.grayColor]}
                                        // dropdownStyle={{
                                        //     //bottom:0,
                                        //     height:height,
                                        // }} 
                                    />
                                    {/* <ModalDropdown ref={o=>this.ModalDropdown =o} defaultValue={ dpt } options={this.getRegions(departmentList)}
                                        textStyle={[genStyles.dropdown_2_text,genStyles._F16_,genStyles.grayColor]} 
                                        dropdownStyle={{
                                            //bottom:0,
                                            top:-50,
                                            height:220,
                                        }} adjustFrame={(st)=>{
                                           return {
                                               top:StyleSheet.flatten(st).top-100,
                                               height:250,
                                               right:15
                                            };
                                        }}
                                        onSelect={(region)=>{
                                            //console.log(region);
                                            this.selectRegion(region)
                                        }}/> */}
                                    {/* <Text style={[genStyles.globalIcon,genStyles.dropabsIco]}>g</Text> */}
                        </View>
                </View>):(null)}

                {/* {!this.state.france ? (
                <View style={styles.inlineDatarow}>
                    <Text style={styles.globalIcon}>6</Text>
                    <Text style={{fontSize:14}}>Departments</Text>
                </View>):(null)} */}

            </View>
       </View>     
    );
}


}

const styles = StyleSheet.create({
    container: {
        flexDirection:'row',
        marginLeft:-7,
        // height: 300,
        flex: 1,
    },
    mapView:{

    },
    depView:{
        flex:1,
        flexDirection:'column'
    },
    webMap:{
        height:210,
        width:200,
    },
    genChk:{
        paddingVertical:0
    },
    globalIcon:{
        fontFamily:'icomoon',
        fontSize:12,
        justifyContent:'center',
        paddingRight:10,
        marginLeft:5,
    },
    readiText:{
        fontSize:FORTAB ? 14:12,
        marginLeft:8, 
        marginTop:-2,
        alignSelf:'center',
        marginRight:5,
        paddingRight:10
    },
    inlineDatarow:{
      flexDirection:'row',
      alignItems:'center',
      alignSelf:'stretch',
      overflow:'visible',
      marginTop:15
    }

});
//{!this.state.france && ? (
                
                //     this.state.departmentList.length > 0 ? 
                //     (<View style={{marginTop:10,marginLeft:5}}>
                //     {this.state.departmentList.map((data,i)=>{
                //         return (
                //             <View style={{marginTop:10}}>
                //                 <RadioButton
                //                     currentValue={this.state.value2}
                //                     value={0}
                //                     onPress={(val)=>{
                //                     console.log(val);
                //                 }}>
                //                     <Text style={[styles.readiText, styles._PR10_]}>Departments1</Text>
                //                 </RadioButton>
                //             </View>
                //         )
                //     })}
                // </View>):(null)}

 {/* <Select containerStyle={{ backgroundColor: "#fff"}}
                                        optionsList={this.state.departmentList}
                                        header={'Sélectionnez le département'}
                                        selectedid={this.state.selectedDept}
                                        onPress={(item) => {
                                            this.setState({selectedDept:item});
                                        //console.log(item);
                                    }}/> */}


{/* <View style={styles.container}>
                <View style={styles.accountdataBlock}>
                    <View style={[styles.mainMap]}>
                        <Row size={12}>
                            <Col sm={7} md={5} lg={3}>
                                <View style={[styles._PB20_, styles.justCenter, styles.advmapHolder]}>
                                    <WebView
                                        onNavigationStateChange={this.onMapPress.bind(this)}
                                        source={{
                                        uri: 'https://boumelita.eu/uneaffairecom/map_iframe_app_small.html'
                                    }}
                                        //style={styles.advmapView}
                                        style={{
                                            //height:300,width:300
                                            height:305,
                                            width:250
                                        }}
                                        />
                                </View>
                            </Col>
                            <Col sm={5} md={5} lg={3}>
                                <View>
                                    <CheckBox
                                        style={[styles.genChk]}
                                        onClick={(checked) => {
                                        console.log(checked);
                                        // 
                                    }}
                                        labelStyle={[styles.blockTitle, styles.orangeColor]}
                                        isChecked={this.state.check1}
                                        rightText={'TOUTE LA FRANCE'}/>
                                    <View style={[styles.inlineDatarow, styles._MT15_]}>
                                        <Text style={[styles.globalIcon, styles._PR10_, styles._ML5_]}>6</Text>
                                        <Text style={styles._F14_}>Region</Text>
                                    </View>
                                    <View style={[styles._MT10_,, styles._ML5_]}>
                                        <RadioButton
                                            currentValue={this.state.value3}
                                            value3={1}
                                            onPress={(val)=>{
                                                console.log(val);
                                            }}>
                                            <Text style={[styles.readiText, styles._PR10_]}>Aquitaine-Limousi.....</Text>
                                        </RadioButton>
                                    </View>
                                    <View style={[styles.inlineDatarow, styles._MT15_]}>
                                        <Text style={[styles.globalIcon, styles._PR10_, styles._ML5_]}>6</Text>
                                        <Text style={styles._F14_}>Departments</Text>
                                    </View>
                                    <View style={[styles._MT10_,, styles._ML5_]}>
                                        <View style={styles._MT10_}>
                                            <RadioButton
                                                currentValue={this.state.value2}
                                                value={0}
                                                onPress={(val)=>{
                                                console.log(val);
                                            }}>
                                                <Text style={[styles.readiText, styles._PR10_]}>Departments1</Text>
                                            </RadioButton>
                                        </View>
                                        <View style={styles._MT10_}>
                                            <RadioButton
                                                currentValue={this.state.value2}
                                                value={1}
                                                onPress={(val)=>{
                                                console.log(val);
                                            }}>
                                                <Text style={[styles.readiText, styles._PR10_]}>Departments2</Text>
                                            </RadioButton>
                                        </View>
                                    </View>
                                </View>
                            </Col>
                        </Row>
                    </View>
                </View>
            </View> */}