import React, { Component } from 'react';
import {StyleSheet,Image,Text,View,TextInput,TouchableOpacity,ScrollView,Dimensions,Platform,Animated,WebView,
ListView,AsyncStorage,Keyboard,Linking,PermissionsAndroid,Modal,ActivityIndicator,Alert} from 'react-native';
import { colors } from '../../config/styles';
import images from '../../config/images';
import styles from '../../config/genStyle';
import ModalDropdown from '../../lib/react-native-modal-dropdown';
import {Column as Col, Row} from 'react-native-flexbox-grid';
import FloatLabelTextInput from '../../lib/react-native-floating-label-text-input';
import {Navigation} from 'react-native-navigation';
import { iconsMap, iconsLoaded,Icon } from '../../config/icons';
import startMain from '../../config/app-main';
import {settings} from '../../config/settings';
import ApiHelper from '../../lib/ApiHelper/ApiHelper';
import {connectD} from '../../config/socket';
import tryGetLocation from '../../redux/utils/location';
import FCM, {FCMEvent} from 'react-native-fcm';
import PayPalPayment from '../../redux/utils/paypal';
import FaIcon from 'react-native-vector-icons/MaterialIcons';
// import {LoginManager} from 'react-native-fbsdk';
// import {GoogleSignin} from 'react-native-google-signin';
import moment from 'moment';
import oauthconfig from '../../config/oauthconfig';
//import PayPal from 'react-native-paypal-wrapper';
var _ = require('lodash');
// redux specific
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as chatActions from '../../redux/reducers/chat';
import * as authActions from '../../redux/reducers/auth';


const HEADER_MAX_HEIGHT = 100;
const HEADER_MIN_HEIGHT = Platform.OS === 'ios' ? 50 : 50;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;
const { height, width } = Dimensions.get('window');
const FORTAB = width < 1025 && width > 721;
const TABLANDSCAPE = width < 1025 && width > 768;
const TABPORTRAIT = width > 721 && width < 769;
const TABPORTTOMOB = width < 769;
const api= new ApiHelper;

class HomeContainer extends Component {
//   static navigatorButtons = {
//       leftButtons: [{
//        icon: images.menuOptions,
//        id: 'menu',
//        title:'Menu',
//      }],
//   };

 static navigatorStyle = Platform.OS === 'ios' ? {
     navBarBackgroundColor: colors.navBarBackgroundColor,
     navBarTextColor: colors.navBarTextColor,
     navBarSubtitleTextColor: colors.navBarSubtitleTextColor,
     navBarButtonColor: colors.navBarButtonColor,
     statusBarTextColorScheme: colors.statusBarTextColorScheme,
     statusBarColor: colors.statusBarColor,
     tabBarBackgroundColor: colors.tabBarBackgroundColor,
     tabBarButtonColor: colors.tabBarButtonColor,
     tabBarSelectedButtonColor: colors.tabBarSelectedButtonColor,
     navBarSubtitleColor: colors.navBarSubtitleColor,
     navBarTextFontFamily: colors.navBarTextFontFamily,
    //  navBarHidden: false,
     //  tabBarHidden: true,
     disableOpenGesture: true,
     drawUnderTabBar: true,
 }:{
    navBarBackgroundColor: colors.navBarBackgroundColor,
    navBarTextColor: colors.navBarTextColor,
    navBarSubtitleTextColor: colors.navBarSubtitleTextColor,
    navBarButtonColor: colors.navBarButtonColor,
    statusBarTextColorScheme: colors.statusBarTextColorScheme,
    statusBarColor: colors.statusBarColor,
    tabBarBackgroundColor: colors.tabBarBackgroundColor,
    tabBarButtonColor: colors.tabBarButtonColor,
    tabBarSelectedButtonColor: colors.tabBarSelectedButtonColor,
    navBarSubtitleColor: colors.navBarSubtitleColor,
    navBarTextFontFamily: colors.navBarTextFontFamily,
   //  navBarHidden: false,
    tabBarHidden: true,
    disableOpenGesture: true,
    drawUnderTabBar: true,
 };

constructor(props) {
    super(props);
    this.gotActiveMap = false;
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.regionOption = {'Grant-Est':'grant-est','Nouvelle-Aquitaine':'nouvelle-aquitaine','Auvergne-Rhône-Alpes':'auvergne-rhone-alpes','Bourgogne-Franche-Comté':'bourgogne-franche-comte','Bretagne':'bretagne','Centre-Val-de-Loire':'centre','Corse':'corse','Île-de-France':'ile-de-france','Occitanie':'occitanie','Normandie':'normandie','Hauts-de-France':'hauts-de-france','Pays-de-la-Loire':'pays-de-la-loire',"Provence-Alpes-Côte-d'Azur":'provence-alpes-cote-dazur','Guadeloupe':'guadeloupe','Martinique':'martinique','Guyane-française':'guyane-francaise','La-Réunion':'la-reunion','Mayotte':'mayotte',"collectivité_d'Outre_mer":"Collectivité d'Outre-Mer"};
    this.state={
        gettingDirection:false,
        check1:false,
        tabs:1,
        tabSelected:1,
        like:1,
        scrolled:true,
        scrollY: new Animated.Value(0),
        Stat:'',
        accessLocation:false,
        SectionList:[],
        currentLocationData:{},
        dataSource: ds.cloneWithRows(['Grant-Est', 'Nouvelle-Aquitaine','Auvergne-Rhône-Alpes','Bourgogne-Franche-Comté','Bretagne','Centre-Val-de-Loire','Corse','Île-de-France','Occitanie','Normandie','Hauts-de-France','Pays-de-la-Loire','Provence-Alpes-Côte-d-Azur','Guadeloupe','Martinique','Guyane-française','La-Réunion','Mayotte','Collectivité d-Outre-Mer']),
        current:false
    };
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
}

componentWillReceiveProps(nextProps){
    //console.log(this.props);
    // console.log(nextProps);
    if(this.props.chat.data !== nextProps.chat.data){
        //if(api.checkLogin()){
            // if(nextProps.chat.data.recent !== undefined && nextProps.chat.data.recent !==null && this.props.chat.data.recent === undefined){
            //     this.props.chatActions.setConv(nextProps.chat.data.recent);
            // }
            this.props.navigator.setTabBadge({
                tabIndex: 1, // (optional) if missing, the badge will be added to this screen's tab
                badge: nextProps.chat.unread > 0 ? nextProps.chat.unread : null // badge value, null to remove badge
            });
            // else if(this.props.chat.data.badge != nextProps.chat.data.badge){
            //     this.displayNotification();
            // }
        //}
        
    }else if(this.props.chat.unread !== nextProps.chat.unread){ 
        // if( this.props.chat.screen != 'Chat' && this.props.chat.screen != 'Singlechat'){
        //     this.displayNotification();
        // }
        if(this.props.chat.screen != 'Singlechat'){
            this.props.navigator.setTabBadge({
                tabIndex: 1, // (optional) if missing, the badge will be added to this screen's tab
                badge: nextProps.chat.unread > 0 ? nextProps.chat.unread : null // badge value, null to remove badge
            });
        }  
    }

if(!(_.isEqual(this.props.chat.notification,nextProps.chat.notification))){
    if( this.props.chat.screen != 'Chat' && this.props.chat.screen != 'Singlechat'){
        this.displayNotification();
    }
}

if(this.props.auth.cart !== nextProps.auth.cart){
    this.props.navigator.setTabBadge({
        tabIndex: 3, // (optional) if missing, the badge will be added to this screen's tab
        badge: nextProps.auth.cart > 0 ? nextProps.auth.cart : null // badge value, null to remove badge
    });
}

if(nextProps.auth.userdata !== null && Object.keys(nextProps.auth.userdata).length > 0 && !(_.isEqual(this.props.auth.userdata,nextProps.auth.userdata))){
    this.setDataInAsync(nextProps.auth.userdata);
}

//console.log(nextProps.auth.isNetOnOff);

if(this.props.auth.isNetOnOff !== nextProps.auth.isNetOnOff && nextProps.auth.isNetOnOff === true){
    //console.log('Shwoing is net off ----- compare');
    this.displayNoInternet();
}
    //console.log(this.props.chat.unread);
    //console.log(nextProps.chat.unread);
    // if(this.props.chat.data.badge !== nextProps.chat.data.badge){
    //     console.log(nextProps.chat.data);
    //     this.displayNotification();
    // }
    // if(this.props.chat.conv !== nextProps.chat.conv){
    //     console.log('on new message');
    //         //this.displayNotification(nextProps.chat.conv);
    // }
}

setDataInAsync(object){
    if(object !== undefined && object !== null && Object.keys(object).length > 0 && this.props.auth.token !== undefined && this.props.auth.token !== null && this.props.auth.token !== ''){
        AsyncStorage.multiSet([['token',object.token],['userdata',JSON.stringify(object)]]);
    }
}

displayNotification(){
    this.props.navigator.showInAppNotification({
        screen: "ChatSticky", // unique ID registered with Navigation.registerScreen
        passProps: {
            active:true
        }, // simple serializable object that will pass as props to the in-app notification (optional)
    });
}
dismiss(){   
    api.checkNet().then(()=>{
        this.props.authActions.setNet(false);
        this.props.authActions.setNetDial(false);
        this.props.navigator.dismissLightBox();
    }).catch((err)=>{
        //console.log('Error while dismissing modal');
        //console.log(err);
        this.props.authActions.setNet(true);
        this.props.authActions.setNetDial(false);
        // this.displayNoInternet();       
    });
}

displayNoInternet(){
    this.props.authActions.setNetDial(true);
    //console.log('Show lightbox called');
    //console.log(this.props.navigator);
    this.props.navigator.showLightBox({
        screen: "netOnOffModal", // unique ID registered with Navigation.registerScreen
        passProps: {
            dismiss:this.dismiss.bind(this)
        }, // simple serializable object that will pass as props to the lightbox (optional)
        style: {
          backgroundBlur: "dark", // 'dark' / 'light' / 'xlight' / 'none' - the type of blur on the background
          backgroundColor: "#0008" // tint color for the background, you can specify alpha here (optional)
        },
        //adjustSoftInput: "resize", // android only, adjust soft input, modes: 'nothing', 'pan', 'resize', 'unspecified' (optional, default 'unspecified')
       });
}

registerForPush(uuid,bool){
    let params={
        token:this.props.token,
        user_id:this.props.userdata.id, 
        uu_id:uuid, 
        platform:Platform.OS
    }
    if(bool){
        params.delete=true;
    }
    let request={
        url:settings.endpoints.pushRegister,
        method:'POST',
        params:params
    } 
    api.FetchData(request).then((result)=>{

    }).catch((err)=>{

    });
}

getFormatDate(str){
    var d= new Date(str);
    //console.log(moment(str,'ddd MMM dd gggg HH:mm:ss Z').toISOString());
    return moment(d).toISOString();
}

fcmConfig(){
    if(Platform.OS === 'ios'){
        FCM.requestPermissions(); // for iOS
    }
    if(this.props.auth.fcmToken == null) {
        
        FCM.getFCMToken().then(token => {
            //console.log(token);
            this.registerForPush(token,false);
            this.props.authActions.setFcmToken(token);
        // store fcm token in your server
        });
    }

// this.refreshListener = FCM.on('refreshToken', (token) => {
//     console.log('refreshToken');
//     if(this.props.auth.fcmToken === undefined || this.props.auth.fcmToken === null || this.props.auth.fcmToken === '') {
//         console.log('token => '+ token);
//     }
//   })


this.notificationListener = FCM.on(FCMEvent.Notification, async (notif) => {
    console.log(notif);
    // there are two parts of notif. notif.notification contains the notification payload, notif.data contains data payload
    if(notif.local_notification){
        //this is a local notification
        console.log('local notification');
        console.log(notif);
    }
    if(notif.opened_from_tray){
        //app is open/resumed because user clicked banner
        console.log('app is open/resumed because user clicked banner');
        //console.log(notif);
        if(notif !== undefined && notif !== null){
        }
    }
});
FCM.getInitialNotification().then((notif)=>{
    console.log(notif);
    if(notif !== undefined && notif !== null){

        /* 
        notif format
        notif={
            ad:"59aa4f7dadf6e33427ce9103",
            adLink:"/annonces/details/sdgvjkfdpokgmdf-b4e5044f416cc3",
            adName:"sdgvjkfdpokgmdf",
            animationType:"none",
            avatar:"http://192.168.0.180:1337/images/users-images/T-avatar.png",
            collapse_key:"com.uneaffaire",
            createdAt:"Thu Sep 14 2017 15:53:25 GMT+0530 (India Standard Time)",
            fcm:{action:"android.intent.action.VIEW"},
            from:"920587787124",
            from_id:"59293b091cef426411205832",
            fullname:"Nauman Pathan",
            "google.message_id":"0:1505384608654540%e1353cdde1353cdd",
            "google.sent_time":1505384608629,
            id:"59ba589d07347d30085b3394",
            is_read:"0",
            message:"sadfgvb",
            msg_count:"1",
            opened_from_tray:1,
            to_id:"593bd9cb55bf17ac081ad48c",
            updatedAt:"Thu Sep 14 2017 15:53:25 GMT+0530 (India Standard Time)",
        }
        */
        let flag = false;
        if(Platform.OS === 'ios'){
            flag = notif.aps.alert && notif.aps.category !== null && notif.ad !== undefined;
        }else{
            flag = notif.opened_from_tray && notif.fcm.action !== null && notif.ad !== undefined;
        }
        if(flag){
            let msg={
                ad:notif.ad,
                adLink:notif.adLink,
                avatar:notif.avatar,
                createdAt:this.getFormatDate(notif.createdAt),
                from_id:notif.from_id,
                fullname:notif.fullname,
                id:notif.id,
                is_read:parseInt(notif.is_read),
                message:notif.message,
                msg_count:parseInt(notif.msg_count),
                to_id:notif.to_id,
                updatedAt:this.getFormatDate(notif.updatedAt),
            }
            // 2017-09-14T10:47:44.408Z
            // Thu Sep 14 2017 16:10:24 GMT+0530 (IST)
            /* 
            var str="Thu Sep 14 2017 16:10:24 GMT+0530 (IST)"
            console.log(moment(str).toISOString());
            */ 
            console.log(msg);
            setTimeout(() => {
                this.openNotif(msg);
            }, 1500);
        }
    }
});
}

componentWillMount(){
    iconsLoaded.then(()=>{
        if(api.checkLogin(this.props.token)){
            this.fcmConfig();
        } 
    });
}

openNotif(data){
    data.typing=false;
    this.props.chatActions.onNewMessage(data);
    let newData = {};
    newData.send_from_id = this.props.auth.userdata.id == data.from_id ? data.to_id : data.from_id;
    newData.ad = data.ad;
    newData.token = this.props.auth.token;
    newData.name =  data.fullname;
    this.props.chatActions.openChat(data);
    console.log(newData);
    // alert(JSON.stringify(newData))
    // console.log('going to chat');
    this.props.navigator.push({
        screen: 'Singlechat',
        passProps:{
            data:newData
        }
    });
}

sendEventToDrawer(){
    this.props.navigator.handleDeepLink({link: 'Drawer',payload:{
        screen:this.props.testID,
        login:true,
        token:this.props.token,
        userdata:this.props.userdata,
    }});
}


onNavigatorEvent(event) {
    if(event.id=='willAppear'){
      this.sendEventToDrawer();
      //this.displayNoInternet();
      api.setAction(this.props.chatActions,this.props.authActions);
      this.props.chatActions.setScreen(this.props.testID);
      if(this.props.auth.token === '' && Object.keys(this.props.auth.userdata).length === 0 && api.checkLogin(this.props.token)){
        this.props.authActions.setUserData(this.props.token,this.props.userdata);
      }

      
     console.log(this.props);
      if(this.props.chat.socket == null && api.checkLogin(this.props.token)){
        connectD(this.props.token, this.props.chatActions,this.props.authActions);
      }
      this.props.navigator.toggleTabs({
            to: 'hidden',
            animated: false,
        });
        this.props.navigator.toggleNavBar({
            to: 'shown',
            animated: false
        });
        this.props.navigator.setStyle({navBarTitleTextCentered: true});
        this.props.navigator.setTitle({
            title: "Accueil",
            id:"Home"
        });
        this.props.navigator.setButtons({
            leftButtons: [{
                icon: iconsMap['menu'],
                id: 'menu',
                title:'Menu',
            }],
            animated:false        
        })
    }
    if(event.id=='didAppear'){
        //this.socketFunctions();
    }
    
    if(event.id == 'back'){
      this.props.navigator.pop({
        animated: true // does the pop have transition animation or does it happen immediately (optional)
      });
    }
    if(event.id == 'menu'){
        this.props.navigator.toggleDrawer({
            side: 'left', // the side of the drawer since you can have two, 'left' / 'right'
            animated: true, // does the toggle have transition animation or does it happen immediately (optional)
        });
    }
    if (event.type == 'DeepLink') {
        //this.handleDeepLinkOld(event);
        this.handleDeepLinkNew(event);
    }
}


handleDeepLinkPage(event){
    const parts = event.link;
    let checkPop = '';
    if(_.isObject(event) && !_.isEmpty(event)) {
        if(_.isObject(event.payload) && !_.isEmpty(event.payload)) {
            checkPop = event.payload.popBack;
        }
    }
    if(parts == 'favoris' && !api.checkLogin(this.props.token)){
        startMain('',{});
    }
    else if(parts == 'WelcomeContainer'){
        this.logOut();
    }else if(parts == 'HomeContainer'){
        this.props.navigator.resetTo({
            screen: 'HomeContainer',
            overrideBackPress: true,
                passProps:{
                    token:this.props.token,
                    userdata:this.props.userdata,
                }
        });
    }else if(parts == 'Singlechat' && event.payload !== undefined){
        this.handlePostAd(parts,event.payload);
    }else{
        this.props.navigator.push({
            screen: parts,
            passProps:{
                token:this.props.token,
                userdata:this.props.userdata,
                checkPop
            }
        });
    }
}

handleDeepLinkTab(event){
    switch(event.link){
        case 'ProductgridContainer':
        this.props.navigator.push({
            screen:'ProductgridContainer',
            passProps:{
                token:this.props.token,
                userdata:this.props.userdata,
            }
        });
        break;
        
        case 'Chat':
        if(!api.checkLogin(this.props.token)){
            startMain('',{});
        }else{
            this.props.navigator.switchToTab({
                tabIndex:1
            })
        }
        break;
        
        case 'postad':
        if(!api.checkLogin(this.props.token)){
            startMain('',{});
        }else{
            this.props.navigator.switchToTab({
                tabIndex:2
            })
        }
        break;
        
        case 'favoris':
        if(!api.checkLogin(this.props.token)){
            startMain('',{});
        }else{
            this.props.navigator.switchToTab({
                tabIndex:3
            })
        }
        break;
        
        case 'myAccount':
        if(!api.checkLogin(this.props.token)){
            startMain('',{});
        }else{
            this.props.navigator.switchToTab({
                tabIndex:4
            })
        }
        break;
    }
}

handleDeepLinkNew(event){
    const parts = event.link;
    if(typeof parts != 'undefined' && parts != 'Drawer'){
        if(parts == 'ProductgridContainer' || parts == 'Chat' || parts == 'postad' || parts == 'favoris' || parts == 'myAccount'){
            this.handleDeepLinkTab(event);
        }else{
            this.handleDeepLinkPage(event);
        }
    }
}

disConnectSocket(){
    //console.log(this.props.chat.socket);
    if(this.props.chat.socket !== undefined && this.props.chat.socket !== null){
        //console.log(this.props.chat.socket);
        //console.log('socket disconnected');
    this.props.chat.socket.disconnect();
    }
}

logOut(){
        // LoginManager.logOut();
        // GoogleSignin.signOut();
    this.registerForPush(this.props.auth.fcmToken,true);
    this.disConnectSocket();
    AsyncStorage.multiRemove(['token','userdata'],function(err){
    }).then(res=>{
        this.props.authActions.setFcmToken(null);
        this.props.chatActions.setSocket(null);
        startMain('',{});
    });
}

handleDeepLinkOld(event){
    const parts = event.link;
    
    if (typeof parts != 'undefined' && parts != 'Drawer') {
        if((parts == 'myAccount' || parts == 'favoris' || parts == 'Chat' || parts == 'postad' || parts == 'Cart') && !api.checkLogin(this.props.token)){
            startMain('',{});
        }else if(parts=='WelcomeContainer'){
                this.logOut();
        }else if(parts == 'HomeContainer'){
            //startMain()
            this.props.navigator.resetTo({
                screen: parts,
                overrideBackPress: true,
                passProps:{
                    token:this.props.token,
                    userdata:this.props.userdata,
                }
            });
        }else if(parts == 'Singlechat' && event.payload !== undefined){
            this.handlePostAd(parts,event.payload);
        }
        else{
            if(parts == 'postad' || parts == 'Chat'){
                this.handlePostAd(parts,null);
            }else if(parts == 'myAccount' && event.payload !== undefined && event.payload.openProfile == 2){
                this.gotoMyProfile();
            }else{
                this.props.navigator.push({
                    screen: parts,
                    passProps:{
                        token:this.props.token,
                        userdata:this.props.userdata,
                    }
                });
            }
        }
    }
}

gotoMyProfile(){
    this.props.navigator.switchToTab({
        tabIndex:4
    })
    // this.props.navigator.push({
    //     screen: 'myAccount',
    //         passProps:{
    //             token:this.props.token,
    //             userdata:this.props.userdata,
    //             openProfile : 2
    //         },
    // });
}

handlePostAd(page,data){
    if(!api.checkLogin(this.props.token)){
        startMain('',{});
    }else if(this.props.userdata !== undefined && this.props.userdata !== null && this.props.userdata.profile_complete == 0){
        this.gotoMyProfile();
    }else if(page == 'Singlechat' && data !== undefined && data !== null){
        let newData = {};
        newData.send_from_id = this.props.auth.userdata.id == data.from_id ? data.to_id : data.from_id;
        newData.ad = data.ad;
        newData.token = this.props.auth.token;
        newData.name =  data.fullname;
        this.props.chatActions.openChat(data);
        this.props.navigator.push({
            screen: 'Singlechat',
            passProps:{
                data:newData
            }
        });  
    }else{
        //console.log('switch to tab '+page);
        let tabi = 0;
        if(page == 'Chat'){
            tabi = 1;
        }else if(page == 'postad'){
            tabi = 2;
        }
        this.props.navigator.switchToTab({
            tabIndex:tabi
        })

        // this.props.navigator.push({
        //     screen: page,
        //     overrideBackPress: true,
        //         passProps:{
        //             token:this.props.token,
        //             userdata:this.props.userdata,
        //             region:this.state.Stat
        //         },
        // });    
    }
}


goto(page){
    // this.props.navigator.showInAppNotification({
    //     screen: "ChatSticky", // unique ID registered with Navigation.registerScreen
    //     passProps: {}, // simple serializable object that will pass as props to the in-app notification (optional)
    // });
  
    //let locationn = {"lat":this.props.auth.cLocation.latitude,"long":this.props.auth.cLocation.longitude};
    if(page == 'postad') {
        this.handlePostAd(page,null);
    } else {
        console.log(this.props.auth.cLocation);
        if(this.props.auth.cLocation != null && this.props.auth.cLocation !== undefined && this.state.current){
            if(this.props.auth.cLocation.latitude === 0 || this.props.auth.cLocation.longitude === 0) {
                this.setState({
                    gettingDirection  : false
                },()=>{
                    Alert.alert(
                        'Information !',
                        'Les données de localisation non disponibles',
                        [
                            {text: 'Ok'},
                        ],
                        { cancelable: false }
                    )

                });
            } else {
                this.getDatafromGoogle(this.props.auth.cLocation.latitude+','+this.props.auth.cLocation.longitude);
            }
        }else{
            this.props.navigator.push({
                screen: page,
                    passProps:{
                        token:this.props.token,
                        userdata:this.props.userdata,
                        region:this.state.Stat,
                        fromHome : true,
                    },
            });
        }
    }
}

getDatafromGoogle(qs){  
    console.log('https://maps.googleapis.com/maps/api/geocode/json?latlng='+qs+'&key='+oauthconfig.googleMapKey);

    fetch('https://maps.googleapis.com/maps/api/geocode/json?latlng='+qs+'&key='+oauthconfig.googleMapKey)
    .then((response) => response.json())
    .then((result)=>{
    console.log(result);
    if(result.status === "OK" && result.results !== undefined && result.results.length > 0){
            let addr_c = result.results[0].address_components;
            let zipcode = null;
            let town =  null;
            if(_.isArray(addr_c)){
                addr_c.map((d,i)=>{
                    if(_.isArray(d.types) && d.types.indexOf("locality") > -1 && d.types.indexOf("political") > -1){
                        town = d.long_name || d.short_name;
                    }
                    if(_.isArray(d.types) && d.types.indexOf("postal_code") > -1){
                        zipcode = d.long_name || d.short_name;
                    }
                });
            }
            // console.log(zipcode);
            // console.log(town);
            if(zipcode !== null && town !== null){              
                this.props.navigator.push({
                    screen: 'ProductgridContainer',
                        passProps:{
                            token:this.props.token,
                            userdata:this.props.userdata,
                            region:this.state.Stat,
                            location :  {"lat":this.props.auth.cLocation.latitude,"long":this.props.auth.cLocation.longitude,isCurrent:true,zip:zipcode,town:town,nearByKm:30} 
                        },
                });
            }else{
                this.handleLocationError();
            }
            this.setState({gettingDirection:false});
        }
    }).catch((err)=>{
        console.log(err);
        this.handleLocationError();
    });   
}

handleLocationError(){
    this.setState({gettingDirection:false},()=>alert('a pas pu obtenir d\'emplacement'));
    
}

onScroll(event){
        var currentOffset = event.nativeEvent.contentOffset.y;
        var direction = currentOffset > this.offset ? this.setState({scrolled:true}) : this.setState({scrolled:false});
        this.offset = currentOffset;       
}
    
componentDidMount() {
    if(!this.gotActiveMap && (typeof navigator.activeMaps == 'undefined' || navigator.activeMaps.length == 0)){
        this.getAvailableMaps();
    }   
    this.getCategory();
    if (Platform.OS === 'android') {
        Linking.getInitialURL().then(url => {
            this.navigate(url);
        });
        Linking.addListener('url', (e) => {
            console.log('url add listner');
            console.log(e);
            // CAlert(e.url);
            if (!_.isEmpty(e.url)) {
                this.navigate(e.url);
            }
          });
    } else {
            Linking.addEventListener('url', this.handleOpenURL);
    }
    setTimeout(()=>{
        if(this.props.auth.cLocation == null){
            tryGetLocation(this.props.authActions).then((result)=>{
                console.log(result);
            }).catch((err)=>{
                console.log(err);
                Alert.alert(
                    'Information !',
                    'Les données de localisation non disponibles',
                    [
                        {text: 'Ok', onPress: () => {
                           //this.showCurrent(false);
                        }},
                    ],
                    { cancelable: false }
                )
                //alert('N\'Données de localisation non disponibles')
            });
            //this.props.authActions.getLocation();
        }
        },5000)
}

handleOpenURL = (event) =>{
    this.navigate(event.url);
}

navigate = (url) => {
    //console.log(url);
    if(url !== undefined && url !== null){
        if(url.indexOf('annonces/details/') > -1){
            let slug= url.split('/').pop();
            //console.log(slug);
            this.props.navigator.push({
                screen:'ProductDetail',
                passProps:{
                    data:{ad_slug:slug}
                }
            });
        }
    }
}

getCategory(){
    let request={
            url:settings.endpoints.getAllCategory,
            method:'POST',
            params:{}
        } 
        let catArray = [];  
        //this.setState({load:true});
        api.FetchData(request).then((result)=>{ 
         // console.log(result);
        catArray[0]={'title':'toutes','key':'toutes',data:[
            {
                id:"",
                name:"Toutes catégories",
                price:0,
        }
        ]}  
        // let i= 1;
        //     for(key in  result.category){
        //         catArray.push({'title':key,'key':i});
        //         catArray[i]['data']=[];
        //         for(key1 in result.category[key]){
        //             catArray[i]['data'].push({id:result.category[key][key1]['id'],name:result.category[key][key1]['name'],key:result.category[key][key1]['id'],price:result.category[key][key1]['price']});
        //         }  
        //         i++;          
        //     }
        catArray = catArray.concat(result.category);
            this.props.authActions.setCategory(catArray);
            //this.setState({SectionList:catArray});
            //console.log(catArray);   
        }).catch((error)=>{
            console.log(error);
        });
}

getAvailableMaps(){
    this.gotActiveMap = true;
    navigator.activeMaps = [];
    let links = ['waze://','http://','maps://'];
    let avals = [];
    for(let key in links){
      Linking.canOpenURL(links[key]).then((supported) => {
        if (supported){
          if(navigator.activeMaps.indexOf('links[key]') == -1)navigator.activeMaps.push(links[key]);
        }
      });
    }
  }

componentWillUnmount() {
    if(this.notificationListener !== undefined && this.notificationListener !==null){
        this.notificationListener.remove();
    }
}

onMapPress(navState){
    let state=navState.url.replace(settings.mapUrl, "")
    if(state!="" ){
        state=state.replace("#","");
        console.log(state);
        if(this.state.Stat == state){
            return false;
        }else{
            this.setState({Stat : state,current:false},()=>this.goto('ProductgridContainer')); 
        }    
    }
}
getRegions(){
    let ret = [];
    Object.keys(this.regionOption).map((key)=>{
        ret.push(key);
    });
    return ret;
    //['Liste des régions','Grant-Est', 'Nouvelle-Aquitaine','Auvergne-Rhône-Alpes','Bourgogne-Franche-Comté','Bretagne','Centre-Val-de-Loire','Corse','Île-de-France','Occitanie','Normandie','Hauts-de-France','Pays-de-la-Loire','Provence-Alpes-Côte-d-Azur','Guadeloupe','Martinique','Guyane-française','La-Réunion','Mayotte','Collectivité d-Outre-Mer']
}

gotoNearMe(){
    console.log(this.props.auth.cLocation);
}

searchNearMe(){
    //setTimeout(()=>{
        if(this.props.auth.cLocation == null){
            this.setState({gettingDirection:true,current:true},()=>{
                tryGetLocation(this.props.authActions).then((result)=>{
                    //console.log(this.props.auth.cLocation);
                    this.setState({gettingDirection:false,state:''},()=>{
                        this.goto('ProductgridContainer');                   
                    });
                }).catch((err)=>{
                    console.log(err);
                    this.setState({gettingDirection:false,state:''},()=>{
                        Alert.alert(
                            'Information !',
                            'Les données de localisation non disponibles',
                            [
                                {text: 'Ok', onPress: () => {
                                   //this.showCurrent(false);
                                }},
                            ],
                            { cancelable: false }
                        )
                      //  alert('N\'Données de localisation non disponibles');
                    });
                   
                });
            })
         
          //this.props.authActions.getLocation();
        }else{
            this.setState({
                current:true,gettingDirection:true
            },()=> this.goto('ProductgridContainer'));
           
           // this.gotoNearMe();
        }
      //},5000)
}

_renderGetDirections(){
    return(
        <Modal visible={this.state.gettingDirection} onRequestClose={() => {}}
        transparent={true} style={StyleSheet.Modal} >
    <View style={{flex:1,alignItems:'center',justifyContent:'center',backgroundColor:"#0008"}}>
        <View style={{backgroundColor:"#fff", alignItems:'center',justifyContent:'center',flexDirection:'row',padding:15,borderRadius:10}}>
            <View style={{padding:5}}>
                <ActivityIndicator size="large" animating={true} color={colors.navBarButtonColor}/>
            </View>
            <View style={{padding:5,maxWidth:150}}>
                <Text>{'Recherche ma position actuelle ...'}</Text>
            </View>
        </View>
    </View></Modal>);
}

selectRegion(rr){
    this.setState({Stat:this.regionOption[Object.keys(this.regionOption)[rr]]},()=>this.goto('ProductgridContainer'));
}
render() {
    const headerTranslate =  this.state.scrollY.interpolate({
            inputRange: [0, 50],
            outputRange: [0, -50],
            extrapolate: 'clamp',
    });
    var postAdd = TABLANDSCAPE ?<TouchableOpacity onPress={()=>{this.goto('postad')}} activeOpacity={0.8} style={[styles.postAddButton]}>
        <Text style={[styles.globalIcon,styles.addPost]}>h</Text>
        </TouchableOpacity>:null
    var postAddTabport = TABPORTTOMOB ?<TouchableOpacity onPress={()=>{this.goto('postad')}} activeOpacity={0.8} style={[styles.postAddButton]}>
        <Icon name="edit" color={"#f15a25"} size={18} />
        </TouchableOpacity>:null
    var religion =  width < 991 ? 
                    <View>
                        <View style={[styles.relative,styles.justCenter,styles._w220_,styles.alignSelf,styles._MT5_]}>
                            <ModalDropdown heightData={"home"} defaultValue={'Liste des régions'} options={this.getRegions()} style={[styles.dropdown_2,styles.dropDown,styles.dropDownHome]} textStyle={[styles.dropdown_2_text,styles._F16_,styles.grayColor]} dropdownStyle={styles.dropdown_2_dropdown} onSelect={(region)=>this.selectRegion(region)}/>
                            {/* <Text style={[styles.globalIcon,styles.dropabsIco]}>g</Text> */}
                        </View>
                    </View>:null;
   var religionListing = width < 1025 && width > 991 ? <ListView style={styles.vertList} dataSource={this.state.dataSource} renderRow={(rowData) => <Text style={styles.rowListing}>{rowData}</Text>}  />:null
    return (
        <ScrollView showsVerticalScrollIndicator={false} style={[styles.mainScollview]} scrollEnabled={false}>
           <View style={[styles.scrollInner]}>
                <View style={styles.productdataMain}>
                        <Animated.View style={[styles.searchHolder,
                            {
                                transform: [{ translateY: headerTranslate },
                                {
                                    scale: this.state.scrollY.interpolate({
                                        inputRange: [-50 , 0, 50],
                                        outputRange: [2, 1, 1]
                                    })
                                }]
                            },
                        ]}>
                        <Image source={images.companylogo} style={styles.uneSym}/>
                        <View style={styles.searchinputHolder}>
                            <TextInput onFocus={()=>{ 
                                Keyboard.dismiss();
                                this.goto('FastSearch');
                                }}style={[styles.searchInput,{marginLeft:5}]} placeholder={"Rechercher..."} underlineColorAndroid={'#0000'}/>
                        </View>
                        <TouchableOpacity activeOpacity={0.8} onPress={()=>this.goto('FastSearch')} style={[styles.icoholderRight]}>
                            {/* <Text style={[styles.genIco,styles.searchIco]}>v</Text> */}
                            <Icon name="search" color={colors.navBarButtonColor} size={22} style={{marginRight:10}}/>
                        </TouchableOpacity>
                        <TouchableOpacity activeOpacity={0.8} onPress={()=>{this.goto('AdvanceSearch')}} style={{borderColor:'#2E89ED',borderWidth:1,padding:3,borderRadius:5}}>
                            {/* <Text style={[styles.searchholderIco,styles.genIco,styles.blueColor,styles.largeIco]} >r</Text> */}
                            {/* <Icon name="mixer" color={colors.navBarButtonColor} size={22} style={{marginRight:5}}/> */}
                                <Text color={colors.navBarButtonColor} style={{marginRight:2,fontSize:14,color:'#2E89ED'}}>Filtres</Text>
                        </TouchableOpacity>
                    </Animated.View>
                    {/*<Animated.View style={[styles._PB5_,styles.relative]} showsVerticalScrollIndicator={false} scrollEventThrottle={1} onScroll={
                        Animated.event(
                            [{ nativeEvent: { contentOffset: { y: this.state.scrollY }}}],
                            { useNativeDriver: Platform.OS === 'ios' ? true : false }, 
                        )
                        }>*/}
                        <View style={[styles.inlineDatarow,styles.spaceBitw,{marginTop:10}]}>
                            <TouchableOpacity style={styles.inlineWithico} onPress={()=>this.goto('ProductgridContainer')}>
                                {/* <Image source={images.map} style={[styles.mapSym,styles._MR10_]}/> */}
                                <Icon name="franceMap" color={"#666"} size={25} style={{marginRight:5}}/>
                                <Text>Toute La France</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.inlineWithico} onPress={()=>{this.searchNearMe()}}>
                                {/* <Text style={[styles.globalIcon,styles.bigIco]}>t</Text> */}
                                <FaIcon name="gps-fixed" color={'#2E89Ed'} size={25} style={{marginRight:5}}/>
                                <Text>Autour de moi</Text>
                            </TouchableOpacity>
                        </View>
                        {/*<View style={(width < 1025 && width > 991) ? styles.inlineDatarow:styles.row}>*/}
                        <Row size={12}>
                            <Col sm={12} md={12} lg={8}>
                                <View style={[styles.mapHolder]}>
                                     <WebView onNavigationStateChange={this.onMapPress.bind(this)}
                                     bounces={false}
                                     scrollEnabled={false} 
                                    //  scalesPageToFit={true}
                                    source={{uri:settings.mapUrl}} 
                                    //source={{uri:'https://boumelita.eu/uneaffairecom/map_iframe_app.html'}} 
                                     style={styles.mapView} 
                                     //style={{height:500,width:500}}
                                     /> 
                                    {/* <WebView source={{html:mapJS.html250}} injectJavascript = {css.JS} style={styles.mapView} /> */}
                                </View>
                                {postAdd}
                            </Col>
                            <Col sm={12} md={12} lg={4}>
                                <View style={styles.verticalLis}>{religionListing}</View>
                            </Col>
                        </Row>
                        {postAddTabport}
                        {religion}
                        
                    {/*</Animated.View>*/}
                </View>
            </View>
            {this._renderGetDirections()}
        </ScrollView>
    );
  }
}

function mapStateToProps(state, ownProps) {
	return {
        chat:state.chat,
        auth:state.auth,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		chatActions: bindActionCreators(chatActions, dispatch),
		authActions: bindActionCreators(authActions, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeContainer);