import React, {Component} from 'react';
import {
    StyleSheet,
    Image,
    Text,
    View,
    TextInput,
    TouchableOpacity,
    ScrollView,
    Dimensions,
    Platform,
    Animated,
    WebView,
    Switch,
    ActivityIndicator,
    Alert,
    Keyboard,
    KeyboardAvoidingView,
    findNodeHandle
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import images from '../../config/images';
import styles from '../../config/genStyle';
import {colors} from '../../config/styles';
import {settings} from '../../config/settings';
import RadioButton from '../../lib/react-radio/index';
// import CheckBox from 'react-native-check-box';
import CheckBox from '../../lib/react-native-check-box';
import FloatLabelTextInput from '../../lib/react-native-floating-label-text-input';
import Tabs from '../../components/Tabs/Tabs';
//import MapSelector from '../../components/MapSelector/MapSelector';
import LocationSelect from '../../components/LocationSelect/LocationSelect';
import ModalCat from '../../components/ModalCat/ModalCat';
import Select from '../../components/Select/Select';
import ImageBox from '../../components/ImageBox/Imagebox';
import ApiHelper from '../../lib/ApiHelper/ApiHelper';
import ActionSheet from 'react-native-actionsheet';
import { iconsMap, iconsLoaded } from '../../config/icons';
import Icon from 'react-native-vector-icons/FontAwesome';
import Loader from '../../components/Loader/Loader';
import { NetworkInfo } from 'react-native-network-info';
import startMain from '../../config/app-main';
//var RCTKeyboardToolbarTextInput = require('react-native-textinput-utils');
// redux specific
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as chatActions from '../../redux/reducers/chat';
import * as authActions from '../../redux/reducers/auth';

const {width, height} = Dimensions.get('window');
const vWidth = width;
const vHeight = (width * width / height);
const HEADER_MAX_HEIGHT = 100;
const HEADER_MIN_HEIGHT = Platform.OS === 'ios' ? 50 : 50;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;
let categoryContent;
const api = new ApiHelper;
const timeout = 1000;

class postad extends Component {

    static navigatorStyle = {
        navBarBackgroundColor: colors.navBarBackgroundColor,
        navBarTextColor: colors.navBarTextColor,
        navBarSubtitleTextColor: colors.navBarSubtitleTextColor,
        navBarButtonColor: colors.navBarButtonColor,
        statusBarTextColorScheme: colors.statusBarTextColorScheme,
        statusBarColor: colors.statusBarColor,
        tabBarBackgroundColor: colors.tabBarBackgroundColor,
        tabBarButtonColor: colors.tabBarButtonColor,
        tabBarSelectedButtonColor: colors.tabBarSelectedButtonColor,
        navBarSubtitleColor: colors.navBarSubtitleColor,
        navBarTextFontFamily: colors.navBarTextFontFamily,
        // navBarHidden: false,
        // tabBarHidden: true,
        disableOpenGesture: true,
        drawUnderTabBar: true
    };

    constructor(props) {
        super(props);
        // if you want to listen on navigator events, set this up
        this.ActionSheetOptions = {
            CANCEL_INDEX: 2,
            DESTRUCTIVE_INDEX: 2,
            options: ['Prendre une photo', 'À partir de la bibliothèque', 'Annuler'],
            title: 'Ajouter des photos?'
        };
        this.ImageBox=[];
        //this.MapSelector=[];
        this.ModalCat=[];
        this.LocationSelect=[];
        this.ActionSheets=[];
        this.refsT=[];
        this.ScrollView=[];
        this.state = {
            sendLoad:false,
            tabList: ['Annonce n°1'],
            priceData:{},
            ipAddress:null,
            loading:true,
            listingArray:[
                {
                    images: [],
                    upload:false,
                    mainImage: '',
                    pack6: false,
                    isVideo: false,
                    videoUrl: '',
                    avantData: {},
                    EnVente: {},
                    adType: 'offers',
                    title: '',
                    title_contre:'',
                    description_contre : '',
                    hideIsDirect : true,
                    category: '',
                    categoryLoad: true,
                    attributeData: [],
                    categoryFilterData: {},
                    description: '',
                    price: 0,
                    isUrgent: false,
                    location:{},
                    address:'',
                    //location: '',
                    //lat: '',
                    //log: '',
                    refer:'',
                    userType: '',
                    comment: '',
                    mobile: '',
                    email: '',
                    username: '',
                    isAccountData: true,
                    hideMobile: false,
                    showCategoryContent: false  
                }
            ],
            listing: {
                images: [],
                upload:false,
                mainImage: '',
                pack6: false,
                isVideo: false,
                hideIsDirect :true,
                videoUrl: '',
                avantData: {},
                EnVente: {},
                adType: 'offers',
                title: '',
                title_contre:'',
                description_contre : '',
                category: '',
                categoryLoad: true,
                attributeData: [],
                categoryFilterData: {},
                description: '',
                price: 0,
                isUrgent: false,
                location:{},
                address:'',
                // location: '',
                // lat: '',
                // log: '',
                refer:'',
                userType: '',
                comment: '',
                mobile: '',
                email: '',
                username: '',
                isAccountData: true,
                hideMobile: false,
                showCategoryContent: false
            },

            currentTab:1,


            showVideo: [false],
            check1: false,
            check2: false,
            checkbox: false,
            tabs: 1,
            tabSelected1: 1,
            tabSelected: 1,
            like: 1,
            scrolled: true,
            scrollY: new Animated.Value(0),
            value: 1,
            value1: 1,
            visible: 0,
            text: "",
            SRC: 'https://www.youtube.com/embed/0DI0WBuicV0',
            backArrow: true,
            onBack: null,
            validAry:[],
            doneBtn:false,
            mainViewHgt: 0,
            footerHgt: 0,
            keyboardOpen: false,
            bottom:0,
            refToSubmit:null
        };
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
    }

    componentWillMount() {
        iconsLoaded.then(() => {});
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
    }

    _keyboardDidShow = (e) => {
        if(Platform.OS === 'ios'){
            //console.log('keyboard open');
            //console.log(e);
            if(this.state.doneBtn){
                this.setState({keyboardOpen:true,bottom:e.endCoordinates.height});
            }
        }
    }

    _keyboardDidHide = (e) => {
        if(Platform.OS === 'ios'){
             //console.log('keyboard hide');
             //console.log(e);
                if(this.state.doneBtn){
                    this.setState({keyboardOpen:false});
                }
        }
    }
    
      componentWillUnmount () {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
      }

    getListing(index){
        return this.state.listingArray[index];
    }

    setListing(index,listing){
        let listArray=this.state.listingArray;
        listArray[index]=listing;
        this.setState({listingArray:listArray});
    }


    setListingAttribute(index,object){
        let listingArray = this.state.listingArray;
        Object.keys(object).map((key) => {
            listingArray[index][key] = object[key];
        });
        this.setState({listingArray:listingArray});
        // if(typeof key == 'string'){
        //     let listingArray = this.state.listingArray;
        //     listingArray[index][key] = value;
        //     this.setState({listingArray:listingArray});
        // }else if(typeof key == 'array' ){

        // }
    }

    sendEventToDrawer() {
        this.props.navigator.handleDeepLink({
            link: 'Drawer',
            payload: {screen: this.props.testID}
        });
    }

    getIp(){
        if(this.state.ipAddress == null){
            NetworkInfo.getIPAddress(ip => {
                this.setState({ipAddress:ip},()=>{
                    this.initialize();
                });
            });
        }
    }

    handleOpen(){
        if(!api.checkLogin(this.props.auth.token)){
            startMain('',{});
        }else if(!api.isProfileComplete(this.props.auth.userdata)){
            this.props.navigator.switchToTab({
                tabIndex:4
            });
        }else{
            this.sendEventToDrawer();
            //this.props.navigator.switchToTab({})
            api.setAction(this.props.chatActions,this.props.authActions);
            this.props.chatActions.setScreen(this.props.testID);
            //console.log(this.props);
            this.getIp();
            this.props.navigator.toggleNavBar({to: 'shown', animated: false});
            this.props.navigator.toggleTabs({animated: false, to: 'hidden'});
            this.props.navigator.setStyle({navBarTitleTextCentered: true});
            this.props.navigator.setTitle({title: "Déposer une annonce"});
            this.props.navigator.setButtons({
                    leftButtons: [
                        {
                            icon: iconsMap['back'],
                            id: 'back2',
                            title: 'back to welcome'
                        }
                    ],
                    animated: false
                });
        }
    }

    onNavigatorEvent(event) {
        //console.log(event.id);
        if (event.id === 'bottomTabSelected' && event.selectedTabIndex == 2) {
            if(this.state.onBack == null){
                //console.log('on back go to tab '+(event.unselectedTabIndex+1));
                if(!api.isProfileComplete(this.props.auth.userdata)){
                    this.setState({onBack: 0});
                }else{
                    this.setState({onBack: event.unselectedTabIndex});
                }
            }
        }
        if (event.id == 'willAppear') {
           this.handleOpen();
        }
        if (event.id == 'didAppear') {
            if(this.state.priceData!== undefined && Object.keys(this.state.priceData).length === 0){
                this.getPriceData();
            }
        }
        if(event.id == 'willDisappear'){
        //     Alert.alert(
        //     'Précurseur',
        //     'Êtes-vous sûr de vouloir supprimer cette liste et ses données?',
        //     [
        //         {text: 'Annuler', onPress: () => {
        //             return false
        //         }, style: 'cancel'},
        //         {text: 'Approuvé', onPress: () => {

        //         }},
        //     ],
        //     { cancelable: false }
        // )
        }
        if (event.id === 'backPress') {
            Alert.alert(
            'Précurseur',
            'Toutes les publications seront perdues. es tu sûr de vouloir partir ?',
            [
                {text: 'Annuler', onPress: () => {}, style: 'cancel'},
                {text: 'Approuvé', onPress: () => {this.goback()}},
            ],
            { cancelable: false }
        )
        }
        if (event.id == 'didDisappear') {
            //this.setState({onBack: null});
        }

        if (event.id == 'back2') {
            Alert.alert(
                'Avertissement!',
                'Toutes les publications seront perdues. Êtes vous sûr de vouloir quitter?',
                [
                    {text: 'Annuler', onPress: () => {}, style: 'cancel'},
                    {text: 'Quitter', onPress: () => {this.goback()}},
                ],
                { cancelable: false }
            )
        }
    }

    goback(){
        this.initialize();
         if (this.state.onBack != null) {
            let onBack = this.state.onBack
            this.setState({onBack: null},()=>{
               this.props.navigator.switchToTab({
                   tabIndex: onBack // (optional) if missing, this screen's tab will become selected
               });
            });
            } else {
                this.setState({onBack: null},()=>{
                    this.props.navigator.switchToTab({
                        tabIndex: 0 // (optional) if missing, this screen's tab will become selected
                    });
                });
            }
    }

    getPriceData(){
        //console.log('will appear')
        this.setState({loading:true},()=>{
            let request={
                url:settings.endpoints.postPriceList,
                method:'POST',
                params:{}
            }
            api.FetchData(request).then((results)=>{
                //console.log(results);
                if(results.status){
                    this.setState({loading:false,priceData:results.post_prices});
                }else{
                    //this.setState({loading:false});
                }
            }).catch((error)=>{
                //this.setState({loading:false});
                //console.log(error);
            });
        });
        
    }    

    handleOnPress(index,value) {
        if (value == 'offers' || value == 'demands' || value === 'trocs') {
            this.setListingAttribute(index,{adType:value});
        } else {
          
        }
    }
    handleOnPress1(value) {
        this.setState({value1: value});
    }

    goto(page,index) {
        const {listingArray} =this.state;
        //let listingArray = this.state.listingArray;
        let listing = listingArray[index];
        let envente=listing.EnVente;
        let avantdata=listing.avantData;
        let listingPrice=parseFloat(listing.price);
        let listPrice=this.calculatePrice(index);
        let totPrice=this.calculateTotalPrice();
        let minusPrice=0;
        if(listing.EnVente.isDirectSell){
            let factor = (this.state.priceData.direct_sell_prcnt.price)/100;
            minusPrice = this.state.priceData.direct_sell_price.price +( parseInt(listing.EnVente.itemQuantity) * parseFloat(listing.EnVente.productPrice) * factor);
            if(parseFloat(minusPrice) === NaN){
                minusPrice = 0;
            }
        }
        listPrice = listPrice - minusPrice;
        totPrice = totPrice - minusPrice;
        this.props.navigator.push({
            screen: page,
            passProps:{
                setEnVenteData:this.setEnVenteData.bind(this),
                setAdData:this.setAdData.bind(this),
                getEnVenteData:this.getEnVenteData.bind(this),
                getAdData:this.getAdData.bind(this),
                getPriceData:this.getPrices.bind(this),
                price:listingPrice,
                listPrice:listPrice,
                totPrice:totPrice,
                index:index
            }
        });
    }
    tabchange(tab) {
        //tab = tab + 1;
        this.setState({currentTab:tab});
        //this.setState({tabs: tab, tabSelected: tab});
    }
    tabchange1(tab) {
        this.setState({tabs1: tab, tabSelected1: tab});
    }

    showhide(value) {
        if (this.state.visible == 0) {
            this.setState({visible: 1},()=>{
                categoryContent = <View style={[styles.flex, styles._HP15_]}>
                <View style={[styles.inlineDatarow]}>
                    <FloatLabelTextInput
                        fieldContainerStyle={styles.genInput2}
                        placeholder={"Prix de"}
                        underlineColorAndroid={'#eee'}/>
                    <FloatLabelTextInput
                        style={[styles.genInput, styles._ML5_]}
                        placeholder={"Prix a"}
                        underlineColorAndroid={'#eee'}/>
                </View>
                <View style={[styles.inlineDatarow]}>
                    <FloatLabelTextInput
                        fieldContainerStyle={styles.genInput2}
                        placeholder={"Km de"}
                        underlineColorAndroid={'#eee'}/>
                    <FloatLabelTextInput
                        style={[styles.genInput, styles._ML5_]}
                        placeholder={"Km a"}
                        underlineColorAndroid={'#eee'}/>
                </View>
                <View style={[styles.inlineDatarow]}>
                    <FloatLabelTextInput
                        fieldContainerStyle={styles.genInput2}
                        placeholder={"Anne de"}
                        underlineColorAndroid={'#eee'}/>
                    <FloatLabelTextInput
                        style={[styles.genInput, styles._ML5_]}
                        placeholder={"Anne a"}
                        underlineColorAndroid={'#eee'}/>
                </View>
            </View>
            })
        } else {
            this.setState({visible: 0},()=>{
                categoryContent = <View></View>
            })
        }
    }

    videoUrl(index) {
        if (this.state.listingArray[index].videoUrl != "") {
            let s=this.state.showVideo;
            s[index]=true;
            this.setState({showVideo: s});
        } else {
            Alert.alert(
                'Alerte!',
                "Merci de compléter l'adresse Url",
                [
                    {text: 'Ok', onPress: () => {
                        //this.props.navigator.pop({animated:true});
                    }},
                ],
                { cancelable: false }
            ) 
            // alert("Please enter video url");
        }
    }

    videoShow() {
        this.setState({
            check1: !this.state.check1
        });
    }

    addImage(index) {
        //console.log(this.ActionSheet);
        this.ActionSheets[index].show();
    }

    selectAction(index,i) {
        //this.ActionSheet.hide();
        this.ImageBox[index].onActionSelect(i);
    }

    selectCat(index,item) {
        let listArray=this.state.listingArray;
        let listing = listArray[index];
        listing.category = item;
        listing.categoryLoad = true;
        listing.categoryFilterData = {};
        listArray[index] = listing;
        this.setState({
            listingArray: listArray
        }, () => {
            this.loadAttributes(index,item);
        });
    }

    loadAttributes(index,item) {
        let request = {
            url: settings.endpoints.getAttributes,
            method: 'POST',
            params: {
                category_id: item,
                token: this.props.auth.token,
                post:true,
            }
        }
        api.FetchData(request).then((results) => {
                let listArray=this.state.listingArray;
                let listing = listArray[index];
                listing.categoryLoad = false;
                listing.attributeData = [];
                if (results.status) {
                    Object.keys(results.attrList).map((key) => {
                            if (results.attrList[key] != "") {
                                let attrib = {};
                                attrib.id = key;
                                attrib.type = results.attrList[key].type;
                                attrib.name = results.attrList[key].name;
                                if (results.attrList[key].type == 'dropdown') {
                                    attrib.options = results.attrList[key].predef;
                                    if(typeof results.attrList[key].predef[0] != 'undefined' && typeof results.attrList[key].predef[0].subAttr != 'undefined'){
                                        attrib.hasSub = true;
                                    }else{
                                        attrib.hasSub = false;
                                    }  
                                }
                                listing.categoryFilterData[key] = '';
                                listing.attributeData.push(attrib);
                            }
                        });
                    //console.log(listing.attributeData);
                }
                 if(results.is_direct === 1) {
                    listing.hideIsDirect = true;
                } else {
                    listing.hideIsDirect = false;
                }
                listArray[index] = listing;
                this.setState({listingArray: listArray});
            })
            .catch((error) => {
                this.setListingAttribute(index,{categoryLoad:false});
                // let listing = this.state.listing;
                // listing.categoryLoad = false;
                // this.setState({listing: listing});
            })
    }

    validLocation(location){
        //console.log(location)
        if(location.isCurrent !== undefined && location.isCurrent !== null){
            if(location.lat !== undefined && location.long !== undefined){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    validateListing(listing) {
        //let valid=true; userType:'', email:'',
        //console.log(1);
        let validObj={
            login:true,
            videoUrl:true,
            title:true,
            title_contre:true,
            category:true,
            categoryContent:true,
            description:true,
            description_contre:true,
            avantData:true,
            directSell:true,
            adType:true,
            price:true,
            location:true,
            address:true,
            mobile:true,
        };
        if (!api.checkLogin(this.props.auth.token)) {
            validObj.login = false;
            //return false;
        }
        // images:[],        
        // if (typeof listing.images == 'undefined' || listing.images == null || listing.images.length < 1) {
        //     return false;
        // }
        // mainImage:'',               
        // if (typeof listing.mainImage == 'undefined' || listing.mainImage == null) {
        //     return false;
        // }
        // isVideo:true, videoUrl:'https://www.youtube.com/embed/k-atPa3QUis',
        if (listing.isVideo && listing.videoUrl == '') {
            validObj.videoUrl = false;
            //return false;
        }
        // location:'', lat:'', log:'',
        //console.log(listing.location);
        if(!listing.isAccountData && (listing.location === undefined || listing.location === null || Object.keys(listing.location).length === 0|| !this.validLocation(listing.location))){
            validObj.location = false;
            //return false;
        }
        /*if (typeof listing.location == '' || typeof listing.lat == 'undefined' || typeof listing.log == 'undefined') {
            return false;
        }*/
        // title:'',
        if (listing.title == "") {
            validObj.title = false;
            //return false;
        }
        // description:'',
        if (listing.description == "") {
            validObj.description = false;
            //return false;
        }

        if(listing.adType === 'trocs') {
            if (listing.title_contre == "") {
                validObj.title_contre = false;
                //return false;
            }
            // description:'',
            if (listing.description_contre == "") {
                validObj.description_contre = false;
                //return false;
            } 
        }

        // category:'',
        //console.log(2);
        if (listing.category == "") {
            validObj.category = false;
            //return false;
        }
        if(this.props.auth.userdata.usertype === 'professional' && !listing.isAccountData && listing.address == ''){
            validObj.address = false;
            //return false;
        }

        // if(this.props.auth.userdata.usertype === 'professional' && (listing.address == '' || listing.address === undefined)) {
        //     validObj.address = false;
        // }
        // avantData:{},
        if (!this.validateAvantData(listing.avantData)) {
            validObj.avantData = false;
            //return false;
        }
        //console.log(3);
        // EnVente:{},
        if (this.props.auth.userdata.usertype == 'professional' && !this.validateEnvente(listing.EnVente)) {
            validObj.directSell = false;
            //return false;
        }
        // adType:'offers',
        //console.log(4);
        if (typeof listing.adType == 'undefined' || listing.adType == null || listing.adType == "") {
            validObj.adType = false;
            //return false;
        }
        //comment:'',
        // if (listing.comment == '') {
        //     return false;
        // }
        //  price:0,
        if (listing.price === NaN || listing.price <= 0) {
            validObj.price = false;
            //return false;
        }
        //console.log(5);
        if (!listing.isAccountData && (listing.mobile == '' || !this.validMobile(listing.mobile))) {
            validObj.mobile = false;
            //return false;
        }
        //
        //console.log(6);

        if (!this.validateCategoryData(listing.attributeData,listing.categoryFilterData)) {
            validObj.categoryContent = false;
            //return false;
        }
        //console.log(7);
        return validObj;
    }

    validMobile(mobile){
        if(isNaN(mobile.replace(/\s/g, ""))){
            //msg = "Le numéro de téléphone doit être un chiffre.";
            return false;
        } else if(mobile.charAt(0) != '0'){
            //msg = 'Le numéro de téléphone commence par 0!';
            return false;
        } else if(mobile.replace(/\s/g, "").length != 10){
            //msg = 'Le numéro de téléphone doit comporter 10 chiffres!';
            return false;
        }else{
            return true;
        }
    }


    validNumber(number){
        number=parseFloat(number)
        if(number === NaN || number <= 0){
            return false;
        }
        return true;
    }
    validateEnvente(data){
        //console.log(data)
        if(typeof data == 'undefined' || data == null){
            return false;
        }
        if(Object.keys(data).length === 0){
            return true;
        }
        //console.log(11)
        if(data.isDirectSell !== true && data.isDirectSell !== false){
            return false;
        }

        if(!data.isDirectSell){
            return true;
        }
        if(data.productName == ''){
            return false;
        } 
        //console.log(12)
        if(!this.validNumber(data.productPrice)){
            return false;
        }
        if(!this.validNumber(data.itemQuantity)){
            return false;
        } 
        //console.log(13)
        let isSimple = data.shippingType == 'Frais_port_simple' ? true : false;
        let isVariable = data.shippingType == 'Frais_port_variable' ? true : false;
        let vL = isVariable ? data.variableShippingData.length : 0;
        if(!isSimple && !isVariable){
            return false;
        }
        //console.log(14)
        if(isSimple && !this.validNumber(data.simpleShippingPrice)){
            return false;
        }
        if(isVariable){
            let ret=true;
            data.variableShippingData.map((data,i)=>{
                if(!this.validNumber(data.price)){
                    ret=false;
                }
                if(data.to < 2){
                    ret=false;
                }
            });
            if(ret === false){
                return false;
            }
        }
        //console.log(15)
        let reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if(reg.test(data.paypalAddress) == false){
            return false;
        }
        return true;
    }

    validateAvantData(data){
        //return true;
        if(typeof data == 'undefined' || data == null){
            return false;
        }
        if(Object.keys(data).length === 0){
            return true;
        }
        if(data.isAdShowcase === true){
            if(typeof data.adShowcase == 'undefined' || data.adShowcase == null || Object.keys(data.adShowcase).length === 0){
                return false;
            }
        }
        if(data.isListShowcase === true){
            if(typeof data.listShowCase == 'undefined' || data.listShowCase == null || Object.keys(data.listShowCase).length === 0){
                return false;
            }
        }
        return true;
    }

    validateCategoryData(attributeData,categoryFilterData) {
        for (i = 0; i < categoryFilterData.length; i++) {
            let key = attributeData[i].id;
            if (typeof categoryFilterData[key] == 'undefined' || categoryFilterData[key] == null || categoryFilterData[key] == '') {
                return false;
            }
        }
        return true;
    }

    validateValidObj(validAry){
        //console.log(validAry);
        let ret = true;
        let obj = {};
        validAry.map((d,i)=>{
            Object.keys(d).map((k)=>{
                if(ret === true && d[k]=== false && (i === (this.state.currentTab-1))){
                    obj.index=i;
                    obj.key=k;
                }
                ret = ret && d[k];
            });
        });
        if(!ret){
            //console.log(obj);
            if(this.refsT[obj.index] !== undefined && this.refsT[obj.index] !== null && Object.keys(this.refsT[obj.index]).length > 0){
                //let newKey = this._getNextKey(index,key);
                //console.log('in next ')
                let newRef=null;
                if(obj.key == 'category'){
                    newRef = this.ModalCat[obj.index];
                }else if(obj.key == 'location'){
                    newRef = this.LocationSelect[obj.index];
                }else{
                    newRef = this.refsT[obj.index][obj.key];
                    this._setFocus(newRef,true);
                }
                this.scrollToRef(obj.index,newRef);
                //this._refStyleRed(this.refsT[index][key]);
            }
        }
        return ret;
    }

    scrollToRef(index,ref){
        if(this.ScrollView[index] !== undefined && this.ScrollView[index] !== null){
            if (ref && ref.inputRef) {
                ref.inputRef().measureLayout(findNodeHandle(this.ScrollView[index]),(x,y)=>{
                    //console.log('x =>'+x);
                    //console.log('y =>'+y);
                    this.ScrollView[index].scrollTo({y: y-50,animated:true});
                });
            } 
        }
    }

    validateAllListing(){
        let validAry=[];
        /* uncomment this to start validation */
        this.state.listingArray.map((listing,index)=>{
            //console.log(this.validateListing(listing));
            //ret = ret && this.validateListing(listing);
            validAry.push(this.validateListing(listing));
        });
        this.setState({validAry:validAry});
        return this.validateValidObj(validAry);        
    }

    addNewTab(){
        let tabList=this.state.tabList;
        let listingArray=this.state.listingArray;
        let i=this.state.tabList.length+1;
        tabList.push('annonce n°'+i);
        let newlisting={
                    images: [],
                    upload:false,
                    mainImage: '',
                    pack6: false,
                    isVideo: false,
                    videoUrl: '',
                    avantData: {},
                    EnVente: {},
                    adType: 'offers',
                    title: '',
                    title_contre: '',
                    category: '',
                    categoryLoad: true,
                    attributeData: [],
                    categoryFilterData: {},
                    description: '',
                    description_contre:'',
                    price: 0,
                    isUrgent: false,
                    location:{},
                    address:'',
                    // location: '',
                    // lat: '',
                    // log: '',
                    refer:'',
                    userType: '',
                    comment: '',
                    mobile: '',
                    email: '',
                    username: '',
                    isAccountData: true,
                    hideMobile: false,
                    showCategoryContent: false  
                };
        listingArray.push(newlisting);
        this.Tabs.setTab(i-1);
        this.setState({tabList:tabList,listingArray:listingArray,currentTab:i});
    }

    addTab(i) {
        this.state.listingArray.map((listing,index)=>{
        let LocationSelect =this.LocationSelect[index].getAddress();
            this.setListingAttribute(index,{
                email:this.props.auth.userdata.email,
                userType: this.props.auth.userdata.usertype,
                location:LocationSelect,
                price : parseFloat(listing.price)
            });
       
        });
        if (this.validateAllListing()) {
            this.addNewTab();
            this.uploadTabImage(i);
        } else {
            Alert.alert(
                'Alerte!',
                'veuillez compléter tous le champs obligatoires',
                [
                    {text: 'Ok', onPress: () => {
                        //this.props.navigator.pop({animated:true});
                    }},
                ],
                { cancelable: false }
            ) 
           // alert('veuillez compléter tous le champs obligatoires');
        }
    }

    uploadTabImage(i){
        //if(this.state.listingArray[i].upload === false){
            this.ImageBox[i].uploadImages(i).then((result)=>{
                if(result.status){
                    let imgs=this.ImageBox[i].getImages();
                    this.setListingAttribute(i,{upload:true,images:imgs.images,mainImage:imgs.main});
                }
            }).catch((err)=>{console.log(err)});
        //}
    }

    convertToserver(listing){
        let obj={};
        let highlight={};
        let directsale={};
        obj.images=listing.images;
        obj.additional_picture=listing.pack6 ? 1 : 0;
        obj.additional_picture_price= obj.additional_picture ==1 ? {
            aditional_pic_price:this.state.priceData.aditional_pic_price.id
        }:"";
        obj.is_video_check=listing.isVideo ? 1 : 0;
        obj.video_price= obj.is_video_check == 1 ? {
            video_price:this.state.priceData.video_price.id
        }:"";
        obj.video=obj.is_video_check == 1 ? listing.videoUrl : "";
        //obj.highlight_data={};

        if(typeof listing.avantData != 'undefined' && listing.avantData != null && Object.keys(listing.avantData).length !== 0){    
            highlight.is_in_showcase=listing.avantData.isAdShowcase ? 1:0;
            highlight.is_in_top_list=listing.avantData.isListShowcase ? 1:0;
            highlight.showcase_data=highlight.is_in_showcase == 1 ? listing.avantData.adShowcase : "";
            highlight.top_list_data=highlight.is_in_top_list == 1 ? listing.avantData.listShowCase : "";
            obj.highlight_data=highlight;
        }

        if(typeof listing.EnVente != 'undefined' && listing.EnVente != null && Object.keys(listing.EnVente).length !== 0){    
            directsale.is_direct_sale=listing.EnVente.isDirectSell ? 1:0;
            directsale.direct_sell_price_data=directsale.is_direct_sale==1 ? {
                direct_sell_price:this.state.priceData.direct_sell_price.id,
                direct_sell_prcnt:this.state.priceData.direct_sell_prcnt.id,
            }:"";
            directsale.product_name=listing.EnVente.productName;
            directsale.product_price=listing.EnVente.productPrice;
            directsale.item_quantity=listing.EnVente.itemQuantity;
            directsale.shipping_type=listing.EnVente.shippingType;
            directsale.shipping_value_simple=directsale.shipping_type == 'Frais_port_simple' ? listing.EnVente.simpleShippingPrice : "";
            directsale.shipping_value_variable=directsale.shipping_type != 'Frais_port_simple' ? listing.EnVente.variableShippingData : ""
            directsale.seller_paypal_url=listing.EnVente.paypalAddress;
            directsale.direct_sale_opt_data=listing.EnVente.directSellData;
            obj.direct_sale_data=directsale;
        }

        obj.ad_type=listing.adType;
        obj.title=listing.title;
        obj.title_contre=listing.title_contre;
        obj.description_contre=listing.description_contre;
        obj.category=listing.category;
        obj.categoryFilterData=listing.categoryFilterData;
        obj.description=listing.description;
        obj.reference= this.props.auth.userdata.usertype === 'professional' ? listing.refer : '';

        obj.price=listing.price;
        obj.is_urgent=listing.isUrgent ? 1 : 0;
        obj.urgent_price= obj.is_urgent == 1 ? {
            urgent_price:this.state.priceData.urgent_price.id
        }:"";
        obj.is_account_address=listing.isAccountData ? 1 : 0;
        obj.location=obj.is_account_address != 1 ? listing.location : '';
        obj.address=obj.is_account_address != 1 ? listing.address:this.props.auth.userdata.address1+", "+this.props.auth.userdata.address2 ;
        obj.ad_user_type=listing.userType;
        obj.telephone=listing.mobile;
        obj.is_phone_hide=listing.hideMobile ? 1 : 0;
        return obj;
    }

    handleAfter(result){
        //console.log(result.tol_val);
        if(result.tol_val > 0){
            this.props.navigator.push({
                screen: 'Cart',
                passProps:{
                    token:this.props.token,
                    userdata:this.props.userdata,
                    checkPop : true
                }
            });
            // this.props.navigator.switchToTab({
            //     tabIndex:3
            // });
        }else{
            this.initialize();
        }
    }

    initialize(){
        console.log('initialize');
        let newState={
            sendLoad:false,
            tabList: ['annonce n°1'],
            priceData:this.state.priceData,
            ipAddress:this.state.ipAddress,
            loading:this.state.priceData !== undefined && this.state.priceData !== null && Object.keys(this.state.priceData).length > 0 ? false:true,
            listingArray:[
                {
                    images: [],
                    upload:false,
                    mainImage: '',
                    pack6: false,
                    isVideo: false,
                    videoUrl: '',
                    avantData: {},
                    hideIsDirect :true,
                    EnVente: {},
                    adType: 'offers',
                    title: '',
                    title_contre:'',
                    category: '',
                    categoryLoad: true,
                    attributeData: [],
                    categoryFilterData: {},
                    description: '',
                    description_contre:'',
                    price: 0,
                    isUrgent: false,
                    location:{},
                    address:'',
                    //location: '',
                    //lat: '',
                    //log: '',
                    refer:'',
                    userType: '',
                    comment: '',
                    mobile: '',
                    email: '',
                    username: '',
                    isAccountData: true,
                    hideMobile: false,
                    showCategoryContent: false  
                }
            ],
            listing: {
                images: [],
                upload:false,
                mainImage: '',
                pack6: false,
                isVideo: false,
                videoUrl: '',
                avantData: {},
                EnVente: {},
                adType: 'offers',
                hideIsDirect : false,
                title: '',
                title_contre:'',
                category: '',
                categoryLoad: true,
                attributeData: [],
                categoryFilterData: {},
                description: '',
                description_contre:'',
                price: 0,
                isUrgent: false,
                location:{},
                address:'',
                // location: '',
                // lat: '',
                // log: '',
                refer:'',
                userType: '',
                comment: '',
                mobile: '',
                email: '',
                username: '',
                isAccountData: true,
                hideMobile: false,
                showCategoryContent: false
            },
            currentTab:1,
            showVideo: [false],
            check1: false,
            check2: false,
            checkbox: false,
            tabs: 1,
            tabSelected1: 1,
            tabSelected: 1,
            like: 1,
            scrolled: true,
            scrollY: new Animated.Value(0),
            value: 1,
            value1: 1,
            visible: 0,
            text: "",
            SRC: 'https://www.youtube.com/embed/0DI0WBuicV0',
            backArrow: true,
            onBack: this.state.onBack,
            validAry:[],
            doneBtn:false,
            mainViewHgt: 0,
            footerHgt: 0,
            keyboardOpen: false,
            bottom:0,
            refToSubmit:null
        };
        this.state = newState;
        this.setState(newState,()=>{
            if(this.ModalCat[0] !== undefined && this.ModalCat[0] !== null){
                this.ModalCat[0].reset();
            }
            if(this.ImageBox[0] !== undefined && this.ImageBox[0] !== null){
                this.ImageBox[0].reset();
            }
            if(this.LocationSelect[0] !== undefined && this.LocationSelect[0] !== null){
                this.LocationSelect[0].reset();
            }
        });
    }

    saveAllData(){
        let serverAry=[];
        this.state.listingArray.map((data,i)=>{
            let obj=this.convertToserver(data);
            serverAry.push(obj);
        });
        let request={
            url:settings.endpoints.postAd,
            method:"POST",
            params:{
                token:this.props.auth.token,
                total_price:this.calculateTotalPrice(),
                user_ip:this.state.ipAddress !=null ? this.state.ipAddress: '',
                listing:serverAry}
        };
        api.FetchData(request).then((result)=>{
            if(result.status){
                this.setState({sendLoad:false},()=>{
                    Alert.alert(
                        'Félicitations!',
                        'Votre annonce a été soumise à verification. Après validation, elle sera en ligne sous 24 heures',
                        [
                            {text: 'fermer', onPress: () => {
                                this.handleAfter(result);
                            }},
                        ],
                        { cancelable: false }
                    )
                });
                //alert('Message édité avec succès');
            }else{
                //alert('Publié non édité');  
                this.setState({sendLoad:false},()=>{
                Alert.alert(
                    'Infructueux',
                    'Publié non édité',
                    [
                        {text: 'Approuvé', onPress: () => {
                            //this.props.navigator.pop({animated:true});
                        }},
                    ],
                    { cancelable: false }
                )  
            });            
            }
        }).catch((err)=>{
            //alert('Quelque chose a mal tourné');
            this.setState({sendLoad:false},()=>{
            Alert.alert(
                'Infructueux',
                'Quelque chose a mal tourné',
                [
                    {text: 'Approuvé', onPress: () => {
                        //this.props.navigator.pop({animated:true});
                    }},
                ],
                { cancelable: false }
            ) 
        });             
        });
    }

    submitAll() {
        // console.log('Submit all Data');
        this.state.listingArray.map((listing,index)=>{
            let LocationSelect =this.LocationSelect[index].getAddress();
                this.setListingAttribute(index,{
                    email:this.props.auth.userdata.email,
                    userType: this.props.auth.userdata.usertype,
                    location:LocationSelect,
                    price : parseFloat(listing.price)
                });
            });
        if(this.validateAllListing()){
            // console.log('After validate');
            let vi=this.state.currentTab-1;
            if(this.ImageBox[vi] !== undefined && this.ImageBox[vi] !== null){
                let n=this.ImageBox[vi].getImages().images;
                if(n !== undefined && n !== null && n.length > 0){
                if(this.ScrollView[vi] !== undefined && this.ScrollView[vi] !== null){
                    this.ScrollView[vi].scrollTo({y: 0, animated: true});
                }
                this.setState({sendLoad:true},()=>{
                    this.ImageBox[vi].uploadImages(vi).then((result)=>{
                        console.log(result);
                       // console.log('Check Result');
                        if(result.status){
                           // console.log('Check if');
                            let imgs=this.ImageBox[vi].getImages();
                            this.setListingAttribute(vi,{upload:true,images:imgs.images,mainImage:imgs.main});
                            setTimeout(()=>{
                                this.saveAllData();
                            },timeout);
                        }else{
                            //console.log(this.props.auth)
                            setTimeout(()=>{
                               // console.log('Check else');
                                this.setState({sendLoad:false},()=>{
                                    alert(result.message);
                                })
                                // this.saveAllData();
                            },timeout);
                        }
                    }).catch((err)=>{
                        console.log(err)
                        //console.log('Check catch');
                        this.setState({sendLoad:false},()=>{
                            alert(err.message);
                        })
                        // setTimeout(()=>{
                        //     this.saveAllData();
                        // },timeout);
                    });
                });
               
            }else if(n !== undefined && n !== null && n.length === 0){
                //console.log('Check else');
                this.setState({sendLoad:true},()=>{
                    setTimeout(() => {
                        this.saveAllData();
                    },timeout);
                });
            }
            }
     }else{
        Alert.alert(
            'Alerte!',
            'veuillez compléter tous le champs obligatoires',
            [
                {text: 'Ok', onPress: () => {
                    //this.props.navigator.pop({animated:true});
                }},
            ],
            { cancelable: false }
        ) 
        // alert('veuillez compléter tous le champs obligatoires');
     }
    }

    calculatePrice(index) {
        let price = 0;
        let listing = this.state.listingArray[index];
        if (listing.pack6) {
            price += 1;
        }
        if (listing.isVideo) {
            price += 1;
        }
        if (listing.isUrgent) {
            price += 1;
        }
        if(api.checkLogin(this.props.auth.token) && this.props.auth.userdata.usertype === 'professional' && this.ModalCat[index] !== undefined && this.ModalCat[index] !== null){
            let catP=this.ModalCat[index].getPrice();
            if(catP !== 0){
                price +=catP;
            }
        }
        if(typeof listing.avantData != 'undefined' && listing.avantData != null && Object.keys(listing.avantData).length !== 0){
            if(listing.avantData.isAdShowcase){
                if(typeof listing.avantData.adShowcase.price && !isNaN(parseFloat(listing.avantData.adShowcase.price))){
                    price=price+listing.avantData.adShowcase.price;
                }
            }
            if(listing.avantData.isListShowcase){
                if(typeof listing.avantData.listShowCase.price && !isNaN(parseFloat(listing.avantData.listShowCase.price))){
                    price=price+listing.avantData.listShowCase.price;
                }
            }
        }
        if(typeof listing.EnVente != 'undefined' && listing.EnVente != null && Object.keys(listing.EnVente).length !== 0){
            if (typeof listing.EnVente.isDirectSell != 'undefined' && listing.EnVente.isDirectSell === true){
                let factor = (this.state.priceData.direct_sell_prcnt.price)/100;
                price = price + this.state.priceData.direct_sell_price.price +( parseInt(listing.EnVente.itemQuantity) * parseFloat(listing.EnVente.productPrice) * factor);
                //price = price + this.state.priceData.direct_sell_price.price + (listing.price * factor);
            }
        }
        return price;
    }

    calculateTotalPrice(){
        let price=0;
        this.state.tabList.map((data,i)=>{
            price=price+this.calculatePrice(i);
        });
        return price;
    }

    getDispPrice(num){
        //return num.toFixed(2).toString().replace('.',',')+' €';
        return num.toFixed(2).toString()+' €';
    }

    removeListing(index){
        Alert.alert(
            'Avertissement!',
            'Votre publication sera perdue. Êtes vous sûr de vouloir supprimer?',
            [
                {text: 'Annuler', onPress: () => {
                
                }},
                {text: 'Supprimer', onPress: () => {
                    let tabList=this.state.tabList;
                    let listingArray=this.state.listingArray;
                    let i=this.state.currentTab-1;
                    tabList.splice(index,1);
                    listingArray.splice(index,1);
                    this.Tabs.setTab(i-1);
                    this.setState({tabList:tabList,listingArray:listingArray,currentTab:i});
                }, style: 'cancel'},
            ],
            { cancelable: false }
        )
    }

    setEnVenteData(index,data){
        this.setListingAttribute(index,{EnVente:data});
    }

    getEnVenteData(index){
        //let env = this.state.listingArray[index].EnVente;
        //env.alreadyIsDirectSell = false;
        return this.state.listingArray[index].EnVente;
    }

    setAdData(index,data){
        this.setListingAttribute(index,{avantData:data});
    }

    getAdData(index){
        return this.state.listingArray[index].avantData;
    }

    getPrices(){
        return this.state.priceData;
    }

    checkUpload(i){
        let curtab=this.state.currentTab;
        let lastIndex=curtab-1;
        if(this.ImageBox[lastIndex] !== undefined && this.ImageBox[lastIndex] !== null){
            this.ImageBox[lastIndex].checkIfUpload(lastIndex);
        }
        this.tabchange(i+1);
    }

    getURL(url){
        if(url !== undefined && url !== null && url !== ""){
            let id = url.split('/').pop();
            if(id.indexOf('watch?v=') > -1){
                id=id.replace("watch?v=",'');
            } else {
                id = id;
            }
            console.log(id);
            return 'https://www.youtube.com/embed/'+id;
        }else {
            return '';
        }
    }

    _setFocus(ref,bool){
        if(ref !== undefined && ref !== null){
            if(bool){
                ref.focus();
                console.log(ref);
            }else{
                ref.blur();
            }
        }
    }

    _getNextKey(index,key){
        const Listing = this.state.listingArray[index];
        switch(key){
            case 'videoUrl':
                return 'title';
            break;
            
            case 'title':
                return 'description';
            break;

            case 'description':
                if(this.props.auth.userdata.usertype == 'professional'){
                    return 'refer';
                }else{
                    return 'price';
                }
            break;

            case 'refer':
                return 'price';
            break;

            case 'price':
                if(Listing.isAccountData){
                    Keyboard.dismiss();
                    return null;
                }else{
                    return 'address';
                }
            break;

            case 'address':
                return 'mobile';
            break;

            case 'mobile':
                if(Listing.adType === 'trocs') {
                    return 'title_contre';
                } else{
                    Keyboard.dismiss();
                    return null;
                }
            break;
            case 'title_contre':
                return 'description_contre';
            break;
            case 'description_contre':
                Keyboard.dismiss();
                return null;
            break;
        }
    }

    getValidationStyle(index,key){
        const {validAry}=this.state;
        //console.log(key);
        if(validAry[index] !== undefined && validAry[index] !== null && validAry[index][key] !== undefined && validAry[index][key] !== null){
            if(validAry[index][key] === false){
                //console.log('red for '+ key );
                return {borderColor:"#F00",borderWidth:1};
            }else{
                return null;
            }
        }else{
            return null;
        }
    }

    _setNextFocus(index,key){
        if(this.refsT[index] !== undefined && this.refsT[index] !== null && Object.keys(this.refsT[index]).length > 0){
            let newKey = this._getNextKey(index,key);
            if(newKey !== null){
            //console.log('in next ')
            this._setFocus(this.refsT[index][newKey],true);
            this.scrollToRef(index,this.refsT[index][newKey]);
            }
            //this._refStyleRed(this.refsT[index][key]);
        }
    }

    removeError(index,key){
        let validAry = this.state.validAry;
        if(validAry[index] !== undefined && validAry[index] !== null && validAry[index][key] !== undefined && validAry[index][key] !== null){
            if(validAry[index][key] === false){
                validAry[index][key] = true;
                this.setState({validAry:validAry});
            }
        }
    }

    getDiscount(){
        if(this.state.priceData !== undefined && this.state.priceData !== null && this.state.priceData.shop_data !== undefined){
            if(this.props.auth.userdata.have_shop === 1 && this.props.auth.userdata.shop_package_id !== undefined && this.props.auth.userdata.shop_package_id !== null && this.props.auth.userdata.shop_package_id !== '' ){
                let u = this.state.priceData.shop_data[this.props.auth.userdata.shop_package_id]
                if(u !== undefined && u !== null ){
                    if(u.type_of_discount === 'discount'){
                        return u.discount_price+'%';
                    }else{
                        return u.discount_price+' €';
                    }
                }
            }
        }
    }



    doneBtn(bool,index,key){
        if(Platform.OS==='ios'){
            //console.log(' done btn '+bool);
            if(bool){
                this.setState({doneBtn:bool, refToSubmit: this.refsT[index][key]});
            }else{
                this.setState({doneBtn:bool, refToSubmit: null});
            }
        }
    }

    triggerOnSubmit(){
        let refToSubmit = this.state.refToSubmit;
        if(refToSubmit !== undefined && refToSubmit !== null ){
            let inp = refToSubmit.inputRef();
            if(inp !== undefined && inp !== null && inp.props && inp.props.onSubmitEditing ){
                inp.props.onSubmitEditing();
            }
        }
    }

    _renderSubCategoryDropDown(index,attribute,selectedParent){
        let subattr=[];
        let subID='';
        attribute.options.map((data,i)=>{
            if(data.id === selectedParent){
                subattr = data.subAttr;
                subID = data.sub_id;
            }
        });

        return subattr.length > 0 && subID!='' ? <Select vkey={selectedParent} containerStyle={{ backgroundColor: "#fff", marginTop: 5}}
                optionsList={subattr}
                header={'sélectionner'}
                selectedid={typeof this.state.listingArray[index].categoryFilterData[subID] != 'undefined' ? this.state.listingArray[index].categoryFilterData[subID]:''}
                onPress={(item) => {
                let listArray =this.state.listingArray;
                let listing = listArray[index];
                listing.categoryFilterData[subID] = item;
                listArray[index]=listing;
                this.setState({listingArray: listArray});
                }}/> : null;
    }

    _renderAnnounce(index,bool) {
        const onTintColor = "#ffbf82";
        const thumbTintColor = "#ff7e00";
        const tintColor = "#b2b2b2";
        let listPrice = this.calculatePrice(index);
        const { listingArray, priceData, tabList } = this.state;
        const Listing = listingArray[index];
        const isUserPro = this.props.auth.userdata.usertype == 'professional' ? true : false;
        this.refsT[index] = {};
        //console.log(priceData);
        return (
            <KeyboardAwareScrollView extraScrollHeight={120}  key={'announce'+index} showsVerticalScrollIndicator={false}
            contentContainerStyle={[{flexGrow: 1,alignSelf: 'stretch',backgroundColor: '#f8f8f8'},bool ? null:{height:0,width:0}]} innerRef={o=> this.ScrollView[index]=o}>
                
                <View style={{flexDirection: 'row',alignItems: 'center',justifyContent: 'flex-end'}}>
                    <Text style={[styles.orangeColor]}>{'Prix de I\ \''+tabList[index]+'  :   '}{this.getDispPrice(listPrice)}{'   |    '}</Text>
                    {index > 0 ?
                    (<Text style={[styles.globalIcon,styles.orangeColor,styles._F20_]} onPress={()=>{this.removeListing(index)}}>8</Text>):(null)}
                </View>
                {this.props.auth.userdata.have_shop == 1 ?
                <View style={{padding:5}}>
                    <Text style={[styles.orangeColor,{fontSize:10,textAlign:'center'}]}>{'Vous avez une réduction de '}{this.getDiscount()}{' sur toute annonce'}</Text>
                </View>:null}
                <View>
                    <ImageBox
                        token={this.props.auth.token}
                        ref={o => this.ImageBox[index] = o}
                        onclickAdd={()=>{this.addImage(index)}}
                        incImage={()=>this.setListingAttribute(index,{pack6:true})}
                        decImage={()=>this.setListingAttribute(index,{pack6:false})}
                        uploadImage={(li)=>this.uploadTabImage(li)}
                        isUserPro={isUserPro}
                        allowed={Listing.pack6 ? 10 : isUserPro ? 6 : 4 }/>
                </View>
                <View style={styles._MT10_}>
                    <View style={[styles.row,styles._MT5_]}>
                        <CheckBox style={[styles.genChk]} onClick={(checked) =>{
                                        this.setListingAttribute(index,{pack6:checked});
                                        if(!checked && this.ImageBox[index] !== undefined && this.ImageBox[index] !== null){
                                            this.ImageBox[index].decImage();
                                        }
                                        }}
                                        labelStyle={styles.midum} 
                                        isChecked={Listing.pack6} 
                                        rightText={'Pack '+(isUserPro ? 4 : 6 )+' photos supplementaires - '+this.getDispPrice(priceData.aditional_pic_price.price)} />
                    </View>
                    <View>
                        <View style={[styles.inlineWithico, styles.justLeft, styles._MT5_]}>
                        <CheckBox
                            style={[styles.genChk]}
                            onClick={(checked) => {
                            this.setListingAttribute(index,{isVideo:checked});
                            }}
                            labelStyle={styles.midum}
                            isChecked={Listing.isVideo}
                            rightText={'Vidéo - '+this.getDispPrice(priceData.video_price.price)}/>
                    </View>
                    {Listing.isVideo ? 
                    <View>
                        <View style={styles.inlineDatarow}>
                            <FloatLabelTextInput
                                autoCapitalize="none" 
                                autoCorrect={false}
                                ref={o=>this.refsT[index]['videoUrl']= o}
                                returnKeyType={'next'} 
                                onSubmitEditing={() => {this._setNextFocus(index,'videoUrl')}}
                                fieldContainerStyle={[styles.genInput2,this.getValidationStyle(index,'videoUrl')]}
                                placeholder={'Adresse URL YouTube'}
                                value={Listing.videoUrl}
                                onChangeTextValue={(text) => {
                                this.setListingAttribute(index,{videoUrl:text});
                                this.removeError(index,'videoUrl'); 
                            }}/>
                            <TouchableOpacity onPress={() => {this.videoUrl(index)}} activeOpacity={0.8}>
                                <Text style={[styles.globalIcon, styles.bigIco]}>h</Text>
                            </TouchableOpacity>
                        </View>
                        {this.state.showVideo[index] ? Platform.OS === 'android' ? ( <WebView style={{width: vWidth,height: vHeight}}
                                source={{html: '<html><head></head><body style="margin: 0px;"><iframe style="border: 0;border-width: 0px;" width="' + vWidth + '" height="' + vHeight + '" src="' + this.getURL(Listing.videoUrl) + '" frameborder="0" allowfullscreen></iframe></body></html>'}}/>) : <WebView style={{width: vWidth,height: vHeight,justifyContent:'center',alignSelf:'center'}}
                                source={{html: '<html><head></head><body style="margin: 0px auto;"><iframe style="border: 0;border-width: 0px;" width="800" height="500" src="' + this.getURL(Listing.videoUrl) + '" frameborder="0" allowfullscreen></iframe></body></html>'}}/>
                            : (<View style={styles.videoRow}>
                                    <Text style={{justifyContent:'center',alignSelf:'center',fontSize:16,fontWeight:'bold'}}>Pour visualiser la video, cliquer sur le stylo !</Text>
                                    <Image style={styles.videoIco} source={images.videoIco}/>
                                </View>)}
                            </View>
                            : (null)} 
                        </View>
                        </View>
                        <View style={{marginTop:10,marginBottom:10}}>
                        <TouchableOpacity onPress={() => {
                                    this.removeError(index,'avantData')
                                    this.goto('mettreAvant',index)}} style={[styles.inlineDatarow, styles.spaceBitw, styles.borderBottom,{paddingBottom:10,padding:5},this.getValidationStyle(index,'avantData')]}>
                            <TouchableOpacity activeOpacity={0.8}  onPress={() => {
                                    this.removeError(index,'avantData')
                                    this.goto('mettreAvant',index)}}>
                                <Text style={[styles._F16_, styles.linkAvant]} >Mettre en avant</Text>
                            </TouchableOpacity>
                            <Text style={[styles.globalIcon, styles.orangeColor]}>6</Text>
                        </TouchableOpacity>
                        {api.checkLogin(this.props.auth.token) && isUserPro && Listing.hideIsDirect
                            ? (<TouchableOpacity onPress={() => {
                                this.removeError(index,'directSell')
                                this.goto('EnVente',index)}} style={[styles.inlineDatarow, styles.spaceBitw, {marginTop:5,padding:5},this.getValidationStyle(index,'directSell')]}>
                                    <TouchableOpacity activeOpacity={0.8} onPress={() => {
                                this.removeError(index,'directSell')
                                this.goto('EnVente',index)}}>
                                        <Text style={[styles._F16_, styles.linkAvant]} >En Vente Directe</Text>
                                    </TouchableOpacity>
                                    <Text style={[styles.globalIcon, styles.orangeColor]}>6</Text>
                                </TouchableOpacity>)
                            : (null)}
                    </View>
                    <View style = {[styles.inlineDatarow, styles._MT10_]}>
                        <RadioButton
                        currentValue={Listing.adType}
                        value={'offers'}
                        onPress={(value)=>this.handleOnPress(index,value)}
                        Text={'OFFRE'}>
                        </RadioButton>
                        <RadioButton
                        currentValue = {Listing.adType}
                        value = {'demands'}
                        onPress = {(value)=>this.handleOnPress(index,value)}
                        Text = {'DEMANDE'}>
                        </RadioButton>
                        <RadioButton
                        currentValue = {Listing.adType}
                        value = {'trocs'}
                        onPress = {(value)=>this.handleOnPress(index,value)}
                        Text = {'TROCS'}>
                        </RadioButton>
                    </View>
                <View style = {[styles._MT10_]}>
                    <Text style={[styles.subTitle2]}>Details</Text>
                   
                    <View style={[styles.relative, styles._MT10_]}>
                       
                        
                    { Listing.adType === 'trocs' ? 
                        <View style={{flex:1,flexDirection:'row'}}>
                             <Icon name="mail-forward" color={'#34c24c'} size={20} style={{marginRight:10}}/>
                             <Text style={{color:'#34c24c'}}>Echange</Text>
                        </View>:null}
                        <View style={styles._MT10_}>
                        <FloatLabelTextInput
                            autoCapitalize="none" 
                            autoCorrect={false}
                            ref={o=>this.refsT[index]['title']= o}
                            returnKeyType={'next'} 
                            onSubmitEditing={() => {this._setNextFocus(index,'title')}}
                            fieldContainerStyle={[styles.genInput2,this.getValidationStyle(index,'title')]}
                            editIco={'h'}
                            value={Listing.title}
                            onChangeTextValue={(value) => {
                            this.setListingAttribute(index,{title:value});
                            this.removeError(index,'title');
                        }}
                            placeholder={"Choisissez un titre"}
                            underlineColorAndroid={'#eee'}/>
                            </View>
                    </View>
                    <View style = {styles._MT10_}>
                        <FloatLabelTextInput
                            autoCapitalize="none" 
                            autoCorrect={false}
                            ref={o=>this.refsT[index]['description']= o}
                            // returnKeyType={'next'} 
                            // onSubmitEditing={() => {this._setNextFocus(index,'description')}}
                            // blurOnSubmit={true}
                            multiline={true}
                            Description={true} editIco={'h'}
                            underlineColorAndroid='#eeeeee'
                            fieldContainerStyle={[styles.genInput2,this.getValidationStyle(index,'description')]}
                            value={Listing.description}
                            onChangeTextValue={(value)=>{
                                this.setListingAttribute(index,{description:value});
                                this.removeError(index,'description');
                            }}
                            numberOfLines={6}
                            placeholder={"Faites une description appropriée de l'annonce "}/>
                    </View>
                    {
                        Listing.adType === 'trocs' ? 
                        <View style = {[styles._MT10_]}>
                         <View style={{flex:1,flexDirection:'row'}}>
                             <Icon name="reply" color={'#2e5f9b'} size={20} style={{marginRight:10}}/>
                             <Text style={{color:'#2e5f9b'}}>Contre</Text>
                        </View>
                        <View style={[styles.relative, styles._MT10_]}>
                        <FloatLabelTextInput
                            autoCapitalize="none" 
                            autoCorrect={false}
                            ref={o=>this.refsT[index]['title_contre']= o}
                            returnKeyType={'next'} 
                            onSubmitEditing={() => {this._setNextFocus(index,'title_contre')}}
                            fieldContainerStyle={[styles.genInput2,this.getValidationStyle(index,'title_contre')]}
                            editIco={'h'}
                            value={Listing.title_contre}
                            onChangeTextValue={(value) => {
                            this.setListingAttribute(index,{title_contre:value});
                            this.removeError(index,'title_contre');
                        }}
                            placeholder={"Choisissez un titre"}
                            underlineColorAndroid={'#eee'}/>
                    </View>
                    <View style = {styles._MT10_}>
                        <FloatLabelTextInput
                            autoCapitalize="none" 
                            autoCorrect={false}
                            ref={o=>this.refsT[index]['description_contre']= o}
                            // returnKeyType={'next'} 
                            // onSubmitEditing={() => {this._setNextFocus(index,'description_contre')}}
                            // blurOnSubmit={true}
                            Description={true} editIco={'h'}
                            underlineColorAndroid='#eeeeee'
                            fieldContainerStyle={[styles.genInput2,this.getValidationStyle(index,'description_contre')]}
                            value={Listing.description_contre}
                            onChangeTextValue={(value)=>{
                                this.setListingAttribute(index,{description_contre:value});
                                this.removeError(index,'description_contre');
                            }}
                            numberOfLines={6}
                            placeholder={"Faites une description appropriée de l'annonce "}/>
                    </View></View>: null
                    }
                    <View style = {[styles._MT10_,this.getValidationStyle(index,'category')]}>
                        <ModalCat
                        SectionList={this.props.auth.categoryList}
                        ref={o => this.ModalCat[index] = o}
                        containerStyle={{flex: 1}}
                        onPress={(item) => {
                            this.removeError(index,'category');
                            this.selectCat(index,item)}}/></View>
                    {Listing.category !='' ? (
                        <View style = {[styles._MT10_,{borderColor:"#ddd",borderWidth:0.5,padding:7}]}>
                            {Listing.categoryLoad ? (
                                <ActivityIndicator animating={true} color={colors.navBarButtonColor} size="small"/>) : (Listing.attributeData.length > 0
                        ? (Listing.attributeData.map((attribute, i) => {
                            if (attribute.type != 'dropdown') {
                                return <View style={styles._MT10_} key={i}>
                                        <FloatLabelTextInput
                                            placeholder={attribute.name}
                                            keyboardType={attribute.type == 'number' ? 'numeric' : 'default'}
                                            fieldContainerStyle={styles.genInput2}
                                            value={Listing.categoryFilterData[attribute.id].toString()}
                                            onChangeTextValue={(value) => {
                                            let listArray =this.state.listingArray;
                                            let listing = listArray[index];
                                            listing.categoryFilterData[attribute.id] = value;
                                            listArray[index]=listing;
                                            this.setState({listingArray: listArray});
                                        }}/>
                                    </View>;
                            } else {
                                return <View key={i}>
                                    <Select vkey={i} containerStyle={{ backgroundColor: "#fff", marginTop: 5}}
                                        optionsList={attribute.options}
                                        header={attribute.name+' choisir'}
                                        selectedid={typeof Listing.categoryFilterData[attribute.id] != 'undefined' ? Listing.categoryFilterData[attribute.id]:''}
                                        onPress={(item) => {
                                        let listArray = this.state.listingArray;
                                        let listing = listArray[index];
                                        listing.categoryFilterData[attribute.id] = item;
                                        listArray[index] = listing;
                                        this.setState({listingArray: listArray});
                                    }}/>
                                    {attribute.hasSub && typeof Listing.categoryFilterData[attribute.id] != 'undefined' ? (
                                     this._renderSubCategoryDropDown(index,attribute,Listing.categoryFilterData[attribute.id])
                                    ):(null)}
                                </View>;
                            }
                        }))
                        : (
                            
                            null
                        ))

                        
                }
                {/* <View>
                                <Text>No Attributes Found</Text>
                            </View> */}
                </View>
                    ):(null)}  


                    {isUserPro ? <View style = {styles._MT10_}>
                        <FloatLabelTextInput
                            autoCapitalize="none" 
                            autoCorrect={false}
                            ref={o=>this.refsT[index]['refer']= o}
                            returnKeyType={'next'} 
                            onSubmitEditing={() => {this._setNextFocus(index,'refer')}}
                            underlineColorAndroid='#eeeeee'
                            fieldContainerStyle={[styles.genInput2,this.getValidationStyle(index,'refer')]}
                            value={Listing.refer}
                            onChangeTextValue={(value)=>{
                                this.setListingAttribute(index,{refer:value});
                                this.removeError(index,'refer');
                            }}
                            placeholder={"Référence"}/>
                            </View> :null}

                            <View style = {styles._MT10_}>
                                <View style={[styles.inlineDatarow, styles._MT10_]}>
                    <FloatLabelTextInput
                        autoCapitalize="none" 
                        autoCorrect={false}
                        ref={o=>this.refsT[index]['price']= o}
                        returnKeyType={'done'} 
                        onSubmitEditing={() => {this._setNextFocus(index,'price')}}
                        fieldContainerStyle={[styles.genInput2,this.getValidationStyle(index,'price')]}
                        placeholder={"Prix *"}
                        keyboardType={'numeric'}
                        value={Listing.price.toString()}
                        underlineColorAndroid={'#eee'}
                        onFocus={()=>{
                            this.doneBtn(true,index,'price');
                            if(Listing.price == 0){
                                this.setListingAttribute(index,{price:''});
                            }
                        }}
                        onBlur={()=>{
                            this.doneBtn(false);
                            if(Listing.price == ''){
                                this.setListingAttribute(index,{price:0});
                            }
                        }}
                        onChangeTextValue={(value) => {
                        this.setListingAttribute(index,{price:value});
                        this.removeError(index,'price');
                    }}/>
                    <CheckBox
                        style={[styles.genChk]}
                        onClick={(checked) => {
                            this.setListingAttribute(index,{isUrgent:checked});
                            }}
                        labelStyle={[styles.midum, styles.orange]}
                        isChecked={Listing.isUrgent}
                        rightText={'Option Urgent - '+this.getDispPrice(priceData.urgent_price.price)}/>
                </View>
                    <View style = {styles._MT10_}>
                        <Text style={[styles.subTitle2]}>Localisation</Text>
                        {/* <MapSelector ref = {o => this.MapSelector[index] = o} /> */}
                        <View style={[styles.inlineDatarow, styles.spaceBitw, styles._MT15_]}>
                            <Text>Adresse du compte</Text>
                            <Switch
                                onTintColor={onTintColor}
                                thumbTintColor={thumbTintColor}
                                tintColor={tintColor}
                                onValueChange={(value) => {
                                if (value) {
                                    this.setListingAttribute(index,{
                                        isAccountData:value,
                                        //username:this.props.auth.userdata.username,
                                        //mobile:this.props.auth.userdata.phone,
                                        //address:this.props.auth.userdata.address1+", "+this.props.auth.userdata.address2,
                                    });
                                    if(this.LocationSelect[index] !== null && this.LocationSelect[index] !== undefined) {
                                        this.LocationSelect[index].showCurrent(false);
                                    }
                                    //listing.username = this.props.auth.userdata.username;
                                    //listing.mobile = this.props.auth.userdata.phone;
                                } else {
                                    this.setListingAttribute(index,{isAccountData:value})
                                    //listing.username = '';
                                    //listing.mobile = '';
                                }
                                //this.setState({listing: listing})
                            }}
                                value={Listing.isAccountData}/>
                        </View>
                            <LocationSelect  
                            isEnable={true} 
                            currentLocation={true}
                            postAd={true}
                            ref = {o => this.LocationSelect[index] = o}
                            fieldContainerStyle={[styles.genInput2,this.getValidationStyle(index,'location')]}
                            authActions={this.props.authActions}
                            removeError={()=>this.removeError(index,'location')} 
                            userdata={this.props.auth.userdata} 

                            isAccount={Listing.isAccountData} />
                                <View style = {styles._MT10_}>
                                    <FloatLabelTextInput
                                autoCapitalize="none" 
                                autoCorrect={false}
                                ref={o=>this.refsT[index]['address']= o}
                                returnKeyType={'next'} 
                                onSubmitEditing={() => {this._setNextFocus(index,'address')}}
                                fieldContainerStyle={[styles.genInput2,this.getValidationStyle(index,'address')]}
                                editable={!Listing.isAccountData}
                                placeholder={"Adresse postale*"}
                                underlineColorAndroid={'#eee'}
                                value={Listing.isAccountData ? this.props.auth.userdata.address1+", "+this.props.auth.userdata.address2 : Listing.address}
                                onChangeTextValue={(value) => {
                                this.setListingAttribute(index,{address:value});
                                this.removeError(index,'address');
                            }}/>
                                </View>
                    </View>
                </View>
                </View>
                <View style={[styles._MT20_, styles._PB15_]}>
                    <Text style={[styles.subTitle2]}>Coordonnées</Text>
                    <View style={[styles.inlineDatarow, styles._MT10_]}>
                        {api.checkLogin(this.props.auth.token)
                            ? (
                                <Text style={[styles.subTitle]}>{isUserPro
                                        ? 'Professionnels'
                                        : 'Particuliers'}</Text>
                            )
                            : (null)}
                    </View>
                    <View style={styles._MT10_}>
                            {/* <FloatLabelTextInput
                                fieldContainerStyle={styles.genInput2}
                                editable={false}
                                placeholder={"Pseudo"}
                                underlineColorAndroid={'#eee'}
                                value={Listing.username}
                                onChangeTextValue={(value) => {
                                //let listing = this.state.listing;
                                //listing.username = value;
                                this.setListingAttribute(index,{username:value});
                                //this.setState({listing: listing});
                            }}/> */}
                        
                        {api.checkLogin(this.props.auth.token)
                            ? (<View style={styles._MT10_}><FloatLabelTextInput
                                fieldContainerStyle={styles.genInput2}
                                value={this.props.auth.userdata.email}
                                placeholder={"E-mail"}
                                editable={false}
                                underlineColorAndroid={'#eee'}/></View>)
                            : (null)}
                        <View style={[styles.relative,styles._MT10_]}>
                            <FloatLabelTextInput
                                autoCapitalize="none" 
                                autoCorrect={false}
                                ref={o=>this.refsT[index]['mobile']= o}
                                returnKeyType={'next'} 
                                onSubmitEditing={() => {this._setNextFocus(index,'mobile')}}
                                onFocus={()=>{this.doneBtn(true,index,'mobile')}}
                                onBlur={()=>{this.doneBtn(false)}}
                                fieldContainerStyle={[styles.genInput2,this.getValidationStyle(index,'mobile')]}
                                placeholder={"Votre téléphone "}
                                editable={!Listing.isAccountData}
                                underlineColorAndroid={'#eee'}
                                maxLength={10}
                                value={Listing.isAccountData ? this.props.auth.userdata.phone : Listing.mobile}
                                editIco={!Listing.isAccountData
                                ? 'h'
                                : ''}
                                keyboardType={'phone-pad'}
                                onChangeTextValue={(value) => {
                                this.setListingAttribute(index,{mobile:value});
                                this.removeError(index,'mobile');
                            }}/>
                        </View>
                        {/* <View style={[styles.inlineDatarow, styles.spaceBitw, styles._MT15_]}>
                            <Text>Adresse du compte</Text>
                            <Switch
                                onTintColor={onTintColor}
                                thumbTintColor={thumbTintColor}
                                tintColor={tintColor}
                                onValueChange={(value) => {
                                if (value) {
                                    this.setListingAttribute(index,{
                                        isAccountData:value,
                                        //username:this.props.auth.userdata.username,
                                        //mobile:this.props.auth.userdata.phone,
                                        //address:this.props.auth.userdata.address1+", "+this.props.auth.userdata.address2,
                                    })
                                    //listing.username = this.props.auth.userdata.username;
                                    //listing.mobile = this.props.auth.userdata.phone;
                                } else {
                                    this.setListingAttribute(index,{isAccountData:value})
                                    //listing.username = '';
                                    //listing.mobile = '';
                                }
                                //this.setState({listing: listing})
                            }}
                                value={Listing.isAccountData}/>
                        </View> */}
                        <View style={[styles.inlineDatarow, styles.spaceBitw, styles._MT15_]}>
                            <Text>Masquer mon téléphone</Text>
                            <Switch
                                onTintColor={onTintColor}
                                thumbTintColor={thumbTintColor}
                                tintColor={tintColor}
                                onValueChange={(value) => {this.setListingAttribute(index,{hideMobile:value}) }}
                                value={Listing.hideMobile}/>
                        </View>
                    </View>
                </View>

                <ActionSheet
                    ref={o => this.ActionSheets[index] = o}
                    title={this.ActionSheetOptions.title}
                    options={this.ActionSheetOptions.options}
                    cancelButtonIndex={this.ActionSheetOptions.CANCEL_INDEX}
                    destructiveButtonIndex={this.ActionSheetOptions.DESTRUCTIVE_INDEX}
                    onPress={(i)=>{this.selectAction(index,i)}}/>
                    
            </KeyboardAwareScrollView>
        );
    }

    render() {
        const {loading, listingArray,currentTab, tabList,sendLoad, mainViewHgt, keyboardOpen, doneBtn,bottom}= this.state;
        let totPrice=0.00;
        if(!loading){
            totPrice = this.calculateTotalPrice();
        }
        
        let mainStyle = {backgroundColor: "#f8f8f8", padding: 7,flex:1};
        
        // if(mainViewHgt != 0 && !loading) {
        //     mainStyle.height = mainViewHgt;
        // }else {
        //     mainStyle.flex = 1;
        // }
        // console.log(this.state)
        // console.log(mainViewHgt);

        return (
            <View 
                style={{backgroundColor: "#f8f8f8", flex: 1}}>
                {loading ? (<View style={[styles.Center]}><Loader/></View>) :(
                <View
                    onLayout={(event) => {
                    var {x, y, width, height} = event.nativeEvent.layout;
                    if(height) {
                        //console.log('on layout');
                        //console.log(event);
                        //console.log(height);
                        this.setState({mainViewHgt: height});
                    }
                }}
                 style={mainStyle}> 
                    <Tabs
                        Scroll={true}
                        ref={o => this.Tabs = o}
                        tabList={tabList}
                        onTabPress={(i) => {
                            this.checkUpload(i);
                            //this.tabchange(i+1)
                    }}/>
                    {listingArray.map((listing,index)=>{
                            if(listingArray[index] !== undefined && listingArray[index] !== null){
                                return this._renderAnnounce(index,currentTab-1 == index);
                            }else{
                                return null;
                            }
                    })}
                </View>)}
                 
                {!loading ?  (<View
                    onLayout={(event) => {
                    //var {x, y, width, height} = event.nativeEvent.layout;
                    //if(height) {
                        //this.setState({footerHgt: height});
                    //}
                    }}
                    style={[styles.bottomFixdad, {paddingLeft: 0,paddingRight: 0}]}>
                    <View  style={[styles.inlineDatarow, styles.justCenter, styles._PB5_, styles._PT10_]}>
                        <TouchableOpacity
                            style={styles.smBtn}
                            activeOpacity={0.8}
                            onPress={() => sendLoad ? null : this.addTab(listingArray.length-1)}>
                            <Text style={styles.smBtnText}>+Ajouter une annonce</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={styles.smBtn}
                            activeOpacity={0.8}
                            onPress={() => sendLoad ? null : this.submitAll()}>
                            {sendLoad ? (<ActivityIndicator size="small" color="#fff" />):(<Text style={styles.smBtnText}>Valider mes annonces</Text>)}
                        </TouchableOpacity>
                    </View>
                    <View style={styles._PB5_}>
                        <Text style={[styles._F14_, styles.center]}>Prix total de mes annonces :<Text style={[styles._F16_, styles.bold]}>{this.getDispPrice(totPrice)}{this.props.auth.userdata.have_shop == 1 ? <Text style={styles.orangeColor}>{' -'}{this.getDiscount()}</Text>:null}</Text>                
                        </Text>
                    </View>
                </View>):(null)}   

                 {doneBtn && keyboardOpen ? (<View style={{position:'absolute',backgroundColor:"#0008",flexDirection:'row',width:width,bottom:bottom,height:45,left:0}}>
                     <View style={{width:width-70,}}></View>
                     <TouchableOpacity style={{backgroundColor:colors.navBarButtonColor,width:70,padding:8,marginVertical:5,alignItems:'center',right:5,justifyContent:'center',borderRadius:5}} activeOpacity={0.8} onPress={()=>this.triggerOnSubmit()}><Text style={{color:"#fff",fontSize:16}}>{'Done'}</Text></TouchableOpacity></View>):(null)}    
            </View>
        );
    }

}

function mapStateToProps(state, ownProps) {
	return {
        chat:state.chat,
        auth:state.auth,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		chatActions: bindActionCreators(chatActions, dispatch),
		authActions: bindActionCreators(authActions, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(postad);
