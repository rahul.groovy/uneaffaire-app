
import React, { Component } from 'react';
import {
  StyleSheet,
  Image,Text,View,TextInput,TouchableOpacity,ScrollView,Switch,AsyncStorage,ActivityIndicator,Alert,Dimensions
} from 'react-native';
import images from '../../config/images';
import styles from '../../config/genStyle';
import {Navigation,Screen,SharedElementTransition} from 'react-native-navigation';
import startMain from '../../config/app-main';
import {GridItem,GridItemFull,ListItem} from '../../components/GridComponents'; 
import {Column as Col, Row} from 'react-native-flexbox-grid';
import ModalDropdown from '../../lib/react-native-modal-dropdown';
import {settings} from '../../config/settings';
import ApiHelper from '../../lib/ApiHelper/ApiHelper';
import Loader from '../../components/Loader/Loader';
import ModalCat from '../../components/ModalCat/ModalCat';
import Tabs from '../../components/Tabs/Tabs';
import { iconsMap, iconsLoaded } from '../../config/icons';
import {Icon} from '../../config/icons';
import moment from 'moment';
// redux specific
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as chatActions from '../../redux/reducers/chat';
import * as authActions from '../../redux/reducers/auth';

const api= new ApiHelper;
const { height, width } = Dimensions.get('window'); 

class favoris extends Component {
// static navigatorButtons = {
//      leftButtons: [{
//        icon: images.back,
//        id: 'back',
//        title:'back to welcome',
//      }],
// };
static navigatorStyle = {
//     navBarBackgroundColor: colors.navBarBackgroundColor,
//     navBarTextColor: colors.navBarTextColor,
//     navBarSubtitleTextColor: colors.navBarSubtitleTextColor,
//     navBarButtonColor: colors.navBarButtonColor,
//     statusBarTextColorScheme: colors.statusBarTextColorScheme,
//     statusBarColor: colors.statusBarColor,
//     tabBarBackgroundColor: colors.tabBarBackgroundColor,
//     tabBarButtonColor: colors.tabBarButtonColor,
//     tabBarSelectedButtonColor: colors.tabBarSelectedButtonColor,
//     navBarSubtitleColor: colors.navBarSubtitleColor,
//     navBarTextFontFamily: colors.navBarTextFontFamily,
    drawUnderTabBar:true,
};
  
constructor(props) {
    super(props);
    //console.log(this.props);
    this.categories  = {"1_d":"1 fois par jour", "1_w": '1 fois par semaine',"2_w":'2 fois par semaine'};
    this.state={
        tabs:1,
        tabSelected:1,
        adDeleteAlert:true,
        adPublishAlert:true,
        topListAlert:true,
        value: 0,
        iconHeart:'m',
        token:'',
        favData:[],
        favLength:0,
        shopData:[],
        shopLength:0,
        alertsData:[],
        alertsLength:0,
        searchData:[],
        searchLength:0,
        catData:[],
        catId:'',
        onBack:null,
        tabData:typeof this.props.tabData != 'undefined'? this.props.tabData:1
    };
    // if you want to listen on navigator events, set this up
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
}

componentWillMount(){    
    iconsLoaded.then(()=>{
       
    });
}
componentDidMount(){   
    let _this   = this;         
    _this.setState({load:true},()=>_this.tabchange(1));            
       // _this.getFavourite();
   
}
loadData(){
    let _this   = this;
    if(typeof this.state.tabData != 'undefined' && this.state.tabData !=''){
        _this.Tabs.setTab(_this.state.tabData-1);
        _this.setState({tabs:_this.state.tabData,tabData:_this.state.tabData},()=>_this.tabchange(_this.state.tabData));  
    }else{
        setTimeout(function() {           
            _this.setState({tabs:_this.state.tabs,tabData:_this.state.tabs},()=>_this.tabchange(_this.state.tabData));            
           // _this.getFavourite();
        }, 10); 
    }   
}

sendEventToDrawer(){
    this.props.navigator.handleDeepLink({link: 'Drawer',payload:{
        screen:this.props.testID
    }});
}
onNavigatorEvent(event) { 
    if (event.id === 'bottomTabSelected') {
        if(event.selectedTabIndex == 3){
            console.log(this.state.back);
            if(this.state.onBack == null ){
                if(!api.isProfileComplete(this.props.auth.userdata)){
                    this.setState({onBack: 0});
                }else{
                    this.setState({onBack: event.unselectedTabIndex},()=>{
                        this.props.navigator.popToRoot({
                            animated: true,
                        });
                    });
                }
                //console.log('on back go to tab '+(event.unselectedTabIndex+1));
            }
        }
    }
    if(event.id=='didAppear'){
        this.loadData();
    }        
    if(event.id == 'willAppear'){
        this.tabchange(1); 
        this.sendEventToDrawer();
        api.setAction(this.props.chatActions,this.props.authActions);
        this.props.chatActions.setScreen(this.props.testID);
        
        //console.log(this.props);
        // AsyncStorage.getItem('token').then(result=>{
        //     if(result != null ){
        //     this.setState({token:result});
        //     }
        // });
        this.props.navigator.toggleNavBar({
            to: 'shown',
            animated: true
        });
        this.props.navigator.toggleTabs({
            animated: true,
            to: 'hidden'
        });
        this.props.navigator.setStyle({navBarTitleTextCentered: true});
        this.props.navigator.setTitle({
            title: "favoris",
        }); 
        this.props.navigator.setButtons({
         leftButtons: [{
            icon: iconsMap['back'],
            id: 'back2',
            title:'back to welcome',
        }],animated:true});
        //setTimeout(function(){ 
            //this.tabchange(1);
        //},10);
    } 
    
    let _this = this
    if(event.id == 'back2'){
        if (this.state.onBack != null) {
            let onBack = this.state.onBack
            this.setState({onBack: null},()=>{
               this.props.navigator.switchToTab({
                   tabIndex: onBack // (optional) if missing, this screen's tab will become selected
               });
            });
        } else {
                this.props.navigator.switchToTab({
                    tabIndex: 0 // (optional) if missing, this screen's tab will become selected
                });
        }
    } 
}
handleOnPress(value){
    this.setState({value:value})
}
goto(page){
    this.props.navigator.push({
      screen: page,
      passProps:{
            token:this.props.auth.token, 
            userdata:this.props.auth.userdata,
        } 
    });
}
navigateTo(page,data){
    let tb = this.state.tab;
    this.setState({tabData:tb},()=>{   
        if(page=='ProductDetail'){
            this.props.navigator.push({
                screen: page,
                sharedElements: ['SharedTextId'+data.id],
                passProps:{
                    data:data,
                    token:this.props.auth.token, 
                    userdata:this.props.auth.userdata,
                }            
        });
        }else if(page== 'Singlechat'){
            this.handleChat(data);
        }else if(page=='UserAnnonces'){
            data["shop_id"] = data["id"];
            delete data["id"];
            let datas={
                UserDetail:data
            }
            this.props.navigator.push({
                screen: page,
                //sharedElements: ['SharedTextId'+data.id],
                passProps:{
                    yes:true,
                    data:datas,
                    token:this.props.auth.token,
                    userdata:this.props.auth.userdata,
                }
            });
        }
        else{
            this.props.navigator.push({
                screen: page,
                sharedElements: ['SharedTextId'+data.id],
                passProps:{
                    yes:true,
                    shop_id:data.id,
                    data:data,
                    token:this.props.auth.token,
                    userdata:this.props.auth.userdata,
                }
            }); 
        }    
    }); 
}
handleChat(data){
    if(!api.checkLogin(this.props.auth.token)){
        startMain('',{});
    }else if(this.props.auth.userdata !== undefined && this.props.auth.userdata !== null && this.props.auth.userdata.profile_complete == 0){
        this.gotoMyProfile();
    }else{
        let newData = {};
       newData.send_from_id = data.user_id;
       newData.ad = data.id;
       newData.token = this.props.auth.token;
       newData.name =  data.user_name;
       this.props.chatActions.openChat(newData);
       this.props.navigator.push({
           screen: 'Singlechat',
           passProps:{
               data:newData
           }
       });
    }
}
gotoMyProfile(){
    this.props.navigator.switchToTab({
        tabIndex:4
    });
}
getFavourite(){   
    let _this = this;
    //_this.setState({load :true});
    if(this.props.auth.token !== null && this.props.auth.token !== '') {
        let request={
            url:settings.endpoints.userFavoris,
            method:'POST',
            params:{token:this.props.auth.token,cat_id:this.state.catId}
        }  
        api.FetchData(request).then((result)=>{ 
            if(result.status){
                _this.setState({
                    load :false,favData:result.userfav.length > 0 ? result.userfav : [],
                    favLength:result.userfav.length,
                    catId:this.state.catId,
                    catData : result.favoris_category !== undefined ? result.favoris_category : [],
                }); 
            }else{
                _this.setState({
                    load :false,favData:[],favLength:0
                })
            }             
        }).catch((error)=>{
            //console.log(error);
            this.setState({load :false});
        });
    }
}
subscribe(id){
    let _this=this;
    _this.setState({load:true},()=>{
        let request={
            url:settings.endpoints.shopSubscribeUnsubscribe,
            method:'POST',
            params:{shop_id:id != 'undefined' ? id:'',token:this.props.auth.token}
          }
          api.FetchData(request).then((result)=>{
            if(result.status){
               _this.setState({load:false},()=>this.getSubscribe());
            }else{
                _this.setState({load:false});
            }
          }).catch((error)=>{
            _this.setState({load:false});
          });
    });    
   
}
getSubscribe(){    
    this.setState({catId:''},()=>{
        let _this = this;
        let request={
            url:settings.endpoints.subscribe,
            method:'POST',
            params:{token:this.props.auth.token}
        }    
        api.FetchData(request).then((result)=>{ 
            if(result.status){
                _this.setState({
                    load :false,  
                    shopData:result.subscrip_list.length > 0 ? result.subscrip_list : [],
                    shopLength:result.subscrip_list.length   
                }); 
            }else{
                _this.setState({load :false,shopData:[],shopLength:0})
            } 
        }).catch((error)=>{
            //console.log(error);
        });
    });
}
getUserAlert(){
    this.setState({load :true,catId:''},()=>{
        let _this = this;
        let request={
            url:settings.endpoints.getUserAlerts,
            method:'POST',
            params:{token:this.props.auth.token}
        }   
        api.FetchData(request).then((result)=>{
            if(result.status){
                _this.setState({
                    load :false,alertsData:typeof result.alertList != 'undefined' ?  result.alertList:[],
                    alertsLength:result.alertList.length,topListAlert:result.notificationData.top_list_alert,
                    adPublishAlert:result.notificationData.ad_publish_alert,
                    adDeleteAlert:result.notificationData.ad_delete_alert
                }); 
            }else{
                _this.setState({
                    load :false,
                    alertsLength:result.alertList.length                  
                })
            } 
        }).catch((error)=>{
            //console.log(error);
        });
    });
    
}
tabchange(tab){
    if(this.state.favLength <=0){
        this.setState({load :true});
    }    
    if(tab == 1){ 
         this.getFavourite();        
    }else if(tab == 2){
        this.getUserAlert();
    }else if(tab == 3){
        this.getUserFavsearch();
    }else if(tab == 4){
        this.setState({load:true},()=>this.getSubscribe());
    }
    this.setState({tabs:tab,tabSelected:tab});
}
getUserFavsearch(){
    this.setState({load :true,catId:''},()=>{
        let _this = this;
        let request={
            url:settings.endpoints.getUserFavsearch,
            method:'POST',
            params:{token:this.props.auth.token}
        }   
        api.FetchData(request).then((result)=>{        
            if(result.status){
                _this.setState({
                    load :false,
                    searchData:result.searchList != undefined ? result.searchList : [],
                    searchLength:result.searchList.length != undefined ? result.searchList.length :0
                }); 
            }else{
                _this.setState({
                    load :false,    
                    searchLength:0              
                })
            } 
        }).catch((error)=>{
            //console.log(error);
        });
    });
    
}
heartchange(id){
    this.setState({load :true},()=>{
        let _this = this;
        let request={
            url:settings.endpoints.favUnfavAd,
            method:'POST',
            params:{token:this.props.auth.token,id:id}
        }
        api.FetchData(request).then((result)=>{
            if(result.status){
                 _this.tabchange(1);       
            }           
        }).catch((error)=>{
            //console.log(error);
        });  
    });
};
changeNotiAlert(value,type){
    let _this = this;   
    let request={
        url:settings.endpoints.saveUserNotiAlert,
        method:'POST',
        params:{token:this.props.auth.token,name:type}
    }
    if(type == 'ad_publish_alert'){
        _this.setState({adPublishAlert:value == true ? true : false});
    }else if(type == 'ad_delete_alert'){
        _this.setState({adDeleteAlert:value == true ? true : false});
    }else if(type == 'top_list_alert'){
        _this.setState({topListAlert:value == true ? true : false});
    }
    api.FetchData(request).then((result)=>{
                  
    }).catch((error)=>{
        //console.log(error);
    });  
}
changeAlertStatus(status,key){
    let _this = this; 
    let markers = this.state.alertsData;
    markers[key].status = status == 0 ? 1 : 0;
    this.setState({markers});
    let request={
        url:settings.endpoints.changeUserAlertStatus,
        method:'POST',
        params:{token:this.props.auth.token,id:markers[key].id}
    }
    api.FetchData(request).then((result)=>{
        //console.log(result);
    }).catch((error)=>{
        //console.log(error);
    }); 
}
deleteAlert(key){    
    let _this = this;    
    Alert.alert(
    'effacer!!',
    'Êtes-vous sûr',
    [
        {text: 'Non', onPress: () => '', style: 'cancel'},
        {text: 'Oui', onPress: () => _this.deleteAl(key)},
    ],
    { cancelable: false }
    )     
}
deleteAl(key){
    let _this = this;    
    let markers = this.state.alertsData;
    let index = markers.indexOf(markers[key]);      
    let request={
        url:settings.endpoints.deleteUserAlert,
        method:'POST',
        params:{token:this.props.auth.token,id:markers[key].id}
    }
    api.FetchData(request).then((result)=>{
        if(result.status){
            if (index > -1) {
                markers.splice(index, 1);
                _this.setState({markers});
            }
        }else{
            alert('Somthing Went Wrong');
        }
    }).catch((error)=>{
        //console.log(error);
    });
}
deleteSearch(key){
    let _this = this;    
    Alert.alert(
    'effacer!!',
    'Êtes-vous sûr',
    [
        {text: 'Non', onPress: () => '', style: 'cancel'},
        {text: 'Oui', onPress: () => _this.deleteSe(key)},
    ],
    { cancelable: false }
    )
}
deleteSe(key){
    let _this = this;    
    let markers = this.state.searchData;
    let index = markers.indexOf(markers[key]);      
    let request={
        url:settings.endpoints.deleteUserFavSearch,
        method:'POST',
        params:{token:this.props.auth.token,id:markers[key].id}
    }
    api.FetchData(request).then((result)=>{
        if(result.status){
            if (index > -1) {
                markers.splice(index, 1);
                _this.setState({markers});
                _this.tabchange(3);
            }
        }else{
            alert('Somthing Went Wrong');
        }
    }).catch((error)=>{
        //console.log(error);
    });
}
changeUserAlertFreq(id,cat){
    let _this = this;
    _this.setState({load:true},()=>{
        let request={
            url:settings.endpoints.changeUserAlertFreq,
            method:'POST',
            params:{token:this.props.auth.token,id:id,freq:cat}
        }
        api.FetchData(request).then((result)=>{
            if(result.status){
               _this.setState({load:false})
               _this.tabchange(2);
            }else{
                alert('Somthing Went Wrong');
            }
        }).catch((error)=>{
            //console.log(error);
        });
    });
}
 
getCat(i){
    this.setState({
        catId:i
    },()=>this.getFavourite());
}
edit(page,id){
    if(page == 'alert'){
        this.props.navigator.push({
        screen: 'CreatAlert',
        passProps:{
                token:this.props.auth.token,
                userdata:this.props.auth.userdata,
                id:id
            } 
        });
    }else if(page == 'favSearch'){
        this.props.navigator.push({
        screen: 'CreatResearch',
        passProps:{
                token:this.props.auth.token, 
                userdata:this.props.auth.userdata,
                id:id
            } 
        });
    }        
}
_renderSharedElement(){
    let ret =[];  
    if(this.state.tabs != 4 && this.state.shopLength > 0){            
        this.state.shopData.map((item,key)=>{ 
           ret.push(<View key={'hidden'+item.id} style={{height:0,width:0}}>
                    <SharedElementTransition sharedElementId={'SharedTextId'+item.id}>
                        <Image style={[styles.proImg,styles.proImgsmall]} source={{uri:item.shop_image}} />
                    </SharedElementTransition>
                </View>);
        });
    }
    if(this.state.tabs != 1 && this.state.favLength > 0 ){
        this.state.favData.map((item,key)=>{ 
           ret.push(<View key={'hidden'+item.id} style={{height:0,width:0}}>
                    <SharedElementTransition sharedElementId={'SharedTextId'+item.id}>
                        <Image style={[styles.proImg,styles.proImgsmall]} source={{uri:item.single_image}} />
                    </SharedElementTransition>
                </View>);
        });
    }
    
    if(ret.length > 0){
        return ret;
    }else{
        return null;
    }
}

goOnGrid = (dt) => {
    let obj={};
    obj.search_string=dt.keyword;
    obj.search_in_title=false;
    obj.search_urgent=false;
    obj.category_id=dt.category;
    obj.ad_type=dt.ad_type;
    obj.category_filter=typeof dt.filter != 'undefined' ? dt.filter.filter_data:{};
    obj.location=typeof dt.location != 'undefined' ? dt.location:{};
    this.props.navigator.push({
        screen: 'ProductgridContainer',
        passProps:{
            token:this.props.auth.token,
            userdata:this.props.auth.userdata, 
            params:obj,
            filterType:'advance'       
        }
    });
}

render() {
    let _this = this;
    let searchDetails = _this.state.searchLength > 0 ? (
        _this.state.searchData.map(function (item,key) {
           return (  <TouchableOpacity key={key+200} onPress={()=>{
            _this.goOnGrid(item);
           }} style={[styles.inlineDatarow,styles.favorisCard,styles._MT0_]}>
                <View style={[styles.row]}>
                    <View style={styles._MR10_}>
                        <Text style={[styles.favouritesTitle,styles.bold,styles._PB5_]}>{item.name}</Text> 
                        <Text style={[styles.contentfavCard,styles._FFM_]}>{item.fav_search_date}</Text> 
                        <Text style={[styles.contentfavCard,styles._FFM_]}>{item.summary}</Text> 
                    </View>
                </View> 
                <View style={[styles.inlineDatarow]}>
                    <TouchableOpacity onPress={()=>_this.goOnGrid(item)} style={[styles.btnSquare]} activeOpacity={0.8}>
                        <Icon name="eye" size={22} color={'#f15a23'}/>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>_this.edit('favSearch',item.id)}  style={[styles.btnSquare,styles._BR0_]} activeOpacity={0.8}>
                        <Text style={[styles.globalIcon,styles.orangeColor,styles.xlIco]}>h</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>_this.deleteSearch(key)} style={[styles.btnSquare]} activeOpacity={0.8}>
                        <Text style={[styles.globalIcon,styles.orangeColor,styles.xlIco]}>8</Text>
                    </TouchableOpacity>
                   
                </View>
            </TouchableOpacity>)
        })
        ):<View style={[styles._MT15_,styles.Center]}><Text>Aucune donnée disponible</Text></View>;;
    let suscribeDetails =
    this.state.shopLength > 0 ? ( 
    this.state.shopData.map(function (item,key) {
    return ( 
            <Row size={12} key={100+key} style={[styles.proRow,styles.prolistRow,styles._MT0_,styles._PT0_]}>
                        <Col sm={5} md={3} lg={2} style={[styles.proCol,styles.listCol]} >
                            <View style={styles.colInner}>
                                <TouchableOpacity style={styles.imageCanvas}  onPress={()=>_this.navigateTo('UserAnnonces',item)}>
                                    <View style={styles.imgInner}> 
                                        <SharedElementTransition sharedElementId={'SharedTextId'+item.id}>
                                                <Image style={[styles.proImg,styles.proImgsmall]} source={{uri:item.shop_image}} />
                                        </SharedElementTransition>
                                    </View>
                                </TouchableOpacity>
                                <View style={[styles.bottomDetail,styles.bottomCaption,styles.relativeCaption]}>
                                    <Text style={[styles.leftAlign,styles.white]}>
                                        <Text style={styles.small}>{item.categoryname}</Text>
                                    </Text>
                                    {item.type == 'professional' &&
                                     <Image style={[styles.proSign,styles.proSignAbsolute,{height:15,}]} source={images.pro}  />}
                                </View>
                            </View>     
                        </Col>
                        <Col sm={7} md={9} lg={10} style={[styles.proCol,styles.listCol]}>
                            <TouchableOpacity style={styles.proCaption} onPress={()=>_this.navigateTo('UserAnnonces',item)}>
                                <Text style={[styles.proName,styles.listingProname,styles._PB10_]} numberOfLines={1}>{item.shopname}</Text>
                                <View style={styles.row}>
                                    <View style={[styles.inlineDatarow,styles._PB10_]}>
                                        <View style={styles.inlineWithico}> 
                                            <Text style={[styles.orangeColor,styles.borderRight,styles.bold,styles._MPR5_]}>{item.department}</Text>
                                            <Text style={[styles.orangeColor,styles.bold,styles.midum]}>{item.town}</Text> 
                                        </View>
                                    </View> 
                                    
                                    <View style={[styles.row,styles._PB10_]}>
                                        <Text style={[styles.orangeColor,styles.bold,styles.midum]}>{item.total_ad_count} annonces</Text>  
                                    </View>
                                    <View style={[styles.inlineDatarow,]}>
                                        <View style={[styles.inlineDatarow,styles._MR10_]}> 
                                            <TouchableOpacity style={[styles.brandColor,styles._MR10_,styles._PL20_,styles._PR20_]} activeOpacity={0.8} 
    onPress={() => _this.subscribe(item.id)}>
        <Text style={[styles.genButton,styles.midum,styles.smbtn,styles._F15_]}> Se désabonner</Text>
                                            </TouchableOpacity>
                                        </View> 
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </Col>
                    </Row>)
                    })
        ):<View style={[styles._MT15_,styles.Center]}><Text>Aucune donnée disponible</Text></View>;;
    let alertDetails =  this.state.alertsLength > 0 ? ( 
          contents =  this.state.alertsData.map(function (item,key) {
            return (  <Col sm={12} md={6} lg={4} style={[styles.notiCol]} key={key}>
                            <View style={[styles._MT15_,styles.cardNotiAlert]}>
                            <View style={[styles.inlineDatarow,styles.spaceBitw,styles._PB10_]}>
                                <Text style={styles.notiCardTitle}>{item.name}</Text>
                                <Text style={styles.notiCardDate}>{item.alert_date}</Text>
                            </View>
                            <View style={[styles.inlineDatarow,styles.spaceBitw]}>
                                <Switch style={styles.genSwitch} onValueChange={()=>_this.changeAlertStatus(item.status,key)}  value={item.status == 1 ? true :false} />
                                <View style={[styles.relative,styles._w120_,styles.dropBorder,styles._MT5_]}>
                                     <ModalDropdown defaultValue={_this.categories[item.alert_freq]}  options={_this.categories}  style={[styles.dropDown]} textStyle={[styles.dropdown_2_text,styles._F14_,styles.grayColor]} dropdownStyle={{height:150,width:200}} onSelect={(value)=>_this.changeUserAlertFreq(item.id,value)}/>
                                    <Text style={[styles.globalIcon,styles.dropabsIco]}>g</Text> 
                                   
                                </View>
                                <View style={styles.dataRow}>
                                <TouchableOpacity style={[styles.icNoti,styles._BR1_]} onPress={()=>_this.edit('alert',item.id)} activeOpacity={0.8}>
                                    <Text style={[styles.globalIcon,styles.orangeColor,styles._F20_]}>h</Text>
                                </TouchableOpacity> 
                                <TouchableOpacity style={styles.icNoti} onPress={()=>_this.deleteAlert(key)}  activeOpacity={0.8}>
                                    <Text style={[styles.globalIcon,styles.orangeColor,styles._F20_]}>8</Text>
                                </TouchableOpacity>
                            </View>
                            </View>
                            </View>
                        </Col>)
                    })
        ):<View style={[styles._MT15_,styles.Center]}><Text>Aucune donnée disponible</Text></View>;;
     let favDetails =  
      this.state.favLength > 0 ? ( 
          contents =  this.state.favData.map(function (data,i) {
            //let dataDate = moment(item.post_on).format("DD MMM [|] hh:mm a");
           // let _this = this;         
                return ( 
                    <ListItem data={data} vkey={i} key={i}
                        onPress={(data)=>{_this.navigateTo('ProductDetail',data)}}
                        onFav={(i,fav)=>{_this.heartchange(data.id)}}
                        currentUser={api.checkLogin(_this.props.auth.token) ? _this.props.auth.userdata:{id:'',token:''}}
                        onChat={(id)=>{_this.navigateTo('Singlechat',data)}}
                        searchType = {data.ad_type}
                    />
                 )
            })
        ):<View style={[styles._MT15_,styles.Center]}><Text>Aucune donnée disponible</Text></View>;;
     if(this.state.tabs == 1){         
        myAccountData = 
        <ScrollView>
                <View style={styles.scrollInner}>
                    <View>
                        <View>
                            <Text style={styles.blockTitle}>Vos Favoris</Text>
                        </View>
                        <View style={styles.accountdataBlock}> 
                             <ModalCat SectionList={this.state.catData} onPress={(item)=>_this.getCat(item)}/>
                        </View>                        
                        <View style={[styles.accountdataBlock]}>
                            <View style={styles.proMain}>
                                   {this.state.favLength > 0 ? favDetails :<View style={[styles._MT15_,styles.Center]}><Text>Aucune donnée disponible</Text></View>}
                            </View>
                        </View>
                    </View>
                </View>
        </ScrollView>
       
     }else if(this.state.tabs == 2){
        myAccountData = 
            <ScrollView showsVerticalScrollIndicator={false} style={styles.mainScollview}>
                <View style={styles.scrollInner}>
                    <View style={styles.myAccountData}>
                        <View style={styles.accountdataBlock}>
                            <Text style={styles.blockTitle}>Gérer mes alertes de notifications Email</Text>
                            <View style={styles.blockmetter}>
                                <View style={[styles.rowBtnMain,styles.rowMesalerts]}>
                                    <Text style={styles.labelText}>Une annonce a été publiée</Text>    
                                     <Switch style={styles.genSwitch}  onValueChange={(value) => _this.changeNotiAlert(value,'ad_publish_alert')} value={_this.state.adPublishAlert == 1 ? true : false} />     
                                </View>                                
                                <View style={[styles.rowBtnMain,styles.rowMesalerts]}>
                                    <Text style={styles.labelText}>une annonce a été supprimée</Text>   
                                     <Switch style={styles.genSwitch}  onValueChange={(value) => _this.changeNotiAlert(value,'ad_delete_alert')}  value={_this.state.adDeleteAlert == 1 ? true : false} />
                                </View>                                                         
                                <View style={[styles.rowBtnMain,styles.rowMesalerts]}>
                                    <Text style={styles.labelText }>une annonce a été remontée en tête de liste</Text>   
                                     <Switch style={styles.genSwitch}  onValueChange={(value) => _this.changeNotiAlert(value,'top_list_alert')}  value={_this.state.topListAlert == 1 ? true : false} />
                                </View> 
                            </View>
                        </View>
                       
                        <View style={[styles.accountdataBlock,styles.infoForm]}>
                             <Text style={styles.blockTitle}>Gérer mes alertes Email</Text>
                              <TouchableOpacity style={[styles.brandColor,styles._w150_]} onPress={()=>{this.goto('CreatAlert')}} activeOpacity={0.8}>
                                 <Text style={[styles.genButton,styles.smallBtn]}>Créer une alerte</Text>
                              </TouchableOpacity>
                        <Row size={12} style={[styles.cardNotiRow]}>
                            {alertDetails}                           
                        </Row>
                        </View>
                    </View>
                </View>
            </ScrollView>
     }else if(this.state.tabs == 3){
        myAccountData = 
            <ScrollView showsVerticalScrollIndicator={false} style={styles.mainScollview}>
                <View style={styles.scrollInner}>
                    <View style={styles.myAccountData}>
                        <View style={[styles.accountdataBlock]}>
                            <Text style={styles.blockTitle}>Vos recherches Préférées</Text>
                            <View style={styles.row}>
                                <TouchableOpacity style={[styles._MR10_]} activeOpacity={0.8} >
                                    <Text style={[styles.smallBtn]} onPress={()=>{this.goto('CreatResearch')}}>Créer une recherche préférée</Text>
                                </TouchableOpacity>
                            </View>                            
                        </View>
                        <View style={styles.accountdataBlock}>
                             {searchDetails}                             
                        </View>
                    </View>
                </View>
            </ScrollView>
     }else if(this.state.tabs == 4){
         myAccountData = 
         <ScrollView>
                <View style={styles.scrollInner}>
                    <View style={styles.myAccountData}>
                        <View style={[styles.accountdataBlock]}>
                            <View style={styles.proMain}>
                                  {suscribeDetails}
                            </View>
                        </View>
                    </View>
                </View>
        </ScrollView>
     }else{
       null
     }
    return (
       <View style={[styles.main1,{padding:7,paddingTop:7}]}>
            {this.state.catId == '' ? this._renderSharedElement():null}
            <Tabs defaultTabIndex={0} tabList={['Gérer mes \n favoris','Gérer mes\n alertes','Recherches \npréférées','Vos \nabonnements']}  onTabPress={(i)=>{this.tabchange(i+1)}} ref={o => this.Tabs = o}/>
            {/* <View style={styles.tabParent}>
                <View style={styles.tabs}>
                    <TouchableOpacity  style={(this.state.tabSelected== 1)?[styles.tabSelected,styles.widthQuater]:[styles.tabnotSelected,styles.widthQuater]} onPress={()=>{this.tabchange(1)}} activeOpacity={0.8}>
                        <Text style={(this.state.tabSelected== 1)?[styles.tabTextSelected,styles.orangeColor]:[styles.tabText]}>{'Gerers mes'}{"\n"}{'favoris'}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity  style={(this.state.tabSelected== 2)?[styles.tabSelected,styles.widthQuater]:[styles.tabnotSelected,styles.widthQuater]} onPress={()=>this.tabchange(2)} activeOpacity={0.8}>
                        <Text style={(this.state.tabSelected== 2)?[styles.tabTextSelected,styles.orangeColor]:[styles.tabText]}>{'Gerer mes'}{"\n"}{'alertes'}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity  style={(this.state.tabSelected== 3)?[styles.tabSelected,styles.widthQuater]:[styles.tabnotSelected,styles.widthQuater]} onPress={()=>this.tabchange(3)} activeOpacity={0.8}>
                        <Text style={(this.state.tabSelected== 3)?[styles.tabTextSelected,styles.orangeColor]:[styles.tabText]}>{"Recherches"}{"\n"}{'préférées'}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity  style={(this.state.tabSelected== 4)?[styles.tabSelected,styles.widthQuater]:[styles.tabnotSelected,styles.widthQuater]} onPress={()=>this.tabchange(4)} activeOpacity={0.8}>
                        <Text style={(this.state.tabSelected== 4)?[styles.tabTextSelected,styles.orangeColor]:[styles.tabText]}> {'Vos'}{"\n"}{'abonnements'}</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.botomOrangeLine}></View>
            </View> */}
              {this.state.load ?<View style={[styles.Center]} ><Loader/></View>:myAccountData}
       </View>
    );
  }
}

function mapStateToProps(state, ownProps) {
	return {
        chat:state.chat,
        auth:state.auth,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		chatActions: bindActionCreators(chatActions, dispatch),
		authActions: bindActionCreators(authActions, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(favoris);