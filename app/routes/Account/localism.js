import React, { Component } from 'react';
import {
  StyleSheet,
  Image,Text,View,TextInput,TouchableOpacity,ScrollView,Dimensions,Platform,Animated,WebView
} from 'react-native';
import { colors } from '../../config/styles';
import images from '../../config/images';
import styles from '../../config/genStyle';
import {Column as Col, Row} from 'react-native-flexbox-grid';
import { iconsMap, iconsLoaded } from '../../config/icons';
const HEADER_MAX_HEIGHT = 100;
const HEADER_MIN_HEIGHT = Platform.OS === 'ios' ? 50 : 50;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;

export default class Localism extends Component {
//   static navigatorButtons = {
//         leftButtons: [{
//             icon: images.back,
//             id: 'back',
//             title:'back to welcome',
//         }],
//         rightButtons: [{   
//             icon:images.check,
//             id: 'right',
//             title:'right',
//         }],
//   };

  
// static navigatorStyle = {
//     navBarBackgroundColor: colors.navBarBackgroundColor,
//     navBarTextColor: colors.navBarTextColor,
//     navBarSubtitleTextColor: colors.navBarSubtitleTextColor,
//     navBarButtonColor: colors.navBarButtonColor,
//     statusBarTextColorScheme: colors.statusBarTextColorScheme,
//     statusBarColor: colors.statusBarColor,
//     tabBarBackgroundColor: colors.tabBarBackgroundColor,
//     tabBarButtonColor: colors.tabBarButtonColor,
//     tabBarSelectedButtonColor: colors.tabBarSelectedButtonColor,
//     navBarSubtitleColor: colors.navBarSubtitleColor,
//     navBarTextFontFamily: colors.navBarTextFontFamily,
//     navBarHidden: false,
//     drawUnderTabBar:true
// };

constructor(props) {
    super(props);
    // if you want to listen on navigator events, set this up
    this.state={
        check1:false,
        tabs:1,
        tabSelected:1,
        like:1,
        scrolled:true,
        scrollY: new Animated.Value(0),
    };
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
}

componentWillMount(){
    iconsLoaded.then(()=>{});
}

 onNavigatorEvent(event) {
    //console.log(event)
     if(event.id=='willAppear'){
        this.props.navigator.toggleNavBar({
      to: 'shown',
      animated: true
    });
    this.props.navigator.toggleTabs({
        animated: true,
        to: 'hidden'
    });
    this.props.navigator.setStyle({navBarTitleTextCentered: true});
    this.props.navigator.setTitle({
      title: "Localisation",
      id:"Localisation"
    });

    this.props.navigator.setButtons({
         leftButtons: [{
            icon: iconsMap['back'],
            id: 'back2',
            title:'back to welcome',
        }],
        rightButtons: [{   
            icon:iconsMap['back'],
            id: 'right',
            title:'right',
        }],animated:false})
     }
     if(event.id=='didAppear'){

     }
    
    if(event.id == 'back2'){
      this.props.navigator.pop({
        animated: true // does the pop have transition animation or does it happen immediately (optional)
      });
    } 
    
    // if (event.type == 'DeepLink') {
    //         const parts = event.link;
    //             if (typeof parts != 'undefined') {
    //                 this.props.navigator.push({
    //                     screen: parts,
    //             });
    //         }
    // }
  }
  goto(page){   
        this.props.navigator.push({
        screen: page
        }); 
  }
  tabchange(tab){
        this.setState({tabs:tab,tabSelected:tab});
  }


  onScroll(event){
        var currentOffset = event.nativeEvent.contentOffset.y;
        var direction = currentOffset > this.offset ? this.setState({scrolled:true}) : this.setState({scrolled:false});
        this.offset = currentOffset;       
}

gettrans(bool){
if(bool){
      return  this.state.scrollY.interpolate({
        inputRange: [0, 50],
        outputRange: [0, -50],
        extrapolate: 'clamp',
    });
    }else{
       return  this.state.scrollY.interpolate({
        inputRange: [0, 50],
        outputRange: [-50, 0],
        extrapolate: 'clamp',
    });  
    }
}
  render() {
    
    const headerTranslate =  this.state.scrollY.interpolate({
            inputRange: [0, 50],
            outputRange: [0, -50],
            extrapolate: 'clamp',
        });
    return (
        <ScrollView showsVerticalScrollIndicator={false} style={[styles.mainScollview,styles._PT5_]}>
            <View style={[styles.scrollInner]}>
                    <View style={styles.MapMain}>

                    </View>
            </View>
        </ScrollView>
    );
  }

}






