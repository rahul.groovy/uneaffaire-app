
import React, { Component } from 'react';
import {
  StyleSheet,
  Image,Text,View,Picker,TextInput,TouchableOpacity,ScrollView,Switch,Modal,AsyncStorage,ActivityIndicator,Dimensions,Platform
} from 'react-native';
import _ from 'lodash';
import images from '../../config/images';
import styles from '../../config/genStyle';
import FloatLabelTextInput from '../../lib/react-native-floating-label-text-input';
import {settings} from '../../config/settings';
import ApiHelper from '../../lib/ApiHelper/ApiHelper';
import DropdownAlert from 'react-native-dropdownalert';
import Loader from '../../components/Loader/Loader';
import LoaderButton from '../../components/LoaderButton/LoaderButton';
import {tryCamera} from '../../redux/utils/location';
import { iconsMap, iconsLoaded } from '../../config/icons';
import ModalCat from '../../components/ModalCat/ModalCat';
import startMain from '../../config/app-main';
import LocationSelect from '../../components/LocationSelect/LocationSelect';
import ImagePicker from 'react-native-image-crop-picker';
import { ActionSheetCustom as ActionSheet } from 'react-native-actionsheet';
// import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import CKeyboardSpacer from '../../components/CKeyboardSpacer';
// redux specific
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as chatActions from '../../redux/reducers/chat';
import * as authActions from '../../redux/reducers/auth';
const api= new ApiHelper;
const { height, width } = Dimensions.get('window');

class createShop extends Component {
    static navigatorStyle = {
        drawUnderTabBar: true,
    };
  constructor(props) {
    super(props);
    this.ActionSheetOptions = {
        CANCEL_INDEX: 2,
        DESTRUCTIVE_INDEX: 2,   
        options: [],
    };
    this.state={
        backimage:'',
        Userdetails:typeof this.props.UserDetail != undefined ? this.props.UserDetail:'',
        title:'',
        desc:'',
        slogan:'',
        siren:'',
        workSche:'',
        catId:'',
        parentId: '',
        siteLink:'',
        fileName:'',
        uri:'',
        load1:false,
        imageFile : {},
        shopData:{},
        options:[ 
            <Text style={styles.actionName}>{'Prendre une photo'}</Text>,
            <Text style={styles.actionName}>{'À partir de la bibliothèque'}</Text>,
            <Text style={styles.actionName}>{'Annuler'}</Text>
        ]
    };
    // if you want to listen on navigator events, set this up
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }
componentDidMount(){ 
    typeof this.props.id != 'undefined' && this.props.id !=  '' ? this.getShopData() :null;
}
getShopData(){
    let _this=this;
    _this.setState({load:true},()=>{
        let request={
            url:settings.endpoints.shopDetails,
            method:'POST',
            params:{shop_id:typeof this.props.id != 'undefined' ? this.props.id:'',token:this.props.auth.token,edit:true} 
          }
          api.FetchData(request).then((result)=>{
            if(result.status){
              _this.setState({
                  load:false,title:result.shop.shopname,backimage:result.shop.shop_image,desc:result.shop.description,
                  shopData : result.shop,
                  siteLink:result.shop.website_url,slogan:result.shop.slogan,siren:result.shop.siret,workSche:result.shop.opening_time,catId:result.shop.category,
                  parentId:result.shop.parent_category
              },()=>{
                  this.ModalCat.setSelectedCat(result.shop.category_name);
                  let obj = {
                      cityzip:result.shop.postal_code,
                      department:result.shop.department,
                      department_slug:result.shop.department_slug,
                      lat:result.shop.lat,
                      long:result.shop.long,
                      region:result.shop.region,
                      region_slug:result.shop.region_slug,
                      town:result.shop.town,
                      town_slug:result.shop.town_slug,
                      zipcode:result.shop.zip,
                      isCurrent:false
                  }
                  if(result.shop.zip != '' && result.shop.town != ''){
                      this.LocationSelect.setLocationData(obj);
                  }  
              });
            }else{
              _this.setState({
                  load:false
              }); 
            }
          }).catch((error)=>{
              console.log(error);
            _this.setState({
                    load:false
                });
          });
    });
    
}
componentWillMount(){
    iconsLoaded.then(()=>{});
}
setModalVisible(visible) {
    this.setState({modalVisible: visible});
}
sendEventToDrawer(){
    this.props.navigator.handleDeepLink({link: 'Drawer',payload:{
        screen:this.props.testID
    }});
}
onNavigatorEvent(event) {
      if(event.id=='willAppear'){       
         this.sendEventToDrawer();
         api.setAction(this.props.chatActions,this.props.authActions);
         this.props.chatActions.setScreen(this.props.testID);
         //console.log(this.props);
         this.props.navigator.toggleTabs({
            animated: true,
            to: 'hidden'
        });
        this.props.navigator.setStyle({navBarTitleTextCentered: true});
        this.props.navigator.setTitle({title: "Gérer votre magasin"});
        this.props.navigator.setButtons({
            leftButtons: [{
            icon: iconsMap['back'],
            id: 'back2',
            title:'',
            }],
        });
      }
    if(event.id=='didAppear'){
        
    }
    if(event.id == 'back2'){
      this.props.navigator.pop({
        animated: true // does the pop have transition animation or does it happen immediately (optional)
      });
    }
} 
goto(page){
    this.props.navigator.push({
      screen: page
    });
}
changePass(){
    
}
doAction(i) {
    if (i === 0) {
        this.openCamera();
    } else if (i === 1) {
        this.imageClick();
    }
}  

showActionSheet() {
    tryCamera().then((result) => {
        if (this.ActionSheet && this.ActionSheet.show) {
            this.ActionSheet.show();
        }
    }).catch((e)=>{
        console.log(e);
    }) 
    
}

openCamera = () => {
    ImagePicker.openCamera({
        width: 1200,
        height: 1200,
        cropping: false,
        mediaType: "photo",
        compressImageQuality : 0.7
    }).then((image) => {
        let newArray = {
            uri: image.path,            
            name:image.path.substr(image.path.lastIndexOf('/') + 1),           
            type: image.mime,
        }
    // console.log(newArray);
        this.setState({backimage:image.path,imageFile:newArray}); 
    }).catch(error => console.log('cancel'));
}

imageClick(){ 
    /**
     * The first arg is the options object for customization (it can also be null or omitted for default options),
     * The second arg is the callback which sends object: response (more info below in README)
     */
    tryCamera().then((result) => {
        if(result){
            this.getCamera();
        }
    }).catch((e)=>{
        console.log(e);
    })   
}
getCamera(){ 
    ImagePicker.openPicker({
        width: 1200,
        height: 1200,
        cropping: false,
        mediaType: "photo",
        compressImageQuality : 0.7
    }).then(image => {
        // console.log(image);
        let newArray = {
            uri: image.path,            
            name:image.path.substr(image.path.lastIndexOf('/') + 1),           
            type: image.mime,
        }
        // console.log(newArray);
        this.setState({backimage:image.path,imageFile:newArray});  
    }); 
}
checkValid(){
    const {title,desc,workSche,siren,slogan,siteLink,catId} = this.state;
    // console.log(this.state);
    let locData = this.LocationSelect.getAddress();
    // return false;
    if(title === undefined || title === null || title === ''){
        this.dropdown.alertWithType('error', 'Erreur', 'Entrez le titre');
        return false;
    }else if(desc === undefined || desc === null || desc === ''){
        this.dropdown.alertWithType('error', 'Erreur', 'Entrez le La description');
        return false;
    }else if(workSche === undefined || workSche === null || workSche === ''){
        this.dropdown.alertWithType('error', 'Erreur', 'Entrez les heures d\'ouverture');
        return false;
    }
    // else if(siren === undefined || siren === null || siren === ''){
    //     this.dropdown.alertWithType('error', 'Erreur', 'Entrez le N\'Siren No');
    //     return false;}
    else if(slogan === undefined || slogan === null || slogan === ''){
        this.dropdown.alertWithType('error', 'Erreur', 'Entrez le slogan');
        return false;
    }
    // else if(siteLink === undefined || siteLink === null || siteLink === ''){
    //     this.dropdown.alertWithType('error', 'Erreur', 'Entrez l\'URL Web');
    //     return false;
    // }
    else if(catId == 0 || catId === '' || catId === undefined || catId === null){
        this.dropdown.alertWithType('error', 'Erreur', 'Entrez la catégorie');
        return false;
    } else if(_.isObject(locData) && !_.isEmpty(locData) && (locData.zipcode === '')){
        this.dropdown.alertWithType('error', 'Erreur', 'Entrez la postal/ville');
        return false;
    }else{
        // console.log(catId);
        // return false;
        this.saveData();
    }

}
saveData(){
    const {imageFile,shopData} = this.state;
    let _this = this;
    console.log(this.state.Userdetails);
    // return false;
   _this.setState({
       // load1 : true
    },()=>{
        const query = new FormData();
        let locData = this.LocationSelect.getAddress();
        console.log(locData);
        var params = { 
            token:this.props.auth.token,
            shop_id:typeof this.props.shop_package_id != 'undefined' ? this.props.shop_package_id:this.props.id,
            category:typeof this.state.catId != 'undefined' ? this.state.catId : this.props.category,
            title:this.state.title,
            description:this.state.desc,
            open_time:this.state.workSche,
            // siret_num:this.state.siren,
            slogan:this.state.slogan,
            site_link:this.state.siteLink,
            days:typeof this.props.days != 'undefined' ? this.props.days:'', 
            name : !_.isEmpty(shopData) && shopData.name !== '' ? shopData.name : this.state.Userdetails.name,
            lastname : !_.isEmpty(shopData) && shopData.lastname !== '' ? shopData.lastname : this.state.Userdetails.firstname,
            address : !_.isEmpty(shopData) && shopData.address !== '' ? shopData.address : this.state.Userdetails.address1,
            telephone : !_.isEmpty(shopData) && shopData.telephone !== '' ? shopData.telephone : this.state.Userdetails.phone,
            parent_category : this.state.parentId,
            location:JSON.stringify(locData), 
            email : !_.isEmpty(shopData) && shopData.email !== '' ? shopData.email : this.state.Userdetails.email
        };
        for (key in params) {
            query.append((key), (params[key]))
        } 
        if (!_.isEmpty(imageFile)) {
            query.append('shop_image', imageFile);
        }
        fetch(settings.api+'app/saveShop', {
            method: 'POST',
            body: query
        }).then((response) => response.json())
            .then((response) => {
                _this.setState({load1:false, imageFile:{}},()=>this.showAlert(response.status,response.message));
        })
        .catch((error) => {
                console.log(error);
        });
    });    
}
showAlert(status,message){
    if(status){
        this.dropdown.alertWithType('success', 'Félicitations!', 'Les données de votre magasin ont été enregistrées.'); 
        setTimeout(()=>{
            startMain(this.props.auth.token,this.props.auth.userdata);
        },500)
    }else{
        this.dropdown.alertWithType('error', 'Erreur', message);
    }     
}
render() {    
    let _this = this;  
    const { shopData } = this.state;
    //console.log(_this.state.Userdetails);
    return ( 
        <View style={[styles.main1,{padding:7,paddingTop:7}]}>            
            { this.state.load ?<View style={[styles.Center]} ><Loader/></View>:<View style={styles.flex}>
            <ScrollView  showsVerticalScrollIndicator={false}  style={[styles.mainScollview,{flexGrow:1}]}>
                <View style={styles.scrollInner}>
                    <View style={styles.myAccountData}>
                         <View style={[styles.accountdataBlock]}>                           
                             <TouchableOpacity  onPress={()=>this.showActionSheet()}>
                                {this.state.backimage != ''?(
                                <Image style={{height:130,width:130,resizeMode:'cover'}} source={{uri:this.state.backimage}}  />):(<Image source={images.user}  style={{height:130,width:130,resizeMode:'cover'}} />)}
                            </TouchableOpacity>
                        </View>
                         <View style={[styles.accountdataBlock,styles.infoForm]}>
                            <View style={[styles.accountdataBlock]}>
                                <Text style={styles.blockTitle}>VOTRE MAGASIN - INFORMATIONS</Text>
                            </View>
                                {/* <Text style={{fontSize:18}}>Votre nom : </Text>
                                <Text style={{fontSize:18}}>{_this.state.Userdetails.name}</Text> */}
                                <FloatLabelTextInput style={styles.genInput} placeholder={"Votre nom *"}  underlineColorAndroid={'#eee'} 
                                onChangeTextValue ={(val) => { 
                                    const { shopData } = this.state;
                                    !_.isEmpty(shopData) && shopData.name !== '' &&
                                    this.setState({
                                        shopData: {
                                            ...shopData,
                                            name: val,
                                        }
                                    });
                                }} 
                                value={shopData.name !== '' ? shopData.name :  _this.state.Userdetails.name} 
                                />
                                {/* <Text style={{fontSize:18}}>Votre nom : </Text>
                                <Text style={{fontSize:18}}>{_this.state.Userdetails.name}</Text> */}
                                <FloatLabelTextInput style={styles.genInput} placeholder={"Prenom *"}  underlineColorAndroid={'#eee'} 
                                    onChangeTextValue ={(val) => { 
                                        const { shopData } = this.state;
                                        !_.isEmpty(shopData) && shopData.lastname !== '' &&
                                        this.setState({
                                            shopData: {
                                                ...shopData,
                                                lastname: val,
                                            }
                                        });
                                    }}
                                value={shopData.lastname !== '' ? shopData.lastname :  _this.state.Userdetails.firstname} 
                                />
                                {/* <Text style={{fontSize:18}}>Adresse postal : </Text>
                                <Text style={{fontSize:18}}>{_this.state.Userdetails.address1+' '+_this.state.Userdetails.address2}</Text> */}
                                <FloatLabelTextInput style={styles.genInput} placeholder={"Adresse postale *"}  underlineColorAndroid={'#eee'} 
                                onChangeTextValue ={(val) => { 
                                    const { shopData } = this.state;
                                    !_.isEmpty(shopData) && shopData.address !== '' &&
                                    this.setState({
                                        shopData: {
                                            ...shopData,
                                            address: val,
                                        }
                                    });
                                }}
                                value={shopData.address !== '' ? shopData.address :  _this.state.Userdetails.address1}
                                />
                                 <LocationSelect  
                                isEnable={true} 
                                nearMe={false}
                                placeholder={"Code postal/Ville"} 
                                ref = {o => _this.LocationSelect = o} 
                                authActions={_this.props.authActions} 
                                //noIco={false} 
                                currentLocation={true} 
                                // postAd={true}
                                fromShop = {true}
                                value={_this.state.Userdetails.city != ''  ?_this.state.Userdetails.city:''} />
                            {/* <View style={{flexDirection:'row',marginBottom:5}}>
                                <Text style={{fontSize:18}}>email : </Text>
                                <Text style={{fontSize:18}}>{_this.state.Userdetails.email}</Text>
                            </View> */}
                                {/* <Text style={{fontSize:18}}>Téléphone : </Text>
                                <Text style={{fontSize:18}}>{_this.state.Userdetails.phone}</Text> */}
                                <FloatLabelTextInput style={styles.genInput} placeholder={"Téléphone *"}  underlineColorAndroid={'#eee'}
                                onChangeTextValue ={(val) => { 
                                    const { shopData } = this.state;
                                    !_.isEmpty(shopData) && shopData.telephone !== '' &&
                                    this.setState({
                                        shopData: {
                                            ...shopData,
                                            telephone: val,
                                        }
                                    });
                                }}
                                value={shopData.telephone !== '' ? shopData.telephone :  _this.state.Userdetails.phone}
                                />
                                {/* <Text style={{fontSize:18}}>Téléphone : </Text>
                                <Text style={{fontSize:18}}>{_this.state.Userdetails.phone}</Text> */}
                                <FloatLabelTextInput style={styles.genInput} placeholder={"email *"}  underlineColorAndroid={'#eee'} 
                                onChangeTextValue ={(val) => { 
                                    const { shopData } = this.state;
                                    !_.isEmpty(shopData) && shopData.email !== '' &&
                                    this.setState({
                                        shopData: {
                                            ...shopData,
                                            email: val,
                                        }
                                    });
                                }} 
                                value={shopData.email !== '' ? shopData.email :  _this.state.Userdetails.email}
                                />                        
                        </View>                        
                        <View style={[styles.accountdataBlock,styles.infoForm]}>
                            <View style={[styles.accountdataBlock]}>
                                <Text style={styles.blockTitle}>Description du magasin</Text>
                            </View> 
                            <View style={styles.accountdataBlock}> 
                                <ModalCat withParent
                                    SectionList={this.props.auth.categoryList}  ref={o => this.ModalCat = o} containerStyle={{flex:3}} onPress={(item, parent)=>this.setState({catId:item, parentId: parent})}/>
                            </View>  
                            <FloatLabelTextInput style={styles.genInput} placeholder={"Titre de magasin *"}  underlineColorAndroid={'#eee'}
                            onChangeTextValue ={(val) => this.setState({title:val})}
                            value={this.state.title}
                            />
                            <FloatLabelTextInput style={styles.genInput} placeholder={"Description de votre magasin... *"}
                            numberOfLines={6}
                            Description={true}
                            underlineColorAndroid={'#eee'} 
                            onChangeTextValue ={(val) => this.setState({desc:val})}
                            value={this.state.desc}
                            />
                            <FloatLabelTextInput style={styles.genInput} placeholder={"URL de site web"}  underlineColorAndroid={'#eee'} 
                            onChangeTextValue ={(val) => this.setState({siteLink:val})} 
                            value={this.state.siteLink} 
                            />
                            <FloatLabelTextInput style={styles.genInput} placeholder={"Slogan de magasin *"}  underlineColorAndroid={'#eee'}
                            onChangeTextValue ={(val) => this.setState({slogan:val})}
                            value={this.state.slogan} 
                            />
                            {this.state.Userdetails.usertype != 'personal' ? 
                            <FloatLabelTextInput style={styles.genInput} placeholder={"N°SIREN  *"}  underlineColorAndroid={'#eee'}
                            value={this.state.Userdetails.numofcompany}
                            editable={false}
                            /> : null
                            }
                            <FloatLabelTextInput style={styles.genInput} placeholder={"Horaires d'ouverture"}  underlineColorAndroid={'#eee'} 
                            onChangeTextValue ={(val) => this.setState({workSche:val})}  
                            value={this.state.workSche}
                            onSubmitEditing={this.checkValid.bind(this)}
                            returnKeyType={'go'}
                            />
                        </View>                                               
                    </View>
                </View>
            </ScrollView>
            <ActionSheet
                ref={(o) => { this.ActionSheet = o; }}
                options={this.state.options}
                cancelButtonIndex={this.ActionSheetOptions.CANCEL_INDEX}
                destructiveButtonIndex={this.ActionSheetOptions.DESTRUCTIVE_INDEX}
                onPress={(i) => {
                    if (Platform.OS === 'ios') {
                      setTimeout(() => {
                        this.doAction(i);
                      }, 500);
                    } else {
                      this.doAction(i);
                    }
                  }}
            />  
            <View style={[styles.bottomArea,styles._MLR15D_]}>
                <LoaderButton load={this.state.load1} onPress={this.checkValid.bind(this)} BtnStyle={[styles.brandColor,styles._MT10_,styles.justCenter,styles.smallBtn2]} text={'Sauvegarder'} textStyle={[styles.genButton,{padding:0,fontSize:14}]} />               
            </View>
            {Platform.OS === 'ios' ? <CKeyboardSpacer /> : (null)} 
        </View>}
             <DropdownAlert ref={(ref) => this.dropdown = ref} onClose={(data) => {}} />
       </View>
    );
  }
}

function mapStateToProps(state, ownProps) {
	return {
        chat:state.chat,
        auth:state.auth,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		chatActions: bindActionCreators(chatActions, dispatch),
		authActions: bindActionCreators(authActions, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(createShop);