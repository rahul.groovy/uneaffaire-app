
import React, { Component } from 'react';
import {StyleSheet,Image,Text,View,Picker,TextInput,TouchableOpacity,ScrollView,Switch,Modal,AsyncStorage,
ActivityIndicator,Dimensions,Platform
} from 'react-native';
import { colors } from '../../config/styles';
import images from '../../config/images';
import styles from '../../config/genStyle';
import _ from 'lodash';
import ModalDropdown from '../../lib/react-native-modal-dropdown';
import LocationSelect from '../../components/LocationSelect/LocationSelect';
import RadioButton from '../../lib/react-radio/index';
import {Navigation,Screen,SharedElementTransition} from 'react-native-navigation';
import CheckBox from '../../lib/react-native-check-box';
import FloatLabelTextInput from '../../lib/react-native-floating-label-text-input';
import {settings} from '../../config/settings';
import ApiHelper from '../../lib/ApiHelper/ApiHelper';
import {Column as Col, Row} from 'react-native-flexbox-grid';
import DropdownAlert from 'react-native-dropdownalert';
import Loader from '../../components/Loader/Loader';
import LoaderButton from '../../components/LoaderButton/LoaderButton';
import { iconsMap, iconsLoaded,Icon } from '../../config/icons';
import {DashBoardItem} from '../../components/GridComponents';
import Orientation from 'react-native-orientation';
import Tabs from '../../components/Tabs/Tabs';
import Select from '../../components/Select/Select';
import startMain from '../../config/app-main';
import {tryCamera} from '../../redux/utils/location';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as chatActions from '../../redux/reducers/chat';
import * as authActions from '../../redux/reducers/auth';
import PayPalPayment from '../../redux/utils/paypal';
import ImagePicker from 'react-native-image-crop-picker';
import { ActionSheetCustom as ActionSheet } from 'react-native-actionsheet';
const api= new ApiHelper;
const { height, width } = Dimensions.get('window');

class myAccount extends Component {
    static navigatorStyle = {
        navBarBackgroundColor: colors.navBarBackgroundColor,
        navBarTextColor: colors.navBarTextColor,
        navBarSubtitleTextColor: colors.navBarSubtitleTextColor,
        navBarButtonColor: colors.navBarButtonColor,
        statusBarTextColorScheme: colors.statusBarTextColorScheme,
        statusBarColor: colors.statusBarColor,
        tabBarBackgroundColor: colors.tabBarBackgroundColor,
        tabBarButtonColor: colors.tabBarButtonColor,
        tabBarSelectedButtonColor: colors.tabBarSelectedButtonColor,
        navBarSubtitleColor: colors.navBarSubtitleColor,
        navBarTextFontFamily: colors.navBarTextFontFamily,
        //navBarHidden: false,
        //tabBarHidden: true,
        disableOpenGesture: true,
        drawUnderTabBar: true
    };
  constructor(props) {
    super(props);
    this.proffe = {}; 
    this.personal = {"Artisans, commerçants et chefs d'entreprise":"Artisans, commerçants et chefs d'entreprise",'Cadres et professions intellectuelles supérieures':'Cadres et professions intellectuelles supérieures','Professions Intermédiaires':'Professions Intermédiaires','Employés':'Employés','Ouvriers':'Ouvriers','Retraités':'Retraités','Etudiant':'Etudiant','Militaire':'Militaire','Sans activité professionnelle':'Sans activité professionnelle','Autres':'Autres'};
    this.ActionSheetOptions = {
        CANCEL_INDEX: 2,
        DESTRUCTIVE_INDEX: 2,   
        options: [],
    };
    this.state={
        check1:true,
        tabs: this.props.openProfile !== undefined ? 2 : 1,
        tabSelected:1,
        modalVisible: false,
        like:false,
        token:null,
        oldPass:'',
        newConfPass:'',
        newPass:'',
        respTxt:'',
        dashData:[],
        dashDataLength:0,
        online : [],
        mainData:[],
        refreshing:false,
        total_ad:0,
        total_offline:0,
        total_online:0,
        total_pending:0,
        shop_data:[],
        shopLength:0,
        credit_data:[],
        creditLength:0,
        wallet_data:[],
        walletLength:0,
        load:false,
        profileData:[],
        backimage:'',
        fileName:'',
        uri:'',
        typeofimg:'',
        radioKey:{},
        checks:[],
        load1:false,
        sort:'date',
        userdata:null,
        onBack:null,
        user_prefix:'M.',
        category:'',
        cateShow:'',
        imageFile:{},
        options:
        [ <Text style={styles.actionName}>{'Prendre une photo'}</Text>,
        <Text style={styles.actionName}>{'À partir de la bibliothèque'}</Text>,
        <Text style={styles.actionName}>{'Annuler'}</Text>]
    };
    // if you want to listen on navigator events, set this up
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }

componentWillMount(){
    iconsLoaded.then(()=>{
        // if(api.checkLogin(this.props.auth.token)){
           
        // }
    });
}
componentDidMount() {
    Orientation.addOrientationListener(this._orientationDidChange);
}

_orientationDidChange = (orientation) =>{
    setTimeout(()=>{
        this.forceUpdate();
    },20);
}

 
setModalVisible(visible) {
    this.setState({modalVisible: visible});
}
  
sendEventToDrawer(){
    this.props.navigator.handleDeepLink({link: 'Drawer',payload:{
        screen:this.props.testID
    }});
}

setSort(val){
    this.setState({
        sort:val
    },()=>this.tabchange(1));
}

handleOpen(){
if(!api.checkLogin(this.props.auth.token)){
    startMain('',{});
}else{
    api.setAction(this.props.chatActions,this.props.authActions);
    this.props.chatActions.setScreen(this.props.testID);    
    this.sendEventToDrawer();
    this.props.navigator.toggleTabs({
        animated: true,
        to: 'hidden'
    });
    this.props.navigator.setStyle({navBarTitleTextCentered: true});
    this.props.navigator.setTitle({title: "Tableau de bord"});
    this.props.navigator.setButtons({
        leftButtons: [{
        icon: iconsMap['back'],
        id: 'back2',
        title:'back to welcome',
        }],
    });
    // AsyncStorage.getItem("userdata").then((value) => {
    //     let p = JSON.parse(value);
    //     this.setState({userdata:p});
    // }).done();
}

}

onNavigatorEvent(event) {
    if (event.id === 'bottomTabSelected' && event.selectedTabIndex == 4) {
        if(this.state.onBack == null ){
            if(!api.isProfileComplete(this.props.auth.userdata)){
                this.setState({onBack: 0});
            }else{
                this.setState({onBack: event.unselectedTabIndex});
            }
            //console.log('on back go to tab '+(event.unselectedTabIndex+1));
            
        }
    }
    if(event.id=='willAppear'){   
        this.handleOpen();
    }
    if(event.id=='didAppear'){
         this.setState({checks:[],check1:false});
        if(!api.isProfileComplete(this.props.auth.userdata)){
            this.tabchange(2);
            if(this.Tabs !== undefined && this.Tabs !== null){
                this.Tabs.setTab(1);
            }
        }else{
            if(Platform.OS !== 'ios' || this.state.tabs != 2 || this.state.profileData.length <= 0){
                this.tabchange(this.state.tabs);
            }
        }
        
        // if(this.props.openProfile !== undefined){
        //     this.tabchange(2);
        // }else{
        //     this.tabchange(this.state.tabs);
        // }
        
    }
    if (event.id == 'willDisappear') {
        //this.setState({onBack: null});
    }
    if(event.id == 'back2'){
        console.log(this.state.onBack);
        if (this.state.onBack != null) {
            let onBack = this.state.onBack
            this.setState({onBack: null},()=>{
               this.props.navigator.switchToTab({
                   tabIndex: onBack // (optional) if missing, this screen's tab will become selected
               });
            });
        } else {
            this.setState({onBack: null},()=>{
                this.props.navigator.switchToTab({
                    tabIndex: 0 // (optional) if missing, this screen's tab will become selected
                });
             });
                // this.props.navigator.pop({
                //     animated: true // does the pop have transition animation or does it happen immediately (optional)
                // });
        }
    }
}

handleOnPress(value){
     if(value != ''){
         this.setState({
             radioKey:value
         });
     }
} 
handleRadio(val){
      let p=this.state.profileData;
      p.newsletter = val;
      this.setState({profileData:p});
}
navigate(page,data){
    this.props.navigator.push({
      screen: page,
      passProps:data
    });
}
changePass(){
        const { profileData } = this.state;
        this.setState({load1:true},()=>{
            if(this.state.newPass != this.state.newConfPass){
                //this.setState({load1:false});
                this.setState({load1:false,respTxt :'Entrez le même mot de passe'});
                return false;
            }else if(this.state.newPass.length < 7){
                //this.setState({load1:false});
                this.setState({load1:false,respTxt :'Entre au moins 8 personnages'});
                return false;
            }else if(this.state.newPass != '' && this.state.newConfPass != ''){ 
                let _this = this;
                let request = {};
                if(profileData.is_social === 1) {
                    request={
                        url:settings.endpoints.changePass,
                        method:'POST',
                        params:{token:this.props.auth.token,currentpassword:'',newpassword:this.state.newPass,retypepassword:this.state.newConfPass,is_social:1}
                    }
                } else {
                    request={
                        url:settings.endpoints.changePass,
                        method:'POST',
                        params:{token:this.props.auth.token,currentpassword:this.state.oldPass,newpassword:this.state.newPass,retypepassword:this.state.newConfPass}
                    }
                }
                api.FetchData(request).then((result)=>{ 
                    if(result.status){
                        this.setState({load1 : false, respTxt : result.message},()=>{
                            setTimeout(function(){ 
                                _this.setState({modalVisible:false,respTxt:''}); 
                            }, 1000);
                        });
                            //this.setState({load1 : false, respTxt : result.message});
                    }else{
                            _this.setState({load1 : false, respTxt : result.message});
                            //this.setState({modalVisible :false});
                    }       
                }).catch((error)=>{
                    //console.log(error);
                    _this.setState({load1 : false, respTxt :''});
                });
            }
       });
       
}
tabchange(tab){     
    this.setState({tabs:tab,tabSelected:tab},()=>{
        if((this.state.shopLength == 0 && tab==4) ){
            this.setState({load : true}); 
        }          
        let _this = this;
        if(tab == 1 ){  
            _this.setState({load : true},()=>{
                let request={
                    url:settings.endpoints.getDashData,
                    method:'POST',
                    params:{token:this.props.auth.token,sortby:this.state.sort}
                }
                api.FetchData(request).then((result)=>{           
                    if(result.status){
                        _this.setState({
                            total_ad : result.total_ad,
                            total_offline : result.total_offline,
                            total_online : result.total_online,
                            total_pending : result.total_pending,
                            dashData : result.adlist.length > 0 ? result.adlist :[],
                            dashDataLength:result.adlist.length,
                            load :false
                        }); 
                    }else{
                        _this.setState({
                            dashData : result.adlist == '' ? [] : [],
                            dashDataLength : 0,
                            load :false
                        });
                    }
                }).catch((error)=>{
                    //console.log(error);
                    _this.setState({load :false});
                });
            });                       
                
        }else if(tab == 2){
                this.setState({load : true},()=>{
                    let request={
                        url:settings.endpoints.getProfieData,
                        method:'POST',
                        params:{token:this.props.auth.token}
                    }
                    api.FetchData(request).then((result)=>{            
                        if(result.status){
                        if(result.categories.length > 0){
                                result.categories.map((item,k)=>{
                                    if(item != 'undefined'){
                                        this.proffe[item.id] = item.name
                                    }             
                                }); 
                        }
                            this.setState({
                                load :false,
                                profileData:result.userDetails,
                                backimage:result.userDetails.image,
                                cateShow:typeof result.userDetails.category_name != 'undefined' &&result.userDetails.category_name != '' ? result.userDetails.category_name : result.userDetails.user_category ,
                                category:result.userDetails.user_category
                            },()=>{
                                let obj = {
                                    cityzip:result.userDetails.postal_code,
                                    department:result.userDetails.department,
                                    department_slug:result.userDetails.department_slug,
                                    lat:result.userDetails.lat,
                                    long:result.userDetails.long,
                                    region:result.userDetails.region,
                                    region_slug:result.userDetails.region_slug,
                                    town:result.userDetails.town,
                                    zipcode:result.userDetails.zipcode,
                                    isCurrent:false
                                }
                                if(result.userDetails.zipcode != '' && result.userDetails.town != ''){
                                    // console.log(obj);
                                    this.LocationSelect.setLocationData(obj);
                                }                        
                            });                   
                        }else{
                            this.setState({load :false});
                        }     
                    }).catch((error)=>{
                        //console.log(error);
                        this.setState({load :false});
                    });
                });
                
        }else if(tab == 3){
                this.setState({load : true},()=>{
                    let request={
                        url:settings.endpoints.creditPackageList,
                        method:'POST',
                        params:{token:this.props.auth.token}
                    }
                    api.FetchData(request).then((result)=>{   
                        if(result.status){
                        this.setState({
                            credit_data:result.credit_data.length > 0 ? result.credit_data : '',
                            creditLength:result.credit_data.length,
                            wallet_data:result.wallet_data.length ? result.wallet_data : '',
                            walletLength:result.wallet_data.length,
                            load :false
                        });
                        }else{
                            this.setState({load :false});
                        }     
                    }).catch((error)=>{
                        //console.log(error);
                        this.setState({load :false});
                    });
                });
                
        }else if(tab == 4){
                let request={
                    url:settings.endpoints.shopPackageList,
                    method:'POST',
                    params:{token:this.props.auth.token}
                }
                api.FetchData(request).then((result)=>{  
                    //console.log(result);     
                    if(result.status){
                        this.setState({
                            shopLength:result.shop_data.length,
                            shop_data:result.shop_data.length > 0 ? result.shop_data : [],
                            load :false
                        });
                    }else{
                        this.setState({load :false});
                    }    
                }).catch((error)=>{
                    //console.log(error);
                    this.setState({load :false});
                });
        }
    });
}
imageClick(){     
    /**
     * The first arg is the options object for customization (it can also be null or omitted for default options),
     * The second arg is the callback which sends object: response (more info below in README)
     */
    tryCamera().then((result) => {
        if(result){
            this.getCamera();
        }
    }).catch((e)=>{
        console.log(e);
    })   
}

doAction(i) {
    if (i === 0) {
        this.openCamera();
    } else if (i === 1) {
        this.imageClick();
    }
}  

showActionSheet() {
    tryCamera().then((result) => {
        if (this.ActionSheet && this.ActionSheet.show) {
            this.ActionSheet.show();
        }
    }).catch((e)=>{
        console.log(e);
    }) 
}

openCamera = () => {
    ImagePicker.openCamera({
        width: 1200,
        height: 1200,
        cropping: false,
        mediaType: "photo",
        compressImageQuality : 0.7
      }).then((image) => {
        let newArray = {
            uri: image.path,            
            name:image.path.substr(image.path.lastIndexOf('/') + 1),           
            type: image.mime,
        }
        // console.log(newArray);
        this.setState({backimage:image.path,imageFile:newArray}); 
      }).catch(error => console.log('cancel'));
}

getCamera = () => {
    ImagePicker.openPicker({
        width: 1200,
        height: 1200,
        cropping: false,
        mediaType: "photo",
        compressImageQuality : 0.7
        }).then(image => {
        // console.log(image);
        let newArray = {
            uri: image.path,            
            name:image.path.substr(image.path.lastIndexOf('/') + 1),           
            type: image.mime,
        }
        // console.log(newArray);
        this.setState({backimage:image.path,imageFile:newArray});  
        });    
}
changeSwitch(value,key){    
    let getData = this.state.dashData;
    getData[key].is_online = !value?0:1;
    this.setState({
        dashData:getData
    },()=>{
        let _this = this;
        let request={
            url:settings.endpoints.onoffad,
            method:'POST',
            params:{token:this.props.auth.token,make:!value?0:1,ad_id:getData[key].id}
        }
        api.FetchData(request).then((result)=>{
            if(result.status){
                this.setState({
                    total_ad : result.adcount[0].total_ad,
                    total_offline : result.adcount[2].total_offline_ad,
                    total_online : result.adcount[1].total_online_ad,
                    total_pending : result.adcount[3].total_pending_ad,
                },()=>{
                    let msg = value ? 'Votre annonce est en ligne':'Votre annonce est hors ligne'
                    this.dropdown.alertWithType('success', 'Success', msg);
                });
            }         
        }).catch((error)=>{
            //console.log(error);
            this.dropdown.alertWithType('error', 'Error', 'Quelque chose a mal tourné');
        }); 
    });
      
}
 
navigateTo(page,data){
      if(page=='ProductDetail'){
        this.props.navigator.push({
        screen: page,
        sharedElements: ['SharedTextId'+data.id],
        passProps:{
          data:data,
          token:this.props.auth.token,
          userdata:this.props.auth.userdata,  
        }
            }); 
      }else{
        this.props.navigator.push({
            screen: page,
            passProps:{
                data:data,
                token:this.props.auth.token,
                userdata:this.props.auth.userdata,
            }
        }); 
    }
     
}
validMobile(mobile){
    let msg = null;
    if(isNaN(mobile.replace(/\s/g, ""))){
        msg = "Le numéro de téléphone doit être un chiffre.";
        //return false;
    } else if(mobile.charAt(0) != '0'){
        msg = 'Le numéro de téléphone commence par 0!';
        //return false;
    } else if(mobile.replace(/\s/g, "").length != 10){
        msg = 'Le numéro de téléphone doit comporter 10 chiffres!';
        //return false;
    }else{
        //return true;
    }
    return msg;
}
saveData(){
    let atpos = this.state.profileData.email.indexOf("@");
    let dotpos = this.state.profileData.email.lastIndexOf(".");
    const {imageFile} = this.state;
    if(this.state.profileData.email.length > 0){
        if (atpos<1 || dotpos<atpos+2 || dotpos+2>=this.state.profileData.email.length) {  
            this.dropdown.alertWithType('error', 'Error', 'Entrez votre email');        
            return false;
        }
    }else{
        this.dropdown.alertWithType('error', 'Error', 'Email required');
        return false;
    }
    let locData = this.LocationSelect.getAddress();
    if(this.state.profileData.phone == '' || locData==''){
        this.dropdown.alertWithType('error', 'Error', 'Remplissez tous les champs');
        return false;
    }
    if(this.state.profileData.usertype != 'personal'){
        if(this.state.profileData.address1 == ''){
            this.dropdown.alertWithType('error', 'Error', 'Veuillez compléter tous les champs obligatoires');
            return false;
        }
    }
    
    // else if(this.state.profileData.phone.length < 10){
    //     this.dropdown.alertWithType('error', 'Error', 'Le nombre doit être de 10 chiffres');
    //     return false;
    // }\
    let msg = this.validMobile(this.state.profileData.phone );
    if(msg !== null){
        this.dropdown.alertWithType('error', 'Error', msg);
         return false;
    }
    let _this = this;   
    const query = new FormData();
    var params = {
            email:this.state.profileData.email,
            phone:this.state.profileData.phone,
            name:this.state.profileData.name,            
            address1:this.state.profileData.address1,
            address2:this.state.profileData.address2,
            prefix:this.state.profileData.user_prefix,
            firstname:this.state.profileData.firstname,
            //postal_code:this.state.profileData.postal_code,
            ///city:this.state.profileData.city,
            remove_image : this.state.backimage == '' ? 1 : 0,
            category:this.state.category,
            location:JSON.stringify(locData),
            token:this.props.auth.token,
            newsletter:this.state.profileData.newsletter, 
    };
    if(this.state.profileData.usertype != 'personal'){
        if(this.state.profileData.companyname == '' || this.state.profileData.numofcompany == ''){
            this.dropdown.alertWithType('error', 'Error', 'Remplissez tous les champs');
            return false;
        }
        params.companyname = this.state.profileData.companyname;
        params.numofcompany= this.state.profileData.numofcompany; 
    }
    this.setState({
        load : true
    },()=>{
        for (key in params) {
            query.append((key), (params[key]))
        }
        const file = {
            uri: this.state.uri,            
            name:this.state.fileName,           
            type:this.state.typeofimg             
        }
        if (!_.isEmpty(imageFile)) {
            query.append('images', imageFile);
        }else{
            query.append('images', '');
        }
        console.log(query);
        // return false;
        fetch(settings.api+'app/updateProfile', {
            method: 'POST',
            body: query
            }).then((response) => response.json())
            .then((response) => {
                console.log(response);
                if(response.status){  
                    this.dropdown.alertWithType('success', 'Félicitations!','Votre Profil a été enregistré avec succès');     
                    _this.setState({load:false,imageFile:[]},()=>{this.LocationSelect.setLocationData(locData);this.setProfileData(response.user_details);});
                    _this.tabchange(2);
                }else{
                    this.dropdown.alertWithType('error','Error',response.message);
                    _this.setState({load:false,imageFile:[]});
                }               
            })
            .catch((error) => {
                console.log(error);
                _this.setState({load:false});
            });
    })
    // _this.setState({load:true});    
    
}
onProgress(p){
    //console.log(p);
}

setProfileData(data){
    AsyncStorage.setItem("userdata", JSON.stringify(data));  
    this.props.authActions.setUserData(this.props.auth.token,data);
}

checked(c,id){
    let cArray = [];
    if(id == null){
        if(c){
            this.state.dashData.map(function(item,key){cArray.push(item.id);});
            this.setState({checks:cArray,check1:c});
        }else{
            this.setState({checks:[],check1:c}); 
        }        
    }else{
        var array = this.state.checks;
        var index = array.indexOf(id);
        if(!c){             
            if (index > -1) {
                array.splice(index, 1);
            }            
        }else{
            array.push(id);
        }  
        this.setState({checks:array,check1:c});
    }
}
goPayment(id){
    if(id != ''){
        let page = 'shopCart';
        this.props.navigator.push({
        screen: page,
        passProps:{
                token:this.props.auth.token,
                userdata:this.props.auth.userdata,
                id:id
            }
        });
    }
}
goShop(id){
    let page = 'createShop';
    this.props.navigator.push({
    screen: page,
    passProps:{
            token:this.props.auth.token,
            UserDetail:this.props.auth.userdata,
            id:this.props.auth.userdata.shop_id
        }
    });
}

_renderSharedElement(){
   if(this.state.tabs != 1 && this.state.dashDataLength > 0){ 
       let ret =[]             
        this.state.dashData.map((item,key)=>{ 
           ret.push(<View key={'hidden'+item.id} style={{height:0,width:0}}>
                    <SharedElementTransition sharedElementId={'SharedTextId'+item.id}> 
                        <Image style={styles.proImg} source={{uri :item.image}} />
                    </SharedElementTransition>
                </View>);
        });
        return ret;
    }else{
        return null;
    }
}
handleRadio1(val){
    let udata = this.state.profileData; 
    udata.user_prefix = val;
    this.setState({profileData:udata});
}
getRegions(){
    return this.proffe;
}
getPersonal(){
    let ret = [];
    Object.keys(this.personal).map((key,val)=>{
        //console.log(val);
        ret[key]=key;
        //ret.push({key:key});
    });
    return ret;
}
goPaypal(){
    let data = this.state.radioKey;
    if(Object.keys(data).length > 0){
        let price = parseFloat(data.credit).toFixed(2).toString();
        //console.log(""+price);return false;
        let params = {
            price: price,
            currency: 'EUR',
            description: 'Wallet Add Balance',
            custom:'paypal_buywallet'
        };
        PayPalPayment(params).then(confirm => {
            if(confirm.response.state=='approved'){
                this.goSuccess(confirm,data);           
            }else{
                alert('Something went wrong with payment');
            }      
        }).catch(error => console.log(error));
    }    
}
goSuccess(confirm,data){
    this.setState({load:true},()=>{
        let request={
            url:settings.endpoints.buyWalletCreadit,
            method:'POST',
            params:{token:this.props.auth.token,wallet_id:data.id,paypal_id:confirm.response.id}
        }
        api.FetchData(request).then((result)=>{
            if(result.status){
                this.setState({load:false},()=>{
                    this.props.navigator.push({
                        screen: 'success',
                        passProps:{
                            status:'Y',
                            message:result.message,
                            token:this.props.auth.token,
                            userdata:this.props.auth.userdata
                        }
                    });
                });  
            }else{
                this.setState({load:false},()=>{
                    alert('Quelque chose s\'est mal passé dans le paiement.')
                });
            } 
        }).catch((error)=>{
            //console.log(error);
            this.setState({load:false},()=>{
                alert('Quelque chose s\'est mal passé dans le paiement.')
            });
        });
    });
    
}
getDispPrice(number){
    if(!isNaN(parseFloat(number))){
        if(number >= 1000){
            let ret='';
            let factor= parseInt(number/1000);
            ret+=factor
            ret+=' ';
            ret+=('000' + (number - (factor*1000))).slice(-3);
            ret+=' €';
            return ret;
        }else{
            let oret='';
            oret+=number;
            oret+=' €';
            return oret;
        }
    }
  }
  removeImage(){
      if(this.state.backimage != ''){
          this.setState({backimage:''});
      }
  }
render() {
    var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "June","July", "Aug", "Sept", "Oct", "Nov", "Dec"];
    let _this = this;
    let walletData =   this.state.walletLength > 0 ? (           
          contents =  this.state.wallet_data.map(function (item,key) {
            return ( 
            <View style={[styles.pdfRow]} key={key}>
                        <View style={[styles.inlineDatarow]}>
                                <View style={styles.imgCol}>
                                    <Image style={styles.pdfSymbol} source={images.pdfIco} />
                                </View>
                                <View style={[styles.pdfData]}>
                                    <View style={[styles.inlineDatarow,styles.spaceBitw,styles._PB5_]}>
                                        <Text style={[styles.genText,styles.grayColor]}>{item.transaction_type != '' || typeof item.transaction_type !='undefined'  ? item.transaction_type : '--'}</Text>
                                        <Text style={[styles.genText,styles.grayColor]}>{item.createdAt != '' ? item.createdAt :'--'}</Text>
                                    </View>
                                    <View style={[styles.inlineDatarow,styles.spaceBitw]}>
                                        <Text style={[styles.genText,styles.bold,styles.grayColor]}>{item.amount != '' ? item.amount :'0'} €</Text>
                                        <Text style={[styles.genText,styles.grayColor]}>{item.paymnet_mode != '' ? item.paymnet_mode : '--'}</Text>
                                    </View>
                                </View>
                        </View>
                      <Text style={styles.dateText}> {item.id}</Text>
                    </View>)
                    })
        ):<View style={[styles._MT15_,styles.Center]}><Text>Aucune donnée disponible</Text></View>;;
        let co = 100;
        let a  = 0;
        let creditRadio = 
        this.state.creditLength > 0 ? (  
          contents =  this.state.credit_data.map(function (item,key) {
            a = co+key;
            return (                 
            <RadioButton currentValue= {_this.state.radioKey} key={co+key} value={item} 
            onPress={(id)=>_this.handleOnPress(id)} labelStyle ={{marginBottom:10}}><Text style={[styles.readiText]}>{item.credit} € {item.extra_credit != '' ? '( + '+item.extra_credit+' € offerts )' :''}</Text></RadioButton>
        )
        })
        ):<View style={[styles._MT15_,styles.Center]}><Text>Aucune donnée disponible</Text></View>;;
        let i = 0;
        //console.log(this.props.auth.userdata.have_shop);
        let ShopData =  this.state.shopLength > 0 ? (   contents =  this.state.shop_data.map(function (item,key) {
            let btn  = null;
            if(_this.props.auth.userdata.have_shop == 1 && _this.props.auth.userdata.shop_package_id == item.id){
                i++;
                btn = <TouchableOpacity style={[styles._MR10_]} activeOpacity={0.8} onPress={()=>_this.goShop(item.id)}>
                             <Text style={[styles.smallBtn]}>Gérer le magasin</Text>
                          </TouchableOpacity>;
            }else if(_this.props.auth.userdata.have_shop == 0 && i == 0){
                btn = <TouchableOpacity style={[styles._MR10_]} activeOpacity={0.8} onPress={()=>_this.goPayment(item.id)}>
                             <Text style={[styles.smallBtn]}>Acheter maintenant</Text>
                          </TouchableOpacity>;
            }
            return (                          
                    <View style={[styles.contBlock,styles.borderBottom]} key={200+key}>
                        <Text style={styles.subTitle}>{item.name}</Text>
                        <View style={styles.contData}>
                            <Text style={styles.smallDesc}>{item.advantage}</Text>
                            <Text style={styles.smallDesc}>{item.subscription}</Text>
                        </View>
                        <View style={styles.row}>
                            {btn}
                        </View>
                    </View>
                )
            })
                        ):<View style={[styles._MT15_,styles.Center]}><Text>Aucune donnée disponible</Text></View>;;
      
       
                                     
    let popup =  <Modal style={styles.modalApear} animationType={"slide"} transparent={true} visible={this.state.modalVisible} onRequestClose={() => {alert("Modal has been closed.")}}>
                        <View style={[styles.modalInner,styles.justCenter]}>
                            {/* <Text style={[styles.globalIcon,styles.modalClose]} onPress={() => {this.setModalVisible(false)}}>{';'}</Text> */}
                            <View style={styles.modalMain}>
                                <Text style={[styles.globalIcon,styles.modalClose]} onPress={() => {this.setModalVisible(false)}}>{';'}</Text>
                                <View style={[styles.modalBody]}>
                                    <View><Text style={styles.subTitle}>Changer le mot de passe</Text></View>
                                    {this.state.profileData.is_social === 1 ? (null) : ( 
                                    <View style={[styles.inlineDatarow,styles._PR10_]}>
                                        <TextInput style={[styles.genInput,styles._PL0_,styles.flex]} secureTextEntry={true} placeholder={"Mot de passe actuel"}  underlineColorAndroid={'#eee'} onChangeText={(text) => this.setState({oldPass:text})}/>
                                    </View>)}
                                    <View style={[styles.inlineDatarow,styles._PR10_]}>    
                                        <TextInput style={[styles.genInput,styles._PL0_,styles.flex]} secureTextEntry={true} placeholder={"Nouveau mot de passe"}  underlineColorAndroid={'#eee'}  onChangeText={(text) => this.setState({newPass:text})}/>
                                    </View>      
                                    <View style={[styles.inlineDatarow,styles._PR10_]}>    
                                        <TextInput style={[styles.genInput,styles._PL0_,styles.flex]} secureTextEntry={true} placeholder={"Retaper le mot de passe"}  underlineColorAndroid={'#eee'}  onChangeText={(text) => this.setState({newConfPass:text})}/>
                                    </View>
                                    <View style={styles._MT15_}>
                                        <Text style={styles.successTxtLbl}>{this.state.respTxt != ''?this.state.respTxt:''}</Text>
                                    </View>
                                    <View style={styles._MT15_}>
                                        {/* <TouchableOpacity style={[styles.brandColor]} activeOpacity={0.8}><Text style={[styles.genButton,styles.smallBtn]} onPress={this.changePass.bind(this)}>Sauver</Text></TouchableOpacity> */}
                                        <LoaderButton load={this.state.load1} onPress={this.changePass.bind(this)} BtnStyle={[styles.brandColor]} 
                                        text={'Sauver'} textStyle={[styles.genButton,styles.smallBtn]} />
                                    </View>
                                </View>
                            </View>
                        </View>
                  </Modal>

     if(this.state.tabs == 1){
         //console.log(this.state.mainData);
         let data =this.state.dashDataLength > 0  ? (                        
        contents =  this.state.dashData.map(function (item,key) {         
            let datas={
                token:_this.props.auth.token,
                ad_id :[item.id], 
            }
            let  ck = _this.state.checks.indexOf(item.id)  > - 1;
        return (
            <DashBoardItem item = {item} vkey={key} key={key} 
        isChecked = {ck}
        onPress={(d)=>_this.navigateTo('ProductDetail', d)}
        onChecked={(c,id)=>_this.checked(c,id)}
        onChangeSwitch={(v,k)=>_this.changeSwitch(v,k)}
        onEdit={(d)=>_this.navigateTo('Modifier', d)}
        onDelete={(d)=>_this.navigate('RemoveAd', {token:_this.props.auth.token,ad_id:[d.id],fromDash:true})}
        onPromote={(d)=>_this.navigateTo('PromoteAd', d)}
        searchType={item.ad_type}
        />)})):<View style={[styles._MT15_,styles.Center]}><Text>Aucune donnée disponible</Text></View>;;
         let dataf = {
            token:_this.props.auth.token,
            ad_id :this.state.checks, 
         };
        myAccountData = 
        <View style={styles.flex}>          
            <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={styles.scrollInner}>
                        <View style={styles.myAccountData}>
                            <View style={[styles.accountdataBlock]}>
                                <Text style={styles.blockTitle}>Statistiques du compte</Text>
                                <Row size={12} style={[styles.statasticksRow]}>
                                        <Col sm={6} md={6} lg={4} style={[styles.statastcksCol]} >
                                            <View style={styles.dataRow}>
                                                <Text style={styles.labelText}>Annonces en ligne</Text>
                                                <Text style={styles.valText}>{typeof this.state.total_online != 'undefined' ? this.state.total_online : '0'}</Text>
                                            </View>
                                            <View style={styles.dataRow}>
                                                <Text style={styles.labelText}>Annonces en attente</Text>
                                                <Text style={styles.valText}>{typeof this.state.total_pending != 'undefined' ? this.state.total_pending : '0'}</Text>
                                            </View>
                                            <View style={styles.dataRow}>
                                                <Text style={styles.labelText}>Annonces hors ligne</Text>
                                                <Text style={styles.valText}>{typeof this.state.total_offline != 'undefined' ? this.state.total_offline : '0'}</Text>
                                            </View>
                                            <View style={styles.dataRow}>
                                                <Text style={styles.labelText}>Annonces total</Text>
                                                <Text style={styles.valText}>{typeof this.state.total_ad != 'undefined' ? this.state.total_ad : '0'}</Text>
                                            </View>
                                        </Col>
                                        <Col sm={6} md={6} lg={4} style={[styles.statastcksCol]} >
                                        <Text style={{fontSize:16,textAlign:'center'}}>{'Solde: '+this.props.auth.userdata.user_credit_display+''+colors.euro}</Text>
                                                <TouchableOpacity style={[styles.brandColor]} activeOpacity={0.8} onPress={()=>{
                                                    this.Tabs.setTab(2)
                                                    this.tabchange(3)}}>
                                                    <Text style={[styles.genButton,styles.midum,styles.smbtn]}>Acheter des crédits</Text>
                                                </TouchableOpacity>
                                                {/*<TouchableOpacity style={[styles.brandColor,styles._MT10_]} activeOpacity={0.8}>
                                                    <Text style={[styles.genButton,styles.midum,styles.smbtn]}>Booster vos announce</Text>
                                                </TouchableOpacity>*/}
                                        </Col>
                                        <Col sm={12} md={12} lg={4} style={width < 1025 && width > 721 ?[styles.statastcksCol]:[styles.statastcksCol,styles._MT20_]} >
                                            <View style={[styles.inlineDatarow]}>
                                                <TouchableOpacity onPress={this.state.checks.length > 0 ? ()=>{this.navigate('avant',dataf)} : null} style={[styles.inlineWithico,styles.flexStart]} activeOpacity={0.8}>
                                                    <Text style={[styles.globalIcon,styles.bigIco]}>7</Text>
                                                    <Text style={[styles.icoText,styles.big]}>Remonter en tête {width < 722 ?"\n":null} liste - 1,00€ </Text>
                                                </TouchableOpacity>
                                                <TouchableOpacity onPress={this.state.checks.length > 0 ? ()=>{this.navigate('RemoveAd',dataf)}:null}activeOpacity={0.8} style={width < 1025 && width > 721 ? [styles.inlineWithico,styles.flexStart,styles._MT10_]:[styles.inlineWithico,styles.flexStart]}>
                                                    <Text style={[styles.globalIcon,styles.bigIco]}>8</Text>
                                                    <Text style={[styles.icoText,styles.big]}>Supprimer la {width < 722 ?"\n":null}séléction</Text>
                                                </TouchableOpacity>
                                            </View>
                                        </Col>
                                </Row>
                            </View>
                            <View style={styles.accountdataBlock}>
                                <View style={[styles._PB0_,styles.dataRow]}>
                                    <View style={[styles.inlineDatarow]}>
                                        <CheckBox
                                            style={[styles.genChk]}
                                            // style={{flex: 1, padding: 10}}
                                            onClick={(checked) =>this.checked(checked,null)}
                                            labelStyle={styles.midum}
                                            isChecked={this.state.dashDataLength == this.state.checks.length? this.state.check1 :false}
                                            rightText={'Tout séléctionner'}
                                        />
                                    </View>
                                    <View style={{width:160}}>
                                        {Platform.OS === 'ios' ? (
                                        <Select 
                                        containerStyle={{ backgroundColor: "#fff", marginTop: 5}}
                                        optionsList={[{id:'date',value:'Trier par date'},{id:'price',value:'Trier par prix'}]}
                                        header={'Trier par'}                                        
                                        onPress={(item) => {this.setSort(item)}}/>
                                        ):(<Picker
                                        mode={'dropdown'}
                                        selectedValue={this.state.sort}
                                        onValueChange={(itemValue, itemIndex) => this.setSort(itemValue)}>
                                            <Picker.Item label="Trier par date" value="date" />
                                            <Picker.Item label="Trier par prix" value="price" />
                                        </Picker>)}
                                     {/* <ModalDropdown dropdownStyle={{maxHeight:100}} defaultValue={'Trier par date'} options={['Trier par date','Trier par prix']}/>  */}
                                    </View>
                                </View>
                            </View>
                            <View style={[styles.accountdataBlock]}>
                                <View style={styles.proMain}>    
                                    {data}                                        
                                </View>
                            </View>
                        </View>
                    </View>
            </ScrollView>
        </View>
     }else if(this.state.tabs == 2){
        let type= this.state.profileData.usertype == 'personal' ? 'PERSONNELLES':'PROFESSIONNELLES';
        let height=this.state.profileData.usertype == 'personal' ? 150 :100;
        let width =this.state.profileData.usertype == 'personal' ? 150 :200;
        myAccountData =
        <View style={styles.flex}>
            <ScrollView showsVerticalScrollIndicator={false} style={styles.mainScollview}>
                <View style={styles.scrollInner}>
                    <View style={styles.myAccountData}>
                        <View style={styles.accountdataBlock}>
                            <Text style={styles.blockTitle}>{'VOS INFORMATIONS '+type}</Text>
                             <TouchableOpacity onPress={()=>this.showActionSheet()}>
                                {this.state.backimage != ''?(
                                <View style={{paddingTop:10,width:width}}>
                                <TouchableOpacity style={{position:'absolute',right:-5,top:0,bottom:0,backgroundColor:colors.navBarButtonColor,height:20,width:20,borderRadius:10,alignItems:'center',zIndex:1,justifyContent:'center'}} onPress={()=>this.removeImage()}>
                                    <Icon name={'close'} color={'#fff'} size={10}></Icon>
                                </TouchableOpacity>
                                <Image style={{height:height,width:width,resizeMode:'cover'}} source={{uri:this.state.backimage}}  /> 
                                </View>                                
                                ):this.state.profileData.usertype == 'personal' ? (<Image style={{height:150,width:150,resizeMode:'cover'}} source={{uri:settings.api+'images/users-images/250-personal.png'}} />):(<Image style={{height:100,width:200,resizeMode:'cover'}}  source={{uri:settings.api+'images/users-images/250-prof.png'}}  />)}
                            </TouchableOpacity> 
                           
                        </View>
                        <View style={[styles.accountdataBlock,styles.infoForm]}>
                        <View style={{paddingBottom:5}}>
                            <Text style={{fontSize:13,color:colors.navBarButtonColor}}>Civilité</Text>
                        </View>
                            <View style={[styles.inlineDatarow,styles._PB15_]}>
                                <RadioButton currentValue={this.state.profileData.user_prefix} 
                                value={'M.'} 
                               onPress={(val)=>this.handleRadio1(val)}
                                Text={'M.'}
                                >
                                </RadioButton>
                                <RadioButton currentValue={this.state.profileData.user_prefix} value={'Mme'} 
                                onPress={(val)=>this.handleRadio1(val)} 
                                Text={'Mme'}>
                                </RadioButton>
                                <RadioButton currentValue={this.state.profileData.user_prefix} value={'Mlle'} 
                                onPress={(val)=>this.handleRadio1(val)} 
                                Text={'Mlle'}>
                                </RadioButton>
                            </View>
                            <View style={styles.genInput3}>
                            <FloatLabelTextInput style={styles.genInput} placeholder={"Nom*"}  underlineColorAndroid={'#eee'} value={this.state.profileData.name != '' ? this.state.profileData.name:''}
                            onChangeTextValue ={(val) => { 
                            let udata = this.state.profileData; 
                            udata.name = val;
                            this.setState({profileData:udata})}} 
                            fieldContainerStyle={styles.genInput2}
                            />
                            </View>
                            <View style={styles.genInput3}>
                            <FloatLabelTextInput style={styles.genInput} placeholder={"Prénom*"}  underlineColorAndroid={'#eee'}
                            onChangeTextValue ={(val) => {                                 
                            let udata = this.state.profileData; 
                            udata.firstname = val;
                            this.setState({profileData:udata})}} 
                            fieldContainerStyle={styles.genInput2}
                             value={this.state.profileData.firstname}/>
                             </View>                           
                            {this.state.profileData.usertype != 'personal' ?
                            <View style={styles.genInput3}>
                            <FloatLabelTextInput style={styles.genInput} placeholder={"Nom de la société"}  underlineColorAndroid={'#eee'}
                            editable={this.state.profileData.companyname == '' ? true :false}
                            onChangeTextValue ={(val) => {                                 
                            let udata = this.state.profileData; 
                            udata.companyname = val;
                            this.setState({profileData:udata})}} 
                            fieldContainerStyle={styles.genInput2}
                             value={this.state.profileData.companyname}/></View>:
                             <View style={styles.genInput3}>
                             <FloatLabelTextInput style={styles.genInput} placeholder={"Pseudo"}  underlineColorAndroid={'#eee'}
                            editable={false}
                            onChangeTextValue ={(val) => {                                 
                            let udata = this.state.profileData; 
                            udata.username = val;
                            this.setState({profileData:udata})}} 
                            fieldContainerStyle={styles.genInput2}
                             value={this.state.profileData.username}/>
                             </View>
                            }
                            {this.state.profileData.usertype != 'personal' ? 
                              <View style={styles.genInput3}><FloatLabelTextInput style={styles.genInput} placeholder={"Numéro de Siret"} keyboardType={'numeric'} underlineColorAndroid={'#eee'} value={String(typeof this.state.profileData.numofcompany != 'undefined' && this.state.profileData.numofcompany != null? this.state.profileData.numofcompany:'')}
                             onChangeTextValue ={(val) => { 
                             let udata = this.state.profileData; 
                             udata.numofcompany = val;
                             this.setState({profileData:udata})}} 
                             fieldContainerStyle={styles.genInput2}                             
                             /></View>:null
                            }  
                            <View style={styles.genInput3}>
                            <FloatLabelTextInput style={styles.genInput} placeholder={"E-Mail"}  underlineColorAndroid={'#eee'} value={this.state.profileData.email}
                            editable={this.state.profileData.email == '' ? true :false}
                            onChangeTextValue ={(val) => { 
                            let udata = this.state.profileData; 
                            udata.email = val;
                            this.setState({profileData:udata})}} 
                            fieldContainerStyle={styles.genInput2}
                            />
                            </View>
                            <View style={styles.genInput3}>
                            <FloatLabelTextInput style={styles.genInput} placeholder={"Télephone*"}  underlineColorAndroid={'#eee'} value={this.state.profileData.phone}
                            keyboardType={'numeric'}
                            maxLength={10}
                            onChangeTextValue ={(val) => { 
                            let udata = this.state.profileData; 
                            udata.phone = val;
                            this.setState({profileData:udata})}} 
                            fieldContainerStyle={styles.genInput2}
                            />
                              </View>                      
                             <View style={{paddingBottom:5,marginTop:5,flexDirection:'column'}}>
                                <Text style={{paddingBottom:5,fontSize:13,color:colors.navBarButtonColor}}>{this.state.profileData.usertype != 'personal' ?'Catégorie principale':'Catégorie socio-professionnelle'}</Text>
                                <ModalDropdown defaultValue={this.state.cateShow == '' ? 'Select Socio-Professionnelle Catégorie':this.state.cateShow}  
                                options={this.state.profileData.usertype != 'personal' ? this.getRegions():this.getPersonal()}
                                style={[styles.dropdown_2,styles.dropDownHome,{alignSelf:'stretch',width:250}]} textStyle={{fontSize:15}} dropdownStyle={{height:150}}
                                 onSelect={(region)=>
                                 this.setState({category:region})
                                 }
                                />
                            </View>
                           
                        </View>
                         <View style={styles.accountdataBlock}>
                            <Text style={styles.blockTitle}>VOTRE ADRESSE</Text>
                        </View>
                        <View style={styles.genInput3}>
                        <LocationSelect  
                        isEnable={true} 
                        nearMe={false}
                        placeholder={"Code postal/Ville*"} 
                        ref = {o => this.LocationSelect = o} 
                        authActions={this.props.authActions}
                        currentLocation={true}
                        fromShop={true}
                        //noIco={false} 
                        //currentLocation={true} 
                        //postAd={true}
                        value={this.state.profileData.city != ''  ?this.state.profileData.city:''} />
                        </View>
                         <View style={[styles.accountdataBlock,styles.infoForm]}> 
                         <View style={styles.genInput3}>
                            <FloatLabelTextInput style={styles.genInput} placeholder={"Adresse"}  underlineColorAndroid={'#eee'} value={this.state.profileData.address1}
                            onChangeTextValue ={(val) => { 
                            let udata = this.state.profileData; 
                            udata.address1 = val;
                            this.setState({profileData:udata})}} 
                            fieldContainerStyle={styles.genInput2}  
                            />
                            </View>
                            <View style={styles.genInput3}>
                            <FloatLabelTextInput style={styles.genInput} placeholder={"Complément adresse"}  underlineColorAndroid={'#eee'} value={this.state.profileData.address2}
                            onChangeTextValue ={(val) => { 
                            let udata = this.state.profileData; 
                            udata.address2 = val;
                            this.setState({profileData:udata})}}
                            fieldContainerStyle={styles.genInput2}  
                            />
                            </View>
                            {/* <FloatLabelTextInput style={styles.genInput} placeholder={"Code postal/Ville"}  underlineColorAndroid={'#eee'} value={this.state.profileData.city}
                            onChangeTextValue ={(val) => { 
                            let udata = this.state.profileData; 
                            udata.city = val;
                            this.setState({profileData:udata})}}                                 
                            /> */}
                            
                            {/* <FloatLabelTextInput style={styles.genInput} placeholder={"Code postal"}  underlineColorAndroid={'#eee'} value={this.state.profileData.postal_code}
                            onChangeTextValue ={(val) => { 
                            let udata = this.state.profileData; 
                            udata.postal_code = val;
                            this.setState({profileData:udata})}} 
                            />                             */}
                        </View>
                        <View style={[styles.accountdataBlock,{paddingBottom:5}]}>
                            <Text style={[styles.blockTitle]}>Newsletter</Text>
                         </View>
                         <View style={{paddingBottom:5}}>
                            <Text style={{fontSize:13,color:colors.navBarButtonColor}}>S'inscrire</Text>
                        </View>
                        <View style={[styles.inlineDatarow,styles._PB15_]}>
                       
                            <RadioButton currentValue={this.state.profileData.newsletter} 
                            value={1} 
                            onPress={(val)=>this.handleRadio(val)}
                            Text={'Oui'}
                            >
                            {/* <Text style={[styles.readiText,styles._PR20_]}>Oui</Text> */}
                            </RadioButton>
                            <RadioButton currentValue={this.state.profileData.newsletter} value={0} 
                            onPress={(val)=>this.handleRadio(val)} Text={'Non'}>
                            {/* <Text style={styles.readiText}>Non</Text> */}
                            </RadioButton>
                        </View>
                    </View>
                </View>
            </ScrollView>
            <ActionSheet
                ref={(o) => { this.ActionSheet = o; }}
                options={this.state.options}
                cancelButtonIndex={this.ActionSheetOptions.CANCEL_INDEX}
                destructiveButtonIndex={this.ActionSheetOptions.DESTRUCTIVE_INDEX}
                onPress={(i) => {
                    if (Platform.OS === 'ios') {
                      setTimeout(() => {
                        this.doAction(i);
                      }, 500);
                    } else {
                      this.doAction(i);
                    }
                  }}
            />    
            <View style={[styles.bottomArea,styles._MLR15D_]}>
                <LoaderButton load={this.state.load} onPress={this.saveData.bind(this)} BtnStyle={[styles.brandColor,styles._MT10_,styles.justCenter,styles.smallBtn2]} text={'Sauvegarder'} textStyle={[styles.genButton,{padding:0,fontSize:14}]} />
                <LoaderButton load={false} onPress={this.setModalVisible.bind(this)} BtnStyle={[styles.brandColor,styles._MT10_,styles.justCenter,styles.smallBtn2]} text={'Changer le mot de passe'} textStyle={[styles.genButton,{padding:0,fontSize:14}]} />
            </View>
               {popup}
        </View>
     
     }else if(this.state.tabs == 3){
        myAccountData = 
            <ScrollView showsVerticalScrollIndicator={false} style={styles.mainScollview}>
                <View style={styles.scrollInner}>
                    <View style={styles.myAccountData}>
                        <View style={styles.accountdataBlock}>
                            <Text style={styles.blockTitle}> {'Achat de crédits (Solde: '+this.props.auth.userdata.user_credit_display+''+colors.euro+')'}</Text>
                           
                            <View style={[styles.inlineDatarow,styles.spaceBitw,styles.creditRadio]}>
                               {creditRadio}                              
                            </View>                            
                            <View style={styles._MT20_}>
                            {/* <Text style={{fontSize:16}}>{'Solde: '+this.props.auth.userdata.user_credit+''+colors.euro}</Text> */}
                                <TouchableOpacity style={[styles.brandColor,styles._MR10_,styles.w120]} activeOpacity={0.8} onPress={()=>this.goPaypal()}>
                                    <Text style={[styles.genButton,styles.smallBtn]}>Acheter</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={styles.accountdataBlock}>
                            <Text style={styles.blockTitle}>Gérer mes achats</Text>
                            <View style={styles.pdfList}>
                               {walletData}                               
                            </View>
                        </View>
                    </View>
                </View>
            </ScrollView>
     }else if(this.state.tabs == 4){
        myAccountData = 
        <ScrollView showsVerticalScrollIndicator={false} style={styles.mainScollview}>
            <View style={styles.scrollInner}>
                <View style={styles.myAccountData}>
                        <View style={styles.accountdataBlock}>
                            <Text style={styles.blockTitle}>Vos offres pour le magasin</Text>
                           {ShopData}
                        </View>
                        {/*<View style={styles.accountdataBlock}>
                            <Text style={styles.blockTitle}>Nos offres pour les espaces publicitaires</Text>
                            <View style={styles.row}>
                                <TouchableOpacity style={[styles._MR10_]} activeOpacity={0.8}>
                                    <Text style={[styles.smallBtn]}>Toutes nos offres publicitaire</Text>
                                </TouchableOpacity>
                            </View>
                        </View>*/}
                </View>
            </View>
        </ScrollView>
     }
    return ( 
        <View style={[styles.main1,{padding:7,paddingTop:7}]}> 
            {this._renderSharedElement()} 
            <Tabs defaultTabIndex={this.props.openProfile !== undefined ? 1 : 0} ref={o => this.Tabs = o} tabList={['Mes \n annonces','Données \n personnelles','Mes \n achats','Nos \n offres']}  onTabPress={(i)=>{this.tabchange(i+1)}}/>
            {this.state.load ?(
                <View style={[styles.Center]}>
                    <Loader/>
                </View>):
                (myAccountData)
            }
             <DropdownAlert ref={(ref) => this.dropdown = ref} onClose={(data) => {}} />
       </View>
    );
  }

}
function mapStateToProps(state, ownProps) {
    return {
        chat:state.chat,
        auth:state.auth,
    };
}   
function mapDispatchToProps(dispatch) {
    return {
        chatActions: bindActionCreators(chatActions, dispatch),
        authActions: bindActionCreators(authActions, dispatch),
    };
}   
export default connect(mapStateToProps, mapDispatchToProps)(myAccount);
/* 
<Row size={12} style={[styles.proRow,styles.prolistRow,styles._MT0_,styles._PT0_]} key={key}>
                    <Col sm={5} md={4} lg={2} style={[styles.proCol,styles.listCol]} >
                        <View style={styles.colInner}>
                            <TouchableOpacity style={styles.imageCanvas} onPress={()=>{_this.navigateTo('ProductDetail',item)}} activeOpacity={0.8}>
                                <View style={styles.imgInner}>
                                {typeof item.video != 'undefined' && item.video != "" ?
                                 (<Image style={styles.hasVideo1} source={images.video} />):(null)}
                                        {item.image != ''  && (
                                        <SharedElementTransition sharedElementId={'SharedTextId'+item.id}> 
                                             <Image style={styles.proImg} source={{uri :item.image}} />
                                        </SharedElementTransition>)
                                         }                                       
                                        <CheckBox
                                            style={[styles.genChk,styles.absoluteCheck]}
                                            onClick={(checked) =>_this.checked(checked,item.id)}
                                            labelStyle={styles.midum}
                                            isChecked={ck}
                                        />
                                </View>
                                </TouchableOpacity>
                                <View style={[styles.bottomDetail,styles.bottomCaption,styles.relativeCaption]}>
                                    <Text style={[styles.leftAlign,styles.white]}>
                                    <Text style={styles.small}>{item.post_date}</Text>
                                    </Text>
                                    <View style={[styles.rightAlign]}>
                                        <View style={styles.relative}><Text style={[styles.globalIcon,styles.camIco,styles.white,styles.camIcoAbs]}>s</Text><Text style={[styles.bold,styles.abscamtxt,styles.white,styles.small]}>{item.imageCount}</Text></View> 
                                    </View>
                                </View>
                            </View>
                        </Col>
                        <Col sm={7} md={8} lg={10} style={[styles.proCol,styles.listCol]}>
                            <View style={styles.proCaption}>
                                <Text style={[styles.proName]} numberOfLines={1}>{item.title}</Text>
                                <View style={[styles.row,styles._MT10_]}>
                                    <View style={[styles.inlineDatarow,styles.spaceBitw]}>
                                        <Text style={[styles.orangeColor,styles.bold,styles.midum]}>{_this.getDispPrice(item.price)}</Text>                                        
                                        <View style={styles.inlineWithico}>{item.is_urgent == 1 && 
                                            <Text style={[styles.inlineWithico,styles.red,styles.small,styles._MPR5_]}> <Text style={[styles.globalIcon,styles.inlineWithico,styles.red]}>y</Text> Urgent</Text>}
                                            {item.is_direct_sale == 1 && item.is_urgent == 1 ?<Text style={styles.borderRight}></Text> :null}
                                            {item.is_direct_sale == 1 && <Text style={[styles.inlineWithico,styles.red,styles.small,styles._MPR0_]}><Text style={[styles.globalIcon,styles.inlineWithico,styles.red]}> w</Text>  Achat Ditect</Text>}
                                        </View>
                                        
                                    </View>
                                    
                                        {item.remain_days != null &&  <View style={[styles.inlineWithico,{marginTop:5}]}>
                                        <Text style={[styles.globalIcon,styles.big]}>e </Text>

Ankit bhatt, [13.09.17 10:50]
<Text style={styles.icoText}>{item.remain_days} Jours Restants</Text>
                                    </View>} 
                                    <View style={[{height:24,width:60,flexDirection:'row'},styles.justCenter]}>
                                        <View style={[{flex:1,flexDirection:'row'},styles.justCenter]}>
                                            <Icon name="eye" size={12} color={'#888'}/>
                                            <Text style={{color:"#888",fontSize:12,marginLeft:3}}>{item.total_views}</Text>
                                        </View> 
                                        <View style={[{flex:1,flexDirection:'row'},styles.justCenter]}>
                                            <Icon name="telephone" size={12} color={'#888'}/>
                                            <Text style={{color:"#888",fontSize:12,marginLeft:3}}>{item.total_clicks}</Text>    
                                        </View>     
                                </View>      
                                    <View style={[styles.inlineDatarow,{marginTop:5}]}>
                                        <View style={[styles.circle,styles.inlineWithico,item.is_active == 1 ? {backgroundColor:'#72bb53'}:{backgroundColor:'#f15a23'}]}></View><Text style={[styles.inlineWithico,styles.orangeColor]}>{item.is_active == 1 ? 'Approved':'Pending'}</Text>
                                    </View>
                                    <View style={[styles.inlineDatarow,styles.spaceBitw,{marginTop:5}]}>
                                        <View style={[styles.inlineDatarow,styles.spaceBitw]}>
                                                //<View style={{width:55}}>
                                                  //  <Text style={[styles.onlineTxt]}>{item.is_online == 1 ? 'En ligne' :'hors ligne'}//</Text>
                                               // </View> 
                                                
                                                
                                                <View style={[styles.inlineDatarow,styles.justCenter,{marginTop:5,alignSelf:'stretch'}]}>
                                                <View style={{width:15}}>
                                                    <Switch style={styles.genSwitch}  value={item.is_online == 1 ? true:false} onValueChange={(value)=>{_this.changeSwitch(value,key)}} />
                                                </View>
                                        {item.is_active == 1 ? 
                                             item.have_update != 1 ?
                                            (<TouchableOpacity onPress={()=>{_this.navigateTo('Modifier',item)}} activeOpacity={0.8}><Text style={[styles.globalIcon,styles.orangeColor,styles.xlIco]}>h </Text></TouchableOpacity>):(
                                                <TouchableOpacity onPress={()=>{}} activeOpacity={0.8}><Text style={[styles.globalIcon,styles.orangeColor,styles.xlIco,{fontSize:25}]}>{'& '}</Text></TouchableOpacity>)
                                        :(null)}
                                           
                                        <TouchableOpacity onPress={()=>{_this.navigate('RemoveAd',datas)}} activeOpacity={0.8}><Text style={[styles.globalIcon,styles.orangeColor,styles.xlIco]}>8 </Text></TouchableOpacity>

                                        {item.is_active == 1  ? 
                                            (<TouchableOpacity onPress={()=>{_this.navigateTo('PromoteAd',item)}} activeOpacity={0.8}><Text style={[styles.globalIcon,styles.orangeColor,styles.xlIco]}>|</Text></TouchableOpacity>)
                                        :(null)}
                            </View>
                                        </View> 
                                        </View>   
                                </View>
                            </View>
                        </Col>
                    </Row> 


*/