import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  Image,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  ScrollView,
  Switch,
  Platform
} from 'react-native';

import { colors } from '../../config/styles';
import images from '../../config/images';
import styles from '../../config/genStyle';
import Dimensions from 'Dimensions';
import Spinner from 'rn-spinner';
import ApiHelper from '../../lib/ApiHelper/ApiHelper';
import { Icon } from '../../config/icons';
var _ = require('lodash');
// redux specific
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as chatActions from '../../redux/reducers/chat';
import * as authActions from '../../redux/reducers/auth';

const {height, width} = Dimensions.get('window');
const api= new ApiHelper;

class Drawer extends Component {
  
  constructor(props) {
    super(props);
    //console.log(this.props);
    this.state = {
      check1: false,
      tabs: 1,
      tabSelected: 1,
      trueSwitchIsOn: true,
      falseSwitchIsOn: false,
      value: 0,
      currentPage: 'HomeContainer',
      token:null,
      userdata:null,
    };
    // if you want to listen on navigator events, set this up
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }

  // static navigatorButtons = {
  //   leftButtons: [{
  //       icon: require('../../images/back.png'),
  //       id: 'back',
  //       title:'back to welcome',
  //     }],
  // };
  /*static navigatorStyle = {
    navBarBackgroundColor: colors.navBarBackgroundColor,
    navBarTextColor: colors.navBarTextColor,
    navBarSubtitleTextColor: colors.navBarSubtitleTextColor,
    navBarButtonColor: colors.navBarButtonColor,
    statusBarTextColorScheme: colors.statusBarTextColorScheme,
    statusBarColor: colors.statusBarColor,
    tabBarBackgroundColor: colors.tabBarBackgroundColor,
    tabBarButtonColor: colors.tabBarButtonColor,
    tabBarSelectedButtonColor: colors.tabBarSelectedButtonColor,
    navBarSubtitleColor: colors.navBarSubtitleColor,
    navBarHidden: true,
};*/

componentWillReceiveProps(nextProps) {
  if(!_.isEqual(this.props.auth.userdata,nextProps.auth.userdata)){
    this.setState({});
  }
}

  onNavigatorEvent(event) {
    if (event.type == 'DeepLink') {
          //this._toggleDrawer();
            const parts = event.link;
            const payload = event.payload;
                if (typeof parts != 'undefined' && typeof payload != 'undefined' && parts == 'Drawer') {
                   //console.log(event);
                   if(payload.login){
                     this.setState({currentPage:payload.screen,token:payload.token,userdata:payload.userdata});
                   } else {
                      this.setState({currentPage:payload.screen});
                   }
                   
            }
    }
    //console.log(event)
    //this.props.navigator.toggleNavBar({});
    //this.props.navigator.setStyle({navBarTitleTextCentered: true});
    //this.props.navigator.setTitle({title: "Chariot"});
    // if (event.id == 'back') {
    //   this.props.navigator.pop({
    //       animated: true // does the pop have transition animation or does it happen immediately (optional)
    //     });
    // }
  }

  goto(page){
    this._toggleDrawer();

    if (page != this.state.currentPage) {
      let payload =  {};
      if(page === 'Cart') {
          payload = {
            popBack : true
          };
      }
      this.props.navigator.handleDeepLink({link: page,payload});
    }
  }

  gotoMyProfile(){
    this._toggleDrawer();
    this.props.navigator.handleDeepLink({link: 'myAccount',payload:{
      openProfile:2
    }});
  }

  _toggleDrawer() {
    this.props.navigator.toggleDrawer({
        to: 'closed',
        side: 'left',
        animated: true,
    });
  }//end _toggleDrawer

  _renderItem(page,icon,iconName,label,selected){
    return(
       <TouchableOpacity  style={selected ? [styles.menuListSelected,styles.menuList]:styles.menuList}
                  onPress={() => {this.goto(page)}} activeOpacity={0.8} >
                  <Icon name={iconName} size={15} color={selected?"#fff":"#000"} style={{marginRight:5}}/>
                  {/* <Text style={[styles.globalIcon, styles.icoMenu,selected? {color:  "#fff"} : null]}>{icon}</Text> */}
                  <Text style={[styles.menuLink,selected? {color:  "#fff"} : null]}>{label}</Text>
      </TouchableOpacity>
    );
  }

  getName(data){
    let name = '';
    name = (data.usertype === "professional" ? data.companyname != '' ? data.companyname :data.username : data.username) + '\n(' + data.user_credit_display + colors.euro + ')' + (data.usertype === "professional" ? ' Pro' : '');
    return name;
  }

  _renderProfile(){
    // console.log(this.props.auth.userdata);
    // console.log(this.props.auth.userdata.image);
    return(
      <TouchableOpacity  
        style={{height:50,flexDirection:'row',alignItems:'center',justifyContent:'center',
                padding:5,paddingLeft:10,backgroundColor:"#f0f0f0"}}
            onPress={() => { this.gotoMyProfile()}} activeOpacity={0.8} >
            <View style={{
              alignItems:'center',justifyContent:'center'
            }}>
              <Image style={{
                height:35,width:35,borderRadius:17.5,
                //borderColor:colors.navBarButtonColor,
                //borderWidth:1.5
              }} source={{uri:this.props.auth.userdata.image}}/>
            </View>
            <View style={{
              //alignItems:'center',
              paddingLeft:10,
              justifyContent:'center',
              flex:1
            }}>
              <Text style={{
              //color:"#333",
                fontSize:13,
                fontWeight:'bold'
                }}>{this.getName(this.props.auth.userdata)}
              </Text>
            </View>
      </TouchableOpacity>
    );
  }

  render() {
    const {token}=this.props.auth;
    let currentPage = this.props.chat.screen;
    return (

      <View style={[styles.drawerView]}>
        <View style={{
          height: Platform.OS=='ios' ? 22 :0
        }}></View>
        <View style={styles.drawerImgCont}>
          <Image style={styles.drawerImage} source={images.companylogomain} />
        </View>
        <ScrollView
          showsVerticalScrollIndicator={false}
          style={[styles.mainScollview, styles.drawerScroll]}>
          <View style={[styles.scrollInner]}>
            <View style={[styles.SideBar, styles._PB15_]}>
                {api.checkLogin(token) ? this._renderProfile():null}
                {!api.checkLogin(token) ?
                (this._renderItem('WelcomeContainer','2','user','S\'identifier / S’inscrire',currentPage=='WelcomeContainer')):(null)}
                {this._renderItem('HomeContainer','F','franceMap','Accueil',currentPage=='HomeContainer')}
                {this._renderItem('ProductgridContainer','[','camera','Les annonces',currentPage=='ProductgridContainer')}
                {this._renderItem('myAccount','2','user','Tableau de bord',currentPage=='myAccount')}
                {this._renderItem('favoris','m','like','Favoris',currentPage=='favoris')}
                {this._renderItem('Chat','$','conversation','Chat',currentPage=='Chat')}
                {this._renderItem('postad','h','edit','Déposer une annonce',currentPage=='postad')}
                {this._renderItem('PracticalInfo','.','information','Infos pratiques',currentPage=='PracticalInfo')}
                {this._renderItem('Cart','w','shopping-cart','Panier',currentPage=='Cart')}
                {api.checkLogin(token) ? (this._renderItem('WelcomeContainer','P','power-button','déconnexion',currentPage=='WelcomeContainer')):(null)}
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}


function mapStateToProps(state, ownProps) {
	return {
        chat:state.chat,
        auth:state.auth,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		chatActions: bindActionCreators(chatActions, dispatch),
		authActions: bindActionCreators(authActions, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(Drawer);