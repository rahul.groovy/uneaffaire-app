import React, { Component } from 'react';
import {
  StyleSheet,
  Image,Text,View,TextInput,TouchableOpacity,ScrollView,Dimensions,Platform,Animated,WebView,Switch
} from 'react-native';

import images from '../../config/images';
import styles from '../../config/genStyle';
import { colors } from '../../config/styles';
import RadioButton from '../../lib/react-radio/index';
import {Navigation,Screen} from 'react-native-navigation';
import CheckBox from 'react-native-check-box';
import {Column as Col, Row} from 'react-native-flexbox-grid';
import ModalDropdown from '../../lib/react-native-modal-dropdown';
import FloatLabelTextInput from '../../lib/react-native-floating-label-text-input';
const HEADER_MAX_HEIGHT = 100;
const HEADER_MIN_HEIGHT = Platform.OS === 'ios' ? 50 : 50;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;
let categoryContent;
export default class avant extends Component {
    // static navigatorButtons = {
    //     leftButtons: [{
    //     icon: images.back,
    //     id: 'back',
    //     title:'back to welcome',
    //     }],
    // };

  
static navigatorStyle = {
     navBarBackgroundColor: colors.navBarBackgroundColor,
     navBarTextColor: colors.navBarTextColor,
     navBarSubtitleTextColor: colors.navBarSubtitleTextColor,
     navBarButtonColor: colors.navBarButtonColor,
     statusBarTextColorScheme: colors.statusBarTextColorScheme,
     statusBarColor: colors.statusBarColor,
     tabBarBackgroundColor: colors.tabBarBackgroundColor,
     tabBarButtonColor: colors.tabBarButtonColor,
     tabBarSelectedButtonColor: colors.tabBarSelectedButtonColor,
     navBarSubtitleColor: colors.navBarSubtitleColor,
     navBarTextFontFamily: colors.navBarTextFontFamily,
    //  navBarHidden: false,
    //  tabBarHidden: false,
     disableOpenGesture: true,
     drawUnderTabBar: true,
};

constructor(props) {
    super(props);
    // if you want to listen on navigator events, set this up
    this.state={
        check1:false,
        check2:false,
        checkbox:false,
        tabs:1,
        tabSelected1:1,
        tabSelected:1,
        like:1,
        scrolled:true,
        scrollY: new Animated.Value(0),
        value:1,
        value1:1,
        visible:0,
        text:"",
        SRC : 'https://www.youtube.com/embed/0DI0WBuicV0',
        backArrow:true,
        onBack:null,
    };
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
}

sendEventToDrawer(){
    this.props.navigator.handleDeepLink({link: 'Drawer',payload:{
        screen:this.props.testID
    }});
}


 onNavigatorEvent(event) {
    //console.log(event);
    if (event.id === 'bottomTabSelected') {
       this.setState({onBack:event.unselectedTabIndex});
    }
      if(event.id=='willAppear'){
          this.sendEventToDrawer();
         this.props.navigator.toggleNavBar({
            to: 'shown',
            animated: false
            });
    this.props.navigator.toggleTabs({
        animated: false,
        to: 'hidden'
    });
    this.props.navigator.setStyle({navBarTitleTextCentered: true});
    this.props.navigator.setTitle({
      title: "Déposer une annonce",
    });
     }
    if(event.id=='didAppear'){

     }
     if(event.id =='didDisappear'){
        this.setState({onBack:null});
    }
   
    if(event.id == 'back'){
        if(this.state.onBack!=null){
            this.props.navigator.switchToTab({
                tabIndex: this.state.onBack // (optional) if missing, this screen's tab will become selected
            });
        }else{
        this.props.navigator.pop({
            animated: true // does the pop have transition animation or does it happen immediately (optional)
        });
        }
    }
   
  }
 handleOnPress(value){
        this.setState({value:value})
 }
 handleOnPress1(value1){
        this.setState({value1:value1})
 }

  goto(page){
        this.props.navigator.push({
            screen: page
        }); 
  }
  tabchange(tab){
        this.setState({tabs:tab,tabSelected:tab});
  }
  tabchange1(tab){
    this.setState({tabs1:tab,tabSelected1:tab});
  }


  onScroll(event){
        var currentOffset = event.nativeEvent.contentOffset.y;
        var direction = currentOffset > this.offset ? this.setState({scrolled:true}) : this.setState({scrolled:false});
        this.offset = currentOffset;       
}

gettrans(bool){
if(bool){
      return  this.state.scrollY.interpolate({
        inputRange: [0, 50],
        outputRange: [0, -50],
        extrapolate: 'clamp',
    });
    }else{
       return  this.state.scrollY.interpolate({
        inputRange: [0, 50],
        outputRange: [-50, 0],
        extrapolate: 'clamp',
    });  
    }
}
showhide(value){
    if(this.state.visible == 0){
        this.setState({visible:1})
        categoryContent  =   
                    <View style={[styles.flex,styles._HP15_]}>
                        <View style={[styles.inlineDatarow]}>
                            <FloatLabelTextInput style={[styles.genInput,]} placeholder={"Prix de"} underlineColorAndroid={'#eee'}/>                                    
                            <FloatLabelTextInput style={[styles.genInput,styles._ML5_]} placeholder={"Prix a"} underlineColorAndroid={'#eee'}/>
                        </View>
                        <View style={[styles.inlineDatarow]}>
                            <FloatLabelTextInput style={[styles.genInput,]} placeholder={"Km de"} underlineColorAndroid={'#eee'}/>                                    
                            <FloatLabelTextInput style={[styles.genInput,styles._ML5_]} placeholder={"Km a"} underlineColorAndroid={'#eee'}/>
                        </View>
                        <View style={[styles.inlineDatarow]}>
                            <FloatLabelTextInput style={[styles.genInput,]} placeholder={"Anne de"} underlineColorAndroid={'#eee'}/>                                    
                            <FloatLabelTextInput style={[styles.genInput,styles._ML5_]} placeholder={"Anne a"} underlineColorAndroid={'#eee'}/>
                        </View>    
                    </View>
    }
    else{
         this.setState({visible:0})
         categoryContent  =<View></View>
    }
}
    
        
    

    videoUrl(){
        if(this.state.text != ""){
            this.setState({SRC:this.state.text});
        }else{
            return false
        }
    }

    videoShow(){
        this.setState({check1: !this.state.check1});
    }

    render() {
        const onTintColor = "#ffa382";
        const thumbTintColor = "#ff0000";
        const tintColor = "#b2b2b2"; 
        const headerTranslate =  this.state.scrollY.interpolate({
                inputRange: [0, 50],
                outputRange: [0, -50],
                extrapolate: 'clamp',
        });
        // let youtubeVideo  = this.videoShow();
    //this.setState({check1:true})
     
        // let youtubeVideo = this.state.check1 == true ?
        //                     <View>
        //                         <View style={styles.inlineDatarow}><FloatLabelTextInput placeholder={'Adresse URL YouTube'} onChangeTextValue={(text) => this.setState({text:text})}/><TouchableOpacity onPress={()=>{this.videoUrl()}} activeOpacity={0.8}><Text style={[styles.globalIcon,styles.bigIco]}>h</Text></TouchableOpacity></View>
        //                         <WebView style={styles.youVideo} source={{html: '<iframe width="100%" height="100%" src="'+this.state.SRC+'" frameborder="0" allowfullscreen></iframe>'}} />
        //                     </View>: alert(55);
    

    if(this.state.tabs == 1){
        postadTab = 
        <ScrollView showsVerticalScrollIndicator={false} style={styles.mainScollview}>
                     <View style={styles.scrollInner}>   
                        <View style={styles.postBlock}>
                            <View style={[styles.inlineDatarow,styles._PB10_,styles.jusRight]}><Text style={[styles.orangeColor]}>{'Prix de I\'annonce N° 1  :   0,00 €   |    '}</Text><Text style={[styles.globalIcon,styles.orangeColor,styles._F20_]}>8</Text></View>
                            <View style={styles.imgUploadView}>
                                <View style={styles.imgUpload}>
                                    <View>
                                        <View style={[styles.flex,styles._MT10_,styles.fullupImgView]}>
                                            <Image source={images.homestay} style={styles.fullupImg}/>
                                            <TouchableOpacity activeOpacity={0.8}  style={styles.cancelImg}>
                                                    <Text style={styles.globalIcon}>{'}'}</Text>
                                            </TouchableOpacity>
                                        </View> 
                                            <ScrollView  ref={(scrollView) => { _scrollView = scrollView; }} automaticallyAdjustContentInsets={false} showsHorizontalScrollIndicator={false} contentContainerStyle={[styles.Carousel]}
                                                horizontal>
                                                        <View style={[styles.inlineDatarow,styles._MT5_]}>
                                                                <View style={styles.placeHolderview}><Image style={[styles.placeHolderImg]} source={images.minic}/>
                                                                    <TouchableOpacity activeOpacity={0.8}  style={styles.cancelImg}>
                                                                        <Text style={styles.globalIcon}>{'}'}</Text>
                                                                    </TouchableOpacity>
                                                                </View>
                                                                <View style={styles.placeHolderview}><Image style={[styles.placeHolderImg]} source={images.IMG16}/>
                                                                    <TouchableOpacity activeOpacity={0.8}  style={styles.cancelImg}>
                                                                        <Text style={styles.globalIcon}>{'}'}</Text>
                                                                    </TouchableOpacity>
                                                                </View>
                                                                <View style={styles.placeHolderview}><Image style={[styles.placeHolderImg]} source={images.IMG16}/>
                                                                    <TouchableOpacity activeOpacity={0.8}  style={styles.cancelImg}>
                                                                        <Text style={styles.globalIcon}>{'}'}</Text>
                                                                    </TouchableOpacity>
                                                                </View>
                                                                <View style={styles.placeHolderview}><Image style={[styles.placeHolderImg]} source={images.IMG16}/>
                                                                    <TouchableOpacity activeOpacity={0.8}  style={styles.cancelImg}>
                                                                        <Text style={styles.globalIcon}>{'}'}</Text>
                                                                    </TouchableOpacity>
                                                                </View>
                                                                <View style={styles.placeHolderview}><Image style={[styles.placeHolderImg]} source={images.IMG16}/>
                                                                    <TouchableOpacity activeOpacity={0.8}  style={styles.cancelImg}>
                                                                        <Text style={styles.globalIcon}>{'}'}</Text>
                                                                    </TouchableOpacity>
                                                                </View>
                                                            </View>    
                                            </ScrollView>
                                    </View>
                                {/*<Text style={styles.grayColor}>{'Ajoutez Jusqu\'a 10 photo'}</Text>*/}
                                </View>
                                <TouchableOpacity elevation={5}  activeOpacity={0.8} style={styles.postadcamera}>
                                    <Text style={[styles.globalIcon,styles.cameraAd]}>A</Text>
                                </TouchableOpacity>  
                            </View>
                        </View>
                        <View style={styles._MT10_}>
                            <View style={[styles.row,styles._MT5_]}>
                                    <CheckBox style={[styles.genChk]}
                                        onClick={(checked) =>{this.setState({check2:!checked})}}
                                        labelStyle={styles.midum} isChecked={this.state.check2}
                                        rightText={'Pack 6 photos supplementaires - 1,00€'}
                                    />
                            </View>
                            <View>
                                <View style={[styles.inlineWithico,styles.justLeft,styles._MT5_]}>
                                        <CheckBox style={[styles.genChk]}
                                            onClick={this.videoShow.bind(this)}
                                            labelStyle={styles.midum} isChecked={this.state.check1}
                                            rightText={'Video - 1,00€'}
                                         />  
                                </View>
                                {this.state.check1 ?
                                <View>
                                    <View style={styles.inlineDatarow}><FloatLabelTextInput placeholder={'Adresse URL YouTube'} onChangeTextValue={(text) => this.setState({text:text})}/><TouchableOpacity onPress={()=>{this.videoUrl()}} activeOpacity={0.8}><Text style={[styles.globalIcon,styles.bigIco]}>h</Text></TouchableOpacity></View>
                                    {/*<WebView style={styles.youVideo} source={{html: '<iframe width="100%" height="100%" src="'+this.state.SRC+'" frameborder="0" allowfullscreen></iframe>'}} />*/}
                                    <View style={styles.videoRow}><Image style={styles.videoIco} source={images.videoIco} /></View>
                                 </View>:null}
                            </View>   
                        </View>  
                        <View style={styles._PTB10_}>
                            <TouchableOpacity style={[styles.inlineDatarow,styles.spaceBitw,styles.borderBottom,styles._PB5_]} activeOpacity={0.8} onPress={()=>{this.goto('mettreAvant')}}>
                                <Text style={[styles._F16_,styles.linkAvant]} >Mettre en avant</Text>
                                <Text style={[styles.globalIcon,styles.orangeColor]}>6</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={[styles.inlineDatarow,styles.spaceBitw,styles._MT5_]} activeOpacity={0.8} onPress={()=>{this.goto('EnVente')}}>
                                <Text style={[styles._F16_,styles.linkAvant]} >En Vente Direct </Text>
                                <Text style={[styles.globalIcon,styles.orangeColor]}>6</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={[styles.inlineDatarow,styles._MT10_]}>
                            <RadioButton currentValue={this.state.value} value={1} onPress={this.handleOnPress.bind(this)}><Text style={[styles.readiText]}>OFFRES</Text></RadioButton>
                            <RadioButton currentValue={this.state.value} value={0} onPress={this.handleOnPress.bind(this)}><Text style={[styles.readiText]}>DEMANDES</Text></RadioButton>
                        </View>
                        <View style={[styles._MT20_]}>
                            <View style={[styles.subTitleBgView]}>
                                 <Text style={[styles.subTitleBg]}>Details</Text>
                            </View>
                            <View style={[styles.relative]}>
                                    <FloatLabelTextInput style={styles.genInput} placeholder={"Choisissez un titre"} underlineColorAndroid={'#eee'}/> 
                                    <Text style={[styles.globalIcon,styles.dropabsIco,styles._T18_]}>h</Text>
                             </View>
                            <View style={styles._MT10_}>
                                <View style={styles.framePstad}>
                                    <View style={styles.relative}>
                                        <TouchableOpacity activeOpacity={0.8} onPress={this.showhide.bind(this)}><View style={[styles.relative,styles.Borderbtmframe,styles._MT5_]}>
                                            {/*<ModalDropdown options={['Choisissiez la Category', 'One']} style={[styles.dropdown_2,styles.dropDown,styles.dropDownHome]} textStyle={[styles.dropdown_2_text,styles._F14_,styles.grayColor]} dropdownStyle={styles.dropdown_2_dropdown} />*/}
                                            <Text style={styles._F14_}>{'Choisissez la catégorie'}</Text>
                                            <Text style={[styles.globalIcon,styles.dropabsIco]}>g</Text>
                                        </View></TouchableOpacity>
                                        {categoryContent}
                                    </View>
                                    <View style={(this.state.visible == 0) ? styles.relative : [styles.relative,styles.txtareaOpen]}>
                                        <TextInput underlineColorAndroid='#eeeeee'  multiline={true} numberOfLines={2} placeholder={"Faite un descriptif approprié de l’annonce"} />
                                        <Text style={[styles.globalIcon,styles.textareaIco]}>h</Text>
                                    </View>
                                </View>
                                <View style={[styles.inlineDatarow,styles._MT10_]}>
                                    <FloatLabelTextInput style={[styles.genInput]}   placeholder={"Prix *"} underlineColorAndroid={'#eee'}/>
                                        <CheckBox style={[styles.genChk]}
                                        onClick={(checked) =>{this.setState({check1:!checked})}}
                                        labelStyle={[styles.midum,styles.orange]} isChecked={this.state.check1}
                                        rightText={'Option Urgent - 1,00€'}
                                    />
                                </View>
                                <View style={styles.relative}>
                                    <FloatLabelTextInput style={[styles.genInput,styles.absicoinput]} placeholder={"Ville ou code postal...."} underlineColorAndroid={'#eee'}/> 
                                    <Text style={[styles.globalIcon,styles.dropabsIco,styles._T18_]}>t</Text>
                                </View>
                             </View>
                        </View>
                         <View style={[styles._MT20_,styles._PB15_]}>
                            <View style={[styles.subTitleBgView]}>
                                 <Text style={[styles.subTitleBg]}>Qui je suis</Text>
                            </View>
                            <View style={styles._MT10_}>
                                <View style={[styles.tabParent]}>
                                    <View style={[styles.tabs,styles._PB0_]}>
                                        <TouchableOpacity  style={(this.state.tabSelected1== 1)?[styles.tabSelected,styles.widthHalf]:[styles.tabnotSelected,styles.widthHalf]} onPress={()=>{this.tabchange1(1)}} activeOpacity={0.8}>
                                            <Text style={(this.state.tabSelected1== 1)?[styles.tabTextSelected,styles.orangeColor]:[styles.tabText]}>Particuliers</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity  style={(this.state.tabSelected1== 2)?[styles.tabSelected,styles.widthHalf]:[styles.tabnotSelected,styles.widthHalf]} onPress={()=>{this.tabchange1(2)}} activeOpacity={0.8}>
                                            <Text style={(this.state.tabSelected1== 2)?[styles.tabTextSelected,styles.orangeColor]:[styles.tabText]}>Professionnels</Text>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={styles.botomOrangeLine}></View>
                                </View>
                                <View style={[styles.relative]}>
                                    <FloatLabelTextInput style={styles.genInput} placeholder={"Pseudo"} underlineColorAndroid={'#eee'}/> 
                                    <Text style={[styles.globalIcon,styles.dropabsIco,styles._T18_]}>h</Text>
                                </View>
                                <View style={styles.relative}>
                                        <FloatLabelTextInput style={styles.genInput} placeholder={"Votre N° de TelePhone"} underlineColorAndroid={'#eee'}/> 
                                        <Text style={[styles.globalIcon,styles.dropabsIco,styles._T18_]}>h</Text>
                                </View>
                                <FloatLabelTextInput style={styles.genInput} placeholder={"Email"} underlineColorAndroid={'#eee'}/> 
                                <View style={[styles.inlineDatarow,styles.spaceBitw,styles._MT15_]}>
                                        <Text>Cacher le numero de telephone</Text>
                                        <Switch onTintColor={colors.navBarSubtitleTextColor} 
                                        //thumbTintColor={colors.navBarSubtitleTextColor}
                                        onValueChange={(value) => this.setState({trueSwitchIsOn1: value})} value={this.state.trueSwitchIsOn1} /> 
                                </View>
                             </View>
                         </View>    
                    </View>
                </ScrollView>
    }
    else if(this.state.tabs == 2){
        postadTab = 
                   <ScrollView showsVerticalScrollIndicator={false} style={styles.mainScollview}>
                     <View style={styles.scrollInner}>   
                        <View style={styles.postBlock}>
                            <View style={[styles.inlineDatarow,styles._PB10_,styles.jusRight]}><Text style={[styles.orangeColor]}>{'Prix de I\'annonce N° 1  :   0,00 €   |    '}</Text><Text style={[styles.globalIcon,styles.orangeColor,styles._F20_]}>8</Text></View>
                            <View style={styles.imgUploadView}>
                                <View style={styles.imgUpload}>
                                    <View>
                                        <View style={[styles.flex,styles._MT10_,styles.fullupImgView]}>
                                            <Image source={images.homestay} style={styles.fullupImg}/>
                                            <TouchableOpacity activeOpacity={0.8}  style={styles.cancelImg}>
                                                    <Text style={styles.globalIcon}>{'}'}</Text>
                                            </TouchableOpacity>
                                        </View>  
                                        <ScrollView  ref={(scrollView) => { _scrollView = scrollView; }} automaticallyAdjustContentInsets={false} showsHorizontalScrollIndicator={false} contentContainerStyle={styles.Carousel}
                                            horizontal>
                                                    <View style={[styles.inlineDatarow,styles._MT5_]}>
                                                            <View style={styles.placeHolderview}><Image style={[styles.placeHolderImg]} source={images.minic}/>
                                                                <TouchableOpacity activeOpacity={0.8}  style={styles.cancelImg}>
                                                                    <Text style={styles.globalIcon}>{'}'}</Text>
                                                                </TouchableOpacity>
                                                            </View>
                                                            <View style={styles.placeHolderview}><Image style={[styles.placeHolderImg]} source={images.IMG16}/>
                                                                <TouchableOpacity activeOpacity={0.8}  style={styles.cancelImg}>
                                                                    <Text style={styles.globalIcon}>{'}'}</Text>
                                                                </TouchableOpacity>
                                                            </View>
                                                            <View style={styles.placeHolderview}><Image style={[styles.placeHolderImg]} source={images.IMG16}/>
                                                                <TouchableOpacity activeOpacity={0.8}  style={styles.cancelImg}>
                                                                    <Text style={styles.globalIcon}>{'}'}</Text>
                                                                </TouchableOpacity>
                                                            </View>
                                                            <View style={styles.placeHolderview}><Image style={[styles.placeHolderImg]} source={images.IMG16}/>
                                                                <TouchableOpacity activeOpacity={0.8}  style={styles.cancelImg}>
                                                                    <Text style={styles.globalIcon}>{'}'}</Text>
                                                                </TouchableOpacity>
                                                            </View>
                                                            <View style={styles.placeHolderview}><Image style={[styles.placeHolderImg]} source={images.IMG16}/>
                                                                <TouchableOpacity activeOpacity={0.8}  style={styles.cancelImg}>
                                                                    <Text style={styles.globalIcon}>{'}'}</Text>
                                                                </TouchableOpacity>
                                                            </View>
                                                        </View>    
                                        </ScrollView>
                                    </View>
                                {/*<Text style={styles.grayColor}>{'Ajoutez Jusqu\'a 10 photo'}</Text>*/}
                                </View>
                                <TouchableOpacity elevation={5}  activeOpacity={0.8} style={styles.postadcamera}>
                                    <Text style={[styles.globalIcon,styles.cameraAd]}>A</Text>
                                </TouchableOpacity>  
                            </View>
                        </View>
                        <View style={styles._MT10_}>
                            <View style={[styles.row,styles._MT5_]}>
                                    <CheckBox style={[styles.genChk]}
                                        onClick={(checked) =>{this.setState({check1:!checked})}}
                                        labelStyle={styles.midum} isChecked={this.state.check1}
                                        rightText={'Pack 6 photos supplementaires - 1,00€'}
                                    />
                            </View>
                            <View>
                                <View style={[styles.inlineWithico,styles.justLeft,styles._MT5_]}>
                                        <CheckBox style={[styles.genChk]}
                                            onClick={(checked) =>{this.setState({check1:!checked})}}
                                            labelStyle={styles.midum} isChecked={this.state.check1}
                                            rightText={'Video - 1,00€'}
                                        />  
                                 </View>     
                            </View>   
                        </View>  
                        <View style={styles._PTB10_}>
                            <View style={[styles.inlineDatarow,styles.spaceBitw,styles.borderBottom,styles._PB5_]}>
                                <TouchableOpacity activeOpacity={0.8}><Text style={[styles._F16_,styles.linkAvant]} onPress={()=>{this.goto('mettreAvant')}}>Mettre en avant</Text></TouchableOpacity>
                                <Text style={[styles.globalIcon,styles.orangeColor]}>6</Text>
                            </View>
                            <View style={[styles.inlineDatarow,styles.spaceBitw,styles._MT5_]}>
                                <TouchableOpacity activeOpacity={0.8}><Text style={[styles._F16_,styles.linkAvant]} onPress={()=>{this.goto('EnVente')}}>En Vente Direct </Text></TouchableOpacity>
                                <Text style={[styles.globalIcon,styles.orangeColor]}>6</Text>
                            </View>
                        </View>
                        <View style={[styles.inlineDatarow,styles._MT10_]}>
                            <RadioButton currentValue={this.state.value} value={1} onPress={this.handleOnPress.bind(this)}><Text style={[styles.readiText,styles._PR15_,styles._ML5_]}>OFFRES</Text></RadioButton>
                            <RadioButton currentValue={this.state.value} value={0} onPress={this.handleOnPress.bind(this)}><Text style={[styles.readiText,styles._PR15_,styles._ML5_]}>DEMANDES</Text></RadioButton>
                        </View>
                        <View style={[styles.relative,styles._MT10_]}>
                            <FloatLabelTextInput style={styles.genInput} placeholder={"Choisissiez un titre"} underlineColorAndroid={'#eee'}/> 
                            <Text style={[styles.globalIcon,styles.dropabsIco,styles._T18_]}>h</Text>
                        </View>
                        <View style={[styles._MT10_]}>
                            <Text style={[styles.subTitle]}>Details</Text>
                            <View style={styles._MT10_}>
                                <View style={styles.framePstad}>
                                    <View style={styles.relative}>
                                        <View style={[styles.relative,styles.Borderbtmframe,styles._MT5_]}>
                                            {/*<ModalDropdown options={['Choisissiez la Category', 'One']} style={[styles.dropdown_2,styles.dropDown,styles.dropDownHome]} textStyle={[styles.dropdown_2_text,styles._F14_,styles.grayColor]} dropdownStyle={styles.dropdown_2_dropdown} />*/}
                                            <TouchableOpacity activeOpacity={0.8} onPress={this.showhide.bind(this)}><Text style={styles._F14_}>{'Choisissiez la Category'}</Text></TouchableOpacity>
                                            <Text style={[styles.globalIcon,styles.dropabsIco]}>g</Text>
                                        </View>
                                        {categoryContent}
                                    </View>
                                    <View style={(this.state.visible == 0) ? styles.relative : [styles.relative,styles.txtareaOpen]}>
                                        <TextInput underlineColorAndroid='#eeeeee'  multiline={true} numberOfLines={2} placeholder={"Faites un descriptif approprie de i ' annuonce"} />
                                        <Text style={[styles.globalIcon,styles.textareaIco]}>h</Text>
                                    </View>
                                </View>
                                <View style={[styles.inlineDatarow,styles._MT10_]}>
                                    <FloatLabelTextInput style={[styles.genInput]}   placeholder={"Prix *"} underlineColorAndroid={'#eee'}/>
                                        <CheckBox style={[styles.genChk]}
                                        onClick={(checked) =>{this.setState({check1:!checked})}}
                                        labelStyle={[styles.midum,styles.orange]} isChecked={this.state.check1}
                                        rightText={'Option Urgent - 1,00€'}
                                    />
                                </View>
                                <View style={styles.relative}>
                                    <FloatLabelTextInput style={[styles.genInput,styles.absicoinput]} placeholder={"Ville ou code postal...."} underlineColorAndroid={'#eee'}/> 
                                    <Text style={[styles.globalIcon,styles.dropabsIco,styles._T18_]}>t</Text>
                                </View>
                             </View>
                        </View>
                         <View style={[styles._MT20_,styles._PB15_]}>
                            <Text style={[styles.subTitle]}>Qui je suis</Text>
                            <View style={styles._MT10_}>
                                <View style={[styles.tabParent]}>
                                    <View style={[styles.tabs,styles._PB0_]}>
                                        <TouchableOpacity  style={(this.state.tabSelected1== 1)?[styles.tabSelected,styles.widthHalf]:[styles.tabnotSelected,styles.widthHalf]} onPress={()=>{this.tabchange1(1)}} activeOpacity={0.8}>
                                            <Text style={(this.state.tabSelected1== 1)?[styles.tabTextSelected,styles.orangeColor]:[styles.tabText]}>Particuliers</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity  style={(this.state.tabSelected1== 2)?[styles.tabSelected,styles.widthHalf]:[styles.tabnotSelected,styles.widthHalf]} onPress={()=>{this.tabchange1(2)}} activeOpacity={0.8}>
                                            <Text style={(this.state.tabSelected1== 2)?[styles.tabTextSelected,styles.orangeColor]:[styles.tabText]}>Professionnels</Text>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={styles.botomOrangeLine}></View>
                                </View>
                                <View style={[styles.relative]}>
                                    <FloatLabelTextInput style={styles.genInput} placeholder={"Comment Vous"} underlineColorAndroid={'#eee'}/> 
                                    <Text style={[styles.globalIcon,styles.dropabsIco,styles._T18_]}>h</Text>
                                </View>
                                <View style={styles.relative}>
                                        <FloatLabelTextInput style={styles.genInput} placeholder={"Entin,Votre No "} underlineColorAndroid={'#eee'}/> 
                                        <Text style={[styles.globalIcon,styles.dropabsIco,styles._T18_]}>h</Text>
                                </View>
                                <FloatLabelTextInput style={styles.genInput} placeholder={"jamjam73100@yahoo.fr"} underlineColorAndroid={'#eee'}/> 
                                <View style={[styles.inlineDatarow,styles.spaceBitw,styles._MT15_]}>
                                        <Text>Cacher le numero de telephone</Text>
                                        <Switch onTintColor={onTintColor}  thumbTintColor={thumbTintColor} tintColor={tintColor} onValueChange={(value) => this.setState({trueSwitchIsOn1: value})} value={this.state.trueSwitchIsOn1} /> 
                                </View>
                             </View>
                         </View>    
                    </View>
                </ScrollView>
        }
     else if(this.state.tabs == 3){
        postadTab = 
                <ScrollView showsVerticalScrollIndicator={false} style={styles.mainScollview}>
                     <View style={styles.scrollInner}>   
                        <View style={styles.postBlock}>
                            <View style={[styles.inlineDatarow,styles._PB10_,styles.jusRight]}><Text style={[styles.orangeColor]}>{'Prix de I\'annonce N° 1  :   0,00 €   |    '}</Text><Text style={[styles.globalIcon,styles.orangeColor,styles._F20_]}>8</Text></View>
                            <View style={styles.imgUploadView}>
                                <View style={styles.imgUpload}>
                                    <View>
                                        <View style={[styles.flex,styles._MT10_,styles.fullupImgView]}>
                                            <Image source={images.homestay} style={styles.fullupImg}/>
                                            <TouchableOpacity activeOpacity={0.8}  style={styles.cancelImg}>
                                                    <Text style={styles.globalIcon}>{'}'}</Text>
                                            </TouchableOpacity>
                                        </View>  
                                        <ScrollView  ref={(scrollView) => { _scrollView = scrollView; }} automaticallyAdjustContentInsets={false} showsHorizontalScrollIndicator={false} contentContainerStyle={styles.Carousel1}
                                            horizontal>
                                                    <View style={[styles.inlineDatarow,styles._MT5_]}>
                                                            <View style={styles.placeHolderview}><Image style={[styles.placeHolderImg]} source={images.minic}/>
                                                                <TouchableOpacity activeOpacity={0.8}  style={styles.cancelImg}>
                                                                    <Text style={styles.globalIcon}>{'}'}</Text>
                                                                </TouchableOpacity>
                                                            </View>
                                                            <View style={styles.placeHolderview}><Image style={[styles.placeHolderImg]} source={images.IMG16}/>
                                                                <TouchableOpacity activeOpacity={0.8}  style={styles.cancelImg}>
                                                                    <Text style={styles.globalIcon}>{'}'}</Text>
                                                                </TouchableOpacity>
                                                            </View>
                                                            <View style={styles.placeHolderview}><Image style={[styles.placeHolderImg]} source={images.IMG16}/>
                                                                <TouchableOpacity activeOpacity={0.8}  style={styles.cancelImg}>
                                                                    <Text style={styles.globalIcon}>{'}'}</Text>
                                                                </TouchableOpacity>
                                                            </View>
                                                            <View style={styles.placeHolderview}><Image style={[styles.placeHolderImg]} source={images.IMG16}/>
                                                                <TouchableOpacity activeOpacity={0.8}  style={styles.cancelImg}>
                                                                    <Text style={styles.globalIcon}>{'}'}</Text>
                                                                </TouchableOpacity>
                                                            </View>
                                                            <View style={styles.placeHolderview}><Image style={[styles.placeHolderImg]} source={images.IMG16}/>
                                                                <TouchableOpacity activeOpacity={0.8}  style={styles.cancelImg}>
                                                                    <Text style={styles.globalIcon}>{'}'}</Text>
                                                                </TouchableOpacity>
                                                            </View>
                                                        </View>    
                                        </ScrollView>
                                    </View>
                                {/*<Text style={styles.grayColor}>{'Ajoutez Jusqu\'a 10 photo'}</Text>*/}
                                </View>
                                <TouchableOpacity elevation={5}  activeOpacity={0.8} style={styles.postadcamera}>
                                    <Text style={[styles.globalIcon,styles.cameraAd]}>A</Text>
                                </TouchableOpacity>  
                            </View>
                        </View>
                        <View style={styles._MT10_}>
                            <View style={[styles.row,styles._MT5_]}>
                                    <CheckBox style={[styles.genChk]}
                                        onClick={(checked) =>{this.setState({check1:!checked})}}
                                        labelStyle={styles.midum} isChecked={this.state.check1}
                                        rightText={'Pack 6 photos supplementaires - 1,00€'}
                                    />
                            </View>
                            <View>
                                <View style={[styles.inlineWithico,styles.justLeft,styles._MT5_]}>
                                        <CheckBox style={[styles.genChk]}
                                            onClick={(checked) =>{this.setState({check1:!checked})}}
                                            labelStyle={styles.midum} isChecked={this.state.check1}
                                            rightText={'Video - 1,00€'}
                                        />  
                                 </View>     
                            </View>   
                        </View>  
                        <View style={styles._PTB10_}>
                            <View style={[styles.inlineDatarow,styles.spaceBitw,styles.borderBottom,styles._PB5_]}>
                                <TouchableOpacity activeOpacity={0.8}><Text style={[styles._F16_,styles.linkAvant]} onPress={()=>{this.goto('mettreAvant')}}>Mettre en avant</Text></TouchableOpacity>
                                <Text style={[styles.globalIcon,styles.orangeColor]}>6</Text>
                            </View>
                            <View style={[styles.inlineDatarow,styles.spaceBitw,styles._MT5_]}>
                                <TouchableOpacity activeOpacity={0.8}><Text style={[styles._F16_,styles.linkAvant]} onPress={()=>{this.goto('EnVente')}}>En Vente Direct </Text></TouchableOpacity>
                                <Text style={[styles.globalIcon,styles.orangeColor]}>6</Text>
                            </View>
                        </View>
                        <View style={[styles.inlineDatarow,styles._MT10_]}>
                            <RadioButton currentValue={this.state.value} value={1} onPress={this.handleOnPress.bind(this)}><Text style={[styles.readiText,styles._PR15_,styles._ML5_]}>OFFERS</Text></RadioButton>
                            <RadioButton currentValue={this.state.value} value={0} onPress={this.handleOnPress.bind(this)}><Text style={[styles.readiText,styles._PR15_,styles._ML5_]}>DEMANDS</Text></RadioButton>
                        </View>
                        <View style={[styles.relative,styles._MT10_]}>
                            <FloatLabelTextInput style={styles.genInput} placeholder={"Choisissiez un titre"} underlineColorAndroid={'#eee'}/> 
                            <Text style={[styles.globalIcon,styles.dropabsIco,styles._T18_]}>h</Text>
                        </View>
                        <View style={[styles._MT10_]}>
                            <Text style={[styles.subTitle]}>Details</Text>
                            <View style={styles._MT10_}>
                                <View style={styles.framePstad}>
                                    <View style={styles.relative}>
                                        <View style={[styles.relative,styles.Borderbtmframe,styles._MT5_]}>
                                            {/*<ModalDropdown options={['Choisissiez la Category', 'One']} style={[styles.dropdown_2,styles.dropDown,styles.dropDownHome]} textStyle={[styles.dropdown_2_text,styles._F14_,styles.grayColor]} dropdownStyle={styles.dropdown_2_dropdown} />*/}
                                            <TouchableOpacity activeOpacity={0.8} onPress={this.showhide.bind(this)}><Text style={styles._F14_}>{'Choisissiez la Category'}</Text></TouchableOpacity>
                                            <Text style={[styles.globalIcon,styles.dropabsIco]}>g</Text>
                                        </View>
                                        {categoryContent}
                                    </View>
                                    <View style={(this.state.visible == 0) ? styles.relative : [styles.relative,styles.txtareaOpen]}>
                                        <TextInput underlineColorAndroid='#eeeeee'  multiline={true} numberOfLines={2} placeholder={"Faites un descriptif approprie de i ' annuonce"} />
                                        <Text style={[styles.globalIcon,styles.textareaIco]}>h</Text>
                                    </View>
                                </View>
                                <View style={[styles.inlineDatarow,styles._MT10_]}>
                                    <FloatLabelTextInput style={[styles.genInput]}   placeholder={"Prix *"} underlineColorAndroid={'#eee'}/>
                                        <CheckBox style={[styles.genChk]}
                                        onClick={(checked) =>{this.setState({check1:!checked})}}
                                        labelStyle={[styles.midum,styles.orange]} isChecked={this.state.check1}
                                        rightText={'Option Urgent - 1,00€'}
                                    />
                                </View>
                                <View style={styles.relative}>
                                    <FloatLabelTextInput style={[styles.genInput,styles.absicoinput]} placeholder={"Ville ou code postal...."} underlineColorAndroid={'#eee'}/> 
                                    <Text style={[styles.globalIcon,styles.dropabsIco,styles._T18_]}>t</Text>
                                </View>
                             </View>
                        </View>
                         <View style={[styles._MT20_,styles._PB15_]}>
                            <Text style={[styles.subTitle]}>Qui je suis</Text>
                            <View style={styles._MT10_}>
                                <View style={[styles.tabParent]}>
                                    <View style={[styles.tabs]}>
                                        <TouchableOpacity  style={(this.state.tabSelected1== 1)?[styles.tabSelected,styles.widthHalf]:[styles.tabnotSelected,styles.widthHalf]} onPress={()=>{this.tabchange1(1)}} activeOpacity={0.8}>
                                            <Text style={(this.state.tabSelected1== 1)?[styles.tabTextSelected,styles.orangeColor]:[styles.tabText]}>Particuliers</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity  style={(this.state.tabSelected1== 2)?[styles.tabSelected,styles.widthHalf]:[styles.tabnotSelected,styles.widthHalf]} onPress={()=>{this.tabchange1(2)}} activeOpacity={0.8}>
                                            <Text style={(this.state.tabSelected1== 2)?[styles.tabTextSelected,styles.orangeColor]:[styles.tabText]}>Professionnels</Text>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={styles.botomOrangeLine}></View>
                                </View>    
                                <View style={[styles.relative]}>
                                    <FloatLabelTextInput style={styles.genInput} placeholder={"Comment Vous"} underlineColorAndroid={'#eee'}/> 
                                    <Text style={[styles.globalIcon,styles.dropabsIco,styles._T18_]}>h</Text>
                                </View>
                                <View style={styles.relative}>
                                        <FloatLabelTextInput style={styles.genInput} placeholder={"Entin,Votre No "} underlineColorAndroid={'#eee'}/> 
                                        <Text style={[styles.globalIcon,styles.dropabsIco,styles._T18_]}>h</Text>
                                </View>
                                <FloatLabelTextInput style={styles.genInput} placeholder={"jamjam73100@yahoo.fr"} underlineColorAndroid={'#eee'}/> 
                                <View style={[styles.inlineDatarow,styles.spaceBitw,styles._MT15_]}>
                                        <Text>Cacher le numero de telephone</Text>
                                        <Switch onTintColor={onTintColor}  thumbTintColor={thumbTintColor} tintColor={tintColor} onValueChange={(value) => this.setState({trueSwitchIsOn1: value})} value={this.state.trueSwitchIsOn1} /> 
                                </View>
                             </View>
                         </View>    
                    </View>
                </ScrollView> 
    }
     else if(this.state.tabs == 4){
        postadTab =
                    <ScrollView showsVerticalScrollIndicator={false} style={styles.mainScollview}>
                     <View style={styles.scrollInner}>   
                        <View style={styles.postBlock}>
                            <View style={[styles.inlineDatarow,styles._PB10_,styles.jusRight]}><Text style={[styles.orangeColor]}>{'Prix de I\'annonce N° 1  :   0,00 €   |    '}</Text><Text style={[styles.globalIcon,styles.orangeColor,styles._F20_]}>8</Text></View>
                            <View style={styles.imgUploadView}>
                                <View style={styles.imgUpload}>
                                    <View>
                                        <View style={[styles.flex,styles._MT10_,styles.fullupImgView]}>
                                            <Image source={images.homestay} style={styles.fullupImg}/>
                                            <TouchableOpacity activeOpacity={0.8}  style={styles.cancelImg}>
                                                    <Text style={styles.globalIcon}>{'}'}</Text>
                                            </TouchableOpacity>
                                        </View>  
                                        <ScrollView  ref={(scrollView) => { _scrollView = scrollView; }} automaticallyAdjustContentInsets={false} showsHorizontalScrollIndicator={false} contentContainerStyle={styles.Carousel}
                                            horizontal>
                                                    <View style={[styles.inlineDatarow,styles._MT5_]}>
                                                            <View style={styles.placeHolderview}><Image style={[styles.placeHolderImg]} source={images.minic}/>
                                                                <TouchableOpacity activeOpacity={0.8}  style={styles.cancelImg}>
                                                                    <Text style={styles.globalIcon}>{'}'}</Text>
                                                                </TouchableOpacity>
                                                            </View>
                                                            <View style={styles.placeHolderview}><Image style={[styles.placeHolderImg]} source={images.IMG16}/>
                                                                <TouchableOpacity activeOpacity={0.8}  style={styles.cancelImg}>
                                                                    <Text style={styles.globalIcon}>{'}'}</Text>
                                                                </TouchableOpacity>
                                                            </View>
                                                            <View style={styles.placeHolderview}><Image style={[styles.placeHolderImg]} source={images.IMG16}/>
                                                                <TouchableOpacity activeOpacity={0.8}  style={styles.cancelImg}>
                                                                    <Text style={styles.globalIcon}>{'}'}</Text>
                                                                </TouchableOpacity>
                                                            </View>
                                                            <View style={styles.placeHolderview}><Image style={[styles.placeHolderImg]} source={images.IMG16}/>
                                                                <TouchableOpacity activeOpacity={0.8}  style={styles.cancelImg}>
                                                                    <Text style={styles.globalIcon}>{'}'}</Text>
                                                                </TouchableOpacity>
                                                            </View>
                                                            <View style={styles.placeHolderview}><Image style={[styles.placeHolderImg]} source={images.IMG16}/>
                                                                <TouchableOpacity activeOpacity={0.8}  style={styles.cancelImg}>
                                                                    <Text style={styles.globalIcon}>{'}'}</Text>
                                                                </TouchableOpacity>
                                                            </View>
                                                        </View>    
                                        </ScrollView>
                                    </View>
                                {/*<Text style={styles.grayColor}>{'Ajoutez Jusqu\'a 10 photo'}</Text>*/}
                                </View>
                                <TouchableOpacity elevation={5}  activeOpacity={0.8} style={styles.postadcamera}>
                                    <Text style={[styles.globalIcon,styles.cameraAd]}>A</Text>
                                </TouchableOpacity>  
                            </View>
                        </View>
                        <View style={styles._MT10_}>
                            <View style={[styles.row,styles._MT5_]}>
                                    <CheckBox style={[styles.genChk]}
                                        onClick={(checked) =>{this.setState({check1:!checked})}}
                                        labelStyle={styles.midum} isChecked={this.state.check1}
                                        rightText={'Pack 6 photos supplementaires - 1,00€'}
                                    />
                            </View>
                            <View>
                                <View style={[styles.inlineWithico,styles.justLeft,styles._MT5_]}>
                                        <CheckBox style={[styles.genChk]}
                                            onClick={(checked) =>{this.setState({check1:!checked})}}
                                            labelStyle={styles.midum} isChecked={this.state.check1}
                                            rightText={'Video - 1,00€'}
                                        />  
                                 </View>     
                            </View>   
                        </View>  
                        <View style={styles._PTB10_}>
                            <View style={[styles.inlineDatarow,styles.spaceBitw,styles.borderBottom,styles._PB5_]}>
                                <TouchableOpacity activeOpacity={0.8}><Text style={[styles._F16_,styles.linkAvant]} onPress={()=>{this.goto('mettreAvant')}}>Mettre en avant</Text></TouchableOpacity>
                                <Text style={[styles.globalIcon,styles.orangeColor]}>6</Text>
                            </View>
                            <View style={[styles.inlineDatarow,styles.spaceBitw,styles._MT5_]}>
                                <TouchableOpacity activeOpacity={0.8}><Text style={[styles._F16_,styles.linkAvant]} onPress={()=>{this.goto('EnVente')}}>En Vente Direct </Text></TouchableOpacity>
                                <Text style={[styles.globalIcon,styles.orangeColor]}>6</Text>
                            </View>
                        </View>
                        <View style={[styles.inlineDatarow,styles._MT10_]}>
                            <RadioButton currentValue={this.state.value} value={1} onPress={this.handleOnPress.bind(this)}><Text style={[styles.readiText,styles._PR15_,styles._ML5_]}>OFFERS</Text></RadioButton>
                            <RadioButton currentValue={this.state.value} value={0} onPress={this.handleOnPress.bind(this)}><Text style={[styles.readiText,styles._PR15_,styles._ML5_]}>DEMANDS</Text></RadioButton>
                        </View>
                        <View style={[styles.relative,styles._MT10_]}>
                            <FloatLabelTextInput style={styles.genInput} placeholder={"Choisissiez un titre"} underlineColorAndroid={'#eee'}/> 
                            <Text style={[styles.globalIcon,styles.dropabsIco,styles._T18_]}>h</Text>
                        </View>
                        <View style={[styles._MT10_]}>
                            <Text style={[styles.subTitle]}>Details</Text>
                            <View style={styles._MT10_}>
                                <View style={styles.framePstad}>
                                    <View style={styles.relative}>
                                        <View style={[styles.relative,styles.Borderbtmframe,styles._MT5_]}>
                                            {/*<ModalDropdown options={['Choisissiez la Category', 'One']} style={[styles.dropdown_2,styles.dropDown,styles.dropDownHome]} textStyle={[styles.dropdown_2_text,styles._F14_,styles.grayColor]} dropdownStyle={styles.dropdown_2_dropdown} />*/}
                                            <TouchableOpacity activeOpacity={0.8} onPress={this.showhide.bind(this)}><Text style={styles._F14_}>{'Choisissiez la Category'}</Text></TouchableOpacity>
                                            <Text style={[styles.globalIcon,styles.dropabsIco]}>g</Text>
                                        </View>
                                        {categoryContent}
                                    </View>
                                    <View style={(this.state.visible == 0) ? styles.relative : [styles.relative,styles.txtareaOpen]}>
                                        <TextInput underlineColorAndroid='#eeeeee'  multiline={true} numberOfLines={2} placeholder={"Faites un descriptif approprie de i ' annuonce"} />
                                        <Text style={[styles.globalIcon,styles.textareaIco]}>h</Text>
                                    </View>
                                </View>
                                <View style={[styles.inlineDatarow,styles._MT10_]}>
                                    <FloatLabelTextInput style={[styles.genInput]}   placeholder={"Prix *"} underlineColorAndroid={'#eee'}/>
                                        <CheckBox style={[styles.genChk]}
                                        onClick={(checked) =>{this.setState({check1:!checked})}}
                                        labelStyle={[styles.midum,styles.orange]} isChecked={this.state.check1}
                                        rightText={'Option Urgent - 1,00€'}
                                    />
                                </View>
                                <View style={styles.relative}>
                                    <FloatLabelTextInput style={[styles.genInput,styles.absicoinput]} placeholder={"Ville ou code postal...."} underlineColorAndroid={'#eee'}/> 
                                    <Text style={[styles.globalIcon,styles.dropabsIco,styles._T18_]}>t</Text>
                                </View>
                             </View>
                        </View>
                         <View style={[styles._MT20_,styles._PB15_]}>
                            <Text style={[styles.subTitle]}>Qui je suis</Text>
                            <View style={styles._MT10_}>
                                <View style={[styles.tabParent]}>
                                    <View style={[styles.tabs,styles._PB0_]}>
                                        <TouchableOpacity  style={(this.state.tabSelected1== 1)?[styles.tabSelected,styles.widthHalf]:[styles.tabnotSelected,styles.widthHalf]} onPress={()=>{this.tabchange1(1)}} activeOpacity={0.8}>
                                            <Text style={(this.state.tabSelected1== 1)?[styles.tabTextSelected,styles.orangeColor]:[styles.tabText]}>Particuliers</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity  style={(this.state.tabSelected1== 2)?[styles.tabSelected,styles.widthHalf]:[styles.tabnotSelected,styles.widthHalf]} onPress={()=>{this.tabchange1(2)}} activeOpacity={0.8}>
                                            <Text style={(this.state.tabSelected1== 2)?[styles.tabTextSelected,styles.orangeColor]:[styles.tabText]}>Professionnels</Text>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={styles.botomOrangeLine}></View>
                                </View>
                                <View style={[styles.relative]}>
                                    <FloatLabelTextInput style={styles.genInput} placeholder={"Comment Vous"} underlineColorAndroid={'#eee'}/> 
                                    <Text style={[styles.globalIcon,styles.dropabsIco,styles._T18_]}>h</Text>
                                </View>
                                <View style={styles.relative}>
                                        <FloatLabelTextInput style={styles.genInput} placeholder={"Entin,Votre No "} underlineColorAndroid={'#eee'}/> 
                                        <Text style={[styles.globalIcon,styles.dropabsIco,styles._T18_]}>h</Text>
                                </View>
                                <FloatLabelTextInput style={styles.genInput} placeholder={"jamjam73100@yahoo.fr"} underlineColorAndroid={'#eee'}/> 
                                <View style={[styles.inlineDatarow,styles.spaceBitw,styles._MT15_]}>
                                        <Text>Cacher le numero de telephone</Text>
                                        <Switch onTintColor={onTintColor}  thumbTintColor={thumbTintColor} tintColor={tintColor} onValueChange={(value) => this.setState({trueSwitchIsOn1: value})} value={this.state.trueSwitchIsOn1} /> 
                                </View>
                             </View>
                         </View>    
                    </View>
                </ScrollView>
    }
    else{
        null;
    }
     // Screen.togg(this, {})
    return (
        <View style={[styles.main,styles._HP15_]}>
            <View style={styles.tabParent}>
                <View style={styles.tabs}>
                    <TouchableOpacity  style={(this.state.tabSelected== 1)?[styles.tabSelected,styles.widthQuater]:[styles.tabnotSelected,styles.widthQuater]} onPress={()=>{this.tabchange(1)}} activeOpacity={0.8}>
                        <Text style={(this.state.tabSelected== 1)?[styles.tabTextSelected,styles.orangeColor]:[styles.tabText]}>Annonce n°1</Text>
                    </TouchableOpacity>
                    <TouchableOpacity  style={(this.state.tabSelected== 2)?[styles.tabSelected,styles.widthQuater]:[styles.tabnotSelected,styles.widthQuater]} onPress={()=>this.tabchange(2)} activeOpacity={0.8}>
                        <Text style={(this.state.tabSelected== 2)?[styles.tabTextSelected,styles.orangeColor]:[styles.tabText]}>Annonce n°2</Text>
                    </TouchableOpacity>
                    <TouchableOpacity  style={(this.state.tabSelected== 3)?[styles.tabSelected,styles.widthQuater]:[styles.tabnotSelected,styles.widthQuater]} onPress={()=>this.tabchange(3)} activeOpacity={0.8}>
                        <Text style={(this.state.tabSelected== 3)?[styles.tabTextSelected,styles.orangeColor]:[styles.tabText]}>Annonce n°3</Text>
                    </TouchableOpacity>
                    <TouchableOpacity  style={(this.state.tabSelected== 4)?[styles.tabSelected,styles.widthQuater]:[styles.tabnotSelected,styles.widthQuater]} onPress={()=>this.tabchange(4)} activeOpacity={0.8}>
                        <Text style={(this.state.tabSelected== 4)?[styles.tabTextSelected,styles.orangeColor]:[styles.tabText]}>Annonce n°4</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.botomOrangeLine}></View>
            </View>
            {postadTab}
                {/*<ScrollView showsVerticalScrollIndicator={false} style={styles.mainScollview}>
                     <View style={styles.scrollInner}>   

                        <View style={styles._MT10_}>
                            <View style={[styles.row,styles._MT5_]}>
                                    <CheckBox style={[styles.genChk]}
                                        onClick={(checked) =>{this.setState({check1:!checked})}}
                                        labelStyle={styles.midum} isChecked={this.state.check1}
                                        rightText={'Pack 6 photos supplementaires - 1,00€'}
                                    />
                            </View>
                            <View>
                                    <View style={[styles.inlineWithico,styles.justLeft,styles._MT5_]}>
                                            <CheckBox style={[styles.genChk]}
                                                onClick={(checked) =>{this.setState({check1:!checked})}}
                                                labelStyle={styles.midum} isChecked={this.state.check1}
                                                rightText={'Video - 1,00€'}
                                            />  
                                     </View>     
                            </View>   
                        </View>  
                        <View style={[styles.inlineDatarow,styles.spaceBitw]}>
                            <View style={[styles.inlineWithico]}>
                              <TouchableOpacity activeOpacity={0.8}><Text style={[styles._F16_,styles.linkAvant]}>Mettre en avant</Text></TouchableOpacity>
                            </View>
                            <Text style={[styles.globalIcon]}>6</Text>
                        </View>
                        <View style={[styles.inlineDatarow,styles._MT10_]}>
                            <RadioButton currentValue={this.state.value} value={1} onPress={this.handleOnPress.bind(this)}><Text style={[styles.readiText,styles._PR15_,styles._ML5_]}>LES OFFERS</Text></RadioButton>
                            <RadioButton currentValue={this.state.value} value={0} onPress={this.handleOnPress.bind(this)}><Text style={[styles.readiText,styles._PR15_,styles._ML5_]}>LES DEMANDS</Text></RadioButton>
                        </View>
                        <View style={styles.relative}>
                            <FloatLabelTextInput style={styles.genInput} placeholder={"Choisissiez un titre"} underlineColorAndroid={'#eee'}/> 
                            <Text style={[styles.globalIcon,styles.dropabsIco]}>h</Text>
                        </View>
                        <View style={[styles._MT15_]}>
                            <Text style={styles.subTitle}>Details</Text>
                            <View style={styles.relative}>
                                  <ModalDropdown options={['Choisissiez la Category', 'One']} style={[styles.dropdown_2,styles.dropDown,styles.dropDownpostad]} textStyle={[styles.dropdown_2_text,styles._F14_,styles.grayColor]} dropdownStyle={styles.dropdown_2_dropdown} />
                                  <Text style={[styles.globalIcon,styles.dropabsIco]}>g</Text>
                            </View>
                            <View style={styles.relative}>
                                <FloatLabelTextInput style={[styles.genInput,styles.absicoinput]} placeholder={"Faites un descriptif approprie de i ' annuonce"} underlineColorAndroid={'#eee'}/> 
                                <Text style={[styles.globalIcon,styles.dropabsIco]}>h</Text>
                             </View>
                             <View style={[styles.inlineDatarow,styles._MT10_]}>
                                        <FloatLabelTextInput style={[styles.genInput,]} placeholder={"Prix *"} underlineColorAndroid={'#eee'}/>
                                            <CheckBox style={[styles.genChk]}
                                            onClick={(checked) =>{this.setState({check1:!checked})}}
                                            labelStyle={[styles.midum,styles.orange]} isChecked={this.state.check1}
                                            rightText={'Option Urgent - 1,00€'}
                                            />
                             </View>
                              <View style={styles.relative}>
                                <FloatLabelTextInput style={[styles.genInput,styles.absicoinput]} placeholder={"Ville ou code postal...."} underlineColorAndroid={'#eee'}/> 
                                <Text style={[styles.globalIcon,styles.dropabsIco]}>t</Text>
                             </View>
                        </View>
                         <View style={[styles._MT20_,styles._PB15_]}>
                            <Text style={styles.subTitle}>Qui je suis</Text>
                            <View style={styles.relative}>
                                 <FloatLabelTextInput style={styles.genInput} placeholder={"Comment Vous"} underlineColorAndroid={'#eee'}/> 
                                 <Text style={[styles.globalIcon,styles.dropabsIco]}>h</Text>
                             </View>
                           <View style={styles.relative}>
                                   <FloatLabelTextInput style={styles.genInput} placeholder={"Entin,Votre No "} underlineColorAndroid={'#eee'}/> 
                                 <Text style={[styles.globalIcon,styles.dropabsIco]}>h</Text>
                             </View>
                            <FloatLabelTextInput style={styles.genInput} placeholder={"jamjam73100@yahoo.fr"} underlineColorAndroid={'#eee'}/> 
                            <View style={[styles.inlineDatarow,styles.spaceBitw,styles._MT15_]}>
                                <Text>Cacher le numero de telephone</Text>
                                <Switch onTintColor={onTintColor}  thumbTintColor={thumbTintColor} tintColor={tintColor} onValueChange={(value) => this.setState({trueSwitchIsOn1: value})} value={this.state.trueSwitchIsOn1} /> 
                            </View>
                         </View>     
                        
       
                    </View>
                </ScrollView>*/}
                  <View style={styles.bottomFixdad}>
                    <View style={[styles.inlineDatarow,styles.justCenter,styles._PB5_,styles._PT10_]}>
                        <TouchableOpacity style={[styles.brandColor,styles._MR5_]} activeOpacity={0.8}>
                                <Text style={[styles.smallBtn,styles._F12_]}>+Ajouter une annonce</Text>
                        </TouchableOpacity>
                         <TouchableOpacity style={[styles.brandColor,styles._ML5_]} activeOpacity={0.8}>
                                <Text style={[styles.smallBtn,styles._F12_]}>Valider mes annonces</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles._PB5_}>
                        <Text style={[styles._F14_,styles.center]}>Prix total de mes annonces :<Text style={[styles._F16_,styles.bold]}>   0,00 € </Text></Text>
                    </View>
                 </View>
        </View>
    );
  }

}






