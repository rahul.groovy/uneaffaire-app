import React, { Component } from 'react';
import {Image,Text,View,TouchableOpacity,ScrollView,Switch,Dimensions,Linking,Platform} from 'react-native';
import styles from '../../config/genStyle';
import { iconsMap, iconsLoaded } from '../../config/icons';
import Icon from 'react-native-vector-icons/FontAwesome';
import {settings} from '../../config/settings';

// redux specific
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as chatActions from '../../redux/reducers/chat';
import * as authActions from '../../redux/reducers/auth';

const { height, width } = Dimensions.get('window');

class PracticalInfo extends Component {
  // static navigatorButtons = {
  //    leftButtons: [{
  //      icon: images.back,
  //      id: 'back',
  //      title:'back to welcome',
  //    }],
  // };
static navigatorStyle = {
    // navBarBackgroundColor: colors.navBarBackgroundColor,
    // navBarTextColor: colors.navBarTextColor,
    // navBarSubtitleTextColor: colors.navBarSubtitleTextColor,
    // navBarButtonColor: colors.navBarButtonColor,
    // statusBarTextColorScheme: colors.statusBarTextColorScheme,
    // statusBarColor: colors.statusBarColor,
    // tabBarBackgroundColor: colors.tabBarBackgroundColor,
    // tabBarButtonColor: colors.tabBarButtonColor,
    // tabBarSelectedButtonColor: colors.tabBarSelectedButtonColor,
    // navBarSubtitleColor: colors.navBarSubtitleColor,
    drawUnderTabBar:true,
    //tabBarHidden:true,
};
  
  constructor(props) {
    super(props);
    //console.log(this.props);
    this.state={
        check1:false,
        tabs:1,
        tabSelected:1,
        trueSwitchIsOn: true,
        falseSwitchIsOn: false,
        value: 0,
    };
    // if you want to listen on navigator events, set this up
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }

  componentWillMount(){
      iconsLoaded.then(()=>{});
  }

  sendEventToDrawer(){
    this.props.navigator.handleDeepLink({link: 'Drawer',payload:{
        screen:this.props.testID
    }});
}


  onNavigatorEvent(event) {
    //console.log(event)
    if(event.id=='willAppear'){
      this.sendEventToDrawer();
      this.props.chatActions.setScreen(this.props.testID);
      //console.log(this.props);
      this.props.navigator.toggleNavBar({
        to: 'shown',
        animated: false
      });
      this.props.navigator.toggleTabs({
        animated: false,
        to: 'hidden'
    });
      this.props.navigator.setStyle({navBarTitleTextCentered: true});
      this.props.navigator.setTitle({title: "Infos pratiques"});
      this.props.navigator.setButtons({
             leftButtons: [{
                icon: iconsMap['back'],
                id: 'back2',
                title:'back to welcome',
            }],
            animated:false
        });   
    }
    if(event.id=='didAppear'){

    }
    
    
    if(event.id == 'back2'){
      this.props.navigator.pop({
        animated: true // does the pop have transition animation or does it happen immediately (optional)
      });
    }
  }

  goto(page){
    this.props.navigator.push({
      screen: page
    });
  }
share(i){
    if(i=='facebook'){
        Linking.openURL('https://www.facebook.com/sharer/sharer.php?u='+settings.api);
    }else if(i=="twitter"){
        Linking.openURL('https://twitter.com/home?status='+settings.api)
    }else if(i=="google"){
        Linking.openURL('https://plus.google.com/share?url='+settings.api)
    }else if(i=="linkdin"){
        Linking.openURL('https://www.linkedin.com/cws/share?url='+settings.api)
    }
}
  render() {
    return (
        <ScrollView showsVerticalScrollIndicator={false} style={[styles.mainScollview]}>
            <View style={styles.scrollInner}>
                <View style={[styles.SideBar,styles._PB15_]}>
                    <View><TouchableOpacity style={[styles.menuList,styles.pageLink]} onPress={()=>{this.goto('contact')}}><Text style={styles.menuLink}>Contact</Text></TouchableOpacity></View>
                    <View><TouchableOpacity style={[styles.menuList,styles.pageLink]} onPress={()=>{this.goto('condition')}}><Text style={styles.menuLink}>Conditions générales de vente</Text></TouchableOpacity></View>
                    <View><TouchableOpacity style={[styles.menuList,styles.pageLink]} onPress={()=>{this.goto('apropos')}}><Text style={styles.menuLink}>À propos</Text></TouchableOpacity></View>
                    <View><TouchableOpacity style={[styles.menuList,styles.pageLink]} onPress={()=>{this.goto('information')}}><Text style={styles.menuLink}>Mentions légales</Text></TouchableOpacity></View>
                    <View><TouchableOpacity style={[styles.menuList,styles.pageLink]} onPress={()=>{this.goto('help')}}><Text style={styles.menuLink}>Aide</Text></TouchableOpacity></View>
                </View>
                <View style={{alignItems:'center',justifyContent:'center'}}>
                    <Text style={[styles.signupTxt,{fontSize:12,fontWeight:'500'}]}>{'Version '+settings.version[Platform.OS]}</Text>
                </View>
                <View style={[styles._MT20_,styles.inlineDatarow,styles._HP15_]}> 
                    <TouchableOpacity activeOpacity={0.8} style={[styles.socialIco,{backgroundColor:"#3B579D"}]} onPress={()=>this.share("facebook")}>
                    <Icon name="facebook"  size={20} color={'#FFF'}/>
                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={0.8} style={[styles.socialIco,{backgroundColor:"#1DA1F2"}]} onPress={()=>this.share("twitter")}> 
                        <Icon name="twitter"  size={25} color={'#FFF'}/>
                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={0.8} style={[styles.socialIco,{backgroundColor:"#0077B5"}]} onPress={()=>this.share("linkdin")}>
                        <Icon name="linkedin"  size={25} color={'#FFF'}/>
                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={0.8} style={[styles.socialIco,{backgroundColor:"#D34836"}]} onPress={()=>this.share("google")}>
                        <Icon name="google-plus"  size={20} color={'#FFF'}/>
                    </TouchableOpacity>
                </View>
               
            </View>
        </ScrollView>
    );
  }

}

function mapStateToProps(state, ownProps) {
	return {
        chat:state.chat,
        auth:state.auth,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		chatActions: bindActionCreators(chatActions, dispatch),
		authActions: bindActionCreators(authActions, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(PracticalInfo);