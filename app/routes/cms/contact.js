import React, { Component } from 'react';
import {
  Image,Text,View,ScrollView,TextInput
} from 'react-native';
import { colors } from '../../config/styles';
import images from '../../config/images';
import styles from '../../config/genStyle';
import {settings} from '../../config/settings';
import ApiHelper from '../../lib/ApiHelper/ApiHelper';
import FloatLabelTextInput from '../../lib/react-native-floating-label-text-input';
import DropdownAlert from 'react-native-dropdownalert';
import LoaderButton from '../../components/LoaderButton/LoaderButton';
import { iconsMap, iconsLoaded } from '../../config/icons';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as chatActions from '../../redux/reducers/chat';
import * as authActions from '../../redux/reducers/auth';
const api= new ApiHelper;

class contact extends Component {
  // static navigatorButtons = {
  //    leftButtons: [{
  //      icon: images.back,
  //      id: 'back',
  //      title:'Back To Login',
  //    }],
  // };
static navigatorStyle = {
    // navBarBackgroundColor: colors.navBarBackgroundColor,
    // navBarTextColor: colors.navBarTextColor,
    // navBarSubtitleTextColor: colors.navBarSubtitleTextColor,
    // navBarButtonColor: colors.navBarButtonColor,
    // statusBarTextColorScheme: colors.statusBarTextColorScheme,
    // statusBarColor: colors.statusBarColor,
    // tabBarBackgroundColor: colors.tabBarBackgroundColor,
    // tabBarButtonColor: colors.tabBarButtonColor,
    // tabBarSelectedButtonColor: colors.tabBarSelectedButtonColor,
    // navBarSubtitleColor: colors.navBarSubtitleColor,
    drawUnderTabBar:true,
    tabBarHidden:true,
};

  constructor(props) {
    super(props);
    this.state={
        button:'+',
        fname:'',
        message:'',
        email:'',
        load:false
    };
    // if you want to listen on navigator events, set this up
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }

  componentWillMount(){
      iconsLoaded.then(()=>{});
  }

  onNavigatorEvent(event) {
    //console.log(event)
    if(event.id=='willAppear'){
    api.setAction(this.props.chatActions,this.props.authActions);
    this.props.chatActions.setScreen(this.props.testID);
      this.props.navigator.toggleNavBar({
        to: 'shown',
        animated: false
      });
      this.props.navigator.toggleTabs({
        to: 'hidden',
        animated: false
      });
      this.props.navigator.setStyle({navBarTitleTextCentered: true});
      this.props.navigator.setTitle({title: "Nous contacter"});
      this.props.navigator.setButtons({
             leftButtons: [{
                icon: iconsMap['back'],
                id: 'back2',
                title:'back to welcome',
            }],
            animated:false
        });   
    }
    if(event.id=='didAppear'){

    }

    if(event.id == 'back2'){
      this.props.navigator.pop({
        animated: true // does the pop have transition animation or does it happen immediately (optional)
      });
    }
  }
  goto(page){
    this.props.navigator.push({screen: page});

  }
  doContact(){
    if(this.state.fname  == '' || this.state.email  == ''|| this.state.message  == '') {
      this.dropdown.alertWithType('error', 'Erreur', 'Remplissez tous les champs');
      return false;
    }  
     if(this.state.email != ''){
          var atpos = this.state.email.indexOf("@");
          var dotpos =this.state.email.lastIndexOf(".");
          if (atpos<1 || dotpos<atpos+2 || dotpos+2>=this.state.email.length) {
              this.dropdown.alertWithType('error', 'Erreur', 'Not a valid e-mail address');
              return false;
          }
     }         
      let request={
          url:settings.endpoints.contact,
          method:'POST',
          params:{email:this.state.email,username:this.state.fname,message:this.state.message}
      }
      this.setState({load:true},()=>{
        api.FetchData(request).then((result)=>{
            if(result.status){
              this.dropdown.alertWithType('success', 'Félicitation!', result.message);            
            } else{
              this.dropdown.alertWithType('error', 'Erreur', result.message); 
            } 
            this.setState({load :false});   
        }).catch((error)=>{
            this.setState({load :false}); 
            //console.log(error);
        });
      }); 
      
  }
  render() {
    return (
      <ScrollView showsVerticalScrollIndicator={false} style={styles.mainScollview}>
           <View style={[styles.scrollInner,styles._HP15_]}>                 
                  <View style={styles.contactTextMain}>
                      <Text style={[styles.blockTitle,styles.contactTiltle1,styles._PB5_]}>Contact</Text>     
                      <FloatLabelTextInput style={[styles.genInput,styles.inputContact]}  placeholder={"Votre nom *"} underlineColorAndroid={'#eee'}
                      onChangeTextValue ={(val) => { 
                      this.setState({fname:val})}} 
                      />
                      <FloatLabelTextInput style={[styles.genInput,styles.inputContact]}  placeholder={"Votre adresses mail *"} underlineColorAndroid={'#eee'}
                      onChangeTextValue ={(val) => { 
                      this.setState({email:val})}} 
                      />            
                      <FloatLabelTextInput  Description placeholderTextColor="#aaa" style={[styles.genInput,styles.inputContact]}  placeholder={"Votre message *"} 
                      underlineColorAndroid={'#eee'}
                      //multiline = {true}
                      numberOfLines = {4} 
                      onChangeTextValue ={(val) => { 
                      this.setState({message:val})}} 
                      />
                      <LoaderButton load={this.state.load} onPress={()=>this.doContact()} BtnStyle={[styles.brandColor,styles._MT20_,styles.justCenter,{height:40}]} text={'Envoyer'} textStyle={styles.genButton} />
                  </View>   
           </View>
           <DropdownAlert ref={(ref) => this.dropdown = ref} onClose={(data) => {}} />
      </ScrollView>
    );
  }
}
function mapStateToProps(state, ownProps) {
  return {
         chat:state.chat,
         auth:state.auth,
  };
 }
 
 function mapDispatchToProps(dispatch) {
  return {
   chatActions: bindActionCreators(chatActions, dispatch),
   authActions: bindActionCreators(authActions, dispatch),
  };
 }
 
 export default connect(mapStateToProps, mapDispatchToProps)(contact);