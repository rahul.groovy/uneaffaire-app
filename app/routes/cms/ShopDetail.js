
import React, { Component } from 'react';
import {
  StyleSheet,
  Image,Text,View,TextInput,TouchableOpacity,ScrollView,Switch,Modal,WebView,Linking,Dimensions,Share,ActivityIndicator
} from 'react-native';
import styles from '../../config/genStyle';
import Communications from 'react-native-communications';
import {settings} from '../../config/settings';
import ApiHelper from '../../lib/ApiHelper/ApiHelper';
import {Navigation,Screen,SharedElementTransition} from 'react-native-navigation';
import { iconsMap, iconsLoaded } from '../../config/icons';
// redux specific
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as chatActions from '../../redux/reducers/chat';
import * as authActions from '../../redux/reducers/auth';

const api= new ApiHelper;
const { height, width } = Dimensions.get('window');
const FORTAB = width < 1025 && width > 721;

class ShopDetail extends Component {
// genModal(){
//     Navigation.showModal({
//         screen: "contactModal", // unique ID registered with Navigation.registerScreen
//         title: "Modal", // title of the screen as appears in the nav bar (optional)
//         passProps: {}, // simple serializable object that will pass as props to the modal (optional)
//         navigatorStyle: {}, // override the navigator style for the screen, see "Styling the navigator" below (optional)
//         navigatorButtons: {}, // override the nav buttons for the screen, see "Adding buttons to the navigator" below (optional)
//         animationType: 'slide-up' // 'none' / 'slide-up' , appear animation for the modal (optional, default 'slide-up')
//     });
// }
_renderHeader(section) { 
    return (
        <View style={styles.conditionsubTitle}>
        <Text style={styles.conditionsubtext}>{section.title}</Text>
        <Text style={styles.plusTab}>{section.button}</Text>        
        </View>
    );
}
_renderContent(section) {
return (
    <View style={styles.conditionContent}>
    <Text>{section.content}</Text>
    </View>
);
}
_shareTextMessage () {
    Share.share({
        message: 'Such sharing! Much wow!'
    })
    .then(this._showResult)
    .catch((err) =>{
        //console.log(err)
    })
}
static navigatorStyle = {
    navBarBackgroundColor: 'rgba(0,0,0,.3)',
    drawUnderNavBar: true,
    topBarElevationShadowEnabled:false,
    //tabBarHidden: true,
};
  
constructor(props) {
    super(props);
    this.state={
        shopDetails : [],
        load:false,
        image:this.props.yes ? this.props.data.shop_image  : '',
        token:this.props.auth.token
    };
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
}


  componentWillMount(){
    iconsLoaded.then(()=>{});
}
onNavigatorEvent(event) {
    if(event.id=='willAppear'){
        api.setAction(this.props.chatActions,this.props.authActions);
        this.props.chatActions.setScreen(this.props.testID);
        //console.log(this.props);
      this.props.navigator.toggleTabs({
        animated: false,
        to: 'hidden'
    });

    this.props.navigator.setStyle({navBarTitleTextCentered: true});
     this.props.navigator.setButtons({
         leftButtons: [{
        icon: iconsMap['back'],
        id: 'back2',
        title:'back to welcome',
      }],animated:false})
    //  this.props.navigator.setTitle({
    //    title: "Produit",
    //  });
    }
    if(event.id=='didAppear'){

    }
    
    if(event.id == 'back2'){
        this.props.navigator.dismissAllModals({
            animationType: 'slide-down' // 'none' / 'slide-down' , dismiss animation for the modal (optional, default 'slide-down')
        });
        //   this.props.navigator.pop({
    //     animated: true // does the pop have transition animation or does it happen immediately (optional)
    //   });
    }
  }
componentWillMount(){ 
    this.getShopData();
}
getShopData(){
    let _this=this;
    _this.setState({load:true},()=>{
        let request={
            url:settings.endpoints.shopDetails,
            method:'POST',
            params:{shop_id:typeof this.props.shop_id != 'undefined' ? this.props.shop_id:'',token:this.props.auth.token} 
          }
          api.FetchData(request).then((result)=>{
            if(result.status){
               _this.setState({
                  shopDetails:result.shop,
                  load:false
               });
            }else{
                _this.setState({
                    load:false
                });
            }
          }).catch((error)=>{
            _this.setState({
                    load:false
                });
          });
    });
   
}
subscribe(){
    let _this=this;
    _this.setState({load:true},()=>{
        let detail = this.state.shopDetails;  
        detail.have_subscribe = detail.have_subscribe == 1 ? 0 : 1;
        _this.setState({shopDetails:detail});
        let request={
          url:settings.endpoints.shopSubscribeUnsubscribe,
          method:'POST',
          params:{shop_id:typeof this.props.shop_id != 'undefined' ? this.props.shop_id:'',token:this.props.auth.token}
        }
        api.FetchData(request).then((result)=>{
          if(result.status){
             _this.setState({load:false});
          }else{
              _this.setState({
                  load:false
              });
          }
        }).catch((error)=>{
          _this.setState({
                  load:false
              });
        });
    });
    
}
  render() {
    let shareOptions = {
        title: "React Native",
        message: "Hola mundo",
        url: "http://facebook.github.io/react-native/",
        subject: "Share Link" //  for email
    };
    var Spinner = require('rn-spinner');
    let type;
    let src = this.state.shopDetails.shop_image != 'undefined' ?this.state.shopDetails.shop_image:'' ;
    if(this.props.yes!='undefined' && this.props.yes){
    type =  <View style={[styles.inlineDatarow,styles.sliderParent]}>            
                <SharedElementTransition
                    sharedElementId={'SharedTextId'+this.props.shop_id}
                    showDuration={400}
                    hideDuration={300}
                    animateClipBounds={true}
                    showInterpolation={{ type: 'linear', easing: 'FastOutSlowIn' }}
                    hideInterpolation={{ type: 'linear', easing: 'FastOutSlowIn' }}
                >
                <Image style={[styles.sliderImage,{height:FORTAB ? 500 : 300 }]}
                    source={{uri:this.state.image}} fadeDuration={0}/>
                </SharedElementTransition>
            </View>;
    }else{
         type =   
            <View style={[styles.inlineDatarow,styles.sliderParent]}> 
                <Image style={[styles.sliderImage,{width:width,height:FORTAB ? 500 : 300 }]}
                    source={{uri:src}} fadeDuration={0}/>
            </View>;
    }

    return (
        <View style={[styles.main,styles._PT0_]}>
            <ScrollView contentContainerStyle={{flexGrow: 1}} showsVerticalScrollIndicator={false} style={[styles.mainScollview]}>
                <View style={styles.scrollInner}>
                        <View style={styles.row}>
                            <ScrollView style={styles.tabPortCaption}
                                  ref={(scrollView) => { _scrollView = scrollView; }}
                                  automaticallyAdjustContentInsets={false}
                                  showsHorizontalScrollIndicator={false}
                                  contentContainerStyle={styles.Carousel}
                                  horizontal>
                                  {type}
                              </ScrollView>
                            {this.state.load ? 
                            <View style={[styles.CenterIndi]} ><ActivityIndicator color='#FF4500' animating={this.state.load} style={[styles.loader]} size="large"/></View>:
                            <View style={[styles.productCaption,styles._HP15_]}>
                                <View style={[styles.dataRow]}>
                                    <View>
                                        <Text style={[styles.productName,styles.large]}>{this.state.shopDetails.shopname}</Text>
                                        <Text style={styles.gendes}>{this.state.shopDetails.slogan}</Text>    
                                    </View>
                                    <View style={styles.inlineDatarow}>
                                        <TouchableOpacity onPress={() => Communications.phonecall(this.state.shopDetails.telephone, true)} activeOpacity={0.8}><Text style={[styles.globalIcon,styles.largeIco,styles.orangeColor]}>*</Text></TouchableOpacity>
                                    </View>
                                    {/*<Text style={[styles.orangeColor,styles.bold]}>12,550 €</Text>*/}
                                </View>
                                <View>
                                    <Text style={styles.blockTitle}>{this.state.shopDetails.total_shop_ad} annonces</Text>
                                    <Text style={styles.gendes}>{this.state.shopDetails.description}</Text>
                                </View>
                                <View style={[styles.dataRow,styles._MT15_]}>
                                    <TouchableOpacity style={[styles.brandColor,styles.widthHalf,styles._MLR5_]} activeOpacity={0.8} onPress={() => Linking.openURL(this.state.shopDetails.website_url)}>
                                        <Text style={[styles.genButton,styles._F16_]}>Accéder au site</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={[styles.brandColor,styles.widthHalf,styles._MLR5_]} activeOpacity={0.8} onPress={() => Linking.openURL('https://www.google.com/maps/dir/Current+Location/'+this.state.shopDetails.address)}>
                                        <Text style={[styles.genButton,styles._F16_]} numberOfLines={3}>{this.state.shopDetails.address}<Text style={[styles.globalIcon,styles._F16_,styles.white]}> t</Text></Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={[styles.proDetail,styles._MT10_]}>
                                    <View style={[styles.inlineDatarow,styles.spaceBitw,styles.prodataRow]}>
                                        <View style={{flex:1}}>
                                        <Text style={[styles.detailType]}>Horaires d'ouverture :</Text></View>
                                        <View style={{flex:1}}>
                                        <Text style={[styles.detailTypeValue]}>{this.state.shopDetails.opening_time}</Text></View>
                                    </View>
                                    <View style={[styles.inlineDatarow,styles.spaceBitw,styles.prodataRow]}>
                                    <View style={{flex:1}}>
                                        <Text style={styles.detailType}>N° SIREN :</Text></View>
                                        <View style={{flex:1}}>
                                        <Text style={styles.detailTypeValue} numberOfLines={1}>{this.state.shopDetails.siret}</Text></View>
                                    </View>
                                </View>                                
                            </View>
                            }
                        </View>
                </View>
            </ScrollView>
            <View style={[styles.bottomFixed,styles.inlineDatarow,styles.spaceBitw]}>
                <View style={[styles.inlineDatarow]}>
                    <Text style={styles.bold}>ABONNÉS</Text><Text>: {this.state.shopDetails.subscription_count} PERSONNES</Text>  
                </View>   
                <View style={[styles.inlineDatarow]}> 
                    <TouchableOpacity style={[styles.brandColor]} activeOpacity={0.8} activeOpacity={0.8} onPress={()=>this.subscribe()}>
                        <Text style={[styles.genButton,styles.midum,styles.smbtn,styles._PL20_,styles._PR20_,styles._F16_]}><Text style={[styles.globalIcon]} >
                            {this.state.shopDetails.have_subscribe == 0 ? 2+' ' :''}
                             </Text>{this.state.shopDetails.have_subscribe == 0 ? 'Suivre':
                             'Abonné' }</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    );
  }
}

function mapStateToProps(state, ownProps) {
	return {
        chat:state.chat,
        auth:state.auth,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		chatActions: bindActionCreators(chatActions, dispatch),
		authActions: bindActionCreators(authActions, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(ShopDetail);