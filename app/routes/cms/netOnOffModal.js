import React, { Component } from 'react';
import {
  Text,View,TextInput,TouchableOpacity,ScrollView,Dimensions
} from 'react-native';

import { colors } from '../../config/styles';
import ApiHelper from '../../lib/ApiHelper/ApiHelper';
import DropdownAlert from 'react-native-dropdownalert';
import { iconsMap, iconsLoaded } from '../../config/icons';
import LoaderButton from '../../components/LoaderButton/LoaderButton';
import Icon from 'react-native-vector-icons/Ionicons';
// redux specific
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as chatActions from '../../redux/reducers/chat';
import * as authActions from '../../redux/reducers/auth';
const api= new ApiHelper;
const {height,width}=Dimensions.get('window');

class netOnOffModal extends Component {
  // static navigatorButtons = {
  //    leftButtons: [{
  //      icon: require('../../images/back.png'),
  //      id: 'back',
  //      title:'Back To Login',
  //    }],
  // };
  static navigatorStyle = {
    navBarBackgroundColor: colors.navBarBackgroundColor,
    navBarTextColor: colors.navBarTextColor,
    navBarSubtitleTextColor: colors.navBarSubtitleTextColor,
    navBarButtonColor: colors.navBarButtonColor,
    statusBarTextColorScheme: colors.statusBarTextColorScheme,
    statusBarColor: colors.statusBarColor,
    tabBarBackgroundColor: colors.tabBarBackgroundColor,
    tabBarButtonColor: colors.tabBarButtonColor,
    tabBarSelectedButtonColor: colors.tabBarSelectedButtonColor,
    navBarSubtitleColor: colors.navBarSubtitleColor,
    navBarTransparent: false,
    navBarTranslucent: false    
};

  constructor(props) {
    super(props);
    this.state={
    };
    // if you want to listen on navigator events, set this up
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }

  componentWillMount(){
    iconsLoaded.then(()=>{});
  }

  onNavigatorEvent(event) {
    //console.log(event)
    if(event.id=='willAppear'){
      this.props.chatActions.setScreen(this.props.testID);
      //console.log(this.props);
     
      //console.log(this.props);
      this.props.navigator.toggleNavBar({
      to: 'shown',
      animated: true
    });
    this.props.navigator.setStyle({navBarTitleTextCentered: true});
    this.props.navigator.setTitle({
      title: "Oops!!",
    });
     this.props.navigator.setButtons({
         leftButtons: [{
          icon: iconsMap['back'],
          id: 'back2',
          title:'Back To Login',
     }],animated:false})
    }
    if(event.id=='didAppear'){
    }
    if(event.id=='willDisappear'){
      this.props.authActions.setNetDial(false);      
    }    
    if(event.id == 'back2'){
      this.props.dismiss();
      this.props.navigator.dismissAllModals({
        animationType: 'slide-down' // 'none' / 'slide-down' , dismiss animation for the modal (optional, default 'slide-down')
      });
      // this.props.navigator.pop({
      //   animated: true // does the pop have transition animation or does it happen immediately (optional)
      // });
    }  
  }
  goto(page){
    this.props.navigator.push({
      screen: page
    });
  }
  dismiss(){
    //this.props.authActions.setNetDial(false);
    this.props.dismiss();
  }
  
  render() {
    return (     
           <View style={[{height:height/2,padding:15,width:width/2,backgroundColor:'#fff',alignSelf:'center',borderRadius:15,borderColor:'#ddd',borderWidth:2}]}>
                <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
                <View style={{flex:3,alignItems:'center',justifyContent:'center'}}>
                <Icon name="md-wifi" color={colors.navBarButtonColor} size={100} />
                <Text style={{fontSize:16,textAlign:'center',fontFamily:'Montserrat',}}>{'Problème de connection'}</Text>
                </View>
                <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
                <TouchableOpacity style={{backgroundColor:colors.navBarButtonColor,padding:8,borderRadius:5}} onPress={()=>this.dismiss()}>
                    <Text style={{color:'#fff',fontSize:16,fontFamily:'Montserrat',fontWeight:'600'}}>Réessayez</Text>
                </TouchableOpacity>
                </View>
                </View>    
            </View>
        );
  }
}

function mapStateToProps(state, ownProps) {
	return {
        chat:state.chat,
        auth:state.auth,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		chatActions: bindActionCreators(chatActions, dispatch),
		authActions: bindActionCreators(authActions, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(netOnOffModal);