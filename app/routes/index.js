import {Navigation,ScreenVisibilityListener} from 'react-native-navigation';
import SigninContainer from './LoginPages/SigninContainer';
import SignupContainer from './LoginPages/SignupContainer';
import SplashScreen from './LoginPages/SplashScreen';
import ForgotContainer from './LoginPages/ForgotContainer';
import WelcomeContainer from './LoginPages/WelcomeContainer';
import ProductgridContainer from './ProductPages/ProductgridContainer';
import UserAnnonces from '../../app/routes/ProductPages/UserAnnonces';
import ProductDetail from './ProductPages/ProductDetail';
import Cart from './ProductPages/Cart';
import myAccount from './Account/myAccount';
import favoris from './Account/favoris';
import remonter from './ProductPages/remonter';
import HomeContainer from './Home/HomeContainer';
import apropos from './cms/apropos';
import condition from './cms/condition';
import information from './cms/information';
import contact from './cms/contact';
import contactModal from './cms/contactModal';
import PracticalInfo from './cms/PracticalInfo';
import Report from './cms/report';
import Managead from './cms/Managead';
import FastSearch from './SearchPages/FastSearch';
import AdvanceSearch from './SearchPages/AdvanceSearch';
import CreatAlert from './SearchPages/CreatAlert';
import CreatResearch from './SearchPages/CreatResearch';
import favourites from './cms/favourites';
import help from './cms/help';
import RemoveAd from './cms/RemoveAd';
import Chat from './Chat/inbox';
import Singlechat from './Chat/singlechat';
import avant from './ProductPages/avant';
import Modifier from './ProductPages/modifier';
import Drawer from './Account/Drawer';
import postad from './Account/postad';
import mettreAvant from './ProductPages/mettreAvant';
import EnVente from './ProductPages/EnVente';
import localism from './Account/localism';
import Helpview from '../../app/routes/cms/Helpview';
import ShopDetail from '../../app/routes/cms/ShopDetail';
import createShop from './Account/createShop';
import shopCart from './ProductPages/shopCart';
import PromoteAd from './ProductPages/PromoteAd';
import ChatSticky from '../components/ChatSticky/ChatSticky';
import success from './ProductPages/success';
import ShopSearch from './SearchPages/ShopSearch';
import YouTubeDemo from './ProductPages/youtube';
import netOnOffModal from './cms/netOnOffModal';
import codePush from 'react-native-code-push';
//import * as chatActions from '../redux/reducers/chat';
const codePushOptions = {
  installMode: codePush.InstallMode.ON_NEXT_RESTART,
  checkFrequency: codePush.CheckFrequency.ON_APP_RESUME,
};

// register all screens of the app (including internal ones)
export function registerScreens(store, Provider) {
    Navigation.registerComponent('SignupContainer', () => SignupContainer, store, Provider);
    Navigation.registerComponent('SigninContainer', () => SigninContainer, store, Provider);
    Navigation.registerComponent('SplashScreen', () => SplashScreen, store, Provider);
    Navigation.registerComponent('ForgotContainer', () => ForgotContainer, store, Provider);
    Navigation.registerComponent('WelcomeContainer', () => codePush(codePushOptions)(WelcomeContainer), store, Provider);
    Navigation.registerComponent('ProductgridContainer', () => ProductgridContainer, store, Provider);
    Navigation.registerComponent('UserAnnonces', () => UserAnnonces, store, Provider);
    Navigation.registerComponent('ProductDetail', () => ProductDetail, store, Provider);
    Navigation.registerComponent('Cart', () => Cart, store, Provider);
    Navigation.registerComponent('HomeContainer', () => HomeContainer, store, Provider);
    Navigation.registerComponent('remonter', () => remonter, store, Provider);
    Navigation.registerComponent('myAccount', () => myAccount, store, Provider);
    Navigation.registerComponent('favoris', () => favoris, store, Provider);
    Navigation.registerComponent('favourites', () => favourites, store, Provider);
    Navigation.registerComponent('help', () => help, store, Provider);
    Navigation.registerComponent('RemoveAd', () => RemoveAd, store, Provider);
    Navigation.registerComponent('apropos', () => apropos, store, Provider);
    Navigation.registerComponent('condition', () => condition, store, Provider);
    Navigation.registerComponent('contact', () => contact, store, Provider);
    Navigation.registerComponent('contactModal', () => contactModal, store, Provider);
    Navigation.registerComponent('PracticalInfo', () => PracticalInfo, store, Provider);
    Navigation.registerComponent('Report', () => Report, store, Provider);
    Navigation.registerComponent('Managead', () => Managead, store, Provider);
    Navigation.registerComponent('information', () => information, store, Provider);
    Navigation.registerComponent('FastSearch', () => FastSearch, store, Provider);
    Navigation.registerComponent('AdvanceSearch', () => AdvanceSearch, store, Provider);
    Navigation.registerComponent('CreatAlert', () => CreatAlert, store, Provider);
    Navigation.registerComponent('CreatResearch', () => CreatResearch, store, Provider);
    Navigation.registerComponent('Chat', () => Chat, store, Provider);
    Navigation.registerComponent('Singlechat', () => Singlechat, store, Provider);
    Navigation.registerComponent('avant', () => avant, store, Provider);
    Navigation.registerComponent('Modifier', () => Modifier, store, Provider);
    Navigation.registerComponent('Drawer', () => Drawer, store, Provider);
    Navigation.registerComponent('postad', () => postad, store, Provider);
    Navigation.registerComponent('mettreAvant', () => mettreAvant, store, Provider);
    Navigation.registerComponent('EnVente', () => EnVente, store, Provider);
    Navigation.registerComponent('localism', () => localism, store, Provider);
    Navigation.registerComponent('Helpview', () => Helpview, store, Provider);
    Navigation.registerComponent('ShopDetail', () => ShopDetail, store, Provider);
    Navigation.registerComponent('createShop', () => createShop, store, Provider);
    Navigation.registerComponent('shopCart', () => shopCart, store, Provider);
    Navigation.registerComponent('PromoteAd', () => PromoteAd, store, Provider);
    Navigation.registerComponent('ChatSticky', () => ChatSticky, store, Provider);
    Navigation.registerComponent('success', () => success, store, Provider);
    Navigation.registerComponent('ShopSearch', () => ShopSearch, store, Provider);
    Navigation.registerComponent('YouTubeDemo', () => YouTubeDemo, store, Provider);
    Navigation.registerComponent('netOnOffModal', () => netOnOffModal, store, Provider);
}
console.disableYellowBox = true;

export function registerScreenVisibilityListener() {

    //console.log(ScreenVisibilityListener);
    new ScreenVisibilityListener({
      willAppear: ({screen}) => console.log(`Screen will appear ${screen}`),
      didAppear: ({screen, startTime, endTime, commandType}) => console.log('screenVisibility', `Screen ${screen} displayed in ${endTime - startTime} millis [${commandType}]`),
      willDisappear: ({screen}) => console.log(`Screen will disappear ${screen}`),
      didDisappear: ({screen}) => console.log(`Screen disappeared ${screen}`)
    }).register();
  }