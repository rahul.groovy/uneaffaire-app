import React, { Component } from 'react';
import {
  StyleSheet,
  Image,Text,View,TextInput,TouchableOpacity
} from 'react-native';

import { colors } from '../../config/styles';
import images from '../../config/images';
//import styles from './styles';
import styles from '../../config/genStyle';

import {Navigation,Screen} from 'react-native-navigation';

export default class SplashScreen extends Component {
  // static navigatorButtons = {
  //    leftButtons: [{
  //      icon: require('../../images/car.png'),
  //      id: 'Login',
  //      title:'Login',
  //    }],
  // };
  // static navigatorStyle = {
  //   navBarBackgroundColor: colors.navBarBackgroundColor,
  //   navBarTextColor: colors.navBarTextColor,
  //   navBarSubtitleTextColor: colors.navBarSubtitleTextColor,
  //   navBarButtonColor: colors.navBarButtonColor,
  //   statusBarTextColorScheme: colors.statusBarTextColorScheme,
  //   statusBarColor: colors.statusBarColor,
  //   tabBarBackgroundColor: colors.tabBarBackgroundColor,
  //   tabBarButtonColor: colors.tabBarButtonColor,
  //   tabBarSelectedButtonColor: colors.tabBarSelectedButtonColor,
  //   navBarSubtitleColor: colors.navBarSubtitleColor,
  //   navBarHidden: true,
  // };

  constructor(props) {
    super(props);
    // if you want to listen on navigator events, set this up
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }

  onNavigatorEvent(event) {
    // if (event.id === 'menu') {
    //   this.props.navigator.toggleDrawer({
    //     side: 'left',
    //     animated: true
    //   });
    // }
    // if (event.id === 'edit') {
    //   Alert.alert('NavBar', 'Edit button pressed');
    // }
    // if (event.id === 'add') {
    //   Alert.alert('NavBar', 'Add button pressed');
    // }
    //this.props.navigator.toggleNavBar({});
    // this.props.navigator.toggleTabs({
    //     animated: false,
    //     to: 'hidden'
    // });
  }

  render() {
      
     // Screen.togg(this, {})
    return (
      <View style={styles.main}>
                <View style={styles.logoCont}>
                    <Image source={images.companylogo} style={styles.logo}></Image>
                </View>
            </View>
    );
  }

}
