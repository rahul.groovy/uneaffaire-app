import React, { Component } from 'react';
import {
  StyleSheet,
  Image,Text,View,TextInput,TouchableOpacity,Keyboard,Platform,AsyncStorage,KeyboardAvoidingView
} from 'react-native';
import { colors } from '../../config/styles';
import images from '../../config/images';
import styles from '../../config/genStyle';
import messages from '../../config/messages';
import {settings} from '../../config/settings';
import ApiHelper from '../../lib/ApiHelper/ApiHelper';
import DropdownAlert from 'react-native-dropdownalert';
import FloatLabelTextInput from '../../lib/react-native-floating-label-text-input';
import DeviceInfo from 'react-native-device-info';
import LoaderButton from '../../components/LoaderButton/LoaderButton';
import { iconsMap, iconsLoaded } from '../../config/icons';
import startMain from '../../config/app-main';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
// redux specific
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as chatActions from '../../redux/reducers/chat';
import * as authActions from '../../redux/reducers/auth';
const api= new ApiHelper;
class SigninContainer extends Component {
  // static navigatorButtons = {
  //    leftButtons: [{
  //      icon: images.back,
  //      id: 'back',
  //      title:'back to welcome',
  //    }],
  // };
static navigatorStyle = {
    navBarBackgroundColor: colors.navBarBackgroundColor,
    navBarTextColor: colors.navBarTextColor,
    navBarSubtitleTextColor: colors.navBarSubtitleTextColor,
    navBarButtonColor: colors.navBarButtonColor,
    statusBarTextColorScheme: colors.statusBarTextColorScheme,
    statusBarColor: colors.statusBarColor,
    tabBarBackgroundColor: colors.tabBarBackgroundColor,
    tabBarButtonColor: colors.tabBarButtonColor,
    tabBarSelectedButtonColor: colors.tabBarSelectedButtonColor,
    navBarSubtitleColor: colors.navBarSubtitleColor,
    drawUnderNavBar: false,
    drawUnderTabBar: true
};

  constructor(props) {
    super(props);
    this.refsT={};
    this.state ={
      email:'',
      password:'',
      load:false,
    }
    // if you want to listen on navigator events, set this up
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }

  componentWillMount(){
    iconsLoaded.then(()=>{});
  }

  onNavigatorEvent(event) {
    if(event.id == 'willAppear'){
      api.setAction(this.props.chatActions,this.props.authActions);
      this.props.navigator.toggleNavBar({
        to: 'shown',
        animated: true,
      });
      this.props.navigator.toggleTabs({
        animated: true,
        to: 'hidden'
      });
      this.props.navigator.setStyle({navBarTitleTextCentered: true,drawUnderNavBar: false,});
      this.props.navigator.setTitle({title: "Se connecter",});
      this.props.navigator.setButtons({
          leftButtons: [{
            icon: iconsMap['back'],
            id: 'back2',
            title:'back to welcome',
          }],
          animated:false
      });
    }
    //console.log(event)
    
    if(event.id == 'back2'){
      this.props.navigator.pop({
        animated: true // does the pop have transition animation or does it happen immediately (optional)
      });
    }
  }

  goto(page){
    this.props.navigator.push({
      screen: page
    });

  }

  getDeviceInfo(){
    let data={
      uuid:DeviceInfo.getUniqueID(),
      manufacturer:DeviceInfo.getManufacturer(),
      brand:DeviceInfo.getBrand(),
      model:DeviceInfo.getModel(),
      id:DeviceInfo.getDeviceId(),
      SystemName:DeviceInfo.getSystemName(),
      SystemVersion:DeviceInfo.getSystemVersion(),
      tablet:DeviceInfo.isTablet(),
      Locale:DeviceInfo.getDeviceLocale(),
      Country:DeviceInfo.getDeviceCountry(),
      Timezone:DeviceInfo.getTimezone(),
    }
    return data;
  }
  displayNoInternet(){
    this.props.authActions.setNetDial(true);
    this.props.navigator.showLightBox({
        screen: "netOnOffModal", // unique ID registered with Navigation.registerScreen
        passProps: {
            dismiss:this.dismiss.bind(this)
        }, // simple serializable object that will pass as props to the lightbox (optional)
        style: {
          backgroundBlur: "dark", // 'dark' / 'light' / 'xlight' / 'none' - the type of blur on the background
          backgroundColor: "#0008" // tint color for the background, you can specify alpha here (optional)
        },
        //adjustSoftInput: "resize", // android only, adjust soft input, modes: 'nothing', 'pan', 'resize', 'unspecified' (optional, default 'unspecified')
       });
}
dismiss(){   
  api.checkNet().then(()=>{
    this.props.authActions.setNet(false);
    this.props.authActions.setNetDial(false);
    this.props.navigator.dismissLightBox();
  }).catch((err)=>{
      //console.log('Error while dismissing modal');
      //console.log(err);
      this.props.authActions.setNet(true);
      this.props.authActions.setNetDial(false);
      // this.displayNoInternet();       
  });
}

  logIn(email,password){
    //console.log(Platform)
    api.checkNet().then(()=>{
      this.props.authActions.setNetDial(false);
      let request={
        url:settings.endpoints.login,
        method:'POST',
        params:{email:email,password:password,device:Platform.OS,device_data:this.getDeviceInfo()}
      }
      this.setState({load:true},()=>{
        api.FetchData(request).then((result)=>{
          //console.log(result);
           if(result.status){
               AsyncStorage.multiSet([['token',result.user_data.token],['userdata',JSON.stringify(result.user_data)]]).then(()=>{
                 startMain(result.user_data.token,result.user_data);
               });
               //this.props.api.setToken(result.user_data.token,result.user_data);               
               // this.props.navigator.push({
               //   screen: 'HomeContainer',
               //   passProps:{
               //     token:result.user_data.token,
               //     userdata:result.user_data,
               //   },
               // });
           }else{
             this.setState({load:false});
             this.dropdown.alertWithType('error', 'Error', result.message);
           }
         }).catch((error)=>{
           this.setState({load:false});
           //console.log(error);
         });
      });
      
  }).catch((err)=>{
    console.log(err);
      this.displayNoInternet();
  });
   
  }

  textFeldValidate(){
  let {email,password}=this.state;
  var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
  Keyboard.dismiss(); 
  let valid = true;
    if (email.length === 0 || password.length === 0) {
      this.dropdown.alertWithType('error', 'Error', messages.noUsername);
      valid = false;
    }else if(reg.test(email) == false){
      this.dropdown.alertWithType('error', 'Error', messages.validEmail);
      valid = false;
    }else if(password.length <8 ){
      this.dropdown.alertWithType('error', 'Error', messages.password8);
      valid = false;
    }else if(password.length >16){
      this.dropdown.alertWithType('error', 'Error', messages.password16);
      valid = false;
    }
    
    if (valid) {
      this.logIn(email,password);
    }
  }

  _setNextFocus(key){
    if(this.refsT !== undefined && this.refsT !== null){
    let nextKey = ''
      if(key == 'email'){
        nextKey = 'password';
      }
      if(nextKey !== '' && this.refsT[nextKey] !== undefined && this.refsT[nextKey] !== null){
        inp = this.refsT[nextKey].inputRef();
        if(inp !== undefined && inp !== null){
          inp.focus();
        } 
      }
    }
  }

  renderIos(){
    return (
      <KeyboardAwareScrollView extraScrollHeight={80} contentContainerStyle={[styles.main,styles._HP15_, { flexGrow: 1 }]} keyboardShouldPersistTaps="handled">
            <View style={[styles.logoCont,styles.FLDOUBLE,{alignItems:'center'}]}>
                <Image source={images.companylogomain} style={styles.logo}></Image>
            </View>
            <View style={[styles.loginMain]}>
                      <FloatLabelTextInput 
                      forIco={"Ico"} 
                      icon={'2'} 
                      style={styles.genInput} 
                      placeholder={"Email"}  
                      underlineColorAndroid={'#eee'}
                      autoCapitalize="none" 
                      autoCorrect={false}
                      ref={o=>this.refsT['email']= o}
                      returnKeyType={'next'}
                      onSubmitEditing={()=>this._setNextFocus('email')}  
                      onChangeTextValue={(text) => this.setState({email:text})}/>
                       
                      <FloatLabelTextInput 
                      forIco={"Ico"} 
                      icon={'K'} 
                      style={styles.genInput} 
                      placeholder={"Mot de passe"}  
                      underlineColorAndroid={'#eee'} 
                      autoCapitalize="none" 
                      autoCorrect={false}
                      ref={o=>this.refsT['password']= o}
                      returnKeyType={'go'}
                      onSubmitEditing={()=>this.textFeldValidate()} 
                      onChangeTextValue={(text) => this.setState({password:text})} secureTextEntry={true}/>
                                    
                  <Text style={styles.forgotTxt} onPress={()=>{this.goto('ForgotContainer')}}>Mot de passe oublié ?</Text>
            </View>    
             <View style={[styles.btnBlock,styles.spaceBitw]}> 
                  <LoaderButton load={this.state.load} onPress={()=>{this.textFeldValidate()}} BtnStyle={[styles.brandColor]} 
                    text={'S\'identifier'} textStyle={styles.genButton} />
             </View> 
            <View>
                <Text style={styles.signupTxt}>{'Vous n\'avez pas de compte? '}<Text style={ styles.signTxt} onPress={()=>{this.goto('SignupContainer')}}>S'inscrire</Text></Text>
            </View>
            
            <DropdownAlert
                ref={(ref) => this.dropdown = ref}
                onClose={(data) => {/*console.log(data);*/}} />
                  </KeyboardAwareScrollView>
            );
  }

  renderAndroid(){
    return (
      <View style={[styles.main,styles._HP15_]}>
            <View style={[styles.logoCont,styles.FLDOUBLE,{alignItems:'center'}]}>
                <Image source={images.companylogomain} style={styles.logo}></Image>
            </View>
            <View style={[styles.loginMain]}>
                      <FloatLabelTextInput 
                      forIco={"Ico"} 
                      icon={'2'} 
                      style={styles.genInput} 
                      placeholder={"Email"}  
                      underlineColorAndroid={'#eee'}
                      autoCapitalize="none" 
                      autoCorrect={false}
                      ref={o=>this.refsT['email']= o}
                      returnKeyType={'next'}
                      onSubmitEditing={()=>this._setNextFocus('email')}  
                      onChangeTextValue={(text) => this.setState({email:text})}/>
                       
                      <FloatLabelTextInput 
                      forIco={"Ico"} 
                      icon={'K'} 
                      style={styles.genInput} 
                      placeholder={"Mot de passe"}  
                      underlineColorAndroid={'#eee'} 
                      autoCapitalize="none" 
                      autoCorrect={false}
                      ref={o=>this.refsT['password']= o}
                      returnKeyType={'go'}
                      onSubmitEditing={()=>this.textFeldValidate()} 
                      onChangeTextValue={(text) => this.setState({password:text})} secureTextEntry={true}/>
                                    
                  <Text style={styles.forgotTxt} onPress={()=>{this.goto('ForgotContainer')}}>Mot de passe oublié ?</Text>
            </View>    
             <View style={[styles.btnBlock,styles.spaceBitw]}> 
                  <LoaderButton load={this.state.load} onPress={()=>{this.textFeldValidate()}} BtnStyle={[styles.brandColor]} 
                    text={'S\'identifier'} textStyle={styles.genButton} />
             </View> 
            <View>
                <Text style={styles.signupTxt}>{'Vous n\'avez pas de compte? '}<Text style={ styles.signTxt} onPress={()=>{this.goto('SignupContainer')}}>S'inscrire</Text></Text>
            </View>
            
            <DropdownAlert
                ref={(ref) => this.dropdown = ref}
                onClose={(data) => {/*console.log(data);*/}} />
                  </View>
            );
  }

  render() {
    return (
         Platform.OS === 'ios' ? this.renderIos() : this.renderAndroid()
    );
  }
}

function mapStateToProps(state, ownProps) {
	return {
        chat:state.chat,
        auth:state.auth,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		chatActions: bindActionCreators(chatActions, dispatch),
		authActions: bindActionCreators(authActions, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(SigninContainer);