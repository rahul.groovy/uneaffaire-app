import React, { Component } from 'react';
import {
  StyleSheet
} from 'react-native';
export const styles=StyleSheet.create({
  logo:{
      width:125,
      height:100,
  },
  main:{
      flex: 1,
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
  },
  
  signupButton:{
    marginTop:20,
  },
  brandColor:{
    backgroundColor:'#f15a23',
  },
  loginMain:{
    
  },
  genInput:{
    width:310,
    borderBottomColor: '#eee', borderWidth: 0,
  },
  logoCont:{
    flex:1,
    flexDirection: 'column',
    justifyContent: 'flex-end'
  },
  btnBlock:{
    flex:1,
    justifyContent: 'space-between'
    
  },
  forgotTxt:{
    marginTop:20,  
    alignSelf: 'stretch',
    textAlign: 'right',
    bottom:10,
  },
  signupTxt:{
    bottom:10,
  },
  signTxt:{
    fontWeight:'bold',
    color:'#606060',
  }
});
