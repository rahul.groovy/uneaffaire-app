import React, { Component } from 'react';
import {
  StyleSheet,
  Image,Text,View,TextInput,TouchableOpacity,ScrollView,Dimensions,Platform,Animated,WebView
} from 'react-native';

import { colors } from '../../config/styles';
import images from '../../config/images';
import styles from '../../config/genStyle';

import {Navigation,Screen} from 'react-native-navigation';
import CheckBox from 'react-native-check-box';
import {Column as Col, Row} from 'react-native-flexbox-grid';
import FloatLabelTextInput from '../../lib/react-native-floating-label-text-input';
const HEADER_MAX_HEIGHT = 100;
const HEADER_MIN_HEIGHT = Platform.OS === 'ios' ? 50 : 50;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;

export default class ProductgridContainer extends Component {
//   static navigatorButtons = {
//      leftButtons: [{
//        icon: require('../../images/back.png'),
//        id: 'back',
//        title:'back to welcome',
//      }],
//   };

  
// static navigatorStyle = {
//     navBarBackgroundColor: colors.navBarBackgroundColor,
//     navBarTextColor: colors.navBarTextColor,
//     navBarSubtitleTextColor: colors.navBarSubtitleTextColor,
//     navBarButtonColor: colors.navBarButtonColor,
//     statusBarTextColorScheme: colors.statusBarTextColorScheme,
//     statusBarColor: colors.statusBarColor,
//     tabBarBackgroundColor: colors.tabBarBackgroundColor,
//     tabBarButtonColor: colors.tabBarButtonColor,
//     tabBarSelectedButtonColor: colors.tabBarSelectedButtonColor,
//     navBarSubtitleColor: colors.navBarSubtitleColor,
// };

constructor(props) {
    super(props);
    // if you want to listen on navigator events, set this up
    this.state={
        check1:false,
        tabs:1,
        tabSelected:1,
        like:1,
        scrolled:true,
        scrollY: new Animated.Value(0),
    };
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
}

 onNavigatorEvent(event) {
    //console.log(event)
      this.props.navigator.toggleTabs({
        animated: true,
        to: 'shown'
     });
    
    if(event.id == 'back'){
      this.props.navigator.pop({
        animated: true // does the pop have transition animation or does it happen immediately (optional)
      });
    }
    

    this.props.navigator.setStyle({navBarTitleTextCentered: true});
    this.props.navigator.setTitle({
      title: "Recherche rapide",

    });
    if(event.id == 'menu'){
        this.props.navigator.toggleDrawer({
            side: 'left', // the side of the drawer since you can have two, 'left' / 'right'
            animated: true, // does the toggle have transition animation or does it happen immediately (optional)
            to: 'open' // optional, 'open' = open the drawer, 'closed' = close it, missing = the opposite of current state
        });
    }
   
  }

  goto(page){   
        this.props.navigator.push({
        screen: page
        }); 
  }
  tabchange(tab){
        this.setState({tabs:tab,tabSelected:tab});
  }


  onScroll(event){
        var currentOffset = event.nativeEvent.contentOffset.y;
        var direction = currentOffset > this.offset ? this.setState({scrolled:true}) : this.setState({scrolled:false});
        this.offset = currentOffset;       
}

gettrans(bool){
if(bool){
      return  this.state.scrollY.interpolate({
        inputRange: [0, 50],
        outputRange: [0, -50],
        extrapolate: 'clamp',
    });
    }else{
       return  this.state.scrollY.interpolate({
        inputRange: [0, 50],
        outputRange: [-50, 0],
        extrapolate: 'clamp',
    });  
    }
}
  render() {
    
    const headerTranslate =  this.state.scrollY.interpolate({
            inputRange: [0, 50],
            outputRange: [0, -50],
            extrapolate: 'clamp',
        });
        let type =  <View style={[styles.inlineDatarow,styles._MLR5_,styles._MT10_]}><View style={styles.proThumbMain}><Image style={styles.proThumb} source={images.homestay} /></View><View style={styles.proThumbMain}><Image style={styles.proThumb} source={images.homestay} /></View><View style={styles.proThumbMain}><Image style={styles.proThumb} source={images.homestay} /></View><View style={styles.proThumbMain}><Image style={styles.proThumb} source={images.homestay} /></View><View style={styles.proThumbMain}><Image style={styles.proThumb} source={images.homestay} /></View><View style={styles.proThumbMain}><Image style={styles.proThumb} source={images.homestay} /></View></View>;

     // Screen.togg(this, {})
    return (
            <ScrollView showsVerticalScrollIndicator={false} style={styles.mainScollview}>
                    {/* <View style={styles.scrollInner}>
                         <View style={[styles.productdataMain,styles._HP15_]}>
                            <Animated.View style={[styles.searchHolder,styles.searchAdvanceIco,{
                                    transform: [{ translateY: headerTranslate },
                                    {
                                        scale: this.state.scrollY.interpolate({
                                            inputRange: [-50 , 0, 50],
                                            outputRange: [2, 1, 1]
                                        })
                                    }]
                                },
                            ]}>
                                <View style={styles.icoholderRight}> 
                                    <Text style={[styles.searchholderIco,styles.genIco,styles._MT10_]}>{'v'}</Text>
                                </View>
                                <View style={styles.searchinputHolder}>
                                    <TextInput style={[styles.searchInput,styles._PL0_]} placeholder={"Rechercher..."} underlineColorAndroid={'#eee'}/>
                                </View>
                            </Animated.View>
                        <View style={[styles.row,styles._MT15_,styles._PB5_]}>
                                <CheckBox style={[styles.genChk]}
                                    onClick={(checked) =>{this.setState({check1:!checked})}}
                                    labelStyle={styles.midum} isChecked={this.state.check1}
                                    rightText={'Rechercher dans le titre'}
                                />
                        </View>
                        <View style={[styles.accountdataBlock,styles._PB5_]}>
                                <View style={[styles.inlineWithico,styles.justLeft,styles._MT5_]}>
                                        <CheckBox style={[styles.genChk]}
                                            onClick={(checked) =>{this.setState({check1:!checked})}}
                                            labelStyle={styles.midum} isChecked={this.state.check1}
                                            rightText={'Annonces urgentes'}
                                        />
                                        <Text style={[styles.globalIcon,styles.small,styles.red]}> y</Text>
                                </View>
                                <View style={[styles.inlineDatarow,styles.spaceBitw,styles._MT5_,styles._PB5_]}>
                                    <View style={styles.inlineWithico}>
                                        <Text style={[styles.globalIcon,styles.mdIco,styles.blueColor]}>{'r'} </Text>
                                        <Text style={[styles.genText,styles.grayColor]}>{'Recherches Avancee'}</Text>
                                    </View>
                                    <Text style={[styles.globalIcon]}>6</Text>
                                </View>
                                <View style={[styles.inlineDatarow,styles.spaceBitw,styles._MT5_,styles._PB5_]}>
                                    <View style={styles.inlineWithico}>
                                        <Text style={[styles.globalIcon,styles.mdIco]}>y </Text>
                                        <Text style={[styles.genText,styles.grayColor]}>{'Recherches Preferee'}</Text>
                                    </View>
                                    <Text style={[styles.globalIcon]}>6</Text>
                                </View>
                        </View>
                        <View style={styles.accountdataBlock}>
                            <Text style={styles.subTitle}>Historique</Text>
                            <TouchableOpacity activeOpacity={0.8} style={[styles.historyRow,styles.inlineDatarow,styles.spaceBitw]}>
                                <Text style={styles.smallTitle}>Foulard</Text>
                                <Text style={styles.globalIcon}>{'<'}</Text>
                            </TouchableOpacity>
                             <TouchableOpacity activeOpacity={0.8} style={[styles.historyRow,styles.inlineDatarow,styles.spaceBitw]}>
                                <Text style={styles.smallTitle}>Velo</Text>
                                 <Text style={styles.globalIcon}>{'<'}</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={[styles.accountdataBlock,styles.justCenter]}>
                            <TouchableOpacity style={[styles.smallOutlineBtn]} activeOpacity={0.8}>
                                <Text style={[styles.genButton,styles.orangeColor,styles.big]}>{'Effacer Tout L\'historique'}</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.accountdataBlock}>
                            <Text style={styles.smallTitle}>Derniers articles vus</Text>
                            <ScrollView
                                ref={(scrollView) => { _scrollView = scrollView; }}
                                automaticallyAdjustContentInsets={false}
                                showsHorizontalScrollIndicator={false}
                                
                                contentContainerStyle={styles.Carousel}
                                horizontal>
                                {type}
                            </ScrollView>
                        </View>
                    </View>
                </View> */}
            </ScrollView>
    );
  }

}






