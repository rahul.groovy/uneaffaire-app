import React, { Component } from 'react';
import {
  StyleSheet,
  Image,Text,View,TextInput,Keyboard,TouchableOpacity,ScrollView,Dimensions,Platform,Animated,WebView,ActivityIndicator
} from 'react-native';
import { colors } from '../../config/styles';
import styles from '../../config/genStyle';
import CheckBox from '../../lib/react-native-check-box';
import { iconsMap, iconsLoaded,Icon } from '../../config/icons';
import {settings} from '../../config/settings';
import ApiHelper from '../../lib/ApiHelper/ApiHelper';
import {Navigation,Screen,SharedElementTransition} from 'react-native-navigation';
import CKeyboardSpacer from '../../components/CKeyboardSpacer';
// redux specific
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as chatActions from '../../redux/reducers/chat';
import * as authActions from '../../redux/reducers/auth';

const HEADER_MAX_HEIGHT = 100;
const HEADER_MIN_HEIGHT = Platform.OS === 'ios' ? 50 : 50;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;
const api = new ApiHelper;

class FastSearch extends Component {
//   static navigatorButtons = {
//      leftButtons: [{
//        icon: images.back,
//        id: 'back',
//        title:'back to welcome',
//      }],
//   };

  
static navigatorStyle = {
    navBarBackgroundColor: colors.navBarBackgroundColor,
    navBarTextColor: colors.navBarTextColor,
    navBarSubtitleTextColor: colors.navBarSubtitleTextColor,
    navBarButtonColor: colors.navBarButtonColor,
    statusBarTextColorScheme: colors.statusBarTextColorScheme,
    statusBarColor: colors.statusBarColor,
    tabBarBackgroundColor: colors.tabBarBackgroundColor,
    tabBarButtonColor: colors.tabBarButtonColor,
    tabBarSelectedButtonColor: colors.tabBarSelectedButtonColor,
    navBarSubtitleColor: colors.navBarSubtitleColor,
    drawUnderTabBar:true,
    //tabBarHidden:true,
};

constructor(props) {
    super(props);
    // if you want to listen on navigator events, set this up
   
    this.state={
        scrollY: new Animated.Value(0),
        token:this.props.auth.token,
        searchInTitle:false,
        urgent:false,
        searchTxt:'',
        recentList:[],
        recentSearchList:[],
        in_history:true,
        load1:false,
        recentListLen:0,
        recentSearchListLen:0,
    };
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
}

componentWillMount(){
    iconsLoaded.then(()=>{});
}

 onNavigatorEvent(event) {
    //console.log(event)
    if(event.id=='willAppear'){
        api.setAction(this.props.chatActions,this.props.authActions);
        this.props.chatActions.setScreen(this.props.testID);
        //console.log(this.props);
    this.props.navigator.toggleNavBar({
      to: 'shown',
      animated: false
    });
    this.props.navigator.toggleTabs({
        animated: false,
        to: 'hidden'
     });
    this.props.navigator.setStyle({navBarTitleTextCentered: true,drawUnderTabBar:true});
    this.props.navigator.setTitle({title: "Recherche rapide"});
    this.props.navigator.setButtons({
         leftButtons: [{
            icon: iconsMap['back'],
            id: 'back2',
            title:'back to welcome',
        }],animated:false});      
    }
    if(event.id=='didAppear'){
        if(typeof this.props.dataSearch != 'undefined'){
            this.setState({
                searchTxt:typeof this.props.dataSearch.search_string != 'undefined'? this.props.dataSearch.search_string :typeof this.props.dataSearch.searchTxt != 'undefined'? this.props.dataSearch.searchTxt : '',
                searchInTitle:typeof this.props.dataSearch.search_in_title != 'undefined' ?this.props.dataSearch.search_in_title:typeof this.props.dataSearch.searchInTitle != 'undefined' ?this.props.dataSearch.searchInTitle:false,
                urgent:typeof this.props.dataSearch.search_urgent != 'undefined' ? this.props.dataSearch.search_urgent:typeof this.props.dataSearch.urgent != 'undefined' ? this.props.dataSearch.urgent:false
            })
        }
        if(api.checkLogin(this.props.auth.token)){
            this.getData();
        }
        setTimeout(()=>{
            if(this.searchInput !== undefined && this.searchInput !== null){
                this.searchInput.focus();
            }
        },500);
        
    }
    if(event.id == 'back2'){
      this.props.navigator.pop({
        animated: true // does the pop have transition animation or does it happen immediately (optional)
      });
    }
    if(event.id == 'menu'){
        this.props.navigator.toggleDrawer({
            side: 'left', // the side of the drawer since you can have two, 'left' / 'right'
            animated: true, // does the toggle have transition animation or does it happen immediately (optional)
            to: 'open' // optional, 'open' = open the drawer, 'closed' = close it, missing = the opposite of current state
        });
    }
   
  }
getData(){
    let _this=this;
    let request={
      url:settings.endpoints.userSearchHistoryList,
      method:'POST',
      params:{token:this.props.auth.token != undefined ? this.props.auth.token : ''}
    }
    api.FetchData(request).then((result)=>{
      if(result.status){
           _this.setState({
               recentList:typeof result.recent_list != 'undefined' ? result.recent_list:[],
               recentListLen:result.recent_list.length,
               recentSearchList:typeof result.history_list != 'undefined'? result.history_list:[],
               recentSearchListLen:result.history_list.length,
           });          
      }else{
          _this.setState({
               recentList:[],
           });
      }
    }).catch((error)=>{
      _this.setState({
              load:false
          });
    }); 
}
navigateTo(page,data){
    data.id = data.ad_id;
      if(page=='ProductDetail'){
            this.props.navigator.push({
                screen: page,
                sharedElements: ['SharedTextId'+data.id],
                passProps:{
                    data:data,
                    token:this.state.token, 
                    userdata:this.props.auth.userdata,
                }            
       });
    }
}
goto(page,aa,items){
    Keyboard.dismiss();
    if(page == 'AdvanceSearch' || page == 'favourites'){
        let dataSearch= {
            search_string:this.state.searchTxt,
            search_urgent:this.state.urgent,
            search_in_title:this.state.searchInTitle,
            ad_type:'offers',
        }
        if(typeof this.props.dataSearch != 'undefined'){
            if(typeof this.props.dataSearch.category_id != 'undefined'){
                dataSearch.category_id = this.props.dataSearch.category_id;
            }
            if(typeof this.props.dataSearch.category_filter != 'undefined'){
                dataSearch.category_filter = this.props.dataSearch.category_filter;
            }
            if(typeof this.props.dataSearch.location != 'undefined'){
                dataSearch.location = this.props.dataSearch.location;
            }
        }
        this.props.navigator.push({
        screen: page,
            passProps:{
                token:this.state.token,
                userdata:this.props.auth.userdata,   
                dataSearch:dataSearch ,
                region :this.props.region !== undefined && this.props.region !== null && this.props.region !=""  ? this.props.region :''
            }
        });
        return false;
    }
    let params ={};    
    if(Object.keys(items).length === 0) {
        let flag = true;
        if(this.state.searchInTitle){
            if(this.state.searchTxt == ''){
                return false;
            }   
        }
        if(this.state.searchTxt == ''){
            aa=false;
        }       
        // if(this.state.searchTxt == '' && !this.state.searchInTitle){  
        //     aa=false;       
        //     if(this.state.urgent){
        //         flag = true;                               
        //     }else{
                 
        //         flag = false;
        //     }            
        // }else if(this.state.searchTxt == '' && this.state.searchInTitle){           
        //     if(!this.state.urgent){                 
        //         flag = false;
        //     }else{
        //         if(this.state.urgent && this.state.searchInTitle){
        //             flag = false;
        //         }                 
        //     } 
        // }
        if(flag){
            params = {
                search_string:this.state.searchTxt,
                search_urgent:this.state.urgent,
                search_in_title:this.state.searchInTitle,
                in_history:aa,
                ad_type:'offers',               
            }
        }else{
            return false;
        }            
    }else{
        if(Object.keys(items).length !== 0){
            delete items['updatedAt'];
            delete items['id'];
            delete items['createdAt'];
            delete items['user_id'];
            items.in_history = false;
            items.ad_type = 'offers';      
            params = items
        }       
    }
    this.props.navigator.push({
        screen: page,
        passProps:{
            token:this.state.token,
            userdata:this.props.auth.userdata, 
            params:params,
            filterType:'basic'         
        }
    });
}
clearHistory(){    
    let _this=this;
    _this.setState({load1:true},()=>{
        let request={
            url:settings.endpoints.clearUserSearchHistory,
            method:'POST',
            params:{token:_this.props.auth.token != undefined ? _this.props.auth.token : ''}
         }
         api.FetchData(request).then((result)=>{        
            if(result.status){
                _this.setState({load1:false,recentSearchList:[],recentSearchListLen:0});
            }else{
                _this.setState({load1:false});
            }
         }).catch((error)=>{
            _this.setState({load1:false});
         }); 
    });
    
}

render() {
    const headerTranslate =  this.state.scrollY.interpolate({
        inputRange: [0, 50],
        outputRange: [0, -50],
        extrapolate: 'clamp',
    });
    let _this = this; 
    let rectentList =  this.state.recentListLen > 0 &&
            this.state.recentList.map(function (item,key) {
            return (<TouchableOpacity style={styles.proThumbMain} key={key} onPress={()=>_this.navigateTo('ProductDetail',item)}><SharedElementTransition sharedElementId={'SharedTextId'+item.ad_id}><Image style={[styles.proThumb,styles._MT10_]} source={{uri:item.ad_image}} /></SharedElementTransition></TouchableOpacity>
        )
    });
    let type = <View style={[styles.inlineDatarow,styles._MLR5_,styles._MT10_]} >{rectentList}</View>
  
    let recentSearch = this.state.recentSearchListLen > 0 && 
            this.state.recentSearchList.map(function (item,vkey) {
            return (<TouchableOpacity key={vkey} activeOpacity={0.8} style={[styles.historyRow,styles.inlineDatarow,styles.spaceBitw]} onPress={()=>_this.goto('ProductgridContainer',!_this.state.in_history,item)}>
                    <Text style={styles.smallTitle}>{item.search_string}</Text>
                    <Text style={[styles.globalIcon,styles.smIco]}>{'<'}</Text>
                </TouchableOpacity>
        )
    });
    return ( <View style={{flex:1}}>
            <ScrollView showsVerticalScrollIndicator={false} style={[styles.mainScollview]}>
                    <View >
                       <View style={[styles.productdataMain,styles._HP15_,styles._PT0_]}> 
                            <Animated.View style={[styles.searchHolder,styles.searchAdvanceIco,{
                                    transform: [{ translateY: headerTranslate },
                                    {
                                        scale: this.state.scrollY.interpolate({
                                            inputRange: [-50 , 0, 50],
                                            outputRange: [2, 1, 1]
                                        })
                                    }]
                                },
                            ]}>
                                <View style={styles.searchinputHolder}>
                                    <TextInput 
                                    style={[styles.searchInput,styles._PL0_]} 
                                    ref={o=>this.searchInput=o}
                                    placeholder={"Rechercher..."} 
                                    underlineColorAndroid={'#eee'}
                                    onChangeText={(text)=>this.setState({searchTxt:text})}
                                    value={typeof this.state.searchTxt != 'undefined' ? this.state.searchTxt:''}
                                />
                                </View>
                                <TouchableOpacity style={styles.icoholderRight} onPress={()=>this.state.searchTxt !=''? this.setState({searchTxt:''}) :null
                                //this.goto('ProductgridContainer',this.state.in_history,{})
                            } >
                            <Icon name={this.state.searchTxt =='' ? "search":'close'} color={colors.navBarButtonColor} size={this.state.searchText =='' ? 19 :16} />
                            </TouchableOpacity>
                            </Animated.View>
                        <View style={[styles.row,styles._MT15_]}>
                                <CheckBox style={[styles.genChk]}
                                    isChecked={this.state.searchInTitle} 
                                    onClick={(checked) =>{this.setState({searchInTitle:checked})}}
                                    labelStyle={styles.midum}
                                    rightText={'Rechercher dans le titre'}
                                />
                        </View>
                        <View style={styles.accountdataBlock}>
                                <View style={[styles.inlineWithico,styles.justLeft,styles._MT5_]}>
                                        <CheckBox style={[styles.genChk]}
                                            isChecked={this.state.urgent} 
                                            onClick={(checked) =>{this.setState({urgent:checked});}}
                                            labelStyle={styles.midum}
                                            rightText={'Annonces urgentes'}
                                        />
                                        <Text style={[styles.globalIcon,styles.small,styles.red]}> y</Text>
                                </View>
                                <TouchableOpacity onPress={()=>{this.goto('AdvanceSearch','',{})}} style={[styles.inlineDatarow,styles.spaceBitw,styles._MT5_]} activeOpacity={0.8}>
                                    <View style={styles.inlineWithico}>
                                        <Text style={[styles.globalIcon,styles.mdIco]}>{'r'} </Text>
                                        <Text style={[styles.genText,styles.grayColor]}>{'Recherche Avancée'}</Text>
                                    </View>
                                    <Text style={[styles.globalIcon]}>6</Text>
                                </TouchableOpacity>
                                <TouchableOpacity activeOpacity={0.8} onPress={()=>{this.goto('favourites','',{})}} style={[styles.inlineDatarow,styles.spaceBitw,styles._MT5_]}>
                                    <View style={styles.inlineWithico}>
                                        <Text style={[styles.globalIcon,styles.mdIco]}>S </Text>
                                        <Text style={[styles.genText,styles.grayColor]}>{'Recherches Préférées'}</Text>
                                    </View>
                                    <Text style={[styles.globalIcon]}>6</Text>
                                </TouchableOpacity>
                        </View>
                        <View style={styles.accountdataBlock}>
                            <Text style={styles.subTitle}>Historique</Text>
                           {this.state.recentSearchListLen >  0 ? recentSearch:<Text style={{marginLeft:5}}>Aucun historique trouvé</Text>}
                        </View>
                        <View style={[styles.accountdataBlock,styles.justCenter]}>
                            <TouchableOpacity style={[styles.smallOutlineBtn]} activeOpacity={0.8} onPress={()=>this.clearHistory()}>
                                {/* <Text style={[styles.genButton,styles.orangeColor,styles.big]}>{'Effacer Tout L\'historique'}</Text> */}
                                {this.state.load1 ? <ActivityIndicator animating={true} size="small" color={"#f15a23"}/>
      : <Text style={[styles.genButton,styles.orangeColor,styles.big,{padding:0}]}>{'Effacer Tout L\'historique'}</Text>} 
                            </TouchableOpacity>
                        </View>
                        <View style={styles.accountdataBlock}>
                            <Text style={styles.subTitle}>Derniers articles vus</Text>
                            <ScrollView
                                ref={(scrollView) => { _scrollView = scrollView; }}
                                automaticallyAdjustContentInsets={false}
                                showsHorizontalScrollIndicator={false} 
                                contentContainerStyle={styles.Carousel}
                                horizontal>
                                {rectentList}
                            </ScrollView>
                        </View>
                    </View>
                </View>
            </ScrollView>
            <View style={styles.bottomArea}>
            <TouchableOpacity style={[styles.brandColor]} activeOpacity={0.8} onPress={()=>this.goto('ProductgridContainer',this.state.in_history,{})}>
                <Text style={[styles.genButton]}>Lancer la recherche</Text>
            </TouchableOpacity>
            </View>
            {Platform.OS === 'ios' ? <CKeyboardSpacer /> : (null)}
        </View>
    );
  }
}

function mapStateToProps(state, ownProps) {
	return {
        chat:state.chat,
        auth:state.auth,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		chatActions: bindActionCreators(chatActions, dispatch),
		authActions: bindActionCreators(authActions, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(FastSearch);
