import React, { Component } from 'react';
import {
  StyleSheet,
  Image,Text,View,TextInput,TouchableOpacity,ScrollView,Dimensions,Platform,Animated,WebView
} from 'react-native';
import { colors } from '../../config/styles';
import images from '../../config/images';
import styles from '../../config/genStyle';
import RadioButton from '../../lib/react-radio/index';
// import CheckBox from 'react-native-check-box';
import CheckBox from '../../lib/react-native-check-box';
import {Column as Col, Row} from 'react-native-flexbox-grid';
import FloatLabelTextInput from '../../lib/react-native-floating-label-text-input';
import { iconsMap, iconsLoaded } from '../../config/icons';
// redux specific
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as chatActions from '../../redux/reducers/chat';
import * as authActions from '../../redux/reducers/auth';

const HEADER_MAX_HEIGHT = 100;
const HEADER_MIN_HEIGHT = Platform.OS === 'ios' ? 50 : 50;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;
const {width,height}=Dimensions.get('window');

class MettreAvant extends Component {


static navigatorStyle = {
    // navBarBackgroundColor: colors.navBarBackgroundColor,
    // navBarTextColor: colors.navBarTextColor,
    // navBarSubtitleTextColor: colors.navBarSubtitleTextColor,
    // navBarButtonColor: colors.navBarButtonColor,
    // statusBarTextColorScheme: colors.statusBarTextColorScheme,
    // statusBarColor: colors.statusBarColor,
    // tabBarBackgroundColor: colors.tabBarBackgroundColor,
    // tabBarButtonColor: colors.tabBarButtonColor,
    // tabBarSelectedButtonColor: colors.tabBarSelectedButtonColor,
    // navBarSubtitleColor: colors.navBarSubtitleColor,
    drawUnderTabBar:true
};

constructor(props) {
    super(props);
    this.AdOptions=['adShowcase1','adShowcase2'];
    this.ListOptions=['showcase1','showcase2','showcase3'];
    this.state={
        priceData:this.props.getPriceData(),
        isAdShowcase:false,
        isListShowcase:false,
        adShowcase:'',
        listShowCase:'',
        price:0,
        listPrice:this.props.listPrice,
        totPrice:this.props.totPrice,
        userPrice:this.props.price,
        alreadyAd:false,
        alreadyList:false,
    };
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
}


componentWillMount(){
    iconsLoaded.then(()=>{});
}

setData(data){
        this.state=data;
        let newData = data;
        this.setState(newData); 
}

getDispPrice(num){
    return num.toFixed(2).toString().replace('.',',')+' €';
}
 
onNavigatorEvent(event) {
    //console.log(event)
    if(event.id=='willAppear'){
        this.props.chatActions.setScreen(this.props.testID);
        //console.log(this.props);
    this.props.navigator.toggleNavBar({to: 'shown',animated: true});
    this.props.navigator.toggleTabs({to:'hidden',animated:true});
    this.props.navigator.setStyle({navBarTitleTextCentered: true});
    this.props.navigator.setTitle({title: "Mettre en Avant"});
    this.props.navigator.setButtons({
             leftButtons: [{
                icon: iconsMap['back'],
                id: 'back2',
                title:'back to welcome',
            }],
            animated:false
        });


    let avantData=this.props.getAdData(this.props.index);
        if(typeof avantData != 'undefined' && Object.keys(avantData).length !== 0){
            let obj=avantData;
            obj.priceData=this.props.getPriceData();
            obj.listPrice=this.props.listPrice;
            obj.totPrice=this.props.totPrice;
            obj.userPrice=this.props.price;            
            this.setData(obj);
        }


    }
    if(event.id=='didAppear'){

    }
    
    if(event.id == 'back2'){
      this.props.setAdData(this.props.index,this.state);
      this.props.navigator.pop({
        animated: true, // does the pop have transition animation or does it happen immediately (optional)
      });
    } 
    if(event.id == 'menu'){
        this.props.navigator.toggleDrawer({
            side: 'left', // the side of the drawer since you can have two, 'left' / 'right'
            animated: true, // does the toggle have transition animation or does it happen immediately (optional)
            to: 'open' // optional, 'open' = open the drawer, 'closed' = close it, missing = the opposite of current state
        });
    }
   
  }
 handleOnPress(value){
        if(this.ListOptions.indexOf(value) > -1 ){
            this.setState({listShowCase:value,isListShowcase:true});
        }
        if(this.AdOptions.indexOf(value) > -1){
            this.setState({adShowcase:value,isAdShowcase:true});
        }
 }

calculatePrice(){
    let price= this.props.index == null ? 1 : 0;
    if(!this.state.alreadyAd && this.state.isAdShowcase){
        if(typeof this.state.adShowcase.price != 'undefined' && !isNaN(parseFloat(this.state.adShowcase.price))){
            price=price+this.state.adShowcase.price;
        }
    }
    if(!this.state.alreadyList && this.state.isListShowcase){
        if(typeof this.state.listShowCase.price != 'undefined' && !isNaN(parseFloat(this.state.listShowCase.price))){
            price=price+this.state.listShowCase.price;
        }
    }
    return price;
}
  render() {
    const {priceData,alreadyAd,alreadyList}=this.state;    
    let totPrice=this.calculatePrice();
    return (
        <View style={[styles.main,styles._HP15_]}>
                <ScrollView showsVerticalScrollIndicator={false} style={styles.mainScollview}>
                     <View style={styles.scrollInner}>
                        <View style={styles._MT10_}>
                            <View style={[styles.row,styles._PB20_]}>
                            {alreadyAd ? (<View style={{position:'absolute',backgroundColor:"#0005",height:'100%',width:'100%',zIndex:22}}></View>):(null)}
                                    <CheckBox style={[styles.genChk]}
                                        onClick={(checked) =>{
                                            if(!alreadyAd){
                                            let adShowcase='';
                                            if(checked){
                                                adShowcase=priceData.showcase_price[0];
                                            }
                                            this.setState({isAdShowcase:checked,adShowcase:adShowcase});
                                            }
                                            }}
                                        labelStyle={styles.midum} isChecked={this.state.isAdShowcase}
                                        rightText={'Mon Annonce En Vitrine - Moins De 1€ / J'}
                                    />
                                    <View style={[styles._MT10_,styles.inlineDatarow]}>
                                            <Image style={styles.leftbnImg} source={images.envitrine} />
                                            <View style={styles._PL10_}>
                                                {priceData.showcase_price.map((data,i)=>{
                                                    return <RadioButton key={'showcase_price'+i} currentValue={this.state.adShowcase} value={data} 
                                                onPress={(val)=>{
                                                    if(!alreadyAd){
                                                    this.setState({adShowcase:val,isAdShowcase:true})
                                                    }
                                                    }}>
                                                    <Text style={[styles.readiText,styles._PB10_]} >{data.label}{' - '}{this.getDispPrice(data.price)}</Text>
                                                </RadioButton>
                                                })}
                                            </View>
                                    </View>
                            </View>
                            <View style={[styles.row,styles._PB20_]}>
                            {alreadyList ? (<View style={{position:'absolute',backgroundColor:"#0005",height:'100%',width:'100%',zIndex:22}}></View>):(null)}
                                    <CheckBox style={[styles.genChk]}
                                        onClick={(checked) =>{
                                            if(!alreadyList){
                                            let listShowCase='';
                                            if(checked){
                                                listShowCase=priceData.toplist_price[0];
                                            }
                                            this.setState({isListShowcase:checked,listShowCase:listShowCase});
                                            }
                                            }}
                                        labelStyle={styles.midum} isChecked={this.state.isListShowcase}
                                        rightText={'Mon Annonce En Tête De Liste - Moins De 1€ / J'}
                                    />
                                    <View style={[styles._MT10_,styles.inlineDatarow]}>
                                            <Image style={styles.leftbnImg} source={images.bn1} />
                                            <View style={styles._PL10_}>
                                                {priceData.toplist_price.map((data,i)=>{
                                                    if(i<3){
                                                    return <RadioButton key={'toplist_price'+i} 
                                                    currentValue={this.state.listShowCase} value={data} 
                                                    onPress={(val)=>{
                                                        if(!alreadyList){
                                                        this.setState({listShowCase:val,isListShowcase:true})
                                                        }
                                                    }}>
                                                    <Text style={[styles.readiText,styles._PB10_]}>{data.label}{' - '}{width < 380 ? '\n':''}{this.getDispPrice(data.price)}</Text>
                                                </RadioButton>}else{
                                                    return null;
                                                }
                                                })}
                                            </View>
                                    </View>
                            </View>
                        </View>              
                    </View>
                </ScrollView>
                       <View style={styles.bottomFixdad}>
                    <View style={[styles._PB10_,styles._PT10_]}>
                        <Text style={[styles._F14_,styles.center]}>PRIX TOTAL DE MES ANNONCES :<Text style={[styles._F16_,styles.bold]}>   {this.getDispPrice(totPrice)}</Text></Text>
                    </View>
                 </View>
        </View>
    );
  }

}

function mapStateToProps(state, ownProps) {
	return {
        chat:state.chat,
        auth:state.auth,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		chatActions: bindActionCreators(chatActions, dispatch),
		authActions: bindActionCreators(authActions, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(MettreAvant);






