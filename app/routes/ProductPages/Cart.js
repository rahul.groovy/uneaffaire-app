import React, { Component } from 'react';
import {
  StyleSheet,
  Image,Text,View,TextInput,TouchableOpacity,ScrollView,Switch,Dimensions,Alert
} from 'react-native';
import { colors } from '../../config/styles';
import images from '../../config/images';
import styles from '../../config/genStyle';
import CheckBox from 'react-native-check-box';
import {settings} from '../../config/settings';
import ApiHelper from '../../lib/ApiHelper/ApiHelper';
import Spinner from 'rn-spinner';
import FloatLabelTextInput from '../../lib/react-native-floating-label-text-input';
import Loader from '../../components/Loader/Loader';
import DropdownAlert from 'react-native-dropdownalert';
import { iconsMap, iconsLoaded } from '../../config/icons';
import FaIcon from 'react-native-vector-icons/FontAwesome';
import PayPalPayment from '../../redux/utils/paypal';
import {FORTAB,TABLANDSCAPE,TABPORTRAIT} from '../../config/MQ'; 
//import Spinner from'rn-spinner';
const { height, width } = Dimensions.get('window');
import {Column as Col, Row} from 'react-native-flexbox-grid';
import LoaderButton from '../../components/LoaderButton/LoaderButton';
import moment from 'moment';
// redux specific
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as chatActions from '../../redux/reducers/chat';
import * as authActions from '../../redux/reducers/auth';
const api= new ApiHelper;
class Cart extends Component {

// static navigatorButtons = {
//      leftButtons: [{
//        icon: images.back,
//        id: 'back',
//        title:'back to welcome',
//      }],
// };
static navigatorStyle = {
     navBarBackgroundColor: colors.navBarBackgroundColor,
     navBarTextColor: colors.navBarTextColor,
     navBarSubtitleTextColor: colors.navBarSubtitleTextColor,
     navBarButtonColor: colors.navBarButtonColor,
     statusBarTextColorScheme: colors.statusBarTextColorScheme,
     statusBarColor: colors.statusBarColor,
     tabBarBackgroundColor: colors.tabBarBackgroundColor,
     tabBarButtonColor: colors.tabBarButtonColor,
     tabBarSelectedButtonColor: colors.tabBarSelectedButtonColor,
     navBarSubtitleColor: colors.navBarSubtitleColor,
     navBarTextFontFamily: colors.navBarTextFontFamily,
     //tabBarHidden: true,
     drawUnderTabBar:true
};  
constructor(props) {
    super(props);
    this.state={
        check1:false,
        value: 0,
        onBack:null,
        cartData:[],
        cartDataLength:0,
        ucredit:typeof this.props.auth.userdata != 'undefined' && this.props.auth.userdata != null ? this.props.auth.userdata.user_credit : 0,
        grandTotal:0,
        discount:0,
        type:'',
        coupon:''
    };
    // if you want to listen on navigator events, set this up
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
}

componentWillMount(){
    iconsLoaded.then(()=>{});
}
sendEventToDrawer(){
    this.props.navigator.handleDeepLink({link: 'Drawer',payload:{
        screen:this.props.testID
    }});
}

onNavigatorEvent(event) {
    //console.log(event)
    if (event.id === 'bottomTabSelected' && event.selectedTabIndex == 3) {
        if(this.state.onBack == null){
            //console.log('on back go to tab '+(event.unselectedTabIndex+1));
            this.setState({onBack:event.unselectedTabIndex});
        }
    }
    if(event.id=='willAppear'){       
        this.sendEventToDrawer();
        //this.props.navigator.switchToTab({})
        this.props.chatActions.setScreen(this.props.testID);
        //console.log(this.props);
        api.setAction(this.props.chatActions,this.props.authActions);
        this.props.navigator.toggleNavBar({
            to: 'shown',
            animated: true
        });
        this.props.navigator.toggleTabs({
            animated: true,
            to: 'hidden'
        });
        this.props.navigator.setStyle({navBarTitleTextCentered: true});
        this.props.navigator.setTitle({title: 'Panier'});
        this.props.navigator.setButtons({
                leftButtons: [{
                    icon: iconsMap['back'],
                    id: 'back2',
                    title: 'back to welcome'
                }],
                 rightButtons: [{
                    //icon: iconsMap[''],
                    id: '',
                    title: ''
                }],animated:false
            });
            
    }
    if(event.id=='didAppear'){
         typeof this.props.auth.userdata != 'undefined' && this.props.auth.userdata != null ? this.setState({ucredit:this.props.auth.userdata.user_credit}) : 0;
         this.getCartData();
    }
    if(event.id =='didDisappear'){
        //this.setState({onBack:null});
    }
    
    if(event.id == 'back2'){
        if(this.state.onBack!=null){
            let onBack = this.state.onBack
            this.setState({onBack: null},()=>{
               this.props.navigator.switchToTab({
                   tabIndex: onBack // (optional) if missing, this screen's tab will become selected
               });
            });
        }else{
            if(this.props.checkPop) {
                this.props.navigator.pop({
                    animated: true,
                });
            } else {
                this.props.navigator.switchToTab({
                    tabIndex: 0 // (optional) if missing, this screen's tab will become selected
                });
            }
        }     
    }
  }
removeView(key){
    let _this = this;    
    Alert.alert(
    'effacer!!',
    'Êtes-vous sûr',
    [
        {text: 'Non', onPress: () => '', style: 'cancel'},
        {text: 'Oui', onPress: () => this.deleteSe(key)},
    ],
    { cancelable: false }
    )
}
doDiscount(){
    if(this.state.coupon == ''){
        this.dropdown.alertWithType('error', 'Error','Entrez le code de coupon');
        return false;
    } 
    let _this = this;
    _this.setState({load1:true},()=>{
        let request={
            url:settings.endpoints.getCoupanPrice,
            method:'POST',
            params:{token:this.props.auth.token != undefined ? this.props.auth.token:'',coupon:this.state.coupon}
        }
        api.FetchData(request).then((result)=>{       
            if(result.status){
                _this.setState({
                    discount:result.disscount_price,
                    load1 :false
                });
                 _this.dropdown.alertWithType('success', 'Success','Coupon appliqué'); 
            }else{
                _this.setState({                
                    load1 :false,
                    discount:0
                });
                 _this.dropdown.alertWithType('error', 'Error',result.message);
            }
        }).catch((error)=>{
            //console.log(error);
        });
    });
    
}
deleteSe(key){
    let _this = this;
    let markers = this.state.cartData;
    _this.setState({load:true},()=>{
        let index = markers.indexOf(markers[key]);      
        let request={
            url:settings.endpoints.deleteSingleCartItem,
            method:'POST',
            params:{token:this.props.auth.token != undefined ? this.props.auth.token:'',id:markers[key].id}
        }
        api.FetchData(request).then((result)=>{
            if(result.status){
                if (index > -1){
                     markers.splice(index, 1);
                    _this.setState({markers,load:false},()=>_this.getCartData());
                }
            }else{
                alert('Somthing Went Wrong');
            }
        }).catch((error)=>{
            //console.log(error);
        }); 
    });
    
}
getCartData(){
    let _this = this;
    _this.setState({load:true},()=>{
        let request={
            url:settings.endpoints.cartList,
            method:'POST',
            params:{token:this.props.auth.token != undefined ? this.props.auth.token:''}
        }
        api.FetchData(request).then((result)=>{       
            if(result.status){
                _this.setState({
                    cartData:result.cartList != undefined ? result.cartList :[] ,
                    type:result.cartList != undefined ? result.cartList[0].type :[] ,
                    cartDataLength:result.cartList.length != undefined ? result.cartList.length :0,
                    grandTotal : result.grandTotal,load :false
                }); 
            }else{
                _this.setState({                
                    load :false,
                    cartData:[],
                    cartDataLength:0,
                    grandTotal : 0
                });
            }
        }).catch((error)=>{
            //console.log(error);
        }); 
    });
    
}

handleOnPress(value){
    this.setState({value:value})
}
goCheckOut(){
    let _this = this;
    _this.setState({load:true},()=>{
        let typeOf = '';
        if(typeof this.state.type != 'undefined'){
            if(this.state.type == 'user_sell'){
                typeOf = 'ad';
            }else if(this.state.type == 'user_adv'){
                typeOf = 'adv';
            }
        }
        let request={
            url:settings.endpoints.checkWallet,
            method:'POST',
            params:{token:this.props.auth.token != undefined ? this.props.auth.token:'',type:typeOf,coupon_code:this.state.coupon != '' ? this.state.coupon :''}
        }
        api.FetchData(request).then((result)=>{       
            if(result.status){           
                if(result.paypal){
                    _this.setState({
                        load :false
                     },()=>this.payWithPaypal(result.amount,false));                 
                }else{
                    _this.props.navigator.push({
                        screen: 'success',
                        passProps:{
                            status:'Y',
                            message:result.message,
                            token:this.props.auth.token,
                            userdata:this.props.auth.userdata
                        }
                    });
                }          
            }else{
                _this.setState({                
                    load :false
                });
            }
        }).catch((error)=>{
            console.log(error);
        });
    });
    
}
payWithPaypal(tot,stat){
    let total = tot;
    if(total != '' && total != 0){
        let price = parseFloat(total).toFixed(2);
        //console.log(""+price);return false;
        let params = {};
        if(typeof this.state.type != 'undefined'){
            if(this.state.type == 'user_sell'){
                typeOf = 'ad';
            }else if(this.state.type == 'user_adv'){
                typeOf = 'adv';
            }
        }
        if(stat){
            params = {
                price: price,
                currency: 'EUR',
                description: 'paypal',
                custom:'paypal_'+typeOf
            };
        }else{
            params = {
                price: price,
                currency: 'EUR',
                description: 'wp',
                custom:'wp_'+typeOf
            };
        }                  
        PayPalPayment(params).then(confirm => {
            if(confirm.response.state=='approved'){
                this.goSuccess(confirm,total);           
            }else{
                alert('Quelque chose s\'est mal passé avec le paiement');
            }       
        }).catch(error => console.log(error));        
    }
}
goSuccess(confirm,data){
    this.setState({load:true},()=>{
        let request={
            url:settings.endpoints.allAdTransaction,
            method:'POST',
            params:{token:this.props.auth.token,paypal_id:confirm.response.id,coupon_code:this.state.coupon != '' ? this.state.coupon :''}
        }
        api.FetchData(request).then((result)=>{
            if(result.status){
                this.setState({load:false},()=>{
                    this.props.navigator.push({
                        screen: 'success',
                        passProps:{
                            status:'Y',
                            message:result.message,
                            token:this.props.auth.token,
                            userdata:this.props.auth.userdata
                        }
                    }); 
                });           
            }else{
                this.setState({load:false},()=>{
                    alert(result.message)
                });
            } 
        }).catch((error)=>{
            //console.log(error);
            this.setState({load:false},()=>{
                alert('Quelque chose s\'est mal passé dans le paiement.')
            });
        });
    });
    
}

goto(page){
    this.props.navigator.push({
      screen: page
    });
}
  render() {
    let _this = this;
    let type = '';
    let catrView = this.state.cartDataLength > 0 ? ( 
                     this.state.cartData.map(function (item,key) { 
                        let dataDate = moment(item.createdAt).format("DD MMM [|] hh:mm a");
                        if(item.type == 'user_sell'){
                            type= 'Purchase Ad';
                        }else if(item.type == 'user_adv'){
                            type= 'Advertisement';
                        } 
                        return ( 
                        <Row size={12} key={key} style={[styles.proRow,styles.prolistRow,{padding:5}]}>
                            <Col sm={5} md={3} lg={2} style={[styles.proCol,styles.listCol]} >
                                <View style={styles.colInner}>
                                    <View style={styles.imageCanvas}>
                                        <View style={styles.imgInner}>
                                            <Image style={styles.cartItemImg} source={{uri:item.image}} />
                                        </View>
                                    </View>
                                </View>     
                            </Col>
                            <Col sm={7} md={9} lg={10} style={[styles.proCol,styles.listCol]}>
                                <View style={[styles.proCaption]}>
                                    {item.ad_type === 'trocs' ? (
                                        <View style={{justifyContent:'center',flex:1}}>
                                            <View style={{flexDirection:'row'}}>
                                                <FaIcon name="mail-forward" color={'#34c24c'} size={15} style={{marginRight:5}}/>
                                                <Text style={[styles.proName,styles.big,styles._PB10_]} numberOfLines={1}>{item.title}</Text>
                                            </View>
                                            <View style={{flexDirection:'row'}}>
                                                <FaIcon name="reply" color={'#2e5f9b'} size={15} style={{marginRight:5}}/>
                                                <Text style={[styles.proName,styles.big,styles._PB10_]} numberOfLines={1}>{item.title_contre}</Text>
                                            </View>
                                        </View>) 
                                        : 
                                        <Text style={[styles.proName,styles.big,styles._PB10_]} numberOfLines={1}>{item.title}</Text>
                                    }
                                    
                                        <View style={styles.row}>
                                            <View style={[styles.dataRow,styles._PB10_]}>
                                                <View style={styles.inlineWithico}>
                                                    <Text>{type}</Text>
                                                </View>                                           
                                            </View> 
                                            <View style={[styles.dataRow,styles._PB10_]}>
                                                <View style={styles.inlineWithico}>
                                                    <Text>{dataDate}</Text>
                                                </View>                                           
                                            </View> 
                                            <View style={[styles.dataRow,styles._PB10_,styles.Valign]}>
                                                <Text style={[styles.orangeColor,styles.bold,styles.midum]}>{item.total_price} €</Text>  
                                                <View>
                                                    <TouchableOpacity onPress={() => _this.removeView(key)} activeOpacity={0.8}><Text style={[styles.globalIcon,styles.bigIco,styles.orangeColor]}>8</Text></TouchableOpacity>
                                                </View>
                                            </View>
                                        </View>
                                </View>
                            </Col>
                        </Row>)
                    })
                ):<View style={[styles._MT15_,styles.Center]}><Text>Aucune donnée disponible</Text></View>;
    let gTotal = (this.state.grandTotal-this.state.ucredit)-this.state.discount;
    let ttotal = Math.round(gTotal * 100) / 100;
    let ctotal = this.state.grandTotal-this.state.discount;
    return (  
 
        <View style={{flex:1}}>
        {this.state.load ?  (<View style={[styles.Center]} ><Loader/></View>) :           
        (<View style={[styles.main,{flex:1,paddingTop:0,padding:0}]}>
            <View style={{flex:7}}>
            <ScrollView showsVerticalScrollIndicator={false} 
            contentContainerStyle={[{flexGrow:1,alignSelf:'stretch',backgroundColor:'#f8f8f8'},{paddingLeft:7,paddingRight:7}]}>
                {/* <View style={{ flex:1,backgroundColor:"#f8f8f8"}}> */}
                    {/* <View style={[styles.row,styles._PB15_]}>
                         <CheckBox
                            style={[styles.genChk]}
                            onClick={(checked) =>{
                            this.setState({check1:!checked})}}
                            labelStyle={styles.midum}
                            isChecked={this.state.check1}
                            rightText={'Tout selectionner'}
                        />
                    </View> */}
                    {/* <View style={{flex:1,flexDirection: 'column',backgroundColor: '#f8f8f8',paddingBottom:2}}> */}
                        {catrView}
                    {/* </View> */}
                    {this.state.cartDataLength > 0 ? 
                    <View style={{paddingVertical:10,flexDirection:'row',backgroundColor:"#fff"
                    }}>
                    <FloatLabelTextInput  style={styles.genInput} placeholder={"Code coupon"}  
                    underlineColorAndroid={'#eee'}
                    onChangeTextValue ={(val) =>this.setState({coupon:val})} 
                    value={this.state.coupon}/>
                    <View style={{
                        alignItems:'center',justifyContent:'center',
                    }} >
                        <LoaderButton onPress={()=>this.doDiscount()} load={this.state.load1} BtnStyle={[styles.brandColor,styles.justCenter,{padding:5}]} text={'Appliquer'} textStyle={[styles.genButton,{padding:5,fontSize:14}]} />       
                    </View>
                    </View>
                    :null}
                    {this.state.cartDataLength > 0 ? 
                        <View style={mainStyle.cont}>
                        <View style={mainStyle.row}>               
                            <Text style={mainStyle.text}>Sous total</Text>               
                            <Text style={mainStyle.text}>{colors.euro}{' '}{this.state.grandTotal}</Text>
                        </View>
                        <View  style={mainStyle.row}>                
                            <Text style={mainStyle.text}>Remise</Text>               
                            <Text style={mainStyle.text}>{colors.euro}{' '}{Math.round(this.state.discount * 100) / 100}</Text>
                        </View>
                        <View style={mainStyle.row}>                
                            <Text style={mainStyle.text}>{'Crédits'}</Text>                
                            <Text style={mainStyle.text}>{colors.euro}{' '}{this.state.grandTotal<this.state.ucredit ? this.state.grandTotal-this.state.discount : this.state.ucredit}</Text>               
                        </View> 
                        <View style={mainStyle.row}>                
                            <Text style={mainStyle.text}>Prix total</Text>                
                            <Text style={mainStyle.text}>{colors.euro}{' '}{this.state.grandTotal>this.state.ucredit ? ttotal : 0}</Text> 
                        </View>
                    </View>
                    :null}
                    
                {/* </View> */}
            </ScrollView>
            </View>
            <View style={[{paddingLeft:0,
            flex:1,
        paddingRight:0,
        paddingTop:10,
        paddingBottom:10,
        borderRadius:0,
        borderColor:'#d6d6d6',
        borderWidth:1,
        backgroundColor:'#fff', 
        //position:'relative',
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf:'stretch'
        }]}>
        <View style={[{flex:1,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
        overflow:'visible'}]}>
            <Text style={{
                fontSize:FORTAB ? 22:18,
    color:'#333',
    //paddingBottom:10,
    //alignSelf:'flex-start'
            }}>{'Panier total'}</Text>
            <Text style={[styles.midum,styles.gray]}>{' ('+this.state.cartDataLength+' Produit(s)) : '}</Text>
            <Text style={[styles.orangeColor,styles.bold,styles.large]}>{colors.euro}{' '}{this.state.grandTotal > this.state.ucredit ? ttotal : 0}</Text> 
        </View>
        {this.state.cartDataLength > 0 &&
        <View style={{flexDirection:'row',flex:1,paddingLeft:5,paddingRight:5}}>
            {/* <View style={[{flex:3,backgroundColor:"#F00"},styles.justCenter]}> */}
            <TouchableOpacity style={{flex:3,alignItems:'center',justifyContent:'center',backgroundColor:'#f15a23',
        padding:8,
        borderColor:'#eee',
        borderRadius:3}} activeOpacity={0.8} onPress={()=>this.state.cartDataLength > 0 ?  this.goCheckOut():null}>
                <Text style={[styles.genButton2,{fontSize:12}]}>Payer avec vos crédits</Text>
            </TouchableOpacity>
            {/* </View> */}
            <View style={[{flex:1},styles.justCenter]}>
                <Text>Ou</Text>
            </View>
            {/* <View style={[{flex:3,backgroundColor:"#F00"},styles.justCenter]}> */}
                {/* paypal button */}
                <TouchableOpacity style={[styles.yellow,styles.btnCont,styles.justCenter,{flexDirection:'row',flex:3}]} activeOpacity={0.8} onPress={()=>this.payWithPaypal(ctotal,true)}>
                    {/* <Text style={[styles.genButton,styles._F16_]}>  */}
                    <Image style={[styles.paypalImg]} source={images.paypal} />
                     {/* <Text style={styles.blackColor}>Caisse</Text>  */}
                     {/* </Text>  */}
            </TouchableOpacity>
            {/* </View> */}
        </View>
        }
            </View>
            <DropdownAlert
        ref={(ref) => this.dropdown = ref}
        onClose={(data) => {/*console.log(data);*/}} />
        </View>)}

        </View>
        
    );
  }
}
const mainStyle = StyleSheet.create({
    cont:{
        flex:1,
        padding:10,
    },
    row:{
        flex:1,
        alignItems:'center',
        justifyContent:'space-between',
        flexDirection:'row',
        paddingTop:10,
        paddingBottom:10,
        borderColor:"#ddd",
        borderBottomWidth:1, 
    },
    col:{
        flex:1,
        alignItems:'center',
        justifyContent:'center',       
    },
    text:{
        color:"#666",
        fontSize:18,
    }
})

function mapStateToProps(state, ownProps) {
	return {
        chat:state.chat,
        auth:state.auth,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		chatActions: bindActionCreators(chatActions, dispatch),
		authActions: bindActionCreators(authActions, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(Cart);


/* paypal button 
<TouchableOpacity style={[styles.yellow,styles.btnCont,styles._MPR5_,styles.justCenter,{flexDirection:'row',flexWrap:'wrap'}]} activeOpacity={0.8} onPress={()=>this.payWithPaypal(ctotal,true)}>
                    <Text style={[styles.genButton,styles._F16_]}> 
                    <Image style={[styles.paypalImg,{marginRight:5}]} source={images.paypal} />
                     <Text style={styles.blackColor}>Caisse</Text> 
                     </Text> 
            </TouchableOpacity>
*/

