import React, { Component } from 'react';
import {
  StyleSheet,
  Image,Text,View,TextInput,TouchableOpacity,ScrollView,Dimensions,Platform,Animated,WebView,Alert
} from 'react-native';
import { colors } from '../../config/styles';
import images from '../../config/images';
import styles from '../../config/genStyle';
import {Column as Col, Row} from 'react-native-flexbox-grid';
import { iconsMap, iconsLoaded } from '../../config/icons';
import {settings} from '../../config/settings';
import ApiHelper from '../../lib/ApiHelper/ApiHelper';
import LoaderButton from '../../components/LoaderButton/LoaderButton';
import Loader from '../../components/Loader/Loader';
import moment from 'moment';
import PayPalPayment from '../../redux/utils/paypal';
// redux specific
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as chatActions from '../../redux/reducers/chat';
import * as authActions from '../../redux/reducers/auth';

const HEADER_MAX_HEIGHT = 100;
const HEADER_MIN_HEIGHT = Platform.OS === 'ios' ? 50 : 50;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;
const api= new ApiHelper;
class avant extends Component {
//   static navigatorButtons = {
//     leftButtons: [{
//     icon: images.back,
//     id: 'back',
//     title:'back to welcome',
//     }],
//   };

  
static navigatorStyle = {
    navBarBackgroundColor: colors.navBarBackgroundColor,
    navBarTextColor: colors.navBarTextColor,
    navBarSubtitleTextColor: colors.navBarSubtitleTextColor,
    navBarButtonColor: colors.navBarButtonColor,
    statusBarTextColorScheme: colors.statusBarTextColorScheme,
    statusBarColor: colors.statusBarColor,
    tabBarBackgroundColor: colors.tabBarBackgroundColor,
    tabBarButtonColor: colors.tabBarButtonColor,
    tabBarSelectedButtonColor: colors.tabBarSelectedButtonColor,
    navBarSubtitleColor: colors.navBarSubtitleColor,
    drawUnderTabBar:true,
};

constructor(props) {
    super(props);
    // if you want to listen on navigator events, set this up
    this.state={
        //value:1,
        //value1:1,
        //value2:1,
        //scrollY: new Animated.Value(0),
        check1:false,
        adList:[],
        adListLen:0,
        btnLoad:false,
        loading:true,
        priceData:{},
    };
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
}
componentWillMount(){
    iconsLoaded.then(()=>{
        this.getDataOfId();
    });
}
onNavigatorEvent(event) {
    if(event.id=='willAppear'){
        // this.props.navigator.toggleNavBar({
        //     to: 'shown',
        //     animated: true
        // });
        api.setAction(this.props.chatActions,this.props.authActions);
        this.props.chatActions.setScreen(this.props.testID);
        //console.log(this.props);
        this.props.navigator.toggleTabs({to:'hidden',animated:true});
        this.props.navigator.setStyle({navBarTitleTextCentered: true});
        this.props.navigator.setTitle({title: "Promouvoir votre annonce"});  
        this.props.navigator.setButtons({
             leftButtons: [{
                icon: iconsMap['back'],
                id: 'back2',
                title:'back to welcome',
            }],
            animated:false
        });      
    }
    if(event.id=='didAppear'){
       
    }

    if(event.id == 'back2'){
      this.props.navigator.pop({
        animated: true // does the pop have transition animation or does it happen immediately (optional)
      });
    }
    if(event.id == 'menu'){
        this.props.navigator.toggleDrawer({
            side: 'left', // the side of the drawer since you can have two, 'left' / 'right'
            animated: true, // does the toggle have transition animation or does it happen immediately (optional)
            to: 'open' // optional, 'open' = open the drawer, 'closed' = close it, missing = the opposite of current state
        });
    }   
}
goto(page){   
    this.props.navigator.push({
        screen: page
    }); 
}
getDataOfId(){
    let _this = this;
    _this.setState({loading:true},()=>{ 
        let request={
            url:settings.endpoints.backToTopList,
            method:'POST',
            params:{token:this.props.auth.token,ad_ids:this.props.ad_id}
        }
        api.FetchData(request).then((result)=>{           
            if(result.status){
                _this.setState({adList:result.ad_list,
                    adListLen:result.ad_list.length,loading:false},()=>{
                        _this.getPriceData();
                    }); 
            }else{
                //_this.setState({});
                _this.handleError('Impossible d\'obtenir des données');
            }
        }).catch((error)=>{
        //console.log(error);
        //_this.setState({load :false});
        _this.handleError('Impossible d\'obtenir des données');
        });
    });
    
}


getPriceData(){
    //this.setState({loading:true});
    if(this.state.priceData!== undefined && Object.keys(this.state.priceData).length === 0){
        let request={
        url:settings.endpoints.postPriceList,
        method:'POST',
        params:{}
    }
        api.FetchData(request).then((results)=>{
            if(results.status){
                this.setState({priceData:results.post_prices,loading:false});
            }else{
                this.handleError('Impossible d\'obtenir des données');
            }
        }).catch((error)=>{
            this.handleError('Impossible d\'obtenir des données');
        });
    }else{
        this.setState({loading:false});
    }
}
handleError(msg){
    Alert.alert(
            'Précurseur',
            msg,
            [
                {text: 'Annuler', onPress: () => {}, style: 'cancel'},
                {text: 'Approuvé', onPress: () => {this.goback()}},
            ],
            { cancelable: false }
        )
}

// gettrans(bool){
//     if(bool){
//         return  this.state.scrollY.interpolate({
//             inputRange: [0, 50],
//             outputRange: [0, -50],
//             extrapolate: 'clamp',
//         });
//     }else{
//         return  this.state.scrollY.interpolate({
//             inputRange: [0, 50],
//             outputRange: [-50, 0],
//             extrapolate: 'clamp',
//         });  
//     }
// }
checked(c){
    this.setState({check1:c});
}

onPay(){
    this.setState({btnLoad:true},()=>{
        let request={
            url:settings.endpoints.saveTopList,
            method:"POST",
            params:{
                token:this.props.auth.token,
                ad_ids:this.props.ad_id,
                duration_id:this.state.priceData.toplist_price[3].id,
            }
        };
        api.FetchData(request).then((result)=>{
            this.setState({btnLoad:false},()=>{
                if(result.status){
                    if(result.paypal){
                        this.payWithPaypal(result.amount);
                    }else{
                        this.props.navigator.push({
                            screen: 'success',
                            passProps:{
                                status:'Y',
                                message:result.message,
                                token:this.props.auth.token,
                                userdata:this.props.auth.userdata
                            }
                        });
                    }               
                }else{               
                    alert('Something went wrong');
                }
            });        
        }).catch((err)=>{
            this.setState({btnLoad:false},()=>{
                alert('Quelque chose a mal tourné');
            });
        });
    });
    // let _this=this;
    // setTimeout(function(){
    //     _this.setState({btnLoad:false});
    // },200);
   
}
payWithPaypal(amt){
    let total=amt;
    if(total != '' && total != 0){
        let price = parseFloat(total).toFixed(2).toString();
        //console.log(""+price);return false;
        let params = {
            price: price,
            currency: 'EUR',
            description: 'paypal',
            custom:'paypal_toplist'
        };
        PayPalPayment(params).then(confirm => {
            if(confirm.response.state=='approved'){
                console.log(confirm);
                this.goSuccess(confirm,total);           
            }        
        }).catch(error => console.log(error));        
    }
}
goSuccess(confirm,data){
    this.setState({loading:true},()=>{
        let request={
            url:settings.endpoints.topListAdTransaction,
            method:'POST',
            params:{
                token:this.props.auth.token,
                ad_ids:this.props.ad_id,
                duration_id:this.state.priceData.toplist_price[3].id,
                paypal_id:confirm.response.id,
            }
        }
        api.FetchData(request).then((result)=>{
            if(result.status){
                this.setState({loading:false},()=>{
                    this.props.navigator.push({
                        screen: 'success',
                        passProps:{
                            status:'Y',
                            message:result.message,
                            token:this.props.auth.token,
                            userdata:this.props.auth.userdata
                        }
                    });
                });              
            }else{
                this.setState({loading:false},()=>{
                    alert('Quelque chose s\'est mal passé dans le paiement.')
                });
            } 
        }).catch((error)=>{
            //console.log(error);
            this.setState({loading:false},()=>{
                alert('Quelque chose s\'est mal passé dans le paiement.')
            });
        });
    });
    
}
/* extra methods */ 
handleOnPress(val){

}
handleOnPress1(val){

}
handleOnPress2(val){

}
handleOnPress3(val){

}

videoShow(){

}
/* extra methods finish */

calculatePrice(){
    let price=0;
    if(Object.keys(this.state.priceData).length !== 0 && this.state.adListLen > 0){
        price=this.state.adListLen*this.state.priceData.toplist_price[3].price;
    }
    return price;
}

getDispPrice(num){
    return num;
}



render() {
    let totPrice=this.calculatePrice();
    let data = 
    this.state.adListLen > 0 ? (  
        contents =  this.state.adList.map(function (item,key) {
            let dataDate = moment(item.post_on).format("DD MMM [|] hh:mm a");
            return (                 
               <Row key={key} size={12} style={[styles.proRow,styles.prolistRow,styles._MT0_,styles._PT0_,styles.Prolistgray]}>
                    <Col sm={5} md={4} lg={3} style={[styles.proCol,styles.listCol]} >
                        <View style={styles.imageCanvas}>
                            <View style={styles.imgInner}>
                                {item.is_video_check == 1 ? <Image style={styles.hasVideo1} source={images.video} />:null}
                                {item.image_name != '' ? <Image style={[styles.proImg]} source={{uri:item.image_name}} resizeMode="stretch"/>:null}
                                
                            </View>
                        </View>
                        <View style={[styles.bottomDetail,styles.bottomCaption]}>
                            <Text style={[styles.leftAlign,styles.white]}>
                                <Text style={styles.small}>{dataDate}</Text>
                            </Text>
                            <View style={[styles.rightAlign]}>
                                <View style={styles.relative}><Text style={[styles.globalIcon,styles.camIco,styles.white,styles.camIcoAbs]}>s</Text><Text style={[styles.bold,styles.abscamtxt,styles.white,styles.small]}>{item.image_count}</Text></View> 
                            </View>
                        </View>
                        </Col>
                    <Col sm={7} md={8} lg={9} style={[styles.proCol,styles.listCol]}>
                        <View style={styles.proCaption}>
                                <Text style={[styles.proName,styles.big]} numberOfLines={1}>{item.title}</Text>
                                <View style={styles.row}>
                                    <View style={[styles.inlineDatarow,styles.spaceBitw,styles._MT5_]}>
                                        <Text style={[styles.orangeColor,styles.bold]}>{item.price} </Text>
                                    </View>
                                    <View style={[styles.inlineDatarow,styles._MT10_,styles._PB15_]}>
                                        {item.is_urgent == 1 ?  <Text style={[styles.inlineWithico,styles.borderRight,styles.red,styles.small]}>Urgent<Text style={styles.globalIcon}> x</Text></Text>:null}
                                       {item.is_direct_sale == 1 ? <Text style={[styles.inlineWithico,styles.red,styles.small]}>Achat Ditect<Text style={styles.globalIcon}> w</Text></Text>:null}
                                        
                                    </View>
                                    <View style={[styles.inlineDatarow]}>
                                        <Text  style={[styles.globalIcon,styles.calenderAvant]}>e</Text><Text style={styles._PL5_}>{item.remain_days} Jours Restants</Text> 
                                    </View>  
                                </View>
                            </View>
                    </Col>
                </Row>
            )
        })
    ):<View style={[styles._MT15_,styles.Center]}><Text>Aucune donnée disponible</Text></View>;;
    return (
        <View style={{flex:1}}>
            {this.state.loading ? (<View style={[styles.Center]}><Loader/></View>) : (
            <ScrollView showsVerticalScrollIndicator={false} style={[styles.mainScollview,{flexGrow:1,padding:7}]}>
                <View style={styles.scrollInner}>
                    <View style={styles.proMain}>
                        {data}                      
                    </View>
                    {this.state.adListLen > 0 ? (
                    <View style={{padding:7}}> 
                           
                            <View> 
                            <Text style={{fontSize:16}}>
                                {'Montant total de votre commande - Remonter en tête de liste : '+this.getDispPrice(totPrice)+' TTC.'}
                                </Text> 
                                                
                               
                            </View>
                    </View> ):(null)}                   
                </View>               
            </ScrollView>)}
            
            {this.state.adListLen > 0 && !this.state.loading ? (
                <View>                 
                <Text style={{padding:8}}>{"En continuant, j'accepte les conditions d'utilisation d'UneAffaire.fr (y compris la renonciation au droit de rétractation). : "+this.getDispPrice(totPrice)+" TTC."}</Text>
                                
            <View style={{padding:7}}> 
                <LoaderButton load={this.state.btnLoad} onPress={()=>this.onPay()} BtnStyle={[styles.brandColor]} text={'Payer'} textStyle={styles.genButton} />
            </View></View> ):(null)}
        </View>
    );
  }
}

function mapStateToProps(state, ownProps) {
	return {
        chat:state.chat,
        auth:state.auth,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		chatActions: bindActionCreators(chatActions, dispatch),
		authActions: bindActionCreators(authActions, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(avant);