import React, { Component } from 'react';
import {StyleSheet,Image,Text,View,TextInput,TouchableOpacity,ScrollView,Switch,Modal,WebView,Animated,
        AsyncStorage,Share,ActivityIndicator,Dimensions,Platform,Alert} from 'react-native';
import { colors } from '../../config/styles';
import images from '../../config/images';
import styles from '../../config/genStyle';
import RadioButton from '../../lib/variant-radio/index';
import {Navigation,Screen,SharedElementTransition} from 'react-native-navigation';

import MapView , {PROVIDER_GOOGLE} from 'react-native-maps'; 
//import Icon from 'react-native-vector-icons/FontAwesome';
import Communications from 'react-native-communications';
import {settings} from '../../config/settings';
import ApiHelper from '../../lib/ApiHelper/ApiHelper';
import ImageSlider from '../../lib/react-native-image-slider/ImageSlider';
import { iconsMap, iconsLoaded, Icon } from '../../config/icons';
import ActionSheet from 'react-native-actionsheet';
import GetDirections from '../../components/react-native-maps-directions';
import Gallery from '../../lib/react-native-gallery/library/Gallery';
//import YouTube from 'react-native-youtube';
import moment from 'moment';
import tryGetLocation from '../../redux/utils/location';
import startMain from '../../config/app-main';
import PayPalPayment from '../../redux/utils/paypal';
// redux specific
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as chatActions from '../../redux/reducers/chat';
import * as authActions from '../../redux/reducers/auth';
const api=new ApiHelper;
const { height, width } = Dimensions.get('window');
const FORTAB = width < 1025 && width > 721;
import {Column as Col, Row} from 'react-native-flexbox-grid';
import Orientation from 'react-native-orientation';
import FaIcon from 'react-native-vector-icons/FontAwesome';
import { Calendar, CalendarList, Agenda } from 'react-native-calendars';
import _ from 'lodash';

class ProductDetail extends Component {
    genModal(){
        Navigation.showModal({
            screen: "contactModal", // unique ID registered with Navigation.registerScreen
            title: "Modal", // title of the screen as appears in the nav bar (optional)
            passProps: {
                id:this.props.data !== undefined ? this.props.data.ad_slug ? this.props.data.ad_slug : this.props.data.id !== undefined ? this.props.data.id:'':'',
                token:this.props.auth.token,
                page:'ProductDetail'
            }, // simple serializable object that will pass as props to the modal (optional)
            navigatorStyle: {}, // override the navigator style for the screen, see "Styling the navigator" below (optional)
            navigatorButtons: {}, // override the nav buttons for the screen, see "Adding buttons to the navigator" below (optional)
            animationType: 'slide-up' // 'none' / 'slide-up' , appear animation for the modal (optional, default 'slide-up')
        });
    }
  _renderHeader(section) {
    return (
      <View style={styles.conditionsubTitle}>
        <Text style={styles.conditionsubtext}>{section.title}</Text>
        <Text style={styles.plusTab}>{section.button}</Text>
        
      </View>
    );
  }
  _renderContent(section) {
    return (
      <View style={styles.conditionContent}>
        <Text>{section.content}</Text>
      </View>
    );
  }

static navigatorStyle = Platform.OS === 'ios' ? {
    navBarBackgroundColor: 'rgba(0,0,0,.3)',
    drawUnderNavBar: true,
    topBarElevationShadowEnabled:false,
    //tabBarHidden: true,
    drawUnderTabBar:true,
    navBarTransparent: true,
    navBarTranslucent: true    
}:{
    navBarBackgroundColor: 'rgba(0,0,0,.3)',
    drawUnderNavBar: true,
    topBarElevationShadowEnabled:false,
    tabBarHidden: true,
    drawUnderTabBar:true,
    navBarTransparent: true,
    navBarTranslucent: true 
};
  
constructor(props) {
    super(props);
     this.ActionSheetOptions = {
            CANCEL_INDEX: 2,
            DESTRUCTIVE_INDEX: 2,
            options: ['Appel', 'SMS', 'Annuler'],
            title: 'Lequel aimes-tu?'
    };
    this.ActionSheets=[];
    this.youVideo=null;
    this.state={
        width:Dimensions.get('window').width,
        height:Dimensions.get('window').height,
        tabs:1,
        tabSelected:1,
        value:1,
        value1:1,
        modalVisible: false,
        starCount: 3.5,
        iconHeart:'m',
        visible: false,
        Details:{},
        UserDetail:[],
        radioArray : [],
        fav:0,
        sliderPos:0,
        min:false,
        youPlay:false,
        image:typeof this.props.data.single_image != 'undefined' ? this.props.data.single_image : typeof this.props.data.image != 'undefined' ? this.props.data.image:this.props.data.ad_image,
        region:{ latitude: 37.78825, longitude: -122.4324, latitudeDelta: 0.0922, longitudeDelta: 0.0421,},
        options:{},
        quantity:1,
        gettingDirection:false,
        totalOfDir:0,
        load1:false,
        calenderModal : false,
        currentMonth:{},
        setData:[]
    };
    // if you want to listen on navigator events, set this up
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  
}
  
componentWillMount(){
    iconsLoaded.then(()=>{});
    this._interval = setInterval(() => {
        const newWidth = Dimensions.get('window').width;
        const newHeight =Dimensions.get('window').height;
        if (newWidth !== this.state.width || newHeight !== this.state.height) {
            this.setState({width: newWidth,height:newHeight});
        }
    }, 16);
}
componentWillUnmount() {
    clearInterval(this._interval);
}
componentDidMount() {
    let _this = this;
    this.setState({load:true},()=>this.getSingleData());
    if(Platform.OS !== 'ios'){
        this.handleAppear();
    }
    
    Orientation.addOrientationListener(this._orientationDidChange);
}

_orientationDidChange = (orientation) =>{
    setTimeout(()=>{
        this.forceUpdate();
    },20);
    setTimeout(()=>{
        if(this.ImageSlider !== undefined && this.ImageSlider !== null && this.ImageSlider._move !== undefined){
            //console.log('Imageslider re-move to last state');
            this.ImageSlider._move(this.state.sliderPos);
        }
        if(this.youTubeWeb !== undefined && this.youTubeWeb !== null){
            let vH = FORTAB ? 500 : 300;
            this.youTubeWeb.injectJavaScript('resize('+this.state.width+','+vH+');');
        }
    },400);
}

handleAppear(){
api.setAction(this.props.chatActions,this.props.authActions);
this.props.chatActions.setScreen(this.props.testID);
this.props.navigator.toggleTabs({
        to: 'hidden',
        animated: true, 
});
this.props.navigator.setButtons({
    leftButtons: [{
        icon: iconsMap['back'],
        id: 'back2',
        title:'back to welcome',
    }],animated:false
})
// this.props.navigator.setStyle({navBarTitleTextCentered: true});
// this.props.navigator.setTitle({
//   title: "Produit",

// });
}

onNavigatorEvent(event) { 
    if(event.id=='willAppear'){
        this.handleAppear();
    }
    if(event.id == 'back2'){
        //console.log(this.props.data.id);
        if(this.state.tabs == 1 && this.props.data.id){
            this.props.navigator.pop({
                animated: true,
                sharedElements: ['SharedTextId'+this.props.data.id],
            });
        }else{
            this.props.navigator.pop({
                animated: true,
            });
        }      
    } 
  }
  
getDispPrice(number){
    if(!isNaN(parseFloat(number))){
        if(number >= 1000){
            let ret='';
            let factor= parseInt(number/1000);
            ret+=factor
            ret+=' ';
            ret+=('000' + (number - (factor*1000))).slice(-3);
            ret+=' €';
            return ret;
        }else{
            let oret='';
            oret+=number;
            oret+=' €';
            return oret;
        }
    }
}

getSingleData(){
    let _this = this;
    let params={};
    params.token = this.props.auth.token;
    if(this.props.data !== undefined && this.props.data !== null){
        if(this.props.data.id !== undefined && this.props.data.id !== null){
            params.ad_id=this.props.data.id;
        }else if(this.props.data.ad_slug !== undefined && this.props.data.ad_slug !== null){
            params.ad_slug=this.props.data.ad_slug;
        }
        
    }
    let request={
        url:settings.endpoints.itemDetail,
        method:'POST',
        params:params
    }
    api.FetchData(request).then((result)=>{
        if(result.status){
             this.setState({load :false,Details:result.adDetails,UserDetail:result.item_user,radioArray:result.abuseCateList,fav:result.adDetails.havefave,
            region:{ latitude: result.adDetails.lat, longitude: result.adDetails.long, latitudeDelta: 0.0922, longitudeDelta: 0.0421,}
        });
        }else{
              this.setState({load :false});     
        }                     
    }).catch((error)=>{ 
        //console.log(error);
        _this.setState({load :false});   
    }); 
}
_shareTextMessage () {
    Share.share({
        title:this.state.Details.title, 
        message:this.state.Details.adurl
    })
    .then(this._showResult)
    .catch(err => console.log(err))
  }

  gotoMyProfile(){
    this.props.navigator.switchToTab({
        tabIndex:4
    })
    // this.props.navigator.push({
    //     screen: 'myAccount',
    //         passProps:{
    //             token:this.props.auth.token,
    //             userdata:this.props.auth.userdata,
    //             openProfile : 2
    //         },
    // });
}

handleChat(){
    if(!api.checkLogin(this.props.auth.token)){
        startMain('',{});
    }else if(this.props.auth.userdata !== undefined && this.props.auth.userdata !== null && this.props.auth.userdata.profile_complete == 0){
        this.gotoMyProfile();
    }else{
        let newData = {};
        //console.log(this.state);
       newData.send_from_id = this.state.UserDetail.user_id;
       newData.ad = this.state.Details.id;
       newData.token = this.props.auth.token;
       newData.name =  this.state.Details.user_name;
       this.props.chatActions.openChat(newData);
       this.props.navigator.push({
           screen: 'Singlechat',
           passProps:{
               data:newData
           }
       });
    }
}


goto(page){
    if(page == 'Singlechat'){
        this.handleChat();
    }else{
    this.props.navigator.push({
        screen: page,
        passProps:{
            data:this.state,
            page:page,
            token:this.props.auth.token,
            userdata:this.props.auth.userdata
        }
    });
}
}
onStarRatingPress(rating) {
    this.setState({
        starCount: rating
    });
}

tabchange(tab){
    if(tab==1 || tab == 2){
        this.setPlayer(false);
    }else if(tab==3){
        this.setPlayer(true);
    }
    this.setState({tabs:tab,tabSelected:tab});
}

setPlayer(bool){
    if(this.youTubeWeb !== undefined && this.youTubeWeb !== null){
        if(bool){
            //console.log(this.youTubeWeb);
            this.youTubeWeb.injectJavaScript("player.playVideo();");
            //let SRC ='<html><head></head><body style="margin: 0;"><iframe width="100%" height="100%" src="'+this.state.Details.video+'" frameborder="0" allowfullscreen></iframe></body></html>';

        }else{
            //console.log('pause video');
            this.youTubeWeb.injectJavaScript("player.pauseVideo();");
        }
    }
}
setModalVisible(visible) {
    this.setState({modalVisible: visible});
}

getRadio(data,da){
    // console.log(data);
    let opt = this.state.options;
    if(opt[data.option_name] === da) {
        opt[data.option_name] = '';
        this.setState({
            options:opt
        },()=>console.log(opt));
    } else {
        opt[data.option_name] = da;
        this.setState({
            options:opt
        },()=>console.log(opt));
    }
    
}
opeAction(){
    this.ActionSheets.show();
}
selectAction(i) {
    if(i == 0){
        Communications.phonecall(this.state.UserDetail.telephone, true);
    }else if(i==1){
        Communications.text(this.state.UserDetail.telephone,'');
    }
}

heartchange(){
    if(!api.checkLogin(this.props.auth.token)){
         this.props.navigator.push({
            screen: 'WelcomeContainer',
            passProps:{
                token:this.props.auth.token,
                userdata:this.props.auth.userdata,
            }           
         });
        return false;
    } 
    let _this = this;
    let request={
        url:settings.endpoints.favUnfavAd,
        method:'POST',
        params:{token:this.props.auth.token,id:this.state.Details.id}
    }
    if(this.state.fav == 1){ 
        this.setState({fav:0});
    }else{
        this.setState({fav:1});
    }
    api.FetchData(request).then((result)=>{
                         
    }).catch((error)=>{ 
        //console.log(error);
    });   
};

showDir(){
    let cLocation = this.props.auth.cLocation;
    const dirData = {
        source: {
            latitude: cLocation.latitude,
            longitude: cLocation.longitude,
        },
        destination: {
            latitude: this.state.Details.lat,
            longitude: this.state.Details.long,
        },
        params: [
            {
                key: "dirflg",
                value: "d"
            }
        ]
    }
    this.refs.GetDirections.exec(dirData); 
}

getDir() {
    if (this.props.auth.cLocation !== null) {
                this.showDir();
    }
    else{
        this.setState({gettingDirection:true},()=>{
            tryGetLocation(this.props.authActions).then((result)=>{
                //console.log(result);
                this.setState({gettingDirection:false},()=>{
                    this.showDir();
                });
            }).catch((err)=>{
                //console.log(err);
                this.setState({gettingDirection:false},()=>{
                    this.handleLocationError();
                });
            });
        });
    }
}
handleLocationError(){
    this.setState({gettingDirection:false},()=>{
        Alert.alert(
            'Information !',
            'Les données de localisation non disponibles',
            [
                {text: 'Ok', onPress: () => {
                   //this.showCurrent(false);
                }},
            ],
            { cancelable: false }
        )
    });
}

onModalClose() {
    this.setState({ min: false });
}

loadJS(){
    /* 
    var tag = document.createElement('script');\
    tag.src = 'https://www.youtube.com/iframe_api';\
          var firstScriptTag = document.getElementsByTagName('script')[0];\
          firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);\

           height: '300',\
              width: '200',\



              var tag = document.createElement('script');\
    tag.src = 'https://www.youtube.com/player_api';\
    var firstScriptTag = document.getElementsByTagName('script')[0];\
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);\
    
    */
    let vW = width;
    let vH = FORTAB ? 500 : 300;
    let videoId  = '';
    if(_.includes(this.state.Details.video, '.be')){
        videoId = this.state.Details.video.split('/').pop();
    } else {
        videoId = this.state.Details.video.replace("https://www.youtube.com/embed/","");
    }
   
    // console.log(videoId);
     //width:"+vW+",\
                //height:"+vH+",\
                //videoId: "+'"'+videoId+'"'+",\


    // onplayer state change event not required here
    /* 
    if (event.data == YT.PlayerState.PLAYING && !done) {\
              setTimeout(stopVideo, 6000);\
              done = true;\
    }\
    */
    let JS="var tag = document.createElement('script');\
    tag.src = 'https://www.youtube.com/player_api';\
    var firstScriptTag = document.getElementsByTagName('script')[0];\
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);\
    var player;\
          function onYouTubeIframeAPIReady() {\
            player = new YT.Player('player', {\
                width:"+vW+",\
                height:"+vH+",\
              videoId: "+'"'+videoId+'"'+",\
                events: {\
                'onReady': onPlayerReady,\
                'onStateChange': onPlayerStateChange\
              }\
            });\
          }\
          function onPlayerReady(event) {\
            event.target.playVideo();\
          }\
          var done = false;\
          function onPlayerStateChange(event) {\
          }\
          function stopVideo() {\
            player.stopVideo();\
          }\
          function resize(width,height) {\
            player.setSize(width,height);\
          }";
          return JS;
}

_renderFullModel(){
    return(
    <Modal onRequestClose={() => this.onModalClose()}
                        transparent={false} 
                        supportedOrientations={['portrait', 'portrait-upside-down', 'landscape', 'landscape-left', 'landscape-right']}
                        visible={this.state.min} style={StyleSheet.Modal} >
                        <View style={{flexDirection:'row',height:50,alignItems:'center',justifyContent:'center',position:'absolute',width:'100%',top:Platform.OS==='ios' ? 25 : 0,zIndex:999}}>
                            <TouchableOpacity style={{alignItems:'center',justifyContent:'center',marginLeft:10}} onPress={()=>this.onModalClose()}>
                                <Icon name="back" color={"#FFF"} size={22} />
                            </TouchableOpacity>
                            <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
                                <Text style={{color:"#FFF"}}>{this.state.sliderPos+1}{'/'}{this.state.Details.all_images.length}</Text>
                            </View>
                        </View>
                        <View style={{ flex: 1 }}>
                            <Gallery
                                style={{ flex: 1, backgroundColor: '#rgba(52, 52, 52, 0.8)' }}
                                initialPage={this.state.sliderPos}
                                pageMargin={10}
                                images={this.state.Details.all_images}
                                onGalleryStateChanged={(idle) => {
                                }}
                                onPageSelected={(page) => {
                                    this.setState({sliderPos:page});
                                    //this.setState({ page: (page + 1) });
                                }}
                            />
                        </View>
                    </Modal>
    );
}
_renderSharedElement(){
    return(
        <View style={this.state.tabs== 1 ? {position:'absolute',zIndex:-5}:{position:'absolute',height:0,width:0}}> 
        <SharedElementTransition
                        sharedElementId={'SharedTextId'+this.props.data.id}
                        showDuration={400}
                        hideDuration={300}
                        animateClipBounds={true}
                        showInterpolation={{ type: 'linear', easing: 'FastOutSlowIn' }}
                        hideInterpolation={{ type: 'linear', easing: 'FastOutSlowIn' }} >
                <Image style={[styles.sliderImage,{height:FORTAB ? 500 : 300 }]} 
                    source={{uri:this.state.image}} fadeDuration={0}/>
        </SharedElementTransition>
        </View>
    );
}

goPayment(){
    if(!api.checkLogin(this.props.auth.token)){
        this.props.navigator.push({
           screen: 'WelcomeContainer',
           passProps:{
               token:this.props.auth.token,
               userdata:this.props.auth.userdata,
           }           
        });
       return false;
    }
    this.setState({load1:true},()=>{
        let request={
            url:settings.endpoints.checkAdForBuy,
            method:'POST',
            params:{token:this.props.auth.token,ad_id:this.props.data.id != '' ? this.props.data.id:'',qty:this.state.quantity,ad_detail:this.state.options}
        }
        api.FetchData(request).then((result)=>{
            if(result.status){
                this.setState({load1:false},()=>{
                    if(result.paypal){
                       this.payWithPaypal(result.amount,result.data.data);
                    }else{
                        this.props.navigator.push({
                            screen: 'success',
                            passProps:{
                                status:'Y',
                                message:result.message,
                                token:this.props.auth.token,
                                userdata:this.props.auth.userdata
                            }
                        });
                    }                
                });
            }else{
                this.setState({load1:false},()=>{
                    alert(result.message);
                });
            }       
        }).catch((error)=>{
            console.log(error);
        });
    });
    
}
payWithPaypal(amounts,datas){
    this.setState({totalOfDir:amounts},()=>{
        let total = amounts;
        if(total != '' && total != 0){
            let price = parseFloat(total).toFixed(2);
            let params = {
                price: price,
                currency: 'EUR',
                description: 'Purchase Ad',
                custom:'paypal_userbuyad'
            };
            //console.log(""+price);return false;
            PayPalPayment(params).then(confirm => {
                if(confirm.response.state == 'approved'){
                    this.goSuccess(confirm,datas);           
                }        
            }).catch(error => console.log(error));        
        }
    });
    
}
goSuccess(confirm,data){
    this.setState({load:true,modalVisible:false},()=>{
        let request={
            url:settings.endpoints.buyAdTransaction,
            method:'POST',
            params:{token:this.props.auth.token,ad_id:this.props.data.id != '' ? this.props.data.id:'',qty:this.state.quantity,ad_detail:data,paypal_id:confirm.response.id}
        }
        api.FetchData(request).then((result)=>{
            if(result.status){
                this.setState({load1:false},()=>{
                    this.props.navigator.push({
                        screen: 'success',
                        passProps:{
                            status:'Y',
                            message:result.message,
                            token:this.props.auth.token,
                            userdata:this.props.auth.userdata
                        }
                    });
                });
            }else{
                this.setState({load:false},()=>{
                    alert('Quelque chose s\'est mal passé dans le paiement.')
                });
            } 
        }).catch((error)=>{
            //console.log(error);
            this.setState({load:false},()=>{
                alert('Quelque chose s\'est mal passé dans le paiement.')
            });
        });
    });
}

_renderBottom(){
    const { Details } = this.state;
    return(
        <View style={{flexDirection:'row',padding:10,borderColor:'#d6d6d6',
        borderWidth:1,
        backgroundColor:'#fff'}}>

        <TouchableOpacity style={{width:50,alignItems:'center',justifyContent:'center'}}
        activeOpacity={0.8} onPress={()=>{this.genModal()}}>
            <Text style={[styles.globalIcon,styles._F26_,styles.grayColor,{textAlign:'center'}]}>9</Text>
        </TouchableOpacity>

        <TouchableOpacity style={{width:50,alignItems:'center',justifyContent:'center'}}
        onPress={()=>{this.goto('Singlechat')}} activeOpacity={0.8}>
        <View style={{height:30,alignItems:'center',justifyContent:'center'}}>
            <Text style={[styles.globalIcon,styles._F26_,styles.grayColor,{textAlign:'center'}]}>f</Text>
            <View style={[styles.circle, this.state.Details.userOnline == 1  ? styles.online : null,{position:'absolute',right:0,top:0}]}></View>
            </View>
        </TouchableOpacity>
        {Details.is_phone_hide === 0 ? (
        <TouchableOpacity style={{width:50,alignItems:'center',justifyContent:'center'}}
        onPress={() =>{
           if(Details.telephone !== '') {
            this.opeAction()
           } 
        }} activeOpacity={0.8}>
            <Text style={[styles.globalIcon,styles._F26_,Details.telephone !== '' ? styles.grayColor : styles.disableColor,{textAlign:'center'}]}>*</Text>
        </TouchableOpacity> ) : (null)}
    
        
        <View style={{flex:1}}></View>
        {this.state.Details.is_direct_sale == 1 ? (
        <TouchableOpacity style={[{backgroundColor:'#f15a23',
        borderRadius:3,width:130,borderRadius:5,alignItems:'center',justifyContent:'center'}]}
        activeOpacity={0.8} onPress={() => {this.setModalVisible(typeof this.state.Details.direct_sale_data != 'undefined' &&this.state.Details.direct_sale_data.quantity == 0 ? false :true)}}>
            <Text 
            style={[{color:'#fff',fontSize:FORTAB ? 16:12,fontFamily:'Montserrat',borderColor:'#eee'}]}>
            {typeof this.state.Details.direct_sale_data != 'undefined' &&this.state.Details.direct_sale_data.quantity == 0 ? 'Épuisé' :'Ajouter au panier' }
            
            </Text>
        </TouchableOpacity>):(null)}

    </View>
    );
}


_renderGetDirections(){
    return(
        <Modal visible={this.state.gettingDirection} onRequestClose={() => {}}
        transparent={true} style={StyleSheet.Modal} >
    <View style={{flex:1,alignItems:'center',justifyContent:'center',backgroundColor:"#0008"}}>
        <View style={{backgroundColor:"#fff", alignItems:'center',justifyContent:'center',flexDirection:'row',padding:15,borderRadius:10}}>
            <View style={{padding:5}}>
                <ActivityIndicator size="large" animating={true} color={colors.navBarButtonColor}/>
            </View>
            <View style={{padding:5,maxWidth:150}}>
                <Text>{'Recherche ma position actuelle ...'}</Text>
            </View>
        </View>
    </View></Modal>);
}

onCalenderClose = () => {
    this.setState({
        calenderModal : false
    });
}

changeData = (monthData) => {
    let setData = [];
    let getData = this.getPackLocationData(true);
    if(_.isArray(getData) && !_.isEmpty(getData)){
        getData.map(function (item,key){
            if(item.month_array.indexOf(monthData.month.toString()) >= 0 && item.year_array.indexOf(monthData.year.toString()) >= 0) {
                setData.push(item);
            }
        });
    }
    setTimeout(() => {
        this.setState({setData});
    }, 500);
    
}

getPackLocationData = (flag) => {
    const { Details } = this.state;
    let object = {};
    if(_.isObject(Details)) {
        if(_.isArray(Details.attr_data) && !_.isEmpty(Details.attr_data)) {
            Details.attr_data.map(function (item,key){
                if(item.attr === 'Pack Location') {
                    if(flag) {
                        object =  item.date_array;
                    } else {
                        object =  item.disable_date_array;
                    }
                }
            });
        }
    }
    return object;
}


_renderCalender = () => {
    var CurrentDate = moment().format('YYYY-MM-DD');
    let data = this.getPackLocationData(false);
    const newHeight =Dimensions.get('window').height;
    const { setData } = this.state;
    // console.log(setData);
    return(
        <Modal
            onRequestClose={() => this.onCalenderClose()}
            transparent={false} 
            supportedOrientations={['portrait', 'portrait-upside-down', 'landscape', 'landscape-left', 'landscape-right']}
            visible={this.state.calenderModal} style={StyleSheet.Modal} 
        >
            <View style={{flex:1}}>
                <View style={{flexDirection:'row',height:50,backgroundColor:"#fff",alignItems:'center',justifyContent:'center'}}>
                    <TouchableOpacity style={{alignItems:'center',justifyContent:'center',padding:5}} onPress={()=>this.onCalenderClose()}>
                        <Icon name="back" color={"#FD5009"} size={16} />
                    </TouchableOpacity>
                    <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
                        <Text style={{color:"#FD5009",fontSize:14}}>{'DISPONIBILITÉS ET TARIFS'}</Text>
                    </View>
                </View>
                <CalendarList
                    current={CurrentDate}
                    pastScrollRange={24}
                    futureScrollRange={24}
                    horizontal
                    pagingEnabled
                    style={{borderBottomWidth: 1, borderBottomColor: 'black',height:newHeight-250}}
                    onVisibleMonthsChange={(months) => {
                        let monthData = months[0];
                        // this.setState({
                        //     quantity:1
                        // });
                        this.changeData(monthData);
                    }}
                    markedDates={data}
                />
                { setData.length > 0 ?
                
                <View style={{borderColor:'#000',borderWidth:1}}>
                    <View style={{flexDirection:'row',justifyContent:'space-between',alignContent:'center',padding:5}}>
                        <View style={{ flex: 1, padding: 5, backgroundColor: '#eee', alignItems: 'center', margin: 1 }}><Text style={{ color: '#000' }}>Période</Text></View>
                        <View style={{ flex: 1, padding: 5, backgroundColor: '#eee',  alignItems: 'center', margin: 1 }}><Text style={{ color: '#000' }}>Prix</Text></View>
                    </View>
                   { setData.length > 0 ?
                        setData.map(function (item) {
                            return (
                                <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                                    <View style={{ flex: 1, padding: 5, borderWidth: 1, borderColor: '#eee', margin: 1 , alignItems: 'center'}}>
                                        <Text>Du {item.start_sting} au {item.end_strind}</Text>
                                    </View>
                                    <View style={{ flex: 1, padding: 5, borderWidth: 1, borderColor: '#eee', margin: 1, alignItems: 'center' }}>
                                    <Text>{item.price}</Text>
                                    </View>
                                </View>
                            )
                        }) : (null)
                    }
                </View>
                :(null)}
            </View>
        </Modal>
    )
}

openCalenderModal = () => {
    this.setState({
        calenderModal : true
    })
}

render() {
        let _this = this;
        let CatData = typeof this.state.Details.attr_data != 'undefined' && this.state.Details.attr_data.map(function (item,key) {
                if(item.attr === 'Pack Location') {
                    return ( <TouchableOpacity style={[styles.inlineDatarow,styles.spaceBitw,styles.prodataRow]} key={key}>
                        <Text style={styles.detailType}>{item.attr}</Text>
                        <TouchableOpacity onPress={()=>{
                            _this.openCalenderModal()
                        }}><Text style={styles.detailTypeValue}>{'Vérifier les dates'}</Text></TouchableOpacity>
                    </TouchableOpacity>)
                } else {
                    return ( <View style={[styles.inlineDatarow,styles.spaceBitw,styles.prodataRow]} key={key}>
                        <Text style={styles.detailType}>{item.attr}</Text>
                        <Text style={styles.detailTypeValue}>{item.value}</Text>
                    </View>)
                }
        });
      let shareOptions = {
            title:this.state.Details.title,
            message: this.state.Details.title,
            url: this.state.Details.adurl,
            subject: this.state.Details.title //  for email
       };
      var Spinner = require('rn-spinner');
        let directSale = {}      
        directSale = (this.state.Details.direct_sale_data);
        //console.log(directSale);
        
        let Datat = null;
        
        if(typeof directSale != 'undefined'){
            Datat =  directSale.diff_options.map((data,i)=>{
                return(
                    <View style={styles.varientBlock} key={i}>
                        <Text style={styles.blockTitle}>{data.option_name}</Text>
                            <View style={[styles.inlineDatarow,styles._MLR5D_]}>                      
                                {data.option_values.map((da,j)=>{ return(
                                        <TouchableOpacity style={styles.varientButton} key={j} activeOpacity={0.8}>
                                        <RadioButton currentValue={this.state.options[data.option_name]} value={da} 
                                        onPress={()=>{_this.getRadio(data,da)}}>
                                                <Text style={[styles.variText]}>{directSale.diff_options[i]['option_values'][j]}</Text>
                                            </RadioButton>
                                        </TouchableOpacity>
                                    )
                                })
                            }
                            </View>
                    </View>
                );
            });
        }
        let price = showPrice = total = shippingPriceShow = '' ;
        let shippingPrice = '';
        let getT = 0;
        if(typeof directSale != 'undefined'){
            if(directSale.shipping_type  == 'Frais_port_simple'){
                price = typeof directSale != 'undefined' ? directSale.price : '';
                showPrice = (this.state.quantity*price)+' €';
                shippingPrice =  typeof directSale != 'undefined' ? (this.state.quantity)*(directSale.shipping_price) : '';
                shippingPriceShow = shippingPrice+' €';
                total = (this.state.quantity*price)+shippingPrice+' €';
                getT = (this.state.quantity*price)+shippingPrice;
            }else if(directSale.shipping_type  == 'Frais_port_variable'){
                if(directSale.variable_prices != 'undefined'){
                    for (var i = 0, len = directSale.variable_prices.length; i < len; i++) {
                        if(this.state.quantity >= directSale.variable_prices[i].min && directSale.variable_prices[i].max >=this.state.quantity){
                            shippingPrice = directSale.variable_prices[i].value;
                        }
                    }
                    price = typeof directSale != 'undefined' ? directSale.price : '';
                    showPrice = (this.state.quantity*price)+' €';
                    shippingPriceShow = shippingPrice+' €';
                    total = (this.state.quantity*price)+shippingPrice+' €';
                    getT = (this.state.quantity*price)+shippingPrice;
                }
            }
        }
        let popup =  <Modal style={styles.modalApear} animationType={"slide"} transparent={true} visible={this.state.modalVisible} onRequestClose={() => {}}>
                        <View style={styles.modalInner}>
                            <View style={styles.modalWrap}>
                                <Text style={[styles.globalIcon,styles.close]} onPress={() => {this.setModalVisible(false)}}>g</Text>
                                <View style={styles.boxApear}>
                                  {Datat != null ? Datat :null}
                                    <View style={styles.varientBlock}>
                                        <View style={styles.dataRow}>
                                            <View style={styles.inlineDatarow}>
                                                <Text style={[styles.boldLabel,styles._MR15_]}>Quantité</Text>
                                                <Spinner max={typeof directSale != 'undefined' ? directSale.quantity : 0} min={ typeof directSale != 'undefined' && directSale.quantity == 0 ? 0 :1} default={ typeof directSale != 'undefined' && directSale.quantity == 0 ? 0 :1} maxStars={4}
                                                color="#f60" numColor="#f60" onNumChange={(num)=>{this.setState({quantity:num}) }}
                                                />
                                            </View>                                            
                                        </View>
                                        <View style={styles.dataRow}>
                                                <Text style={[styles.boldLabel,styles._MR10_]}>Sous-total</Text>
                                                <Text>{showPrice}</Text>
                                        </View>
                                        <View style={[styles.dataRow,styles._PB5_]}>
                                            <Text style={styles.boldLabel}>Frais d'envoi pour la France</Text>
                                            <Text>{shippingPriceShow} </Text>
                                        </View>
                                        <View style={[styles.dataRow,styles._PB5_]}>
                                            <Text style={styles.boldLabel}>COÛT TOTAL</Text>
                                            <Text>{total}</Text>
                                        </View>
                                    </View>
                                   
                                    <TouchableOpacity style={[styles.yellow,{height:40}]} onPress={() => {this.state.load1 ? null:this.goPayment()}} activeOpacity={0.8}>
                                    {this.state.load1 ? <ActivityIndicator animating={true} size='large' color={colors.navBarButtonColor}/>:
                                     typeof directSale != 'undefined' && directSale.quantity == 0 ?<Text style={[styles.genButton,styles._F18_]}><Text style={styles.blackColor}>Épuisé</Text></Text>:<Text style={[styles.genButton,styles._F16_]}><Image style={styles.paypalImg} source={images.paypal} /><Text style={styles.blackColor}>Passer à la caisse</Text></Text>
                                    }
                                    </TouchableOpacity>  
                                </View>
                            </View>
                        </View>
                  </Modal>
    let type;
    if(this.state.tabs == 1){
        type= <View style={{ 
        flexDirection:'row',
        overflow:'visible',
        height:FORTAB ? 500 : 300,
        }}>
                    {this.state.Details.all_images !== undefined ? (
                        <View>
                                <ImageSlider style={{width: this.state.width}} height = {FORTAB ? 500 : 300}
                                ref={o=>this.ImageSlider = o}
                                images={this.state.Details.all_images}
                                position={this.state.sliderPos}
                                onPositionChanged={(position) => {this.setState({sliderPos:position})}}
                                onPress={(obj)=>{this.setState({min:true})}}/>
                                </View>):(null)}
                        </View>;
    }else if(this.state.tabs == 2){
        type= <View style={[styles.mapWrap,{width:this.state.width}]}>
                <MapView 
                style={[styles.map,{width:this.state.width}]} 
                region={this.state.region} 
                onPress={()=>this.getDir()}>
                    <MapView.Marker 
                    coordinate={{latitude:this.state.region.latitude,longitude:this.state.region.longitude}} 
                    title={this.state.Details.title}
                    description={this.state.Details.cityzip} 
                    onCalloutPress={()=>this.getDir()} />
                </MapView>
        </View>
    }else{
        type= null;
    }

    let loadJS = '';
    let SRC = '';
    const { Details } = this.state;
    if(this.state.Details.video !== undefined){
        loadJS= this.loadJS();
        SRC= '<!DOCTYPE html><html><body style="margin: 0;"><div id="player"></div><script>'+loadJS+'</script></body></html>';
        // console.log(SRC);
        //SRC ='<html><head></head><body style="margin: 0;"><iframe width="100%" height="100%" src="'+this.state.Details.video+'" frameborder="0" allowfullscreen></iframe><script>'+loadJS+'</script></body></html>';

        // <html><head></head><body style="margin: 0;">
        // </body></html>
        // <script>'+loadJS+'</script>
    }
    return (
        <Animated.View style={[styles.main,styles._PT0_,{
            backgroundColor:"#fff"
        }]}>
            <ScrollView  contentContainerStyle={{flexGrow: 1,alignSelf:'stretch',
        backgroundColor:'#f8f8f8',}} showsVerticalScrollIndicator={false}>  
                <View style={styles.scrollInner}>
                        <View style={styles.row}>
                             {/* <ScrollView style={styles.tabPortCaption}
                                  ref={(scrollView) => { _scrollView = scrollView; }}
                                  automaticallyAdjustContentInsets={false}
                                  showsHorizontalScrollIndicator={false}
                                  contentContainerStyle={styles.Carousel}
                                  horizontal> */}
                                  {this.props.data.id ? this._renderSharedElement():null}
                                  {/* {this._renderSharedElement()} */}
                                  {type}                
                                  {/* <View style={[this.state.tabs == 3 ? styles.videoHolder:{position:'absolute',width:0,height:0}]}>
                                     <YouTube
                                      ref={o => this.youVideo =o}
                                         apiKey={oauthconfig.youtubeKey}
                                         videoId={this.state.Details.video.replace("https://www.youtube.com/embed/","")}   // The YouTube video ID
                                         play={true}             // control playback of video with true/false
                                         fullscreen={false}       // control whether the video should play in fullscreen or inline
                                         loop={true}
                                         hidden={this.state.tabs != 3}           // control whether the video should loop when ended
                                         style={styles.youVideo} 
                                         onReady={e => console.log(e)}
                                         onChangeState={e =>  console.log(e)}
                                         onChangeQuality={e =>  console.log(e)}
                                         onError={e =>  console.log(e)}
                                         />):(null)}
                                         </View> */}
                                      {typeof this.state.Details != "undefined" && typeof this.state.Details.video != "undefined" && this.state.Details.video != "" ?(
                                        <View style={[this.state.tabs == 3 ? styles.videoHolder:{position:'absolute',width:0,height:0}]}>
                                      <WebView ref={o=>this.youTubeWeb = o} 
                                          style={{width:this.state.width,height: FORTAB ? 500 : 300}} 
                                          source={{html:SRC}} 
                                          javaScriptEnabled={true}
                                          //injectedJavaScript={loadJS}
                                          /></View>):(null)}
                              {/* </ScrollView> */}
                            {this.state.load ? 
                            <View style={[styles.CenterIndi]} ><ActivityIndicator color='#FF4500' animating={this.state.load} style={[styles.loader,{marginTop:30}]} size="large"/></View>:
                            <View style={[styles.productCaption,styles._HP15_,{width:this.state.width}]}>
                                <View style={[styles.dataRow]}>
                                    <View style={{width:this.state.width/1.8}}>
                                        <Text style={[styles.productName,styles.large]}>{this.state.Details.title}</Text>
                                        {/* {this.state.Details.cityzip ? 
                                        (<Text style={{fontSize:15,color:colors.navBarButtonColor,textDecorationLine: 'underline'}} onPress={()=>this.getDir()}>{this.state.Details.cityzip}</Text>):(null)} */}
                                    </View>
                                    <View style={[styles.inlineDatarow,{width:this.state.width/2.2,justifyContent:'center'}]}>
                                        <TouchableOpacity onPress={()=>{this.tabchange(1)}} style={styles.inlineIcoBox} activeOpacity={0.8}>
                                            <Text style={(this.state.tabSelected== 1)?[styles.globalIcon,styles._F26_,styles.orangeColor]:[styles.globalIcon,styles._F26_]}>{'['}</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={()=>{this.tabchange(2)}} style={styles.inlineIcoBox} activeOpacity={0.8}>
                                            <Text style={(this.state.tabSelected== 2)?[styles.globalIcon,styles._F26_,styles.orangeColor]:[styles.globalIcon,styles._F26_]}>{'^'}</Text>
                                        </TouchableOpacity>
                                        {typeof this.state.Details != "undefined" && typeof this.state.Details.video != "undefined" && this.state.Details.video != "" ? (
                                        <TouchableOpacity onPress={()=>{this.tabchange(3)}} style={styles.inlineIcoBox} activeOpacity={0.8}>
                                            <Text style={(this.state.tabSelected== 3)?[styles.globalIcon,styles._F26_,styles.orangeColor]:[styles.globalIcon,styles._F26_]}>{'%'}</Text>
                                        </TouchableOpacity>):(null)}
                                    </View>
                                    {/*<Text style={[styles.orangeColor,styles.bold]}>12,550 €</Text>*/}
                                </View>
                                <View style={[styles.dataRow]}>
                                    <Text style={[styles.orangeColor,styles.bold,styles._F18_]}>{(this.state.Details.price)}</Text>
                                    <Text style={[styles.blueColor,styles.gendes,styles.autodicoTxt]}> {this.state.Details.post_on}</Text>
                                </View>
                                <View style={[styles.dataRow,styles.Valign,{width:this.state.width}]}>
                                <View style={[styles.inlineDatarow,{width:this.state.width/1.78}]}>
                                     {this.state.Details.is_urgent == 1 && 
                                        <Text style={[styles.inlineWithico,styles.red,styles.small,styles._MPR5_]}> <Text style={[styles.globalIcon,styles.inlineWithico,styles.red]}>y</Text> Urgent</Text>}
                                        {this.state.Details.is_direct_sale == 1 && this.state.Details.is_urgent == 1 ?<Text style={styles.borderRight}></Text> :null}
                                        {this.state.Details.is_direct_sale == 1 && <Text style={[styles.inlineWithico,styles.red,styles.small,styles._MPR0_]}><Text style={[styles.globalIcon,styles.inlineWithico,styles.red]}> w</Text>  Achat Ditect</Text>}
                                    </View>
                                    <View style={[styles.inlineDatarow,{width:this.state.width/2.2,justifyContent:'center'}]}>
                                            <TouchableOpacity onPress={()=>this.heartchange()} activeOpacity={0.8} style={styles.inlineIcoBox}>
                                                <Text style={[styles.globalIcon,styles.orangeColor,styles._F26_]}>
                                                     {this.state.fav == 1 ? 'n':'m'}
                                                </Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={this._shareTextMessage.bind(this)} activeOpacity={0.8} style={styles.inlineIcoBox}>
                                                <Text style={[styles.globalIcon,styles.orangeColor,{fontSize:25,marginRight:9.5}]}>{'{'}</Text>
                                            </TouchableOpacity>
                                            {this.props.auth.userdata.id == this.state.Details.user_id ? (
                                            <TouchableOpacity onPress={()=>{this.goto('Managead')}} activeOpacity={0.8} style={styles.inlineIcoBox}>
                                                <Text style={[styles.globalIcon,styles._MR5_,styles.orangeColor,{fontSize:22,marginRight:9.5}]}>{'!'}</Text>
                                            </TouchableOpacity>):(
                                            <TouchableOpacity onPress={()=>{this.goto('Report')}} activeOpacity={0.8}>
                                                <Text style={[styles.globalIcon,styles._MR5_,styles.orangeColor,{fontSize:25,marginRight:9.5}]}>{'@'}</Text>
                                            </TouchableOpacity>)}
                                     </View>
                                        {/*<StarRating
                                            disabled={false}
                                            maxStars={5}
                                            rating={this.state.starCount}
                                            selectedStar={(rating) => this.onStarRatingPress(rating)}
                                        />*/}
                                </View>
                                <View style={[styles.proDetail,styles._MT10_]}>
                                  
                                    <View style={[styles.inlineDatarow,styles.spaceBitw,styles.prodataRow]}>
                                        <Text style={styles.detailType}>Catégorie</Text>
                                        <Text style={styles.detailTypeValue}>{this.state.Details.categoryname}</Text>
                                    </View>                                    
                                    {CatData}
                                    {
                                        this.state.Details.cityzip !== undefined && this.state.Details.cityzip !== '' ?  <View style={[styles.inlineDatarow,styles.spaceBitw,styles.prodataRow]}>
                                        <Text style={styles.detailType}>Lieu</Text>
                                        <TouchableOpacity onPress={()=>{this.tabchange(2)}}><Text style={[styles.detailTypeValue,{color:'#2E89Ed',fontWeight:'bold'}]}>{this.state.Details.cityzip}</Text></TouchableOpacity>
                                    </View> : null
                                    }                                   
                                </View>
                                <View style={[styles._MT5_,styles.prodataRow]}>
                                  <Text style={[styles.blockTitle]}>Description</Text>
                                  {
                                            Details.ad_type === 'trocs' ? 
                                            (<View style={{justifyContent:'center',flex:1}}>
                                            <View style={{flexDirection:'column'}}>
                                                <View style={{flexDirection:'row'}}>
                                                    <FaIcon name="mail-forward" color={'#34c24c'} size={18} style={{marginRight:5}}/>
                                                    <Text style={{color:'#34c24c',marginLeft:3}}>Echange</Text>
                                                    <Text style={[{marginLeft:5,color:'#666',fontWeight:'500'}]}>{Details.title}</Text>
                                                </View>
                                                <Text style={{marginTop:5}}>{Details.description}</Text>
                                            </View>
                                            <View style={{borderColor:'#eee',borderWidth:1,marginTop:10}}></View>
                                            <View style={{flexDirection:'column',marginTop:10}}>
                                                <View style={{flexDirection:'row'}}>
                                                    <FaIcon name="reply" color={'#2e5f9b'} size={18} style={{marginRight:5}}/>
                                                    <Text style={{color:'#2e5f9b',marginLeft:3}}>Contre</Text>
                                                    <Text style={[{marginLeft:5,color:'#666',fontWeight:'500'}]}>{Details.title_contre}</Text>
                                                </View>
                                                <Text style={{marginTop:5}}>{Details.description_contre}</Text>
                                            </View>
                                        </View>) 
                                            : 
                                            ( <Text style={styles.gendes}>{this.state.Details.description}</Text>)
                                        }
                                 
                                </View>
                               
                                <View style={styles._MT15_}>
                                  <View style={[styles.inlineDatarow,styles._MT10_]}>
                                      <Image style={{height:30,width:30,marginRight:5,resizeMode:'cover'}} source={{uri:this.state.UserDetail.image}} />
                                      <View><Text style={styles.blockTitle}>{this.state.UserDetail.name}</Text></View>
                                  </View>
                                  {this.state.UserDetail.have_shop == 1 &&
                                  <TouchableOpacity activeOpacity={0.8} onPress={()=>{this.goto('UserAnnonces')}}  style={[styles.inlineDatarow,styles._MT10_]}>
                                   
                                      <Text style={[styles.globalIcon,styles._PR10_,styles.large]}>~</Text>
                                      <Text style={[styles.blueColor,styles.gendes,styles.autodicoTxt,{color:'#2E89Ed',fontWeight:'bold'}]}>Voir toutes ses annonces</Text>
                                  </TouchableOpacity>
                                  }
                                  <View style={[styles.inlineDatarow,styles._MT10_]}>
                                      <Text style={[styles.globalIcon,styles._PR10_,styles.large]}>:</Text>
                                      <Text style={[styles.blueColor,styles.gendes,styles.autodicoTxt]}>Inscrit : { moment(this.state.Details.user_reg_on).format("DD MMM [|] HH:MM")}</Text>
                                  </View>
                                   {this.state.UserDetail.have_shop == 1 &&
                                    <View style={[styles.inlineDatarow,styles._MT10_]}>
                                    {this.state.Details.ad_user_type == 'professional'&&
                                      <Image  source={images.pro} style={styles._MR5_}/>
                                    }
                                    <Text style={[styles.gendes,styles.autodicoTxt]}>{ typeof this.state.UserDetail.siret != 'undefined' && this.state.Details.ad_user_type == 'professional'&& this.state.UserDetail.siret != '' && this.state.UserDetail.siret !== null ? "N° SIRET:  "+this.state.UserDetail.siret:''}</Text>
                                </View>
                                   }
                                </View>
                                {/* <View style={styles.addHolder}>
                                    <Image style={[styles.addImg]} source={images.add} />
                                </View> */}
                                {/*<Accordion
                                    sections={SECTIONS}
                                    renderHeader={this._renderHeader}
                                    renderContent={this._renderContent} 
                                    content={test}
                                />   */}
                                {typeof this.state.Details != 'undefined' && typeof this.state.Details.all_images != 'undefined'  && this._renderFullModel()}
                            </View>  
                            }                      
                            {popup}
                            {this._renderGetDirections()}
                        </View>   
                </View>
                        { this._renderCalender()}
            </ScrollView>
            {this.props.auth.userdata.id !== undefined && this.props.auth.userdata.id == this.state.UserDetail.user_id ? (null) : (
            this.state.load ? null : this._renderBottom() 
            )}
            <ActionSheet
                    ref={o => this.ActionSheets = o}
                    title={this.state.UserDetail.telephone !== '' ? this.state.UserDetail.telephone : '' }
                    options={this.ActionSheetOptions.options}
                    cancelButtonIndex={this.ActionSheetOptions.CANCEL_INDEX}
                    destructiveButtonIndex={this.ActionSheetOptions.DESTRUCTIVE_INDEX}
                    onPress={(i)=>{this.selectAction(i)}}/>
            <GetDirections
                ref="GetDirections"
            />
        </Animated.View>
    );
  }

}

function mapStateToProps(state, ownProps) {
	return {
        chat:state.chat,
        auth:state.auth,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		chatActions: bindActionCreators(chatActions, dispatch),
		authActions: bindActionCreators(authActions, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductDetail);