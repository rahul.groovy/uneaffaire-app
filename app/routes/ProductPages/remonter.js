
import React, { Component } from 'react';
import {
  StyleSheet,
  Image,Text,View,TextInput,TouchableOpacity,ScrollView,Switch,Dimensions
} from 'react-native';
import images from '../../config/images';
import styles from '../../config/genStyle';
import {Column as Col, Row} from 'react-native-flexbox-grid';
import { iconsMap, iconsLoaded } from '../../config/icons';
// redux specific
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as chatActions from '../../redux/reducers/chat';
import * as authActions from '../../redux/reducers/auth';
const { height, width } = Dimensions.get('window');

class remonter extends Component {
//   static navigatorButtons = {
//      leftButtons: [{
//        icon: images.back,
//        id: 'back',
//        title:'back to welcome',
//      }],
//   };
// static navigatorStyle = {
//     navBarBackgroundColor: colors.navBarBackgroundColor,
//     navBarTextColor: colors.navBarTextColor,
//     navBarSubtitleTextColor: colors.navBarSubtitleTextColor,
//     navBarButtonColor: colors.navBarButtonColor,
//     statusBarTextColorScheme: colors.statusBarTextColorScheme,
//     statusBarColor: colors.statusBarColor,
//     tabBarBackgroundColor: colors.tabBarBackgroundColor,
//     tabBarButtonColor: colors.tabBarButtonColor,
//     tabBarSelectedButtonColor: colors.tabBarSelectedButtonColor,
//     navBarSubtitleColor: colors.navBarSubtitleColor,
// };
  
  constructor(props) {
    super(props);
    //console.log(this.props);
    this.state={
        check1:false,
        tabs:1,
        tabSelected:1,
        trueSwitchIsOn: true,
        falseSwitchIsOn: false,
        value: 0,
    };
    // if you want to listen on navigator events, set this up
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }

  componentWillMount(){
    iconsLoaded.then(()=>{});
}


  onNavigatorEvent(event) {
    //console.log(event)
    if(event.id=='willAppear'){
        this.props.chatActions.setScreen(this.props.testID);
        //console.log(this.props);
    this.props.navigator.toggleNavBar({
      to: 'shown',
      animated: true
    });
    this.props.navigator.toggleTabs({
        animated: false,
        to: 'hidden'
    });
    this.props.navigator.setStyle({navBarTitleTextCentered: true});
    this.props.navigator.setTitle({title: "Remonter en tête de liste"});
     this.props.navigator.setButtons({
         leftButtons: [{
        icon: iconsMap['back'],
        id: 'back2',
        title:'back to welcome',
      }],animated:false})     
    }
    if(event.id=='didAppear'){

    }

    if(event.id == 'back2'){
      this.props.navigator.pop({
        animated: true // does the pop have transition animation or does it happen immediately (optional)
      });
    }
  }

  handleOnPress(value){
        this.setState({value:value})
 }

  goto(page){
    this.props.navigator.push({
      screen: page
    });

  }
  tabchange(tab){
        this.setState({tabs:tab,tabSelected:tab});
  }
  render() {
    return (
        <ScrollView showsVerticalScrollIndicator={false} style={[styles.mainScollview]}>
            <View style={styles.scrollInner}>
                <View style={[styles.main,styles._HP15_]}>
                        <Text style={styles.blockTitle}>{'Remonter en tête de liste immédiatement'}</Text>
                        <View style={styles.proMain}>
                     <Row size={12} style={[styles.proRow,styles.prolistRow,styles._PT0_,styles.Prolistgray]}>
                        <Col sm={4} md={4} lg={4} style={[styles.proCol,styles.listCol]} >
                            <View style={styles.imageCanvas}>
                                <View style={styles.imgInner}>
                                    <Image style={styles.hasVideo} source={images.video} />
                                    <Image style={styles.proImg} source={images.IMG10} />
                               </View>
                            </View>
                            <View style={[styles.bottomDetail,styles.bottomCaption]}>
                                <Text style={[styles.leftAlign,styles.white]}>
                                    <Text style={styles.small}>19 Dec | 09h09</Text>
                                </Text>
                                <View style={[styles.rightAlign]}>
                                    <View style={styles.relative}><Text style={[styles.globalIcon,styles.camIco,styles.white,styles.camIcoAbs]}> s </Text><Text style={[styles.bold,styles.abscamtxt,styles.white,styles.small]}>3</Text></View> 
                                </View>
                            </View>
                        </Col>
                        <Col sm={8} md={8} lg={8} style={[styles.proCol,styles.listCol]}>
                            <View style={styles.proCaption}>
                                <Text style={[styles.proName,styles.big]} numberOfLines={1}>3008 familiale grise à restaurer</Text>
                                <View style={styles.row}>
                                    <View style={[styles.inlineDatarow,styles.spaceBitw,styles._MT5_]}>
                                        <Text style={[styles.orangeColor,styles.bold]}>12,550 €</Text> 
                                    </View>
                                    <View style={[styles.inlineDatarow,styles._MT10_,styles._PB15_]}>
                                        <Text style={[styles.inlineWithico,styles.borderRight,styles.red,styles.small]}>Urgent<Text style={styles.globalIcon}> x</Text></Text>
                                        <Text style={[styles.inlineWithico,styles.red,styles.small]}>Achat Ditect<Text style={styles.globalIcon}> w</Text></Text>
                                    </View>
                                    <View style={[styles.inlineDatarow,styles.spaceBitw]}>
                                        <Text style={styles.small}>Sport {'&'} Hobbies <Image style={styles.proSign} source={images.pro} /> {'\n'} St Martin du Mont / Saone et loire</Text>
                                        <Text style={[styles.globalIcon,styles.deleteRemonterlist]}>8</Text>
                                    </View>  
                                </View>
                            </View>
                        </Col>
                    </Row> 
                      <Row size={12} style={[styles.proRow,styles.prolistRow,styles._PT0_,styles.Prolistgray]}>
                        <Col sm={4} md={4} lg={4} style={[styles.proCol,styles.listCol]} >
                            <View style={styles.imageCanvas}>
                                <View style={styles.imgInner}>
                                    <Image style={styles.hasVideo} source={images.video} />
                                    <Image style={styles.proImg} source={images.IMG10} />
                               </View>
                            </View>
                            <View style={[styles.bottomDetail,styles.bottomCaption]}>
                                <Text style={[styles.leftAlign,styles.white]}>
                                    <Text style={styles.small}>19 Dec | 09h09</Text>
                                </Text>
                                <View style={[styles.rightAlign]}>
                                    <View style={styles.relative}><Text style={[styles.globalIcon,styles.camIco,styles.white,styles.camIcoAbs]}> s </Text><Text style={[styles.bold,styles.abscamtxt,styles.white,styles.small]}>3</Text></View> 
                                </View>
                            </View>
                        </Col>
                        <Col sm={8} md={8} lg={8} style={[styles.proCol,styles.listCol]}>
                            <View style={styles.proCaption}>
                                <Text style={[styles.proName,styles.big]} numberOfLines={1}>3008 familiale grise à restaurer</Text>
                                <View style={styles.row}>
                                    <View style={[styles.inlineDatarow,styles.spaceBitw,styles._MT5_]}>
                                        <Text style={[styles.orangeColor,styles.bold]}>12,550 €</Text> 
                                    </View>
                                    <View style={[styles.inlineDatarow,styles._MT10_,styles._PB15_]}>
                                        <Text style={[styles.inlineWithico,styles.borderRight,styles.red,styles.small]}>Urgent<Text style={styles.globalIcon}> x</Text></Text>
                                        <Text style={[styles.inlineWithico,styles.red,styles.small]}>Achat Ditect<Text style={styles.globalIcon}> w</Text></Text>
                                    </View>
                                    <View style={[styles.inlineDatarow,styles.spaceBitw]}>
                                        <Text style={styles.small}>Sport {'&'} Hobbies <Image style={styles.proSign} source={images.pro} /> {'\n'} St Martin du Mont / Saone et loire</Text>
                                        <Text style={[styles.globalIcon,styles.deleteRemonterlist]}>8</Text>
                                    </View>  
                                </View>
                            </View>
                        </Col>
                    </Row>
                     <Row size={12} style={[styles.proRow,styles.prolistRow,styles._PT0_,styles.Prolistgray]}>
                        <Col sm={4} md={4} lg={4} style={[styles.proCol,styles.listCol]} >
                            <View style={styles.imageCanvas}>
                                <View style={styles.imgInner}>
                                    <Image style={styles.hasVideo} source={images.video} />
                                    <Image style={styles.proImg} source={images.IMG10} />
                               </View>
                            </View>
                            <View style={[styles.bottomDetail,styles.bottomCaption]}>
                                <Text style={[styles.leftAlign,styles.white]}>
                                    <Text style={styles.small}>19 Dec | 09h09</Text>
                                </Text>
                                <View style={[styles.rightAlign]}>
                                    <View style={styles.relative}><Text style={[styles.globalIcon,styles.camIco,styles.white,styles.camIcoAbs]}> s </Text><Text style={[styles.bold,styles.abscamtxt,styles.white,styles.small]}>3</Text></View> 
                                </View>
                            </View>
                        </Col>
                        <Col sm={8} md={8} lg={8} style={[styles.proCol,styles.listCol]}>
                            <View style={styles.proCaption}>
                                <Text style={[styles.proName,styles.big]} numberOfLines={1}>3008 familiale grise à restaurer</Text>
                                <View style={styles.row}>
                                    <View style={[styles.inlineDatarow,styles.spaceBitw,styles._MT5_]}>
                                        <Text style={[styles.orangeColor,styles.bold]}>12,550 €</Text> 
                                    </View>
                                    <View style={[styles.inlineDatarow,styles._MT10_,styles._PB15_]}>
                                        <Text style={[styles.inlineWithico,styles.borderRight,styles.red,styles.small]}>Urgent<Text style={styles.globalIcon}> x</Text></Text>
                                        <Text style={[styles.inlineWithico,styles.red,styles.small]}>Achat Ditect<Text style={styles.globalIcon}> w</Text></Text>
                                    </View>
                                    <View style={[styles.inlineDatarow,styles.spaceBitw]}>
                                        <Text style={styles.small}>Sport {'&'} Hobbies <Image style={styles.proSign} source={images.pro} /> {'\n'} St Martin du Mont / Saone et loire</Text>
                                        <Text style={[styles.globalIcon,styles.deleteRemonterlist]}>8</Text>
                                    </View>  
                                </View>
                            </View>
                        </Col>
                    </Row>
                            {/*<Row size={12} style={[styles.proRow,styles.prolistRow,styles._PT0_]}>
                                <Col sm={4} md={4} lg={2} style={[styles.proCol,styles.listCol]} >
                                    <View style={styles.imageCanvas}>
                                        <View style={styles.imgInner}>
                                            <Image style={[styles.proImg,styles.proImgremonter]} source={images.minic} />
                                        </View>
                                    </View>
                                </Col>
                                <Col sm={8} md={8} lg={10} style={[styles.proCol,styles.listCol]}>
                                    <View style={styles.proCaption}>
                                        <Text style={[styles.proName,styles.big]} numberOfLines={1}>3008 familiale grise à restaurer</Text>
                                        <Text style={[styles.smallDesc,styles.small]}>Lorem ipsum dolor sit amet, consectetur</Text>
                                        <View style={styles.row}>
                                            <View style={[styles.inlineDatarow,styles.spaceBitw,styles._MT10_]}>
                                                <Text style={[styles.orangeColor,styles.bold]}>12,550 €</Text>
                                                <Text style={[styles.inlineWithico,styles.borderRight,styles.red,styles.small]}>Urgent<Text style={styles.globalIcon}> x</Text></Text>
                                                <Text style={[styles.inlineWithico,styles.red,styles.small]}>Achat Ditect<Text style={styles.globalIcon}> w</Text></Text>
                                            </View>
                                            <View style={[styles.inlineDatarow,styles.spaceBitw,styles._MT5_]}>
                                                <TouchableOpacity style={[styles.inlineDatarow,styles.discuterbtn]}activeOpacity={0.8}>
                                                   <Text style={[styles.inlineWithico,styles._MR0_,styles._F12_]}>Hors ligne</Text><Text style={[styles.circle,styles.offline,styles.inlineWithico]}></Text>
                                                </TouchableOpacity> 
                                                <Text style={styles.fullDate}>9 dec, 2016</Text>
                                            </View>
                                        </View>
                                    </View>
                                </Col>
                            </Row> */}
                        </View>
                        <View style={[styles.inlineDatarow,styles.jusRight,styles._MT10_]}><Text style={[styles._F16_]}>Prix total :    <Text style={styles.bold}> 0,00 €</Text></Text></View>
                        <Text style={[styles.genText,styles._MT15_]}>En continuant, j'accepte les conditions d'utilisation de UneAffaire.fr (y compris la renonciation au droit de rétractation).</Text>
                        <View style={[styles.inlineDatarow,styles.spaceBitw,styles._MT15_,styles._PB15_]}>
                            <TouchableOpacity activeOpacity={0.8}>
                                <Text style={[styles.smallBtn,styles.slatebgColor,styles.w120]}>Annuler</Text>
                            </TouchableOpacity>
                            <TouchableOpacity activeOpacity={0.8}>
                                <Text style={[styles.smallBtn,styles.w120]}>Valider</Text>
                            </TouchableOpacity>
                        </View>
                </View>
            </View>
        </ScrollView>
    );
  }

}

function mapStateToProps(state, ownProps) {
	return {
        chat:state.chat,
        auth:state.auth,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		chatActions: bindActionCreators(chatActions, dispatch),
		authActions: bindActionCreators(authActions, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(remonter);






