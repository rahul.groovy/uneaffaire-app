import React, {Component} from 'react';
import {
    StyleSheet,
    Image,
    Text,
    View,
    TextInput,
    TouchableOpacity,
    ScrollView,
    Switch,
    Modal,
    WebView,
    Animated,
    AsyncStorage,
    Share,
    ActivityIndicator,
    Dimensions
} from 'react-native';
import { colors } from '../../config/styles';
import styles from '../../config/genStyle';
import {iconsMap, iconsLoaded, Icon} from '../../config/icons';
//import YouTube from 'react-native-youtube';
import oauthconfig from '../../config/oauthconfig';
import ApiHelper from '../../lib/ApiHelper/ApiHelper';
// redux specific
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as chatActions from '../../redux/reducers/chat';
import * as authActions from '../../redux/reducers/auth';

const api = new ApiHelper;
const {height, width} = Dimensions.get('window');
const FORTAB = width < 1025 && width > 721;
import {Column as Col, Row} from 'react-native-flexbox-grid';
import Orientation from 'react-native-orientation';


class YouTubeDemo extends Component {

    constructor(props) {
        super(props);
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
    }

    componentWillMount() {
        iconsLoaded.then(() => {
        });
    }

    onNavigatorEvent(event) {
        if (event.id == 'willAppear') {
            this.props.chatActions.setScreen(this.props.testID);
            //console.log(this.props);
            this.props.navigator.toggleTabs({to: 'hidden', animated: true});
            this.props.navigator.setButtons({
                    leftButtons: [
                        {
                            icon: iconsMap['back'],
                            id: 'back2',
                            title: 'back to welcome'
                        }
                    ],
                    animated: false
                })
        }
        if (event.id == 'back2') {
            this.props.navigator.pop({animated: true});
        }
    }

    static navigatorStyle = {
        navBarBackgroundColor: colors.navBarBackgroundColor,
        navBarTextColor: colors.navBarTextColor,
        navBarSubtitleTextColor: colors.navBarSubtitleTextColor,
        navBarButtonColor: colors.navBarButtonColor,
        statusBarTextColorScheme: colors.statusBarTextColorScheme,
        statusBarColor: colors.statusBarColor,
        tabBarBackgroundColor: colors.tabBarBackgroundColor,
        tabBarButtonColor: colors.tabBarButtonColor,
        tabBarSelectedButtonColor: colors.tabBarSelectedButtonColor,
        navBarSubtitleColor: colors.navBarSubtitleColor,
        // navBarHidden: false,
        // tabBarHidden: false,
        //navBarHideOnScroll:true,
        drawUnderNavbar: true,
        drawUnderTabBar: true
    };

    render() {
        let SRC ='<html><head></head><body style="margin: 0;"><iframe width="100%" height="100%" src="'+'https://www.youtube.com/embed/'+'MsdG3fuiaZI'+'" frameborder="0" allowfullscreen></iframe></body></html>';
        return (
             <View style={{flex:1}}>
                {/* <YouTube ref={o => this.youVideo = o} 
                    apiKey={oauthconfig.youtubeKey} 
                    videoId={'MsdG3fuiaZI'} // The YouTube video ID
                    play={true} // control playback of video with true/false
                    fullscreen={false} // control whether the video should play in fullscreen or inline
                    loop={true} hidden={false} // control whether the video should loop when ended
                    style={styles.youVideo} 
                    onReady={e => console.log(e)} 
                    onChangeState={e => console.log(e)} 
                    onChangeQuality={e => console.log(e)} 
                    onError={e => console.log(e)}/> */}

                    {/* <View style={styles.videoHolder}>
                    <WebView style={styles.youVideo2} source={{html:SRC}} />
                    </View> */}
            </View>
        );
    }
}

function mapStateToProps(state, ownProps) {
    return {chat: state.chat, auth: state.auth};
}

function mapDispatchToProps(dispatch) {
    return {
        chatActions: bindActionCreators(chatActions, dispatch),
        authActions: bindActionCreators(authActions, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(YouTubeDemo);