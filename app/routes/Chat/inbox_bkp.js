import React, { Component } from 'react';
import { StyleSheet, Image, Platform,StatusBar,TouchableOpacity,View,ActivityIndicator, ScrollView} from 'react-native';
import { colors } from '../../config/styles';
import images from '../../config/images';
import { settings } from '../../config/settings';
import { Text, Button, Icon, Container, Content, Footer, Header, } from 'native-base';
import TimeAgo from 'react-native-timeago';

export default class InboxContainer extends Component {
    constructor(props){
        super(props)        
            this.state={
                animating: true,
                load: true,  
                data: [], 
                count: 0           
            }
            let date = new Date();
            //this.offsetTime = date.getTimezoneOffset();
            this.offsetTime = 0;
    }
    
    fetchUserMsg(){
        let _this = this;
        // this.setState({load:true});
        var params = {
            LOGIN_ID: this.props.navigator.user.id,
        };
        var query = "";
        for (key in params) {
            query += encodeURIComponent(key) + "=" + encodeURIComponent(params[key]) + "&";
        }        
        fetch(settings.api+'index.php/api/getUserChatList', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            },
            body: query
            }).then((response) => response.json())
            .then((response) => {

                if(response.status){
                    _this.setState({
                        load:false,
                        count:response.total,         
                        data:response.data.items});
                }else{
                    _this.setState({
                        load:false,
                        count:0,         
                        data:[]});
                }
        })
        .catch((error) => {});     
    }

    loadData(){
        this.props.navigator.reloadInbox = this.fetchUserMsg.bind(this);
        this.fetchUserMsg();
    }
    
    openChat(index,data){
        let _U = data.U;
        this.props.navigator.push({
            id: 'singlechat',
            data: data
        });
        let sdata = this.state.data;
        sdata[index].U = 0;
        this.setState({data:sdata});
    }

    render() {
        let _this = this;
        let nomsg = this.state.load != true ? 
        <View style={StyleSheet.flatten({flex: 1, alignItems: 'center', justifyContent: 'center',padding: 30})}>
        <Text style={{color: "#CCC", fontFamily: settings.fonts.Light,fontSize: 22,textAlign:'center'}}>
            No messages to display!
        </Text>
        </View>
        : null;
        let loading = <View style={styles.Center}>
                    <ActivityIndicator color="#5385ba" animating={true} style={[styles.loader]} size="large"/>
                    </View>
        return (
            <Container style={StyleSheet.flatten([styles.container,{width:this.props.width}])}>
                <StatusBar animated={true} backgroundColor={colors.statusbarColor} barStyle={settings.statusbarStyle}/>
				<View style={{
					backgroundColor:colors.statusbarColor,
					height: Platform.OS ==='ios' ? colors.statusbarHeight:0
				}}></View>
                <View style={[styles.header,styles.hblue]}>
                    <View style={styles.hcenter}>
                        <Text style={StyleSheet.flatten(styles.textCenter)}>Inbox</Text>
                    </View>
                </View>
                <View style={{flex:1}}>
                
                    { this.state.load ?
                    loading :  <ScrollView keyboardShouldPersistTaps={'handled'}>
                    <View style={StyleSheet.flatten(styles.main)}>
                    { this.state.count > 0 ? (
                        this.state.data.map(function(item, index){
                            date = new Date(item.Time*1000 + _this.offsetTime * 1000);
                        return (
                            <TouchableOpacity key={index} onPress={()=>{_this.openChat(index,item)}} style={StyleSheet.flatten(styles.chatview)}>
                                <View style={StyleSheet.flatten(styles.leftview)}>
                                    <Image source={{uri:item.OtherUserPic}} style={StyleSheet.flatten(styles.img)}></Image>
                                </View>
                                <View style={StyleSheet.flatten(styles.centerview)}>
                                    <View style={StyleSheet.flatten(styles.chatname_date)}>
                                    <Text style={StyleSheet.flatten(styles.namefont)}>{item.OtherUser}</Text>
                                    <TimeAgo style={styles.time} time={date} />
                                    </View>
                                    <Text style={StyleSheet.flatten(styles.chatfont)}>{item.Msg}</Text>
                                    <Text style={StyleSheet.flatten(styles.itemName)}>{item.ITEM_NAME}</Text>
                                </View>
                                {item.U > 0 && <Text style={StyleSheet.flatten(styles.unread)}>{item.U}</Text> }
                            </TouchableOpacity>
                            )
                        })):nomsg
                    }
                    </View></ScrollView>}
               
                </View> 
            </Container>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: null,
        height: null,
	    backgroundColor:'#FFF',
    },
    headerview: {
        flexDirection: 'row',
        backgroundColor: '#FFF',
        justifyContent: 'space-between',
        alignItems: 'center',
        alignSelf: 'stretch'
    },
    nomsgfound:{
        alignSelf:'center',
        justifyContent:'center'
    },
    icon: {
        fontFamily: settings.fonts.Icon,
        fontSize: 20,
        color: '#000',
        padding: 10
    },
    mainfont: {
        fontFamily: settings.fonts.Normal,
        fontSize: 15,
    },
    Center: {
        flex:1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    main: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#fff'
    },
    chatview: {
        padding: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderBottomWidth: 1,
        borderColor: '#EEE'
    },
    img: {
        height: 60,
        width: 60,
        borderRadius: 30,
    },
    leftview: {
        width: 70
    },
    centerview: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'flex-start',
    },
    timefont: {
        fontFamily: settings.fonts.Normal,
        fontSize: 13,
        color: '#5683BA',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    chatname_date: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignSelf: 'stretch',
    },
    rightview: {
        flexDirection: 'column',
        alignItems: 'center',
        
    },
    namefont: {
        fontFamily: settings.fonts.Normal,
        fontSize: 14,
        paddingTop: 2,
        paddingBottom:4,
    },
    chatfont: {
        fontFamily: settings.fonts.Normal,
        fontSize: 12,
        color: '#333',
        paddingBottom:4,
    },
    unread: {
        position: 'absolute',
        bottom: 6,
        left: 50,
        backgroundColor: 'red',
        fontFamily: settings.fonts.Normal,
        fontSize: 12,
        color: '#FFF',
        borderRadius: 11,
        padding: 4,
        minWidth: 22,
        textAlign: 'center'
    },
    itemName: {
        fontFamily: settings.fonts.Normal,
        fontSize: 12,
        color: '#444'
    },
    statusview: {
        marginTop: 9,
        height: 10,
        width: 10,
        backgroundColor: '#5683BA',
        borderRadius: 5,
    },
    separator: {
        height: 1,
        backgroundColor: '#bbb',
    },
    footerview: {
        backgroundColor: '#FFF',
    },
     header:{
        flexDirection: 'row',
        backgroundColor: colors.headerColor,
        height: 50,
        alignItems: 'center'
    },
    hleft: {
        width: 50,
        justifyContent: 'center',
        alignItems: 'center'
    },
    hcenter: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    hright: {
        width: 50
    },
 
    menuIco:{
            fontFamily:settings.fonts.Icon,
            fontSize:25,
            color:'#fff'
    },
    textCenter:{
        color:'#ffffff',
        fontFamily:settings.fonts.Normal,
        fontSize:14,
    },
    time: {
        fontFamily:settings.fonts.Normal,
        fontSize:12,
        color: '#666'
    }
});