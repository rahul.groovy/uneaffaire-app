import React, { Component } from 'react';
import { StyleSheet, Image, Platform, ActivityIndicator, Keyboard, ScrollView, findNodeHandle } from 'react-native';
import { colors } from '../../config/styles';
import images from '../../config/images';
import { settings } from '../../config/settings';
import { Text, Button, View, Icon, Container, Content, Footer, Header, Item, Input, Label, InputGroup, Form } from 'native-base';
import TimeAgo from 'react-native-timeago';
import AndroidBackButton from "react-native-android-back-button";
const UIManager = require('NativeModules').UIManager;

export default class SingleChatContainer extends Component {
    constructor(props){
        super(props);
        this.state = {
            more: false,
            load: true,
            data: [],
            count: 0,
            page: 1,
            msg: '',
            oldFirst: null,
            newFirst: null,
        };
        this.chatView = null;
        this.sHeight = 0;
        this.soHeight = 0;
        this.isScrollEnd = true;
        let date = new Date();
        this.offsetTime = 0;
        this.Refs = [];
        this.moreLoad = false;
        this.scrollY = 0;
        this.props.navigator.messageCalls = this.updateMessage.bind(this);
        if(typeof this.props.data.PID != 'undefined'){
            this.props.data.AdId = this.props.data.PID;
        }
    }

    componentWillUnmount(){
        this.props.navigator.messageCalls = null;
    }

    updateMessage(data){    
        try{
        if(typeof data.mtype != 'undefined' && data.mtype == 'chat'){
            if(data.tid == this.props.data.OtherUserId && data.fid == this.props.navigator.user.id &&
                data.ptype == this.props.data.Type && (data.pid == this.props.data.AdId || data.PID == this.props.data.AdId )){
                    let msg = data.fcm.body;
                    let sdata = this.state.data;
                    sdata.push(
                        {
                            FROM_ID: data.from,
                            MESSAGE: msg,
                            DATE: new Date().getTime()/1000,
                            PROFILE: data.imgSrc
                        }
                    );
                    this.setState({data:sdata});          
            }
            else{
                return false;
            }
        }
        }catch(err){
            alert(err.message)
        }
    }

    onBack(){
        this.props.navigator.pop();
        return true;
    }

    componentDidMount(){        
        this.fetchData();
        if(typeof this.props.navigator.reloadInbox != 'undefined'){
            this.props.navigator.reloadInbox();
        }
    }

    fetchData(){
        
        let _this = this;
        if(!this.moreLoad) this.setState({load:true});
        var params = {
            LOGIN_ID: this.props.navigator.user.id,
            OTHER_USER_ID: this.props.data.OtherUserId,
            TYPE_ID: this.props.data.AdId,
            TYPE: this.props.data.Type,
            PAGE: this.state.page
        };
        var query = "";
        for (key in params) {
            query += encodeURIComponent(key) + "=" + encodeURIComponent(params[key]) + "&";
        }        
        fetch(settings.api+'index.php/api/viewChat', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            },
            body: query
            }).then((response) => response.json())
            .then((response) => {
                this.props.navigator.setChatBadge({type:'-',n:response.oldU});   
                let oldFirst = response.data.items.length;            
                response.data.items = response.data.items.concat(_this.state.data);
                //console.log(response);
                if(response.status){
                    _this.setState({
                        load:false,
                        count:response.total,         
                        data:response.data.items,
                        more:response.more,
                        page: _this.state.page + 1,
                        oldFirst: oldFirst
                    });
                }else{
                    _this.setState({
                        load:false,
                        count:0,         
                        data:[]});
                }
        })
        .catch((error) => {
            //console.log(error);
        });     
    }

    sendMessage(){
        if(this.state.msg != ''){
        let data = this.state.data;
        
        userText = this.state.msg.replace(/^\s+/, '').replace(/\s+$/, '');
        if (userText === ''){
            return false;
        }
        
        this.state.msg = this.state.msg.replace(/[^@\s]*@[^@\s]*\.[^@\s]*/g, "[removed]");
        this.state.msg = this.state.msg.replace(/[a-zA-Z]*[:\/\/]*[A-Za-z0-9\-_]+\.+[A-Za-z0-9\.\/%&=\?\-_]+/ig, "[removed]");
        this.state.msg = this.state.msg.replace(/(1-?)?(-?\([0-9]{3}\)|[0-9]{3})(-?[0-9]{3}){2}[0-9]/g, "[removed]");
        data.push(
             {
                FROM_ID: this.props.navigator.user.id,
                MESSAGE: this.state.msg,
                DATE: parseInt(new Date().getTime()/1000)
            }
        );
        fetch(settings.api+'index.php/api/Sendmessage', {
            method: 'POST',
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            body: 'FromId='+this.props.navigator.user.id+'&ToId='+this.props.data.OtherUserId+'&PID='+this.props.data.AdId+'&TYPE='+this.props.data.Type+'&Message='+encodeURIComponent(this.state.msg)
            }).then((response) => response.json())
            .then((response) => {
                if(typeof this.props.navigator.reloadInbox != 'undefined'){
                    this.props.navigator.reloadInbox();
                }
            });
        this.setState({data:data,msg:''});
        Keyboard.dismiss();
        }
    
    }

    updateScroll(){
        if(this.moreLoad){
            let _cThis = this;
            setTimeout(function(){
                 //var firstMsg = $('.message:first');
                // var curOffset = firstMsg.offset().top - $(document).scrollTop();
                // firstMsg.before(firstMsg.clone());
                // $(document).scrollTop(firstMsg.offset().top-curOffset);
                let oY = nY = 0;
                const handle = findNodeHandle(_cThis.Refs[0]);
                UIManager.measureLayoutRelativeToParent(handle,(e) => {console.error(e)}, 
                (x, y, w, h) => {
                    oY = y;
                    const handle1 = findNodeHandle(_cThis.Refs[_cThis.state.oldFirst]);
                    UIManager.measureLayoutRelativeToParent(handle1,(e) => {console.error(e)}, 
                    (x, y, w, h) => {
                    nY = y;
                        _cThis.chatView.scrollTo({y:_cThis.scrollY + (nY - oY),animated:false});
                        _cThis.moreLoad = false;
                    });
                });
                
                

                // console.log(_cThis.Refs)
                // console.log(_cThis.Refs[_cThis.state.oldFirst])

                
            },1)
        }
        if(this.isScrollEnd && this.chatView != null){
            this.chatView.scrollTo({y:this.sHeight - this.sMHeight,animated:false});
        }
    }

    chatScroll(event){
        this.scrollY = event.nativeEvent.contentOffset.y;
        if(event.nativeEvent.contentOffset.y < 50 && !this.state.load && this.state.more && !this.moreLoad){
            this.moreLoad = true;
            this.fetchData();
        }
        if((this.sHeight - this.sMHeight) == event.nativeEvent.contentOffset.y){
            this.isScrollEnd = true;
        }else{
            this.isScrollEnd = false;
        }
    }

    scrollInit(event){
        var {x, y, width, height} = event.nativeEvent.layout;
        this.sMHeight = height;
    }
    
    render() {
        let curUser = this.props.navigator.user.id;
        let _this = this;
        return (
            <Container style={StyleSheet.flatten(styles.main)}>
                 <AndroidBackButton onPress={() => this.onBack()} />
                <Header style={StyleSheet.flatten(styles.headerview)}>
                    <Text style={StyleSheet.flatten(styles.icon)} onPress={() => this.onBack()}>)</Text>
                    <Text style={StyleSheet.flatten(styles.mainfont)}>{this.props.data.OtherUser}</Text>
                    <Text>       </Text>
                </Header>
                <View style={{flex:1}}>
               
                    { _this.state.load ?
                    <View style={StyleSheet.flatten(styles.Center)}>
                    <ActivityIndicator color="#5385ba" animating={true} style={[styles.loader]} size="large"/>
                    </View> :   <ScrollView keyboardShouldPersistTaps={'handled'} ref={(chatScrollView) => { if(chatScrollView != null){ this.chatView = chatScrollView; this.updateScroll(); }}} 
                    onContentSizeChange={(contentWidth, contentHeight)=>{
                        this.sHeight = contentHeight;
                        this.updateScroll();
                    }}
                    onLayout={this.scrollInit.bind(this)}
                    onScroll={this.chatScroll.bind(this)}>
                    <View style={StyleSheet.flatten(styles.submain)}>
                        {
                        this.state.data.map(function(item, index){
                            let date = new Date(item.DATE*1000 + _this.offsetTime * 1000);
                            return (
                                <View key={index}  ref={(cmp)=>{if(cmp != null){ _this.Refs[index] = cmp; }}} style={StyleSheet.flatten([styles.chatview,item.FROM_ID == curUser ? styles.cvEnd : null])}>
                                    {
                                    item.FROM_ID != curUser &&
                                    <View style={StyleSheet.flatten(styles.imageView)}>
                                        <Image source={{uri:item.PROFILE}} style={StyleSheet.flatten(styles.img)}></Image>
                                    </View>
                                    }
                                    <View style={StyleSheet.flatten([styles.rightview,item.FROM_ID != curUser ? styles.msgImg : null])}>
                                        <View style={StyleSheet.flatten(styles.messageview)}>
                                            <Text style={StyleSheet.flatten([styles.chatfont,
                                            item.FROM_ID != curUser ? styles.colorWhite : null ])}>
                                            {item.MESSAGE}
                                            </Text>
                                        </View>
                                        <TimeAgo style={item.FROM_ID != curUser ? styles.chattimeleft : styles.chattimeright} time={date} />
                                    </View>
                                </View>
                            );    
                        })
                        }
                    </View></ScrollView>}
                </View>

                <Footer style={StyleSheet.flatten(styles.footerview)}>
                    <View style={StyleSheet.flatten(styles.left)}>
                        <InputGroup rounded>
                            <Input placeholder='Type your text here' style={StyleSheet.flatten(styles.inputbox)}
                            autoCapitalize={'none'} autoCorrect={false} autoFocus={false}
                            onSubmitEditing={() => { this.sendMessage(); }} onChangeText={msg => this.setState({ msg:msg })} value={this.state.msg}
                            returnKeyType={'send'}  />
                        </InputGroup>
                    </View>
                    <View style={StyleSheet.flatten(styles.right)}>
                        <Button info style={StyleSheet.flatten(styles.sendbtn)} onPress={this.sendMessage.bind(this)} >
                            <Text style={StyleSheet.flatten(styles.sendFont)}>F</Text>
                        </Button>
                    </View>
                </Footer>
            </Container>
        )
    }
}
const styles = StyleSheet.create({
    main: {
        flex:1,
        backgroundColor: '#EEE'
    },
    chatViewMain: {
        backgroundColor: '#EEE',
    },
    Center: {
        flex:1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    submain: {
        paddingBottom: 10
    },
    headerview: {
        flexDirection: 'row',
        backgroundColor: '#FFF',
        justifyContent: 'space-between',
        alignItems: 'center',
        alignSelf: 'stretch'
    },
    icon: {
        fontFamily: settings.fonts.Icon,
        fontSize: 20,
        color: '#000',
        padding: 10
    },
    mainfont: {
        fontFamily: settings.fonts.Light,
        fontSize: 15,
    },
    sendFont: {
        fontFamily: settings.fonts.Icon,
        fontSize: 20
    },
    chatview: {
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 15,
        flexDirection: 'row',
        alignItems: 'flex-end',
        justifyContent: 'flex-start',
        minWidth: 200
    },
    cvEnd: {
        justifyContent: 'flex-end'
    },
    img: {
        height: 40,
        width: 40,
        borderRadius: 20
    },
    imageView: {
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        borderBottomLeftRadius: 20,
        backgroundColor: '#4a66a0' 
    },
    leftview: {
        alignSelf: 'flex-start',
    },
    rightview: {
        flexDirection: 'column',
        alignItems: 'flex-start',
        padding: 5,
        backgroundColor: '#FFF',
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 0,
    },
    msgImg: {
        backgroundColor: '#4a66a0',
        borderBottomLeftRadius: 0,
        borderBottomRightRadius: 10,
    },
    chatfont: {
        fontFamily: settings.fonts.Normal,
        fontSize: 13,
        color: '#333'
    },
    colorWhite: {
        color: '#FFF'
    },
    messageview: {
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 10,
        paddingBottom: 3
    },
    footerview: {
        height: 50,
        padding: 5,
        backgroundColor: '#FFF',
        flexDirection: 'row',
        justifyContent: 'space-between',

    },
    chattimeleft: {
        fontFamily: settings.fonts.Normal,
        fontSize: 10,
        color: '#FFF9',
        alignSelf: 'flex-end',
        paddingRight: 10,
        paddingBottom: 5
    },
    chattimeright: {
        fontFamily: settings.fonts.Normal,
        fontSize: 10,
        color: '#666',
        paddingLeft: 10,
        paddingBottom: 5
    },
    decoration: {
        position: 'absolute',
        width: 0,
        height: 0,
        right: 100,
        backgroundColor: '#bbb',
    },
    right: {
        flex: 1,
        alignSelf: 'flex-end',
        alignItems: 'center'
    },
    left: {
        flex: 1
    },
    sendbtn: {
        backgroundColor: '#5683BA',
        height: 40,
        width: 70,
        justifyContent: 'center',
        borderRadius: 35,
        alignSelf: 'center',
    },
    inputbox: {
        fontFamily: settings.fonts.Normal,
        fontSize: 14,
        height: 40
    }
});