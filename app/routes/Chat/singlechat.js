import React, { Component } from 'react';
import {
  StyleSheet,
  Image,Text,View,TextInput,TouchableOpacity,ScrollView,Dimensions,Platform,Animated,WebView,Footer,InputGroup,Input,Button,KeyboardAvoidingView
} from 'react-native';
import { colors } from '../../config/styles';
import {settings} from '../../config/settings';
import styles from '../../config/genStyle';
import { iconsMap, iconsLoaded, Icon } from '../../config/icons';
import {emitD} from '../../config/socket';
import moment from 'moment';
import ApiHelper from '../../lib/ApiHelper/ApiHelper';
import { ActionSheetCustom as ActionSheet } from 'react-native-actionsheet';
var _ = require('lodash');
// redux specific
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as chatActions from '../../redux/reducers/chat';
import * as authActions from '../../redux/reducers/auth';
import ImagePicker from 'react-native-image-crop-picker';
import {tryCamera} from '../../redux/utils/location';
import CKeyboardSpacer from '../../components/CKeyboardSpacer';
const api = new ApiHelper;
import 'moment/locale/fr'  // without this line it didn't work
moment.locale('fr')
const dimen = Dimensions.get('window');
class Singlechat extends Component {
  
static navigatorStyle = {
    navBarBackgroundColor: colors.navBarBackgroundColor,
    navBarTextColor: colors.navBarTextColor,
    navBarSubtitleTextColor: colors.navBarSubtitleTextColor,
    navBarButtonColor: colors.navBarButtonColor,
    statusBarTextColorScheme: colors.statusBarTextColorScheme,
    statusBarColor: colors.statusBarColor,
    tabBarBackgroundColor: colors.tabBarBackgroundColor,
    tabBarButtonColor: colors.tabBarButtonColor,
    tabBarSelectedButtonColor: colors.tabBarSelectedButtonColor,
    navBarSubtitleColor: colors.navBarSubtitleColor,
    drawUnderTabBar:true,
};

constructor(props) {
    super(props);
    // if you want to listen on navigator events, set this up
    this.ActionSheetOptions = {
        CANCEL_INDEX: 2,
        DESTRUCTIVE_INDEX: 2,   
        options:
        [ <Text style={styles.actionName}>{'Prendre une photo'}</Text>,
        <Text style={styles.actionName}>{'À partir de la bibliothèque'}</Text>,
        <Text style={styles.actionName}>{'Annuler'}</Text>]
    };
    this.state={
        check1:false,
        tabs:1,
        tabSelected:1,
        like:1,
        scrolled:true,
        scrollY: new Animated.Value(0),
        message: '',
        loading:true,
        tabShow: new Animated.Value(120),
        tabScale:new Animated.Value(1),
        isTyping : false,
        imageFile : {},
        fullImage:'',
        options:
        [ <Text style={styles.actionName}>{'Prendre une photo'}</Text>,
        <Text style={styles.actionName}>{'À partir de la bibliothèque'}</Text>,
        <Text style={styles.actionName}>{'Annuler'}</Text>]
    };
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
}

componentWillMount(){
    iconsLoaded.then(()=>{});
}

componentWillReceiveProps(nextProps){
    //console.log(this.props.chat.detailMsgCount);
    //console.log(nextProps.chat.detailMsgCount);
    if(this.props.chat.screen == 'Singlechat'  && nextProps.chat.socket !== null && !_.isEqual(this.props.chat.socket,nextProps.chat.socket)){
        //console.log('after connectD');
        this.getChatData();
    }
    if(this.props.chat.detailMsgCount !== nextProps.chat.detailMsgCount){
        if(this.state.loading){
            this.setState({loading:false},()=>{
                this.scrollToEnd();
            });
        }else{
            this.scrollToEnd();
        }
    }
}

scrollToEnd(){
    setTimeout(()=>{
        //console.log('in scroll');
        if(this.ScrollView !== undefined && this.ScrollView !== null){
            this.ScrollView.scrollToEnd({animated:true});
        }
    },100);
}
getChatData(){
    if(this.props.data !== undefined && this.props.data !== null){
        console.log('getting data');
        emitD('messages_app',this.props.chat.socket,this.props.data);
    }   
}

 onNavigatorEvent(event) {
    //console.log(event)
    if(event.id=='willAppear'){
        api.setAction(this.props.chatActions,this.props.authActions);
        this.props.chatActions.setScreen(this.props.testID);
        //console.log(this.props);
        this.props.navigator.toggleTabs({
            animated: false,
            to: 'hidden'
        });
        this.props.navigator.toggleNavBar({
            to: 'shown',
            animated: true
        });
         this.props.navigator.setStyle({navBarTitleTextCentered: true});
        this.props.navigator.setTitle({
            title: this.props.data !== undefined && this.props.data.name !== undefined ? this.props.data.name : '',
        });
        /* 
        rightButtons: [{
       icon: iconsMap['gear'],
       id: 'setting',
       title:'back to welcome',
     }],
        */
        this.props.navigator.setButtons({
            leftButtons: [{
                icon: iconsMap['back'],
                id: 'back2',
                title:'back to welcome',
            }],animated:false
        });
        this.getChatData();  
    }
    if(event.id=='didAppear'){
        //console.log(this.props.data);
        this.props.chatActions.setConvDetailId(this.props.data.ad);
    }

    if(event.id=='willDisappear'){
        this.props.chatActions.setDetail([]);
        this.emitTyping(false);
        //console.log(this.props.chat);
    }
   
    if(event.id == 'back2'){
      this.props.navigator.pop({
        animated: true // does the pop have transition animation or does it happen immediately (optional) 
      }); 
    }
    if(event.id == 'menu'){
        // this.props.navigator.toggleDrawer({
        //     side: 'left', // the side of the drawer since you can have two, 'left' / 'right'
        //     animated: true, // does the toggle have transition animation or does it happen immediately (optional)
        //     to: 'open' // optional, 'open' = open the drawer, 'closed' = close it, missing = the opposite of current state
        // });
    }
  }

  goto(page){   
        this.props.navigator.push({
            screen: page
        }); 
  }
  tabchange(tab){ 
        this.setState({tabs:tab,tabSelected:tab});
  }

sendMessage(){
    let {message} = this.state;
    if(message.trim() != ''){
        let obj ={
            token:this.props.auth.token, 
            receive_from_id:this.props.data.send_from_id, 
            id:this.props.auth.userdata.id,
            message:message, 
            ad:this.props.data.ad
        };
        emitD('sendMessage_app',this.props.chat.socket,obj);
        this.emitTyping(false);
    }
    this.setState({message:''});
}

emitTyping(bool){
    let obj ={
        token:this.props.auth.token,
        id:this.props.data.send_from_id,
        ad:this.props.data.ad,
        typing:bool
    };
    emitD('isTyping',this.props.chat.socket,obj);
}

_renderMessage(data,i){
    
        let ret=null;
        if(data.from_id != this.props.auth.userdata.id){
            ret= <View key={`message_${data.id}`} style={[styles.chatRecRow,styles.rowBtnMain,styles.ItemTop]}>
                <Image source={{uri:data.avatar}} style={styles.msgProImg}></Image>
                <View style={styles.recRowMsg}>
                        {data.message_type === 'IMG' ? 
                        <TouchableOpacity onPress={()=>{
                            this.setImage(data.mainImg)
                        }}>
                              <Image source={{uri:data.img}} style={styles.attachPhoto}></Image>
                         </TouchableOpacity>
                        :  
                        <View>
                            <View style={styles.msgData}><Text style={styles.msgText}>{data.message}</Text></View>
                            <View style={styles.selfStart}><Text style={[styles._DATE_,styles.justLeft]}>{moment(data.createdAt).fromNow()}</Text></View>
                        </View>
                        }
                </View>
            </View>;
        }else{
            ret=<View key={`message_${data.id}`} style={[styles.chatSenderRow,styles.inlineDatarow,styles.ItemTop]}>
            <View style={styles.senderRowMsg}>
                    {data.message_type === 'IMG' ? 
                        <TouchableOpacity onPress={()=>{
                            this.setImage(data.mainImg)
                        }}>
                              <Image source={{uri:data.img}} style={styles.attachPhoto}></Image>
                         </TouchableOpacity>
                        :  
                        <View>
                            <View style={[styles.msgData,styles.orange]}><Text style={[styles.msgText,styles.white]}>{data.message}</Text></View>
                            <View style={styles.selfStart}><Text style={[styles._DATE_,styles.justLeft]}>{moment(data.createdAt).fromNow()}</Text></View>
                        </View>
                        }
                {/* <View>
                    <View style={[styles.msgData,styles.orange]}><Text style={[styles.msgText,styles.white]}>{data.message}</Text></View>
                    <View style={styles.selfAlign}><Text style={[styles._DATE_,styles.justLeft]}>{moment(data.createdAt).fromNow()}</Text></View>
                </View> */}
            </View>
            <Image source={{uri:data.avatar}} style={[styles.msgProImg,styles.senderImg]}></Image>
        </View>;
        }
        return ret;
}

doAction(i) {
    if (i === 0) {
        this.openCamera();
    } else if (i === 1) {
        this.imageClick();
    }
}

openCamera = () => {
    ImagePicker.openCamera({
        width: 1200,
        height: 1200,
        cropping: false,
        mediaType: "photo",
        compressImageQuality : 0.7
      }).then((image) => {
        let newArray = {
            uri: image.path,            
            name:image.path.substr(image.path.lastIndexOf('/') + 1),           
            type: image.mime,
        }
        console.log(newArray);
        
        this.setState({backimage:image.path,imageFile:newArray},()=>{
            this.sendAttach();
        }); 
      }).catch(error => console.log('cancel'));
}

imageClick(){     
    /**
     * The first arg is the options object for customization (it can also be null or omitted for default options),
     * The second arg is the callback which sends object: response (more info below in README)
     */
    tryCamera().then((result) => {
        if(result){
            this.getCamera();
        }
    }).catch((e)=>{
        console.log(e);
    })   
}

sendAttach = () => {
    const {imageFile} = this.state;
    // alert('Send Attacj');
    const query = new FormData();
    // this.setState({
    //     load : true
    // },()=>{
        
        var params = {
            token:this.props.auth.token,
            receive_from_id:this.props.data.send_from_id, 
            ad_id:this.props.data.ad,
        };
        for (key in params) {
            query.append((key), (params[key]))
        }
        if (!_.isEmpty(imageFile)) {
            query.append('file', imageFile);
        }else{
            query.append('file', '');
        }
        console.log(query);
        // alert(JSON.stringify(query))
        // return false;
        fetch(settings.api+'app/chatImageUpload', {
            method: 'POST',
            body: query
            }).then((response) => response.json())
            .then((response) => {
                console.log(response);
                // alert(JSON.stringify(response))
                if(response.status){ 
                   
                }else{
                    
                }               
            })
            .catch((error) => {
                console.log(error);
                // _this.setState({load:false});
            });
    //})
}

setImage = (image) => {
    this.setState({
        fullImage : image
    });
}

getCamera = () => {
    ImagePicker.openPicker({
        width: 1200,
        height: 1200,
        cropping: false,
        mediaType: "photo",
        compressImageQuality : 0.7
      }).then(image => {
        // console.log(image);
        let newArray = {
            uri: image.path,            
            name:image.path.substr(image.path.lastIndexOf('/') + 1),           
            type: image.mime,
        }
        console.log(newArray);
        this.setState({backimage:image.path,imageFile:newArray},()=>{
            this.sendAttach();
        });
      });
}

showActionSheet() {
    tryCamera().then((result) => {
        if (this.ActionSheet && this.ActionSheet.show) {
            this.ActionSheet.show();
        }
    }).catch((e)=>{
        console.log(e);
    }) 
}


render() {
      const {convDetail,conv} = this.props.chat;
      const { loading , fullImage } = this.state;
      let typing = false;
      if(this.props.data !== undefined && this.props.data !== null) {
        let ad_id = this.props.data.ad;
         typing =  _.find(conv, function(o) { return (o.ad ===  ad_id && o.typing) });
      }
    return (
        <View style={{flex:1}}>
      {loading ? (
        <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
                    <Text style={{fontSize:20}}>Chargement des messages ...</Text>
                    </View>
      ):(<View style={[styles.main,{padding:7,paddingTop:7}]}>
                {typeof convDetail != 'undefined' && convDetail.length > 0 ? (
                    
                    <ScrollView showsVerticalScrollIndicator={false}
                    ref = {o => this.ScrollView = o}  
                    style={[{flexGrow:1,backgroundColor:'#f8f8f8',}]}>
                        {convDetail.map((d,i)=>{
                            return this._renderMessage(d,i);
                        })}
                        </ScrollView>
                ):(
                    <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
                        <Text style={{fontSize:20}}>Aucun message trouvé</Text>
                    </View>
                    )}   
                    {typing ?  <View style={{width: 100,alignSelf:'center',position:'absolute',bottom:70,backgroundColor: 'rgba(255,255,255,1)',borderWidth:1,borderColor: colors.navBarButtonColor,borderRadius:20, shadowColor: '#fff',
    shadowOffset: { width: 1, height: 1},
    shadowOpacity: 1,
    shadowRadius: 7,elevation:5}}>
                <View style={{width: 100,flexDirection:'row',padding:5,alignContent:'center',justifyContent:'center'}}>
                    <Text style={{color:colors.navBarButtonColor,textAlign:'center'}}>écrit...</Text>
                </View>
            </View>  : null}
                    
                    <View style={{marginLeft:-7,marginRight:-7,height:50,flexDirection:'row',alignItems:'center',justifyContent:'center',           borderTopWidth:1,borderColor:'#c0c0c0'
                    }}>
                    <TouchableOpacity 
                    onPress={()=>this.showActionSheet()}
                    style={{
                        alignItems:'center',
                        justifyContent:'center',
                        width:50}}>
                      <Text style={[styles.globalIcon,styles.attachIco]}>/</Text>
                    </TouchableOpacity> 
                    <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
                        <TextInput 
                        placeholder={"Tapez ici le message......"} 
                        underlineColorAndroid='#0000' 
                        style={[styles.msgInput,{alignSelf:'stretch'}]}
                        value={this.state.message}
                        onFocus={()=>{this.emitTyping(true)}}
                        onChangeText={(text)=>{
                            this.setState({message:text},()=>{this.emitTyping(true)});
                        }}
                        onBlur={()=>{this.emitTyping(false)}}
                        // blurOnSubmit={true}
                        // onSubmitEditing={()=>this.sendMessage()}
                        multiline={true}
                        // returnKeyType="go"
                        // returnKeyLabel=""
                        />
                    </View>
                    <TouchableOpacity style={{
                        alignItems:'center',
                        justifyContent:'center',
                        width:50}} onPress={()=>this.sendMessage()}>
                      <Text style={[styles.globalIcon,styles.sendIco]}>\</Text>
                    </TouchableOpacity>
                </View>
                {
                    fullImage !== '' ? 
                        <View style={[styles.canvas, { backgroundColor: 'rgba(0,0,0,0.5)', alignItems: 'center',justifyContent:'center' }]}>
                            <TouchableOpacity 
                                onPress={()=>{ this.setState({fullImage:''}) }} 
                                style={{ position:'absolute',top:5,right:5,padding: 5}}
                            >
                                <Icon name="close" size={15} color={'#fff'}/>
                            </TouchableOpacity>
                            
                            <Image
                            resizeMode="contain"
                            source={{uri: fullImage}}
                            style={{height:(dimen.height-50),width: (dimen.width-50)}}
                             /> 
                        </View>
                    : 
                        (null)
                }
                <ActionSheet
                    ref={(o) => { this.ActionSheet = o; }}
                    options={this.state.options}
                    cancelButtonIndex={this.ActionSheetOptions.CANCEL_INDEX}
                    destructiveButtonIndex={this.ActionSheetOptions.DESTRUCTIVE_INDEX}
                    onPress={(i) => {
                        if (Platform.OS === 'ios') {
                        setTimeout(() => {
                            this.doAction(i);
                        }, 500);
                        } else {
                        this.doAction(i);
                        }
                    }}
                />  
                 {Platform.OS === 'ios' ? <CKeyboardSpacer /> : (null)}
                </View>        
        )}
        </View>
    );
  }
}

function mapStateToProps(state, ownProps) {
	return {
        chat:state.chat,
        auth:state.auth,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		chatActions: bindActionCreators(chatActions, dispatch),
		authActions: bindActionCreators(authActions, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(Singlechat);

// imp data format layout
/* 
<View style={[styles.chatRecRow,styles.rowBtnMain,styles.ItemTop]}>
                            <Image source={images.user} style={styles.msgProImg}></Image>
                            <View style={styles.recRowMsg}>
                                <View>
                                    <View style={styles.msgData}><Text style={styles.msgText}>{'Hi , How are you'}</Text></View>
                                    <View style={styles.selfStart}><Text style={[styles._DATE_,styles.justLeft]}>TUESDAY , 10:31 AM</Text></View>
                                </View>
                            </View>
                        </View>
                        <View style={[styles.chatRecRow,styles.rowBtnMain,styles.ItemTop]}>
                            <Image source={images.user} style={styles.msgProImg}></Image>
                            <View style={styles.recRowMsg}>
                                <View>
                                    <View style={styles.msgData}><Text style={styles.msgText}>{'Hi , How are you'}</Text></View>
                                    <View style={styles.selfStart}><Text style={[styles._DATE_,styles.justLeft]}>TUESDAY , 10:31 AM</Text></View>
                                </View>
                            </View>
                        </View>
                        <View style={[styles.chatRecRow,styles.rowBtnMain,styles.ItemTop]}>
                            <Image source={images.user} style={styles.msgProImg}></Image>
                            <View style={styles.recRowMsg}>
                                <View>
                                    <View style={styles.msgData}><Text style={styles.msgText}>Hi , How are you</Text></View>
                                    <View style={styles.selfStart}><Text style={[styles._DATE_,styles.justLeft]}>TUESDAY , 10:31 AM</Text></View>
                                </View>
                            </View>
                        </View>
                        <View style={[styles.chatSenderRow,styles.inlineDatarow,styles.ItemTop]}>
                            <View style={styles.senderRowMsg}>
                                <View>
                                    <View style={[styles.msgData,styles.orange]}><Text style={[styles.msgText,styles.white]}>Fine , and you</Text></View>
                                    <View style={styles.selfAlign}><Text style={[styles._DATE_,styles.justLeft]}>TUESDAY , 10:31 AM</Text></View>
                                </View>
                            </View>
                            <Image source={images.user} style={[styles.msgProImg,styles.senderImg]}></Image>
                        </View>
                        <View style={[styles.chatSenderRow,styles.inlineDatarow,styles.ItemTop]}>
                            <View style={styles.senderRowMsg}>
                                <View>
                                <View style={[styles.msgData,styles.orange]}><Text style={[styles.msgText,styles.white]}>{'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis ac neque. Duis vulputate commodo lectus, ac blandit elit tincidunt'}</Text></View>
                                <View style={styles.selfStart}><Text style={[styles._DATE_,styles.justLeft]}>TUESDAY , 10:31 AM</Text></View>
                                </View>
                            </View>
                            <Image source={images.user} style={[styles.msgProImg,styles.senderImg]}></Image>
                        </View>
                        <View style={[styles.chatRecRow,styles.rowBtnMain,styles.ItemTop]}>
                            <Image source={images.user} style={styles.msgProImg}></Image>
                            <View style={styles.recRowMsg}>
                                <View style={styles.msgData}><Image source={images.homestay} style={styles.msgImage}></Image></View>
                                <View style={styles.selfStart}><Text style={[styles._DATE_,styles.justLeft]}>TUESDAY , 10:31 AM</Text></View>
                            </View>
                        </View>
                        <View style={[styles.chatSenderRow,styles.rowBtnMain,styles.ItemTop]}>
                            <View style={styles.senderRowMsg}>
                                <View>
                                    <View style={[styles.msgData,styles.orange]}>
                                        <View style={styles.inlineDatarow}>
                                                <Text style={[styles.globalIcon,styles.docIco]}>=</Text>
                                                <View>
                                                    <Text style={[styles.big,styles.white]}>car_splash.pdf</Text>
                                                    <Text style={styles.sizeText}>24 MB</Text>
                                                </View>
                                        </View>
                                    </View>
                                    <View style={styles.selfStart}><Text style={[styles._DATE_,styles.justLeft]}>TUESDAY , 10:31 AM</Text></View>
                                </View>
                            </View>
                            <Image source={images.user} style={[styles.msgProImg,styles.senderImg]}></Image>
                        </View>

*/ 




