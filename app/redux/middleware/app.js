//import {Platform,Permission} from 'react-native';
export default function ({ dispatch, getState }) {
    return next => (action) => {
        switch (action.type) {
            case 'auth/TRY_GET_LOCATION': {
                
                return next(action);
            }

            case 'auth/CHECK_PERMISSION' : {
                return next(action);
            }

            case 'auth/REQUEST_LOCATION' : {
                return next(action);
            }

            case 'auth/GET_LOCATION' : {
                return next(action);
            }

            default:
                return next(action);
        }
    };
};