/* eslint-disable global-require */
/* eslint-disable no-undef */
import { createStore, applyMiddleware } from 'redux';
import rootReducer from '../reducers/rootReducer';
import thunk from 'redux-thunk';
import appMiidleWare from '../middleware/app';

let middleware = [thunk,appMiidleWare];

// if (__DEV__) {
// 	const reduxImmutableStateInvariant = require('redux-immutable-state-invariant')();
// 	const createLogger = require('redux-logger');

// 	const logger = createLogger({ collapsed: true });
// 	middleware = [...middleware, reduxImmutableStateInvariant, logger];
// } else {
	middleware = [...middleware];
//}

export default function configureStore(initialState) {
	return createStore(
		rootReducer,
		initialState,
		applyMiddleware(...middleware)
	);
}