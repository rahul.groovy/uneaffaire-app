//import * as types from '../../constants/actionTypes';
//import initialState from './initialState';
import ApiHelper from '../../lib/ApiHelper/ApiHelper';
const api= new ApiHelper;
const types ={
	LOAD:'auth/LOAD',
	SET_DATA:'auth/SET_DATA',
	SET_LOCATION:'auth/SET_LOCATION',
	SET_CATEGORY:'auth/SET_CATEGORY',
	SET_LOCATION_PERMISSION:'auth/SET_LOCATION_PERMISSION',
	SET_USER:'auth/SET_USER',
	SET_CART:'auth/SET_CART',
	SET_FCM_TOKEN:'auth/SET_FCM_TOKEN',
	SET_NET:'auth/SET_NET',
	SET_NET_REQ_RESPONSE:'auth/SET_NET_REQ_RESPONSE',
	SET_NET_DIAL:'auth/SET_NET_DIAL'
}

const initialState={
	token:'',
	userdata:{},
	cLocation:null,
	load:false,
	categoryList:[],
	accessLocation:true,
	cart:0,
	fcmToken:null,
	isNetOnOff:false,
	isNetOnOffDia:false,
	lastRequest: null,
	lastResolve: null
}

export default function reducer(state = initialState, action) {
	switch (action.type) {

		case types.LOAD:
			return {
				...state,
				load: true
			};

			case types.SET_DATA:
			return {
				...state,
				token:action.token,
				userdata:action.userdata,
			};
			
			case types.SET_LOCATION:
			return {
				...state,
				cLocation:action.location
			};

			case types.SET_CATEGORY:
			return {
				...state,
				categoryList:action.categoryList
			};

			case types.SET_LOCATION_PERMISSION:
			return {
				...state,
				accessLocation:action.accessLocation
			};

			case types.SET_USER:
			return {
				...state,
				userdata:action.userdata,
			};

			case types.SET_CART:
			return {
				...state,
				cart:action.cart,
			};

			case types.SET_FCM_TOKEN:
			return {
				...state,
				fcmToken:action.fcmToken,
			};

			case types.SET_NET:
			//console.log('Type set net called');
			//console.log(action.isNetOnOff);
			return {
				...state,
				isNetOnOff:action.isNetOnOff
			};
			
			case types.SET_NET_REQ_RESPONSE:
			//console.log('seting last req and resp');
			//console.log(action.lastRequest);
			//console.log(action.lastResolve);

			return {
				...state,
				lastRequest:action.lastRequest,
				lastResolve:action.lastResolve,
			};

			case types.SET_NET_DIAL:
			return {
				...state,
				isNetOnOffDia:action.isNetOnOffDia,
			};

		default:
			return state;
	}
}

export function load(){
    return (dispatch) =>
    dispatch({
        type:types.LOAD
    })
}

export function setUserData(token,data){
	let tok = '';
	let udata = {};
	if(token !== undefined && token !== null && token !== '' ){
		tok=token;
	}
	if(data !== undefined && data !== null && Object.keys(data).length > 0 ){
		udata=data;
	}
    return (dispatch) =>
    dispatch({
		type:types.SET_DATA,
		token:tok,
		userdata:udata
    })
}

export function setUser(data){
	return (dispatch) =>
    dispatch({
		type:types.SET_USER,
		userdata:data
    })
}

export function setLocation(location){
    return (dispatch) =>
    dispatch({
		type:types.SET_LOCATION,
		location
    })
}

export function setCategory(categoryList){
    return (dispatch) =>
    dispatch({
		type:types.SET_CATEGORY,
		categoryList
    })
}

export function setPermission(bool){
	return (dispatch) =>
    dispatch({
		type:types.SET_LOCATION_PERMISSION,
		accessLocation:bool
    })
}

export function setCart(num){
	return (dispatch) =>
    dispatch({
		type:types.SET_CART,
		cart:num
    })
}

export function setFcmToken(t){
	return (dispatch) =>
    dispatch({
		type:types.SET_FCM_TOKEN,
		fcmToken:t
    })
}

export function setNet(t, lastRequest = null, lastResolve = null){
	return (dispatch, getState) => {
		const _state = getState();
		//console.log('Net Set Net ' + t);
		//console.log(_state);
		if (t === false){
			//console.log('Got net enabled');
			if(_state.auth.lastRequest && _state.auth.lastResolve){
				//console.log('Make call again');
				//console.log(_state.auth.lastRequest);
				//console.log(_state.auth.lastResolve);
				//console.log(api);
				api.FetchData(_state.auth.lastRequest)
				.then((resp) => {
					_state.auth.lastResolve(resp);
					dispatch({
						type:types.SET_NET,
						isNetOnOff:t,
					});
				})
				.catch((err) => {
					//console.log('setnet Fetch error');
					console.log(err);
				});
			} else {
				//console.log('No req resp');
				dispatch({
					type:types.SET_NET,
					isNetOnOff:t,
				});	
			}
			
		} else {
			//console.log('Got net disabled');
			dispatch({
				type:types.SET_NET,
				isNetOnOff:t,
			});
			//console.log(lastRequest);
			//console.log(lastResolve);
			if(lastRequest !== null && lastResolve !== null){
				//console.log('Savign last request and last resolve to store');
				if(lastRequest === false) lastRequest = null;
				if(lastResolve === false) lastResolve = null;
				dispatch({
					type:types.SET_NET_REQ_RESPONSE,
					lastRequest,
					lastResolve
				})
			}
		}
	}
}

export function setNetDial(t){
	return (dispatch) =>
    dispatch({
		type:types.SET_NET_DIAL,
		isNetOnOffDia:t
    })
}