import { combineReducers } from 'redux';
import chat from './chat';
import auth from './auth';

const rootReducer = combineReducers({
    chat,
    auth
});

export default rootReducer;