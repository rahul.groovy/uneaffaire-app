//import {FontAwesome,Ionicons,MaterialIcons} from 'react-native-vector-icons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { colors } from './styles';

import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import icoMoonConfig from './selection.json';
const Icon = createIconSetFromIcoMoon(icoMoonConfig);

// const icons = {
// 	android: [
// 		Ionicons.getImageSource('navicon', 20, 'red'),
// 		Ionicons.getImageSource('cog', 20, 'red'),
// 		Ionicons.getImageSource('list-ul', 20, 'red'),
//     Ionicons.getImageSource('envelope', 20, 'red'),
//     Ionicons.getImageSource('ios-arrow-back',20,colors.navBarButtonColor),
// 	],
// 	ios: [
// 		Ionicons.getImageSource('ios-home', 20, 'red'),
// 		Ionicons.getImageSource('ios-settings', 20, 'red'),
// 		Ionicons.getImageSource('ios-apps', 20, 'red'),
//     Ionicons.getImageSource('ios-mail', 20, 'red'),
//     Ionicons.getImageSource('ios-arrow-back',20,colors.navBarButtonColor),
// 	]
// }
// let newIcons=[];
// Promise.all(icons[Platform.OS])
// 			.then(vals => {
//         console.log(vals);
//         newIcons=vals;
// 				//naviconIcon = vals[0]
// 				//cogIcon = vals[1]
// 				//itemsIcon = vals[2]
// 				//envelopeIcon = vals[3]
// 				//loaded = true
//         //resolve(true)
//         startApp()
// 			})
// 			.catch(err => reject(err))
// 			.done()

// define your suffixes by yourself..
// here we use active, big, small, very-big..


/* 
other icons
  "ios-person": [30, "#bbb"],
  "ios-person--big": [50, "#bbb"],

  "ios-person--active": [30, "#fff"],
  "ios-person--active--big": [50, "#fff"],
  "ios-person--active--very-big": [100, "#fff"],

  "ios-people": [30, "#bbb"],
  "ios-people--active": [30, "#fff"],

  "ios-keypad": [30, "#bbb"],
  "ios-keypad--active": [30, "#fff"],

  "ios-chatbubbles": [30, "#bbb"],
  "ios-chatbubbles--active": [30, "#fff"],

  // Use other Icon provider, see the logic at L39
  "facebook": [30, "#bbb", FontAwesome],
  "facebook--active": [30, "#fff", FontAwesome],
    "ios-arrow-back":[30,colors.navBarButtonColor],
  "ios-arrow-back--big":[50,colors.navBarButtonColor],

*/

const smallIco=15;
const normalIco=21;
const bigIco=30;


const replaceSuffixPattern = /--(active|big|small|very-big)/g;
const icons = {
  "left-arrow":[normalIco,colors.navBarButtonColor],
  "left-arrow--small":[smallIco,colors.navBarButtonColor],
  "left-arrow--big":[bigIco,colors.navBarButtonColor],

  "left-angle-bracket":[normalIco,colors.navBarButtonColor],
  "left-angle-bracket--small":[smallIco,colors.navBarButtonColor],
  "left-angle-bracket--big":[bigIco,colors.navBarButtonColor],
  
  "back":[normalIco,colors.navBarButtonColor],
  "back--small":[smallIco,colors.navBarButtonColor],
  "back--big":[bigIco,colors.navBarButtonColor],

  "deposer-annonce":[normalIco,colors.navBarButtonColor],
  "deposer-annonce--small":[smallIco,colors.navBarButtonColor],
  "deposer-annonce--big":[bigIco,colors.navBarButtonColor],

  "shopping-cart":[normalIco,colors.navBarButtonColor],
  "shopping-cart--small":[smallIco,colors.navBarButtonColor],
  "shopping-cart--big":[bigIco,colors.navBarButtonColor],

  "user":[normalIco,colors.navBarButtonColor],
  "user--small":[smallIco,colors.navBarButtonColor],
  "user--big":[bigIco,colors.navBarButtonColor],

  "listing":[normalIco,colors.navBarButtonColor],
  "listing--small":[smallIco,colors.navBarButtonColor],
  "listing--big":[bigIco,colors.navBarButtonColor],

  "edit":[normalIco,colors.navBarButtonColor],
  "edit--small":[smallIco,colors.navBarButtonColor],
  "edit--big":[bigIco,colors.navBarButtonColor],

  "facebook-logo":[normalIco,colors.navBarButtonColor],
  "facebook-logo--small":[smallIco,colors.navBarButtonColor],
  "facebook-logo--big":[bigIco,colors.navBarButtonColor],
  
  "google":[normalIco,colors.navBarButtonColor],
  "google--small":[smallIco,colors.navBarButtonColor],
  "google--big":[bigIco,colors.navBarButtonColor],

  "twitter":[normalIco,colors.navBarButtonColor],
  "twitter--small":[smallIco,colors.navBarButtonColor],
  "twitter--big":[bigIco,colors.navBarButtonColor],

  "google-glass-logo":[normalIco,colors.navBarButtonColor],
  "google-glass-logo--small":[smallIco,colors.navBarButtonColor],
  "google-glass-logo--big":[bigIco,colors.navBarButtonColor],
  

  "grid":[normalIco,colors.navBarButtonColor],
  "grid--small":[smallIco,colors.navBarButtonColor],
  "grid--big":[bigIco,colors.navBarButtonColor],

  "list":[normalIco,colors.navBarButtonColor],
  "list--small":[smallIco,colors.navBarButtonColor],
  "list--big":[bigIco,colors.navBarButtonColor],
  
  "conversation":[normalIco,colors.navBarButtonColor],
  "conversation--small":[smallIco,colors.navBarButtonColor],
  "conversation--big":[bigIco,colors.navBarButtonColor],

  "location":[normalIco,colors.navBarButtonColor],
  "location--small":[smallIco,colors.navBarButtonColor],
  "location--big":[bigIco,colors.navBarButtonColor],
  
  "telephone":[normalIco,colors.navBarButtonColor],
  "telephone--small":[smallIco,colors.navBarButtonColor],
  "telephone--big":[bigIco,colors.navBarButtonColor],

  "chat":[normalIco,colors.navBarButtonColor],
  "chat--small":[smallIco,colors.navBarButtonColor],
  "chat--big":[bigIco,colors.navBarButtonColor],

  "menu":[normalIco,colors.navBarButtonColor],
  "menu--small":[smallIco,colors.navBarButtonColor],
  "menu--big":[bigIco,colors.navBarButtonColor],

  "like":[normalIco,colors.navBarButtonColor],
  "like--small":[smallIco,colors.navBarButtonColor],
  "like--big":[bigIco,colors.navBarButtonColor],

  "view-list":[normalIco,colors.navBarButtonColor],
  "view-list--small":[smallIco,colors.navBarButtonColor],
  "view-list--big":[bigIco,colors.navBarButtonColor],

  "power-button":[normalIco,colors.navBarButtonColor],
  "power-button--small":[smallIco,colors.navBarButtonColor],
  "power-button--big":[bigIco,colors.navBarButtonColor],

  "franceMap":[normalIco,colors.navBarButtonColor],
  "franceMap--small":[smallIco,colors.navBarButtonColor],
  "franceMap--big":[bigIco,colors.navBarButtonColor],
  
  "bookmark":[normalIco,colors.navBarButtonColor],
  "bookmark--small":[smallIco,colors.navBarButtonColor],
  "bookmark--big":[bigIco,colors.navBarButtonColor],

  "bookmark-tag":[normalIco,colors.navBarButtonColor],
  "bookmark-tag--small":[smallIco,colors.navBarButtonColor],
  "bookmark-tag--big":[bigIco,colors.navBarButtonColor],

  "camera":[normalIco,colors.navBarButtonColor],
  "camera--small":[smallIco,colors.navBarButtonColor],
  "camera--big":[bigIco,colors.navBarButtonColor],

  "information":[normalIco,colors.navBarButtonColor],
  "information--small":[smallIco,colors.navBarButtonColor],
  "information--big":[bigIco,colors.navBarButtonColor],

  "gear":[normalIco,colors.navBarButtonColor],
  "gear--small":[smallIco,colors.navBarButtonColor],
  "gear--big":[bigIco,colors.navBarButtonColor],

  "calendar":[normalIco,colors.navBarButtonColor],
  "calendar--small":[smallIco,colors.navBarButtonColor],
  "calendar--big":[bigIco,colors.navBarButtonColor],

  "euro-outer":[normalIco,colors.navBarButtonColor],
  "euro-outer--small":[smallIco,colors.navBarButtonColor],
  "euro-outer--big":[bigIco,colors.navBarButtonColor],

  "euro-fill":[21,colors.navBarButtonColor],
  "euro-fill--small":[smallIco,colors.navBarButtonColor],
  "euro-fill--big":[bigIco,colors.navBarButtonColor],

  "calendar-fill":[21,colors.navBarButtonColor],
  "calendar-fill--small":[smallIco,colors.navBarButtonColor],
  "calendar-fill--big":[bigIco,colors.navBarButtonColor],

  "sync":[21,colors.navBarButtonColor,FontAwesome5],
  "sync--small":[smallIco,colors.navBarButtonColor,FontAwesome5],
  "sync--big":[bigIco,colors.navBarButtonColor,FontAwesome5],
}

const defaultIconProvider = Icon;

let iconsMap = {};
let iconsLoaded = new Promise((resolve, reject) => {
  new Promise.all(
    Object.keys(icons).map(iconName => {
      const Provider = icons[iconName][2] || defaultIconProvider; // Ionicons
      return Provider.getImageSource(
        iconName.replace(replaceSuffixPattern, ''),
        icons[iconName][0],
        icons[iconName][1]
      )
    })
  ).then(sources => {
    Object.keys(icons).forEach((iconName, idx) => iconsMap[iconName] = sources[idx])

    // Call resolve (and we are done)
    resolve(true);
  })
});

export default function getIcon(iconName,size,color,p){
	return new Promise((resolve,reject)=>{
		let Provider='';
		if(p=='fa'){
			Provider=FontAwesome;
		}else if(p=='ion'){
			Provider=Ionicons;
		}else if(p=='ico'){
			Provider=Icon;
		}
		return Provider.getImageSource(
        iconName.replace(replaceSuffixPattern, ''),
        icons[iconName][0],
        icons[iconName][1]
      )
	});
}

export {
  iconsMap,
	iconsLoaded,
  getIcon,
  Icon
};

// const icons = {

// };

// export default icons;