import React,{Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableWithoutFeedback
} from 'react-native';

export default class RadioButton extends Component{
    
    render(){
    return(
       <TouchableWithoutFeedback onPress={() => this.props.onPress(this.props.value)}>
        <View style={{ flexDirection: 'row',paddingLeft:15,paddingRight:15,paddingTop:5,paddingBottom:5, }}>
            <View style={[{
                height: null,
                width: null,
                position:'absolute',
                left:0,
                right:0,
                top:0,
                bottom:0,
                
                borderRadius: 0,
                borderWidth: 1,
                borderColor:this.props.value===this.props.currentValue ? '#ff6624':'#aaaaaa',
                alignItems: 'center',
                justifyContent: 'center',
                flex:1,
            }]}>
            {
            this.props.value===this.props.currentValue ?
            <View style={{
              height: null,
              width: null,
              position:'absolute',
              right:0,
              bottom:0,
            }}>
                <Image style={{width:18,height:18,}} source={require('./radioChk.png')}/>
            </View>
            : null
            }
            </View>
              {this.props.children}
            </View>
            </TouchableWithoutFeedback>
    );
}

}