import React,{Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableWithoutFeedback
} from 'react-native';
import styles from '../../config/genStyle';

export default class RadioButton extends Component{
    
    render(){
    return(
            <TouchableWithoutFeedback onPress={() => this.props.onPress(this.props.value)}>
        <View style={{ flexDirection: 'row',alignContent:'center' }}>
            <View style={[{
            height: this.props.outerCircleSize || 16,
            width: this.props.outerCircleSize || 16,
            borderRadius: this.props.outerCircleSize/2 || 8,
            borderWidth: this.props.outerCircleWidth || 2,
            borderColor: this.props.outerCircleColor || '#ff6624',
            alignItems: 'center',
            justifyContent: 'center',
            marginBottom:5
            }]}>
            {
            this.props.value===this.props.currentValue ?
            <View style={{
              height: this.props.innerCircleSize || 8,
              width: this.props.innerCircleSize || 8,
              borderRadius: this.props.innerCircleSize/1 || 4,
              backgroundColor: this.props.innerCircleColor || '#ff6624',
            }}/>
            : null
            }
            </View>
            {this.props.Text ? <Text style={{fontSize:14,
        fontFamily:'Montserrat',marginHorizontal:5,marginTop:-1}}>{this.props.Text ? this.props.Text : null}</Text>:this.props.children}
              
            </View>
            </TouchableWithoutFeedback>
    );
}

}